function getXmlHttpRequestObject() {
    if (window.XMLHttpRequest) {
        return new XMLHttpRequest();
    }
    else if (window.ActiveXObject) {
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
    alert('can\'t load data');
    return null;
}

function AjaxParam(name, value) {
    this.name = name;
    this.value = value;
}

var sendReq;

function retrieveData(url, handler) {
    sendReq = getXmlHttpRequestObject();
    if (sendReq.readyState == 4 || sendReq.readyState == 0) {
        sendReq.open("POST", url, true);
        sendReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
        sendReq.onreadystatechange = eval(handler);
        var params = '';
        for (var i = 2; i < arguments.length; i++) {
            var param = arguments[i];
            params += param.name + '=' + param.value;
            if (i < arguments.length - 1) {
                params += '&';
            }
        }
        params = encodeURI(params);
        sendReq.send(params);
    }
}

function complete() {
    return (sendReq.readyState == 4 && sendReq.status == 200);
}

function getResponseData() {
    return sendReq.responseText;
}