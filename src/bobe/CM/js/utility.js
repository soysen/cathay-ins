/*
程式：utility.js雜項JS utility
作者：新契約小組
功能：提供雜項的JavaScript控制呈現
完成：2004/02/05
更新：2004/03/30林秉毅－增加submitToUpper
更新：940519 增加IBM submitOnce,enableElements
更新：940610 IBM 修改submitOnce
更新：940613 laycf 修改disableButton
*/
//Content List
//stat():固定畫面MainTitle
//fix():固定畫面MainTitle
//submitOnce(button, openWindow)
/**
 * 固定畫面MainTitle。 <br>
 */

function stat() {
	var bar1 = document.getElementById('bar1');
	if (bar1) {
		var a = pageYOffset + window.innerHeight - bar1.height - 15;
		bar1.top = a;
		setTimeout('stat()', 2);
	}
}
/**
 * 固定畫面。 <br>
 */

function fix() {
	var nome = navigator.appName;
	if (nome == 'Netscape') {
		stat();
	} else {
		var bar1 = document.getElementById('bar1');
		if (bar1) {
			var a = document.body.scrollTop - bar1.offsetHeight + 30;
			bar1.style.top = a;
		}
	}
}
// submit form


function submitOnce(button, openWindow) {
	var form = button.form;
	var field = document.createElement("input");
	var doc = button.ownerDocument;
	field.type = "hidden";
	field.name = button.name;
	field.value = button.value;
	form.appendChild(field);
	// if the button owner document have iframe inside,
	// If the target aims at some iframe, add the event control to remove cover.
if(doc.frames){	
	if (doc.frames.length && form.target != '_self' && form.target != '') {
		for (var i = 0; i < doc.frames.length; i++) {
			if (form.target == doc.frames[i].name) {
				doc.frames[i].frameElement.onreadystatechange = function () {
					if (this.readyState == 'complete') {
						CSRUtil.hidePageCover();
					}
				}
				break;
			}
		}
	}
}
	if (!openWindow) {
		CSRUtil.ProcessPageCover();
	} else {
		form.target = "_blank"
		button.disabled = true;
	}
	form.submit();
}
//===========================================================================
//功能說明:轉全形字
//地址功能版
//(1)將~轉成-
//(2)將大寫英文轉大寫全形英文
//===========================================================================


function chgFullAddr(half) {
	whole = ''; //全形字
	chghalf = ''; //轉換之半形字
	str_length = half.length;
	for (i = 0; i < str_length; i++) {
		if (half.charCodeAt(i) >= 0 && half.charCodeAt(i) <= 255) {
			chghalf = chghalf + half.charAt(i);
		}
	}
	str_length = chghalf.length;
	whole = half;
	for (i = 0; i < str_length; i++) {
		xx = chghalf.charAt(i);
		if (xx == "~") {
			whole = whole.replace("~", String.fromCharCode("-".charCodeAt() + 65248)); //將~轉成-
		} else if (xx == " ") {
			whole = whole.replace(" ", "　"); //半形空白轉成全形空白
		} else {
			whole = whole.replace(xx, String.fromCharCode(xx.toUpperCase().charCodeAt() + 65248));
		}
	}
	return whole;
}