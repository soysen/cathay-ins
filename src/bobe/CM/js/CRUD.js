function doAJAXInsert(url, insertKeyArr, tableDef, isAsync) {
    if (!Object.isArray(insertKeyArr)) {
        return;
    }

    if (tableDef.length < 2) {
        return alert('Incorrect table definication!');
    }

    var tableId = tableDef[0];
    try {
        var t = $$('table[id="' + tableId + '"]');
        if (t.length == 0) {
            throw "Cannot find table";
        }
    } catch(e) {
        return alert('Cannot find table [' + tableId + ']');
    }

    var columnDef = tableDef[1];
    if (!Object.isArray(columnDef)) {
        return  alert('Incorrect column definication!');
    }

    var isAsynchronized = true;
    if((isAsync != undefined) && (!isAsync)) {
        isAsynchronized = false;
    }

    var param = getQueryStrFromInput(insertKeyArr);
    new Ajax.Request(url, {
        parameters:param,
        asynchronous:isAsynchronized,
        onSuccess: function(XHT, resp) {
            if (resp.ErrMsg.returnCode == 0) {
                if (Object.isArray(resp.newVo)) {
                    removeRows(dataTableId);
                    bindTableData(tableId, columnDef, resp.newVo);
                }else{
                    bindTableData(tableId, columnDef, new Array(resp.newVo));
                }
            }
        }
    });
}


function doAJAXQuery(url, queryKeyArr, tableDef, isAsync) {
    if (!Object.isArray(queryKeyArr)) {
        return;
    }

    if (tableDef.length < 2) {
        return alert('Incorrect table definication!');
    }

    var tableId = tableDef[0];
    try {
        var t = $$('table[id="' + tableId + '"]');
        if (t.length == 0) {
            throw "Cannot find table";
        }
    } catch(e) {
        return alert('Cannot find table [' + tableId + ']');
    }

    var columnDef = tableDef[1];
    if (!Object.isArray(columnDef)) {
        return  alert('Incorrect column definication!');
    }

    var isAsynchronized = true;
    if((isAsync != undefined) && (!isAsync)) {
        isAsynchronized = false;
    }

    removeRows(tableId);

    var param = getQueryStrFromInput(queryKeyArr);
    new Ajax.Request(url, {
        parameters:param,
        asynchronous:isAsynchronized,
        onSuccess: function (XHT, resp) {
            if (resp.ErrMsg.returnCode == 0) {
                if (resp.resultDataList && Object.isArray(resp.resultDataList)) {
                    bindTableData(tableId, columnDef, resp.resultDataList);
                }
            }
        }
    });
}


function doAJAXUpdate(url, keyArr, oldDataObj, updateKeyArr, queryKeyArr, tableDef, isClearData, isAsync) {
    removeRows(dataTableId);

    if (!Object.isArray(updateKeyArr)) {
        return;
    }

    if (tableDef.length < 2) {
        return alert('Incorrect table definication!');
    }

    var tableId = tableDef[0];
    try {
        var t = $$('table[id="' + tableId + '"]');
        if (t.length == 0) {
            throw "Cannot find table";
        }
    } catch(e) {
        return alert('Cannot find table [' + tableId + ']');
    }

    var columnDef = tableDef[1];
    if (!Object.isArray(columnDef)) {
        return  alert('Incorrect column definication!');
    }

    var isAsynchronized = true;
    if ((isAsync != undefined) && (!isAsync)) {
        isAsynchronized = false;
    }


    new Ajax.Request(url, {
        parameters:{"updateKey":getQueryStrFromObject(keyArr, oldDataObj, true), "updateData":getQueryStrFromInput(updateKeyArr, true),
        "queryKey":getQueryStrFromInput(queryKeyArr, true)},
        asynchronous:isAsynchronized,
        onSuccess: function(XHT, resp) {
            if (resp.ErrMsg.returnCode == 0) {
                if (resp.resultDataList) {
                    bindTableData(tableId, columnDef, resp.resultDataList);
                    if (isClearData != undefined && isClearData) {
                        doClearData();
                    }
                }
            }
        }
    });
}


function doAJAXDelete(url, deleteKeyArr, deleteDataObj, queryKeyArr, tableDef, isClearData, isAsync) {
    removeRows(dataTableId);

    if (!Object.isArray(deleteKeyArr)) {
        return;
    }

    if (tableDef.length < 2) {
        return alert('Incorrect table definication!');
    }

    var tableId = tableDef[0];
    try {
        var t = $$('table[id="' + tableId + '"]');
        if (t.length == 0) {
            throw "Cannot find table";
        }
    } catch(e) {
        return alert('Cannot find table [' + tableId + ']');
    }

    var columnDef = tableDef[1];
    if (!Object.isArray(columnDef)) {
        return  alert('Incorrect column definication!');
    }

    var isAsynchronized = true;
    if ((isAsync != undefined) && (!isAsync)) {
        isAsynchronized = false;
    }

    new Ajax.Request(url, {
        parameters:{"deleteKey":getQueryStrFromObject(deleteKeyArr, deleteDataObj, true),"queryKey":getQueryStrFromInput(queryKeyArr, true)},
        asynchronous:isAsynchronized,
        onSuccess: function(XHT, resp) {
            if (resp.ErrMsg.returnCode == '0') {
                if (resp.resultDataList) {
                    bindTableData(tableId, columnDef, resp.resultDataList);
                     if (isClearData != undefined && isClearData) {
                        doClearData();
                    }
                }
            }
        }
    });
}


function removeRows(tableId) {
    var nRow = $(tableId).tBodies[0].rows.length;
    for (var i = nRow; i >= 1; i--) {
        $(tableId).tBodies[0].deleteRow(i - 1);
    }
}

// $H({type:"innerText",align:"right", innerText:'DIV_NO'});
// $H({type:"innerText",align:"left", innerText:function(data){return data.abc + ':' + data.xyz}});
// {type:"innerText",align:"right", innerText:'DIV_NO'};
// {type:"innerText",align:"left", innerText:function(data){return data.abc + ':' + data.xyz}};
// {type:"text",align:"right", value:'DIV_NO'};
// {type:"text",align:"left", value:function(data){return data.abc + ':' + data.xyz}};
function bindTableData(tableId, columnDefArr, dataArr, isShowData) {
    if(!Object.isArray(dataArr)){
        return;
    }
    dataArr.each(function(data) {
        // create row
        var TR = createNewRow(tableId, -1);
        var rowIndex = document.getElementById(tableId).tBodies[0].rows.length - 1;

        // create td
        columnDefArr.each(function(colDef, cellIndex) {
            var TD;
            if (Object.isHash(colDef) || typeof colDef == 'object') {
                // convert object to hash
                if(typeof colDef == 'object'){
                   colDef = $H(colDef);
                }

                if (colDef.get('type') == 'innerText') {
                    // customize td attribute
                    if (colDef.get('innerText') != undefined && Object.isFunction(colDef.get('innerText'))) {
                        // if innerText is a function
                        var newColDef = colDef.clone();
                        newColDef.set('innerText', colDef.get('innerText')(data));
                    } else {
                        // if innerText is a db column name
                        var newColDef = colDef.clone();
                        newColDef.set('innerText', data[colDef.get('innerText')]);
                    }
                    TD = createNewTD(TR, newColDef);

                } else if (colDef.get('type') == 'text') {
                    // customize td attribute
                    if (colDef.get('value') != undefined && Object.isFunction(colDef.get('value'))) {
                        // if innerText is a function
                        var newColDef = colDef.clone();
                        newColDef.set('value', colDef.get('value')(data));
                    } else {
                        // if innerText is a db column name
                        var newColDef = colDef.clone();
                        newColDef.set('value', data[colDef.get('value')]);
                    }
                    TD = createNewTD(TR, $H({}));
                    var input = createInput(TD, newColDef, rowIndex, data, isShowData);
                    TD.appendChild(input);

                } else {
                    // default
                    TD = createNewTD(TR, $H({align:'center'}));
                    var input = createInput(TD, colDef, rowIndex, data, isShowData);
                    TD.appendChild(input);
                }
            } else if (Object.isFunction(colDef)) {
                TD = createNewTD(TR, $H({align:'center', innerText: colDef(data)}));
            } else {
                TD = createNewTD(TR, $H({align:'center',innerText:data[colDef]== undefined?'':data[colDef]}));
            }
        });
    });
}


function createNewRow(tableId, rowIndex) {
    var tbody = document.getElementById(tableId).getElementsByTagName("tbody")[0];

    // row bg css class
    var newTR = tbody.insertRow(rowIndex);
    newTR.align = 'center';
    newTR.className = (tbody.rows.length) % 2 == 1 ? "tbBlue3" : "tbYellow2";
    return newTR;
}


function createNewTD(TR, attributeHash) {
    var newTD;
    newTD = TR.insertCell(-1);
    attributeHash.each(function(pair){
        try {
            newTD[pair.key] = pair.value;
        } catch(e) {
        }
    });
//    newTD.align = "center";
    return newTD;
}



function createInput(TDRef, inputDefHash, index, data, isShowData) {
    var input;
    if (inputDefHash.get("type").toLowerCase() == "radio") {
       // input = document.createElement('<input type="radio" name="' + inputDefHash.get(name) + '" />');
       input = document.createElement("input");
       input.type = "radio";
       input.name = inputDefHash.get(name);
    } else {
        input = document.createElement("input");
    }

    var callback = "";
    var eventName = "";
    inputDefHash.each(function(pair) {
        if (pair.key.startsWith("on")) {
            // binding event
            eventName = pair.key.substr(2, pair.key.length - 1);
            callback = pair.value;
        } else {
            input[pair.key] = pair.value;
        }
    });

    if (callback != "") {
        // if onclick callback specified, pass to default handler as argument to call chain
        if(isShowData == false){
            Event.observe(input, eventName, extablishCallback.bindAsEventListener(this, index, data, input, callback));
        }   else{
            Event.observe(input, eventName, doShowData.bindAsEventListener(this, index, data, input, callback));
        }
    } else {
        // default onclick callback
        if(isShowData == false){
            Event.observe(input, eventName, extablishCallback.bindAsEventListener(this, index, data, input));
        }   else {
            Event.observe(input, eventName, doShowData.bindAsEventListener(this, index, data, input));
        }
    }

    return input;
}


function extablishCallback(event){
    var args = $A(arguments);
    var index = args[1];
    var data = args[2];
    var element = args[3];

    // invoke callback  pass row index and data object as arguemnts
    if (args.length > 4) {
        args[4](index, data, element);
    }
}



function doShowData(event) {
    var args = $A(arguments);
    var index = args[1];
    var data = args[2];
    var element = args[3];

    // bind data to input element , ignore if not exist
    $H(data).each(function(pair) {
        try {
            $(pair.key).value = pair.value;
        } catch(e) {
        }
    });

    // invoke callback  pass row index and data object as arguemnts
    if (args.length > 4) {
        args[4](index, data, element);
    }
}



function doClearData() {
    $$('input[type="text"]').each(function(element) {
        element.value = "";
    });
}


function getQueryStrFromInput(keyArr, isToJSON) {
    var param = new Hash();
    keyArr.each(function(element) {
        if (Object.isFunction(element)) {
            // if is function , then invoke and set return value as query string
            var paramPair = element();
            try {
                var paramPairHash = $A($H(paramPair));
                if (paramPairHash.length == 1) {
                    param.set(paramPairHash[0].key, paramPairHash[0].value);
                } else {
                    return alert('Return value of function [' + element + '] is error\n' + $H(paramPair).toJSON());
                }
            } catch(e) {
                return alert('Return type of function [' + element + '] is error');
            }
        } else {
            try {
                param.set(element, $(element).value);
            } catch(e) {
            }
        }
    });

    if (!isToJSON) {
        return param.toQueryString();
    } else {
        return param.toJSON();
    }
}

function getQueryStrFromObject(keyArr, dataObj, isToJSON) {
    var param = new Hash();
    keyArr.each(function(element) {
        if (Object.isFunction(element)) {
            // if is function , then invoke and set return value as query string
            var paramPair = element();
            try {
                var paramPairHash = $A($H(paramPair));
                if (paramPairHash.length == 1) {
                    param.set(paramPairHash[0].key, paramPairHash[0].value);
                } else {
                    return alert('Return value of function [' + element + '] is error\n' + $H(paramPair).toJSON());
                }
            } catch(e) {
                return alert('Return type of function [' + element + '] is error');
            }
        } else {
            try {
                param.set(element, dataObj[element]);
            } catch(e) {
            }
        }
    });

    if (!isToJSON) {
        return param.toQueryString();
    } else {
        return param.toJSON();
    }
}
