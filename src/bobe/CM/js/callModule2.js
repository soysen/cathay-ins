/*
	This is a Beta version for callModule.js
	Fixing the following bugs..
	1. Lost focus when drag_drop window.
	2. Fixed size window

	Jauwei, 2013-11-08
*/
var CallModule2={
	dragapproved : false,
	initialwidth : 0,
	initialheight : 0,
	
	drag_drop : function (e, nameSpace) {
		var ns = nameSpace ? nameSpace : '';
	    if (CallModule2.dragapproved && (event.button == 1 || event.button == 0)) {
	        $('dwindow' + ns).style.left = tempx + event.clientX - offsetx + 'px';
	        $('dwindow' + ns).style.top = tempy + event.clientY - offsety + 'px';
	    }
	},
	
	initializedrag : function (e, nameSpace) {
		var ns = nameSpace ? nameSpace : '';
	    offsetx = event.clientX;
	    offsety = event.clientY;
	    $('dwindowcontent' + ns).style.display = 'none'; //extra
	    tempx = parseInt($('dwindow' + ns).style.left);
	    tempy = parseInt($('dwindow' + ns).style.top);
	    CallModule2.dragapproved = true;
	    var win = $('dwindow' + ns);
	    win.style.filter = 'alpha(opacity=50)';
	    win.style.opacity = '0.25';
	    Event.observe(document, 'mousemove', CallModule2.drag_drop.bindAsEventListener(win, ns));
	},
	
	loadWindow : function (url, width, height, params, nameSpace) {
		var ns = nameSpace ? nameSpace : '';
		CallModule2.createDiv(nameSpace);
		/*
		try {
			var parentNS = parent[0].name.substring(6);
			if ($(parent['dwindow' + parentNS])) {
				var el = $(parent['dwindow' + parentNS]);
				var maxWidth = Math.floor(el.style.width.gsub('px', '')) - 100;
				var maxHeight = Math.floor(el.style.height.gsub('px', '')) - 50;
				if (maxWidth > 0 && width > maxWidth) {
					width = maxWidth;
				}
				if (maxHeight > 0 && height > maxHeight) {
					height = maxHeight;
				}
			} else if($('bar1')) {
				var el = $('bar1');
				var maxWidth = Math.floor(el.offsetWidth) - 100;
				if (maxWidth > 0 && width > maxWidth) {
					width = maxWidth;
				}
			}
		} catch (e) {
		}
		*/
	    $('dwindow' + ns).style.display = '';
	    
	    //if (CallModule2.initialwidth==0){
	    	CallModule2.initialwidth = width + 'px';
	    //}
	    
	    //if (CallModule2.initialheight==0){
	    	CallModule2.initialheight = height + 'px';
	    //}
	    
	    $('dwindow' + ns).style.width = CallModule2.initialwidth;
	    $('dwindow' + ns).style.height = CallModule2.initialheight;
	    $('dwindow' + ns).style.left = '30px';
	    $('dwindow' + ns).style.top = document.body.scrollTop * 1 + 40 + 'px';
	    var form = $('eform' + ns);
	    form.action = url;
	    if (params) {
	        for (var name in params) {
	            var element = new Element('input', {type : 'hidden', name : name, value : params[name]});
	            form.insert(element);
	        }
	    }
	    if(nameSpace){
	    	var element = new Element('input', {type :'hidden', name : 'NAME_SPACE', value : nameSpace});
	    	form.insert(element);
	    }
	    form.submit();
	    form.childElements().each(function(el) {el.remove()});
	},
	
	closeWin: function (nameSpace) {
		var ns = nameSpace ? nameSpace : '';
	    $('cframe' + ns).src = '';
	    $('dwindow' + ns).style.display = 'none';
	},
	
	stopdrag: function (nameSpace) {
		var ns = nameSpace ? nameSpace : '';
	    CallModule2.dragapproved = false;
	    $('dwindow' + ns).onmousemove = null;
	    $('dwindowcontent' + ns).style.display = ''; //extra
	    $('dwindow' + ns).style.filter = 'alpha(opacity=100)';
	    $('dwindow' + ns).style.opacity = '1';
	},
	
	createDiv: function (nameSpace) {
		var ns = nameSpace ? nameSpace : '';
		if (!$('dwindow' + ns)) {
			var template = new Template(
		    '<div id="dwindow#{NS}" style="position:absolute;background-color:#EBEBEB;cursor:hand;left:0px;top:0px;display:none;z-index:100;" onMousedown="CallModule2.initializedrag(event, \'#{NS}\')" onMouseup="CallModule2.stopdrag(\'#{NS}\')" onSelectStart="return false" onmouseover="this.style.border=\'0.1cm groove #DFEFFF\'" >'
		    + '<div align="right" style="background-color:#99CCFF"><img name="CallModule2.closeWin" src="../../../images/CM/close.gif" onmouseover="this.style.cursor=\'default\'" onClick="CallModule2.closeWin(\'#{NS}\')"></div>'
		    + '<div id="dwindowcontent#{NS}" style="height:100%" onmouseover="this.style.cursor=\'wait\'">'
		    + '<iframe name="cframe#{NS}" id="cframe#{NS}" src="" width=100% height=100%></iframe>'
		    + '</div>'
		    + '</div>'
		    + '<form id="eform#{NS}" name="eform#{NS}" method="post" target="cframe#{NS}"></form>');
		
		    var tmp = template.evaluate({NS:ns});
		    document.body.insertAdjacentHTML('afterBegin', tmp);
	    }
	}
}