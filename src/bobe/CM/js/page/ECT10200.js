
doCheckWorkingHoliday = function(from, key,MSG_Working_Holiday){
	var template = '<div>' +'<div class="col-sm-12 mb16 thead bold"> 海外度假打工專案提醒</div>' ;
				
	template += '<div class="col-sm-12" align="center">'+ MSG_Working_Holiday +'</div>' ;
	template += '<div class="col-sm-12"><input class="btn-filled btn btn-lg mt32" id="toCheck" type="button"  value="海外遊學打工專案介紹" /><input class="btn-filled btn btn-lg mt32" id="goNext" type="button" value="繼續投保" /></div>' ;
	
	template += '</div>';
				
	$('#divCheckWorkingHolidayConfirm').empty();
	$('#divCheckWorkingHolidayConfirm').append(template);
	
	$('#toCheck').click(function(){
		$.fancybox.close();
		$.unblockUI();
		window.open("https://goo.gl/kdHHxr",'_blank');
	});
	
	$('#goNext').click(function(){
		$.fancybox.close();
		doShowProductList(from, key);
	});
	
	$.fancybox.open({
		href : '#divCheckWorkingHolidayConfirm',
			autoSize: true,
			maxWidth: 800,
			helpers     : { overlay : {closeClick: false} },
			afterClose:function(){
				$.unblockUI();
			}
	});
	
}