switchProved(0)
var key = "";
initAllArea();
switch_natural_legal()
switch_company_register_nation();
switch_company_nation();
init_PolicyForm()
init_mailWay()
init_I_agree()
changeSendAsApplcnt()
changeSendAsCompany()
selectPolicyForm();

function showinsProved(countryArrayStr){
	if("TW" == countryArrayStr){
		$(".countryControlFlag").hide();
	}
}


function test() {
	alertMsg("test-myForm:" + $("#form1").serialize())
}

function changeInsuredAsApplcnt() {

	if ($("#isApplcntCB").prop("checked") == true) {
		$("#insured_name").prop("readonly", "readonly");
		$("#insured_u_id").prop("readonly", "readonly");
		$("#insured_yy").prop("disabled", "disabled");
		$("#insured_mm").prop("disabled", "disabled");
		$("#insured_dd").prop("disabled", "disabled");
		$("#insured_name").val($("#member_name").val());
		$("#insured_u_id").val($("#member_u_id").val());
		$("#insured_yy").val($("#member_yy").val());
		$("#insured_mm").val($("#member_mm").val());
		$("#insured_dd").val($("#member_dd").val());
		$("#insured_name").attr("style", "cursor:not-allowed");
		$("#insured_u_id").attr("style", "cursor:not-allowed");
		$("#insured_yy").attr("style", "cursor:not-allowed");
		$("#insured_mm").attr("style", "cursor:not-allowed");
		$("#insured_dd").attr("style", "cursor:not-allowed");
	} else {
		$("#insured_name").prop("readonly", "");
		$("#insured_u_id").prop("readonly", "");
		$("#insured_yy").prop("disabled", "");
		$("#insured_mm").prop("disabled", "");
		$("#insured_dd").prop("disabled", "");
		$("#insured_name").attr("style", "");
		$("#insured_u_id").attr("style", "");
		$("#insured_yy").attr("style", "");
		$("#insured_mm").attr("style", "");
		$("#insured_dd").attr("style", "");
	}
}

function changeSendAsApplcnt() {
	
	if ($("#isApplcntAddress").prop("checked") == true) {
		$("#mail_city").prop("disabled", "disabled");
		$("#mail_county").prop("disabled", "disabled");
		$("#mail_address").prop("readonly", "readonly");
		$("#mail_city").attr("style", "cursor:not-allowed");
		$("#mail_county").attr("style", "cursor:not-allowed");
		$("#mail_address").attr("style", "cursor:not-allowed");
		$("#mail_city").val($("#member_city").val());
		$("#mail_county").val($("#member_county").val());
		$("#mail_address").val($("#member_address").val());
	} else {
		$("#mail_city").prop("disabled", "");
		$("#mail_county").prop("disabled", "");
		$("#mail_address").prop("readonly", "");
		$("#mail_city").attr("style", "");
		$("#mail_county").attr("style", "");
		$("#mail_address").attr("style", "");
	}
}
function changeApplcntAsMember() {
	if ($("#isMemberCB").prop("checked") == true) {
		$("#applcnt_name").prop("readonly", "readonly");
		$("#applcnt_name").prop("style", "cursor: not-allowed")
		$("#applcnt_name").val($("#member_name").val());

		$("#applcnt_u_id").prop("readonly", "readonly");
		$("#applcnt_u_id").prop("style", "cursor: not-allowed")
		$("#applcnt_u_id").val($("#member_u_id").val());

		$("#applcnt_yy").prop("disabled", "disabled");
		$("#applcnt_yy").prop("style", "cursor: not-allowed")
		$("#applcnt_yy").val($("#member_yy").val());

		$("#applcnt_mm").prop("disabled", "disabled");
		$("#applcnt_mm").prop("style", "cursor: not-allowed")
		$("#applcnt_mm").val($("#member_mm").val());

		$("#applcnt_dd").prop("disabled", "disabled");
		$("#applcnt_dd").prop("style", "cursor: not-allowed")
		$("#applcnt_dd").val($("#member_dd").val());

		$("#applcnt_mobile").prop("disabled", "disabled");
		$("#applcnt_mobile").prop("style", "cursor: not-allowed")
		$("#applcnt_mobile").val($("#member_phone").val());

	} else {
		$("#applcnt_name").prop("readonly", "");
		$("#applcnt_name").prop("style", "cursor: text")
		$("#applcnt_u_id").prop("readonly", "");
		$("#applcnt_u_id").prop("style", "cursor: text")
		$("#applcnt_yy").prop("disabled", "");
		$("#applcnt_yy").prop("style", "cursor: pointer")
		$("#applcnt_mm").prop("disabled", "");
		$("#applcnt_mm").prop("style", "cursor: pointer")
		$("#applcnt_dd").prop("disabled", "");
		$("#applcnt_dd").prop("style", "cursor: pointer")
		$("#applcnt_mobile").prop("disabled", "");
		$("#applcnt_mobile").prop("style", "cursor: text")
	}
}
function switchProved(type) {
	if ($("#isSchengen").val() == "true") {
		$("#insProvedCheckBox").prop("checked",true);
		$("#insProvedDiv").prop("style", "display:block")
		if (type == 1) {
			// 2016-11-16 1.若為申根地區，申請英文投保證明選項，提示訊息完，不能關閉
			if(! $("#insProvedCheckBoxDiv").hasClass("checked")){
				$("#insProvedCheckBoxDiv").addClass("checked");
			}
			alertMsg("旅遊地區為申根國家,一律申請英文投保證明")
		}
	} else {
		if ($("#insProvedCheckBox").prop("checked") == true) {
			$("#insProvedDiv").css("display", "block");
		} else {
			$("#insProvedDiv").css("display", "none");
		}
	}
	

	// alertMsg("insProvedCheckBox:" + $("#insProvedCheckBox").prop("checked"))
}

// 閏年

function check_month_day(who) {
	/*
	 * 逢4的倍數閏， 例如：西元1992、1996年等，為4的倍數，故為閏年。 逢100的倍數不閏，
	 * 例如：西元1700、1800、1900年，為100的倍數，當年不閏年。 逢400的倍數閏，
	 * 例如：西元1600、2000、2400年，為400的倍數，有閏年 逢4000的倍數不閏， 例如：西元4000、8000年，不閏年。
	 * 
	 */
	var year = $("#" + who + "yy").val() * 1 + 1911
	var month = $("#" + who + "mm").val()
	var mon_days = new Array()
	mon_days[1] = 31;
	mon_days[2] = 28;
	mon_days[3] = 31;
	mon_days[4] = 30;
	mon_days[5] = 31;
	mon_days[6] = 30;
	mon_days[7] = 31;
	mon_days[8] = 31;
	mon_days[9] = 30;
	mon_days[10] = 31;
	mon_days[11] = 30;
	mon_days[12] = 31;

	if (month == 2) {
		if (year % 4 == 0) {
			mon_days[2] = 29
		}
		if (year % 100 == 0) {
			mon_days[2] = 28
		}
		if (year % 400 == 0) {
			mon_days[2] = 29
		}
		if (year % 4000 == 0) {
			mon_days[2] = 28
		}
	}

	makeDayOption(mon_days[month], who)
}
function makeDayOption(days, who) {
	var html = "";
	html += '<option selected="selected" value="">請選擇日期</option>'
	for (var i = 1; i <= days; i++) {
		html += '<option value="' + i + '">' + i + '日</option>'
	}

	$("#" + who + "dd").html(html)

}

function changeRelation() {
	if ($("#relation_benefit_insured").val() == "L") {
		$("#benefit_name").prop("disabled", true);
		$("#benefit_mobile").prop("disabled", true);
		$("#benefit_city").prop("disabled", true);
		$("#benefit_county").prop("disabled", true);
		$("#benefit_addr").prop("disabled", true);

		$("#benefit_name").val("");
		$("#benefit_mobile").val("");
		$("#benefit_city").val("Default");
		$("#benefit_county").val("");
		$("#benefit_addr").val("");
		$("#benefit_city").attr("style", "cursor:not-allowed");
		$("#benefit_county").attr("style", "cursor:not-allowed");
		$("#benefit_addr").attr("style", "cursor:not-allowed");

	} else {
		$("#benefit_name").prop("disabled", false);
		$("#benefit_mobile").prop("disabled", false);
		$("#benefit_city").prop("disabled", false);
		$("#benefit_county").prop("disabled", false);
		$("#benefit_addr").prop("disabled", false);

		$("#benefit_city").attr("style", "");
		$("#benefit_county").attr("style", "");
		$("#benefit_addr").attr("style", "");
	}
}
function initAllArea() {
	if ($("#benefit_county_Html_save").val() != "") {
		$("#benefit_county").html($("#benefit_county_Html_save").val());
		$("#benefit_county").val($("#benefit_county_value_save").val());
	}
	if ($("#applcnt_county_Html_save").val() != "") {
		$("#applcnt_county").html($("#applcnt_county_Html_save").val());
		$("#applcnt_county").val($("#applcnt_county_value_save").val());
	}
}
function changeCity(who) {

	var _city = $("#" + who + "_city").val()

	$.post("../EBP0_Z000/getCountyByCityHtml", {
		city : _city
	}, function(data) {
		$("#" + who + "_county").html(data.msg);
	});
}

init_postnumber()
function init_postnumber() {
	changeCounty("benefit");
	changeCounty("applcnt")
	changeCounty("mail")
}
function changeCounty(who) {
	var county = $("#" + who + "_county").val();
	$("#" + who + "_county").find("option").each(function(i) {
		if ($(this).val() == county) {
			$("#" + who + "_postnumber").val($(this).attr("postnumber"))

		}
	});
	$("#benefit_county_Html_save").val($("#benefit_county").html());
	$("#benefit_county_value_save").val($("#benefit_county").val());
	$("#applcnt_county_Html_save").val($("#applcnt_county").html());
	$("#applcnt_county_value_save").val($("#applcnt_county").val());
	$("#mail_county_Html_save").val($("#mail_county").html());
	$("#mail_county_value_save").val($("#mail_county").val());

}

function switch_natural_legal_check_by_ajax(key){
	var who = $('input[name=natural_legal_person]:checked').val();
	// 檢查是否可更換
	var url = "../EBPT_1300_T3/changeInsureWay?key=" + key + "&person=" + who;
	$.ajax({
		url: url,
	    type: 'GET',
	    success: function(data){
	    	if(data.result){
	    		switch_natural_legal();
			}else{
				if (who == "natural_person") {
					$("#natural_person").parent("div").removeClass("checked")
					$("#natural_person").prop("checked", false);
					$("#legal_person").parent("div").addClass("checked")
					$("#legal_person").prop("checked", true);
				}else{
					$("#natural_person").parent("div").addClass("checked")
					$("#natural_person").prop("checked", true);
					$("#legal_person").parent("div").removeClass("checked")
					$("#legal_person").prop("checked", false);
				}
				alertMsg("要保人限制為會員本人，無法變更。");
			}
	    }
	});
}

function switch_natural_legal() {
	
	var who = $('input[name=natural_legal_person]:checked').val();
	var a1 = $('input[name=natural_legal_person]');
	
	if (who == "natural_person") {
		// 自然人
		$("#legal_person_div").css("display", 'none');
		$("#natural_person_div").css("display", 'block');
		
		$(a1[0]).parent("div").addClass("checked");
		$(a1[1]).parent("div").removeClass("checked");
	} else {
		if("97" == is97BlockCode){
			alertMsg("黑名單代瑪97，禁止使用法人。");
			$(".switch_natural_legal:eq(1)").removeClass("checked");
			$(".switch_natural_legal:eq(0)").addClass("checked");
		}else{
			// 法人
			$("#legal_person_div").css("display", 'block');
			$("#natural_person_div").css("display", 'none');
	
			$(a1[0]).parent("div").removeClass("checked");
			$(a1[1]).parent("div").addClass("checked");
		}
	}
	quicklyPayCheck();
}

//判斷是否顯示快速理賠
function quicklyPayCheck(){
	var personType = $('input[name=natural_legal_person]:checked').val();
    if (personType == "natural_person") {
    	if("A" == $("#travelType").val()){
    		$("#quicklyPay").show();
    	}else{
    		$("#quicklyPay").hide();
    	}
    }else{
    	$("#quicklyPay").hide();
    }
	
}


function clickAgreeSmsNotify(){
	var AGREE_SMS_NOTIFY = $('input[name="AGREE_SMS_NOTIFY"]');
	var checked = AGREE_SMS_NOTIFY.parent("div").hasClass( "checked" );
	if(checked && AGREE_SMS_NOTIFY.val() == "true"){
		$('input[name=AGREE_SMS_NOTIFY]').val("false");
		AGREE_SMS_NOTIFY.parent("div").removeClass("checked");
	}else{
		$('input[name=AGREE_SMS_NOTIFY]').val("true");
		AGREE_SMS_NOTIFY.parent("div").addClass("checked");
	}
}

function switch_company_nation() {
	var who = $('input[name=company_nation]:checked').val();

	var a1 = $('input[name=company_nation]');
	if (who == "other") {
		// 其他
		$("#company_other_nation").prop("disabled", false);

		$(a1[0]).parent("div").removeClass("checked");
		$(a1[1]).parent("div").addClass("checked");
	} else {
		// 台灣
		$("#company_other_nation").prop("disabled", true);
		$(a1[0]).parent("div").addClass("checked");
		$(a1[1]).parent("div").removeClass("checked");
	}
	
}

function switch_company_register_nation() {
	var who = $('input[name=company_register_nation]:checked').val();

	var a1 = $('input[name=company_register_nation]');
	if (who == "other") {
		// 其他
		$("#company_register_other_nation").prop("disabled", false);
		$(a1[1]).parent("div").addClass("checked");
		$(a1[0]).parent("div").removeClass("checked");
	} else {
		// 台灣
		$("#company_register_other_nation").prop("disabled", true);
		$(a1[0]).parent("div").addClass("checked");
		$(a1[1]).parent("div").removeClass("checked");

	}

}
function init_I_agree() {
	var who = $('input[name=I_agree]:checked').val();
	var a1 = $('input[name=I_agree]');
	if (who == "true") {
		$(a1[0]).parent("div").addClass("checked")

	} else {
		$(a1[0]).parent("div").removeClass("checked")

	}

}
function init_mailWay() {
	var who = $('input[name=mailWay]:checked').val();
	var a1 = $('input[name=mailWay]');
	$(a1[0]).parent("div").removeClass("checked")
	$(a1[1]).parent("div").removeClass("checked")

	if (who == "1") {
		$(a1[0]).parent("div").addClass("checked")

	} else if (who == "2") {

		$(a1[1]).parent("div").addClass("checked")

	}

}
function init_PolicyForm() {
	var who = $('input[name=policyForm]:checked').val();
	var a1 = $('input[name=policyForm]');

	switch (who) {
	case "P":
		$(a1[0]).parent("div").addClass("checked")
		$(a1[1]).parent("div").removeClass("checked")
		break;
	case "E":
		$(a1[0]).parent("div").removeClass("checked")
		$(a1[1]).parent("div").addClass("checked")

		break;
	}
	// alertMsg($("#mailWay_ordinary").parent("div").attr("class"))
}
function selectPolicyForm() {
	var who = $('input[name=policyForm]:checked').val();
	var a1 = $('input[name=policyForm]');

	switch (who) {
	case "P":
		$("#mailWay_ordinary").parent("div").removeClass("disabled");
		$("#mailWay_ordinary").prop("disabled", false);
		$("#mailWay_ordinary").parent("div").attr("style", "");

		$("#mailWay_registered").parent("div").removeClass("disabled");
		$("#mailWay_registered").prop("disabled", false);
		$("#mailWay_registered").parent("div").attr("style", "");
		
		// [申根國家方案] 當 "保單形式" 選擇為 "紙本保單" 時，"寄送方式" 沒有限制只能選 "掛號"
		if($("#isSchengen").val() == "true"){
			$("#mailWay_ordinary").parent("div").addClass("disabled");
			$("#mailWay_ordinary").prop("disabled", true);
			$("#mailWay_ordinary").parent("div").attr("style", "cursor:not-allowed");
			
			$("#mailWay_ordinary").parent("div").removeClass("checked");
			$("#mailWay_ordinary").prop("checked", false);

			$("#mailWay_registered").parent("div").addClass("checked");
			$("#mailWay_registered").prop("checked", true);
		}else{
			$("#mailWay_ordinary").parent("div").addClass("checked");
			$("#mailWay_ordinary").prop("checked", true);

			$("#mailWay_registered").parent("div").removeClass("checked");
			$("#mailWay_registered").prop("checked", false);
		}
		
		break;
	case "E":
		$("#mailWay_ordinary").parent("div").addClass("disabled");
		$("#mailWay_ordinary").prop("disabled", true);
		$("#mailWay_ordinary").parent("div").attr("style", "cursor:not-allowed");

		$("#mailWay_registered").parent("div").addClass("disabled");
		$("#mailWay_registered").prop("disabled", true);
		$("#mailWay_registered").parent("div").attr("style", "cursor:not-allowed");

		$("#mailWay_ordinary").parent("div").removeClass("checked");
		$("#mailWay_ordinary").prop("checked", false);
		
		
		$("#mailWay_registered").parent("div").removeClass("checked");
		$("#mailWay_registered").prop("checked", false);
		
		break;
	}
}

//取得個人的生日
function getPersonBirthday(){
	var year  = $("input[id='member_yy']").val();
	var month = $("input[id='member_mm']").val();
		month = parseInt(month) < 10 ? '0' + month : month;
	var date  = $("input[id='member_dd']").val();
		date = parseInt(date) < 10 ? '0' + date : date;
	return (parseInt(year) + 1911) + "-" + month + "-" + date;
};

//取得每個「被保人身分證字號/居留證號碼」
function getPersonsId(){
	return $("input[id^='a_']").map(function() { return $(this).val(); }).get().join().split(",");
};

//取得每個「意外事故身故殘廢保險金」
function getAmount21S(){
	return $("input[id^='group_amount_21_p_']").map(function() { return $(this).val(); }).get().join().split(",");
};


function goSubmit(_key, checkFlight) {
	$.blockUI();

	setTimeout(function(){
		var isOk = checkAll(checkFlight);
		if (isOk == true) {
			//要保人 - 黑名單 -> 被保人 - 黑名單 -> 通算 -> 送出
			try {
				doBlockCheckType1(_key);//要保人 - 黑名單
			}catch(e){
				$.unblockUI();
			}
		}else{
			$.unblockUI();
		}
	},200);  
	
}

//要保人 - 黑名單
function doBlockCheckType1(_key){
	
	var type = ("natural_person" == $('input[name=natural_legal_person]:checked').val()) ? true : false;
	
	var rejectCode = "";
	
	var id = (type) ? $("#applcnt_u_id").val() : $("input[name='company_id']").val();
	var name = (type) ? $("#applcnt_name").val() : $("#companyName").val();
	var url = "../EBPT_1300_T3/blockCheck?id=" +id+ "&name=" + encodeURI(encodeURI(name)) + "&kind=true&type=" + type;
	
	$.ajax({ 
		url: url, 
		type: 'GET',
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){ 
			rejectCode = data.reject;
		}
	}).done(function() {
		if("" != rejectCode && "undefined" != rejectCode){
			$.unblockUI();
			alertMsg("親愛的顧客您好：\n您的資料要保人『"+id+"』須由專人為您處理\n請播打服務專線 0800-212-880，將有專人為您服務！\n\n服務時間：每週一至週五 08:30~17:30");
			return;
		}else{
			doBlockCheckType2(type,_key);//被保人 - 黑名單
		}
	});
}

//被保人 - 黑名單
function doBlockCheckType2(type,_key){
	
	var rejectCode = "";
	
	var id = $("#insured_u_id").val();
	var name = $("#insured_name").val();
	
	var url = "../EBPT_1300_T3/blockCheck?id=" + id + "&name=" + encodeURI(encodeURI(name)) + "&kind=false&type=" + type;
	
	$.ajax({
		url: url,
		type: 'GET',
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){ rejectCode = data.reject; }
	}).done(function(){
		if("" != rejectCode && "undefined" != rejectCode){
			$.unblockUI();
			alertMsg("親愛的顧客您好：\n您的資料被保人『"+id+"』須由專人為您處理\n請播打服務專線 0800-212-880，將有專人為您服務！\n\n服務時間：每週一至週五 08:30~17:30");
			return;
		}else{
			doCalLimit(_key);//通算
		}				
	});	
}

function doCalLimit(key){
	var id = $("#insured_u_id").val();
	var birthday = getPersonBirthday();
	var amount21s = $("#amount_21").val();
	
	var insureWayFlag = "";
	if ("legal_person" == $('input[name=natural_legal_person]:checked').val()) {
		insureWayFlag = "fax";
	}else{
		insureWayFlag = insureWay;
	}
	
	var OTP = insureWayFlag == "web" ? "Y" : "N";
	var ISGROUP = "N";
	var url = "../EBPT_1300_T3/calLimit?insureWay=" + insureWayFlag + "&start_date=" + start_date + "&end_date=" + end_date + "&u_id=" + id + "&birthdays=" + birthday + "&amount21s=" + amount21s + "&key=" + key + "&OTP=" + OTP + "&ISGROUP=" + ISGROUP;
	$.ajax({
		url: url,
	    type: 'GET',
	    success: function(data){
	    	if("" == data.errorMsg){
	    		$('#relation_applcnt_insured').attr('disabled', false);
	    		$('#refqueryid').val(data.referenceId);
	    		$("#cellular_phone").val($("#TEMP_cellular_phone").val());
	    		$("#tel_zip").val($("#TEMP_tel_zip").val());
	    		$("#tel").val($("#TEMP_tel").val());
	    		$("#tel_ext").val($("#TEMP_tel_ext").val());
	    		$("#city").val($("#TEMP_city").val());
	    		$("#county").val($("#TEMP_county").val());
	    		$("#address").val($("#TEMP_address").val());
	    		$("#zip").val($('#TEMP_county option:selected').attr("postnumber"));
	    		$("#form1").attr("action", "../EBPT_1400_T4/T4?key=" + key);
	    		$("#form1").submit();
	    	}else{
	    		$.unblockUI();
	    		alertMsg(data.errorMsg);
	    	}
	    }
	});
};

function checkAll(checkFlight) {
	var isOk = true;
	// 檢查被保人資料
	if (!$("#isApplcntCB").prop("checked")) {
		// 如果不是“同為要保人”,才需檢查
		if ($("#insured_name").val() == "") {
			alertMsg("請輸入姓名");
			$("#insured_name").focus();
			return false;
		}
		if ($("#insured_u_id").val() == "") {
			alertMsg("請輸入身分證字號/居留證號碼");
			$("#insured_u_id").focus();
			return false;
		}
		if(!validation.checkID($("#insured_u_id").val().toUpperCase())){
			alertMsg(" 身分證字號/居留證號碼  錯誤");
			$("#insured_u_id").focus();
			return false;
		}

		if ($("#insured_yy").val() == "Default") {
			alertMsg("請選擇年份");
			$("#insured_yy").focus();

			return false;
		}
		if ($("#insured_mm").val() == "Default") {
			alertMsg("請選擇月份");
			$("#insured_mm").focus();

			return false;
		}
		if ($("#insured_dd").val() == "Default") {
			alertMsg("請選擇日期");
			$("#insured_dd").focus();

			return false;
		}

	}
	
	if(!/^09[0-9]{8}$/.test($("#TEMP_cellular_phone").val())){
		alertMsg("被保人手機號碼必為09開頭 /必為10碼");
		$("#TEMP_cellular_phone").focus();
		return false;
	}
	
	if ($("#TEMP_tel_zip").val() != "" && !validateNum($("#TEMP_tel_zip").val())) {
		alertMsg("被保人日間市話區碼只允許輸入數字")
		$("#TEMP_tel_zip").focus();
		return false;
	}
	if ($("#TEMP_tel").val() != "" && !validateNum($("#TEMP_tel").val())) {
		alertMsg("被保人日間市話號碼只允許輸入數字")
		$("#TEMP_tel").focus();
		return false;
	}
	if ($("#TEMP_tel_ext").val() != "" && !validateNum($("#TEMP_tel_ext").val())) {
		alertMsg("被保人日間市話分機只允許輸入數字")
		$("#TEMP_tel_ext").focus();
		return false;
	}
	
	if("Default" == $('#TEMP_city').val()){
		alertMsg("請選擇被保人通訊縣市");
		$("#TEMP_city").focus();
		return false;
	}
	
	if("" == $('#TEMP_county').val()){
		alertMsg("請選擇被保人通訊行政區");
		$("#TEMP_city").focus();
		return false;
	}
	
	if("" == $("#TEMP_address").val()){
		alertMsg("請輸入被保人通訊地址");
		$("#TEMP_address").focus();
		return false;
	}
	
	// 申請英文投保證明
	// alertMsg($("#insProvedCheckBox").prop("checked"))
	if ($("#insProvedCheckBox").prop("checked")) {
		if ($("#pass_name").val() == "") {
			alertMsg("請輸入護照英文姓名");
			$("#pass_name").focus();
			return false;
		}else{
			 if(!(/^[^\u4E00-\u9FA5\uF900-\uFA2D]+$/.test( $("#pass_name").val() ))){   
				alertMsg("被保人護照英文姓名不能輸入中文");
			    $("#pass_name").focus();
				return false;
		     }  
		}
		if ($("#pass_num").val() == "") {
			alertMsg("請輸入護照號碼");
			$("#pass_num").focus();
			return false;
		}
	}
	// 意外身故受益人
	
	if ($("#relation_benefit_insured").val() == "Def") {
		alertMsg("請選擇受益人關係");
		$("#relation_benefit_insured").focus();

		return false;
	}
	
	if($("#relation_benefit_insured").val()=="L"){
		//法定繼承人,不檢察
	}else{
		if ($("#benefit_name").val() == "") {
			alertMsg("請輸入受益人姓名");
			$("#benefit_name").focus();
			return false;
		}
		if ($("#benefit_mobile").val() == "") {
			alertMsg("請輸入受益人手機號碼");
			$("#benefit_mobile").focus();

			return false;
		}
		if (!validateNum($("#benefit_mobile").val()) || $("#benefit_mobile").val().length != 10) {
			alertMsg("受益人手機號碼只允許輸入數字10碼");
			$("#benefit_mobile").focus();

			return false;
		}
		if ($("#benefit_city").val() == "Default") {
			alertMsg("請選擇受益人縣市");
			$("#benefit_city").focus();

			return false;
		}
		if ($("#benefit_county").val() == "") {
			alertMsg("請選擇受益人行政區");
			$("#benefit_county").focus();

			return false;
		}
		if ($("#benefit_addr").val() == "") {
			alertMsg("請輸入受益人地址");
			$("#benefit_addr").focus();

			return false;
		}
	}
	
	
	// 要保人資料

	// alertMsg($("input[name=natural_legal_person]").val()+" / "+$("#applcnt_tel_zip").val() )
	 // alertMsg($("input[name=natural_legal_person]")+" / "+$("#applcnt_tel_zip").val() )
	 var natural_legal_person="";
	  $("input[name=natural_legal_person]").each(function(){
		 // alertMsg($(this).val()+"  / "+$(this).prop("checked"))
		  if($(this).prop("checked")==true){
			  natural_legal_person=$(this).val();
		  }
	  });
	 
	if (natural_legal_person == "natural_person") {
		// 自然人(同會員本人)
		if ($("#applcnt_tel_zip").val() == "9999") {
			alertMsg("請輸入要保人日間市話區碼")
			$("#applcnt_tel_zip").focus();

			return false;
		}
		if ($("#applcnt_tel").val() == "") {
			alertMsg("請輸入要保人日間市話號碼")
			$("#applcnt_tel").focus();

			return false;
		}
		
		if (!validateNum($("#applcnt_tel_zip").val())) {
			alertMsg("要保人日間市話區碼只允許輸入數字")
			$("#applcnt_tel_zip").focus();
			return false;
		}
		if (!validateNum($("#applcnt_tel").val())) {
			alertMsg("要保人日間市話號碼只允許輸入數字")
			$("#applcnt_tel").focus();
			return false;
		}
		if ($("#applcnt_tel_ext").val() != "" && !validateNum($("#applcnt_tel_ext").val())) {
			alertMsg("要保人日間市話分機只允許輸入數字")
			$("#applcnt_tel_ext").focus();
			return false;
		}
		
		// alertMsg($("#isApplcntAddress").prop("checked"))
		if ($("#isApplcntAddress").prop("checked") == false) {
			// 寄單地址同通訊地址,不同時,須檢查
			if ($("#mail_city").val() == "Default") {
				alertMsg("請選擇要保人縣市")
				$("#mail_city").focus();

				return false;
			}
			if ($("#mail_county").val() == "") {
				alertMsg("請選擇要保人行政區")
				$("#mail_county").focus();

				return false;
			}
			if ($("#mail_address").val() == "") {
				alertMsg("請輸入要保人通訊地址")
				$("#mail_address").focus();

				return false;
			}
		}
		if ($("#applcnt_email").val() == "") {
			alertMsg("請輸入要保人電子信箱")
			$("#applcnt_email").focus();

			return false;
		}
		if ($("#relation_applcnt_insured").val() == "") {
			alertMsg("請選擇要保人關係")
			$("#relation_applcnt_insured").focus();

			return false;
		}
		if(isShowPerson){
			if ($("#applcnt_occupation").val() == "") {
				alertMsg("請選擇要保人職業類別")
				$("#applcnt_occupation").focus();

				return false;
			}
			if ($("#applcnt_position").val() == "") {
				alertMsg("請選擇要保人職稱")
				$("#applcnt_position").focus();

				return false;
			}
		}
		if ($("#applcnt_yearIncome").val() == "") {
			alertMsg("請選擇要保人年收入與其他收入金額")
			$("#applcnt_yearIncome").focus();

			return false;
		}
		if ($("#applcnt_yearAssets").val() == "") {
			alertMsg("請選擇要保人財務與資產狀況金額")
			$("#applcnt_yearAssets").focus();

			return false;
		}

	} else {
		// 法人
		if ($("#companyName").val() == "") {
			alertMsg("請輸入公司名稱")
			$("#companyName").focus();

			return false;
		}
		if(isShowCompany){
			if ($("#company_occupation").val() == "") {
				alertMsg("請選擇公司行業別")
				$("#company_occupation").focus();

				return false;
			}
		}
		if ($("#company_id").val() == "") {
			alertMsg("請輸入公司統一編號")
			$("#company_id").focus();

			return false;
		}
		if (!validateNum($("#company_id").val()) || $("#company_id").val().length != 8) {
			alertMsg("公司統一編號只允許輸入數字8碼")
			$("#company_id").focus();

			return false;
		}
		if ($("#calendar_date").val() == "") {
			alertMsg("請輸入公司西元成立年份")
			$("#calendar_date").focus();

			return false;
		}
		if (!validateNum($("#calendar_date").val())) {
			alertMsg("公司西元成立年份只允許輸入數字")
			$("#calendar_date").focus();

			return false;
		}
		if ($("#captl").val() == "") {
			alertMsg("請輸入公司資本額（萬元）")
			$("#captl").focus();

			return false;
		}
		if ($("#turnover").val() == "") {
			alertMsg("請輸入公司營業收入（萬元）")
			$("#turnover").focus();

			return false;
		}
		if ($("#representative").val() == "") {
			alertMsg("請輸入公司負責人姓名")
			$("#representative").focus();

			return false;
		}
		if ($("#representative_phone").val() == "") {
			alertMsg("請輸入公司手機號碼")
			$("#representative_phone").focus();

			return false;
		}
		if ($("#company_tel_zip").val() == "9999") {
			alertMsg("請輸入公司日間市話區碼")
			$("#company_tel_zip").focus();

			return false;
		}
		if ($("#company_tel").val() == "") {
			alertMsg("請輸入公司日間市話區碼")
			$("#company_tel").focus();

			return false;
		}
		
		if ($("#company_city").val() == "Default") {
			alertMsg("請選擇公司縣市")
			$("#company_city").focus();

			return false;
		}
		if ($("#company_county").val() == "Default") {
			alertMsg("請選擇公司行政區")
			$("#company_county").focus();

			return false;
		}
		if ($("#company_address").val() == "") {
			alertMsg("請輸入公司地址")
			$("#company_address").focus();

			return false;
		}
		if ($("#company_email").val() == "") {
			alertMsg("請輸入公司電子信箱")
			$("#company_email").focus();

			return false;
		}
		if ($("#company_yearIncome").val() == "") {
			alertMsg("請選擇公司金額")
			$("#company_yearIncome").focus();

			return false;
		}
		if ($("#company_yearAssets").val() == "") {
			alertMsg("請選擇公司金額")
			$("#company_yearAssets").focus();

			return false;
		}
		/*if ($("#relation_applcnt_insured").val() == "") {
			alertMsg("請選擇公司為被保險人之")
			$("#relation_applcnt_insured").focus();

			return false;
		}*/
	}

	if ($("#emergency_name").val() == "") {
		alertMsg("請輸入台灣緊急連絡人姓名")
		$("#emergency_name").focus();

		return false;
	}
	if ($("#emergency_mobile").val() == "") {
		alertMsg("請輸入台灣緊急連絡人手機號碼")
		$("#emergency_mobile").focus();

		return false;
	}
	if (!validateNum($("#emergency_mobile").val()) || $("#emergency_mobile").val().length != 10) {
		alertMsg("台灣緊急連絡人手機號碼只允許輸入數字10碼")
		$("#emergency_mobile").focus();

		return false;
	}
	if ($("#emergency_tel_zip").val() == "9999") {
		alertMsg("請輸入台灣緊急連絡人聯絡人日間市話區碼")
		$("#emergency_tel_zip").focus();

		return false;
	}
	if ($("#emergency_tel").val() == "") {
		alertMsg("請輸入台灣緊急連絡人日間市話號碼")
		$("#emergency_tel").focus();

		return false;
	}
	if (!validateNum($("#emergency_tel_zip").val())) {
		alertMsg("台灣緊急連絡人日間市話區碼只允許輸入數字")
		$("#emergency_tel_zip").focus();
		return false;
	}
	if (!validateNum($("#emergency_tel").val())) {
		alertMsg("台灣緊急連絡人日間市話號碼只允許輸入數字")
		$("#emergency_tel").focus();
		return false;
	}
	if ($("#emergency_tel_ext").val() != "" && !validateNum($("#emergency_tel_ext").val())) {
		alertMsg("台灣緊急連絡人日間市話分機只允許輸入數字")
		$("#emergency_tel_ext").focus();
		return false;
	}
	
	if ($("#I_agree").prop("checked") == false) {
		alertMsg("請先同意上述須知及審閱相關聲明事項")
		return false;
	}
	
	//快速理賠區塊 驗證
	if($('input[name="AGREE_SMS_NOTIFY"]').val() == "true"){
		var flightNo1 = $("#FLIGHT_NO_1").val();
		var flightNo2 = $("#FLIGHT_NO_2").val();
		if ($("#FLIGHT_DATE_1").val() == "" && $("#FLIGHT_DATE_2").val() == ""){
			alertMsg("請輸入出發日期或回程日期");
			$("#FLIGHT_DATE_1").focus();
			return false;
		}else{			
			if ($("#FLIGHT_DATE_1").val() && $("#FLIGHT_DATE_1").val() != "") {
				if (flightNo1 == "") {
					alertMsg("請輸入出發航班號碼");
					$("#FLIGHT_NO_1").focus();		
					return false;
				}
			}	
			if ($("#FLIGHT_DATE_2").val() && $("#FLIGHT_DATE_2").val() != "") {
				if (flightNo2 == "") {
					alertMsg("請輸入回程航班號碼");
					$("#FLIGHT_NO_2").focus();
		
					return false;
				}
			}			
		}
		var re = /^[a-zA-Z0-9]+$/;
		var englishRe = /^[a-zA-Z]+$/;
		var numberRe =  /^[0-9]+$/;
		if (flightNo1 && flightNo1 != "") {
			if ( flightNo1.length < 3 || !re.test(flightNo1) || numberRe.test(flightNo1) || englishRe.test(flightNo1) ) {
				alertMsg("出發航班編號，必須為英文+數字至少3碼，不得輸入空格及其他符號(例如-,:>等)");
				$("#FLIGHT_NO_1").focus();
				return false;
			}			
		}
		if (flightNo2 && flightNo2 != "") {
			if ( flightNo2.length < 3 || !re.test(flightNo2) || numberRe.test(flightNo2) || englishRe.test(flightNo2) ) {
				alertMsg("回程航班編號，必須為英文+數字至少3碼，不得輸入空格及其他符號(例如-,:>等)");
				$("#FLIGHT_NO_2").focus();
				return false;
			}			
		}	
		
		if(!checkFlight){
			return checkFlightCode();
		}
	}
	
	return isOk;
}


function checkFlightCode(){
	var checkFilght = true;
	$.ajax({
		  type: 'POST',
		  url: "../EBPT_1300_T3/checkFlightCode",
		  data: {
			  	id : $("#member_u_id").val(),
			  	startDate : start_date,
			  	endDate : end_date,
			  	flightDate1 : $("#FLIGHT_DATE_1").val(),
			  	flightNo1 : $("#FLIGHT_NO_1").val(),
			  	flightDate2 : $("#FLIGHT_DATE_2").val(),
			  	flightNo2 : $("#FLIGHT_NO_2").val()
			},
		  async: false,
		  success: function(data) {
			  if(data && data.RETURN_CODE != '0000'){
				  flightCheckConfirm(data);
				  checkFilght = false;
			  }
		  }
	});		
	
	return checkFilght;
}


var flightCheckConfirm = function(data){
	var template = '<div>' +'<div class="col-sm-12 mb16 thead bold">航班檢核</div>' ;
				
	template += '<div class="col-sm-12" align="left">' + data.RETURN_MSG + '</div>' ;
	template += '<div class="col-sm-12"><input class="btn-filled btn btn-lg mt32" id="cancelFlightCheckConfirm" type="button"  value="再次確認" />';
	template += '<input class="btn-filled btn btn-lg mt32" id="flightCheckConfirmGoNext" type="button" value="確認無誤" /></div>' ;
	
	template += '</div>';
				
	$('#divFlightCheckConfirm').empty();
	$('#divFlightCheckConfirm').append(template);
	
	$('#cancelFlightCheckConfirm').click(function(){
		$("#FLIGHT_NO_1").focus();
		$.fancybox.close();
	});
	
	$('#flightCheckConfirmGoNext').click(function(){
		$.fancybox.close();
		goSubmit(key, true);
	});
	
	$.fancybox.open({
		href : '#divFlightCheckConfirm',
			autoSize: true,
			maxWidth: 800,
			helpers     : { overlay : {closeClick: false} }
	});
}




function changeSendAsCompany(){
		
	if ($("#isCompanyAddress").prop("checked") == true) {
		$("#mail_company_city").prop("disabled", "disabled");
		$("#mail_company_county").prop("disabled", "disabled");
		$("#mail_company_address").prop("readonly", "readonly");

		$("#mail_company_city").attr("style", "cursor:not-allowed");
		$("#mail_company_county").attr("style", "cursor:not-allowed");
		$("#mail_company_address").attr("style", "cursor:not-allowed");


		$("#mail_company_city").val($("#company_city").val());
		$("#mail_company_county").val($("#company_county").val());
		$("#mail_company_address").val($("#company_address").val());
		

	} else {

		$("#mail_company_city").prop("disabled", "");
		$("#mail_company_county").prop("disabled", "");
		$("#mail_company_address").prop("readonly", "");
		$("#mail_company_city").attr("style", "");
		$("#mail_company_county").attr("style", "");
		$("#mail_company_address").attr("style", "");
	}
}
function changeAddition(id) {
	var addition_count = $("#addition_count").val();
	if ($("#add_input_" + id).prop("checked") == true) {
		$("#premium_label").html($("#add_" + id).attr("premium"));
		$("#amount_21_label").html(wanLabel($("#add_" + id).attr("amount_21")));
		$("#amount_22_label").html(wanLabel($("#add_" + id).attr("amount_22")));
		$("#amount_23_label").html(wanLabel($("#add_" + id).attr("amount_23")));
		$("#amount_61_label").html(wanLabel($("#add_" + id).attr("amount_61")));
		$("#produc_title").html($("#add_" + id).attr("title"));

		$("#combo").val($("#add_" + id).attr("combo"));
		$("#premium_combo").val($("#add_" + id).attr("premium_combo"));

		if ($("#add_" + id).attr("premium") != $("#premium_org").val()) {
			$("#premium_label").addClass("maincolor");
		}
		if ($("#add_" + id).attr("amount_21") != $("#amount_21_org").val()) {
			$("#amount_21_label").addClass("maincolor");
		}

		if ($("#add_" + id).attr("amount_22") != $("#amount_22_org").val()) {
			$("#amount_22_label").addClass("maincolor");
		}

		if ($("#add_" + id).attr("amount_23") != $("#amount_23_org").val()) {
			$("#amount_23_label").addClass("maincolor");
		}

		if ($("#add_" + id).attr("amount_61") != $("#amount_61_org").val()) {
			$("#amount_61_label").addClass("maincolor");
		}

		$("#premium").val($("#add_" + id).attr("premium"));
		$("#amount_21").val($("#add_" + id).attr("amount_21"));
		$("#amount_22").val($("#add_" + id).attr("amount_22"));
		$("#amount_23").val($("#add_" + id).attr("amount_23"));
		$("#amount_61").val($("#add_" + id).attr("amount_61"));

		for (var i = 0; i < addition_count; i++) {
			if (id != i) {
				if ($("#add_" + i).hasClass("checked")) {
					$("#add_input_" + i).prop("checked", false);
					$("#add_" + i).removeClass("checked");
				}

			}
		}
	} else {
		// 回復原值
		$("#premium_label").html($("#premium_org").val());
		$("#amount_21_label").html(wanLabel($("#amount_21_org").val()));
		$("#amount_22_label").html(wanLabel($("#amount_22_org").val()));
		$("#amount_23_label").html(wanLabel($("#amount_23_org").val()));
		$("#amount_61_label").html(wanLabel($("#amount_61_org").val()));
		$("#produc_title").html($("#produc_title_org").val());

		$("#premium_label").removeClass("maincolor");
		$("#amount_21_label").removeClass("maincolor");
		$("#amount_22_label").removeClass("maincolor");
		$("#amount_23_label").removeClass("maincolor");
		$("#amount_61_label").removeClass("maincolor");

		$("#premium").val($("#premium_org").val());
		$("#combo").val($("#combo_org").val());
		$("#premium_combo").val($("#premium_combo_org").val());
		$("#amount_21").val($("#amount_21_org").val());
		$("#amount_22").val($("#amount_22_org").val());
		$("#amount_23").val($("#amount_23_org").val());
		$("#amount_61").val($("#amount_61_org").val());
	}
}
function wanLabel(num) {
	return num / 10000 + "萬";
}

function select_frequently_insured(insured_seqno_input) {

	$("#insured_seqno_input").val(insured_seqno_input);
}
function select_frequently_insured_sure() {
	if($("#insured_seqno_input").val() != null && $("#insured_seqno_input").val() != undefined
		&& $("#insured_seqno_input").val() != ""){
		$("#isApplcntCB").prop("checked", false)
		$("#isApplcntCB").parent().removeClass("checked")
		changeInsuredAsApplcnt();
		
		$("input[name=insured_seqno]").each(function() {

			if ($(this).prop("checked") == true) {
				$(this).parent().removeClass("checked")
				$(this).prop("checked", false);
			}

		})

		// var url = "../EBPT_1400_T4/save_frequently_travel_insured?key=" + key;
		var url = "../EBPT_1300_T3/get_frequently_travel_insured";
		var insured_seqno = $("#insured_seqno_input").val();
		var data = "insured_seqno=" + insured_seqno;
		$.post(url, data, function(data, status) {

			$("#insured_name").val(data.insured_name)
			$("#insured_u_id").val(data.insured_id)

			var insured_birthday_str = data.insured_birthday.substring(0, 10);

			var insured_birthday_arr = insured_birthday_str.split("-")
			var year = insured_birthday_arr[0] - 1911;

		
			$("#insured_yy").val(year)
			$("#insured_mm").val(parseInt(insured_birthday_arr[1]))
			$("#insured_dd").val(parseInt(insured_birthday_arr[2]))
		});
	}
}

function changeCompanyId(){
	var url = "../EBPT_1300_T3/getIsShowCompany?company_id="+$("#company_id").val();
	$.ajax({
	    url: url,
	    type: 'POST',
	    success: function(data){ 
	    	isShowCompany = !data.isShowCompany;
			var styleStr;
			if(isShowCompany){
				styleStr = "display:block";
			}else{
				styleStr = "display:none";
			}
			for(var i=0;i<=6;i++){
				$("#company_show_"+i).attr("style",styleStr);
			}
	    },
	    error: function(data) {
			console.log(data);
	    }
	});
}

function autoFill(){
	$('#applcnt_occupation option:eq(2)').prop('selected', true);
	$('#applcnt_position option:eq(2)').prop('selected', true);
	$('#applcnt_yearIncome option:eq(2)').prop('selected', true);
	$('#applcnt_yearAssets option:eq(2)').prop('selected', true);
	$('#emergency_tel_zip option:eq(2)').prop('selected', true);
	
	$('#relation_benefit_insured option:eq(2)').prop('selected', true);
	$('#benefit_city option:eq(2)').prop('selected', true);
	$('#applcnt_tel_zip option:eq(2)').prop('selected', true);
	
	$("#pass_name").val("jimmy huang");
	$("#pass_num").val("53535051");
	$("#benefit_name").val("瘦一人");
	$("#benefit_mobile").val("0919123456");
	$("#benefit_addr").val("anto-受益人通訊地址-5號");
	$("#applcnt_tel").val("27381818");
	$("#applcnt_tel_ext").val("888");
	$("#companyName").val("非常愛現");
	$("#company_id").val("53535051");
	$("#calendar_date").val("2006");
	$("#captl").val("7000");
	$("#turnover").val("2000");
	$("#representative").val("a&J");
	$("#representative_phone").val("09055201314");
	$("#company_tel").val("23933390");
	$("#company_tel_ext").val("501");
	$("#company_address").val("台北市八德路一段5號5F");
	$("#emergency_name").val("葉媽媽");
	$("#emergency_mobile").val("0933111222");
	$("#emergency_tel").val("23055718");
	$("#emergency_tel_ext").val("888");
	//$("#benefit_city").val("嘉義");
	//$("#benefit_county").val("東石");
	$("#aaaaa").val("");
}

// 變更 要保人之
function changeRelationApplcnt(){
	var relation_applcnt_insured = $("#relation_applcnt_insured");
	var applcnt_u_id = $("#applcnt_u_id").val();
	var insured_u_id = $("#insured_u_id").val();
	var applcnt_yy = $("#applcnt_yy").val();
	var insured_yy = $("#insured_yy").val();
	var applcnt_mm = $("#applcnt_mm").val();
	var insured_mm = $("#insured_mm").val();
	var applcnt_dd = $("#applcnt_dd").val();
	var insured_dd = $("#insured_dd").val();
	// 當要/被保人不同人時，"為被保險人之" 選擇 "本人" 沒有擋下來 (比對依據：ID、生日)
	if((applcnt_u_id != insured_u_id 
		|| applcnt_yy != insured_yy
		|| applcnt_mm != insured_mm
		|| applcnt_dd != insured_dd)
		&& relation_applcnt_insured.val() == "1"){
		relation_applcnt_insured.val("");
		alertMsg("要保人和被保人資料不同, 請重新確認");
		return;
	}
	
	// 當要/被保人性別相同時，"為被保險人之" 選擇 "配偶" 沒有擋下來 (比對依據：ID、生日) 性別判定居留證第2碼(男性是A或C或1、女性是B或D或2)
	if(applcnt_u_id != undefined && applcnt_u_id != null && applcnt_u_id != ""
		&& insured_u_id != undefined && insured_u_id != null && insured_u_id != ""){
		var applcnt_u_id_gender = applcnt_u_id.substring(1,2);
		var insured_u_id_gender = insured_u_id.substring(1,2);
		if((((applcnt_u_id_gender == "A" || applcnt_u_id_gender == "C" || applcnt_u_id_gender == "1") 
				&& (insured_u_id_gender == "A" || insured_u_id_gender == "C" || insured_u_id_gender == "1"))
			|| ((applcnt_u_id_gender == "B" || applcnt_u_id_gender == "D" || applcnt_u_id_gender == "2") 
				&& (insured_u_id_gender == "B" || insured_u_id_gender == "D" || insured_u_id_gender == "2")))
			&& relation_applcnt_insured.val() == "2"){
			relation_applcnt_insured.val("");
			alertMsg("要保人和被保人性別相同, 請重新確認");
			return;
		}
	}
	
	if(applcnt_yy != undefined && applcnt_yy != null && applcnt_yy != ""
		&& insured_yy != undefined && insured_yy != null && insured_yy != ""){
		// 5. [T3-1][要保人] 當要保人年齡比被保險人年齡小時，"為被保險人之" 選擇 "父母" 沒有擋下來
		if((parseInt(applcnt_yy) > parseInt(insured_yy)
			|| (parseInt(applcnt_yy) == parseInt(insured_yy) && parseInt(applcnt_mm) > parseInt(insured_mm))
			|| (parseInt(applcnt_yy) == parseInt(insured_yy) && parseInt(applcnt_mm) == parseInt(insured_mm) && parseInt(applcnt_dd) > parseInt(insured_dd)))
			&& relation_applcnt_insured.val() == "3"){
			relation_applcnt_insured.val("");
			alertMsg("要保人年齡比被保險人年齡小, 請重新確認");
			return;
		}
		
		// 6. [T3-1][要保人] 當要保人年齡比被保險人年齡大，但相差歲數不到 15 歲時，"為被保險人之" 選擇 "父母" 沒有擋下來
		if((parseInt(insured_yy) - parseInt(applcnt_yy) < 15
			|| parseInt(insured_yy) - parseInt(applcnt_yy) == 15 && parseInt(applcnt_mm) > parseInt(insured_mm)
			|| parseInt(insured_yy) - parseInt(applcnt_yy) == 15 && parseInt(applcnt_mm) == parseInt(insured_mm) && parseInt(applcnt_dd) > parseInt(insured_dd))
			&& relation_applcnt_insured.val() == "3"){
			relation_applcnt_insured.val("");
			alertMsg("要保人年齡和被保險人年齡相差歲數不到 15 歲, 請重新確認");
			return;
		}
		
		// 7. [T3-1][要保人] 當要保人年齡比被保險人年齡大時，"為被保險人之" 選擇 "子女" 沒有擋下來
		if((parseInt(applcnt_yy) < parseInt(insured_yy)
			|| (parseInt(applcnt_yy) == parseInt(insured_yy) && parseInt(applcnt_mm) < parseInt(insured_mm))
			|| (parseInt(applcnt_yy) == parseInt(insured_yy) && parseInt(applcnt_mm) == parseInt(insured_mm) && parseInt(applcnt_dd) < parseInt(insured_dd)))
			&& relation_applcnt_insured.val() == "4"){
			relation_applcnt_insured.val("");
			alertMsg("要保人年齡比被保險人年齡大, 請重新確認");
			return;
		}
		
		// 8. [T3-1][要保人] 當要保人年齡比被保險人年齡小，但相差歲數不到 15 歲時，"為被保險人之" 選擇 "子女" 沒有擋下來
		if((parseInt(applcnt_yy) - parseInt(insured_yy) < 15
			|| parseInt(applcnt_yy) - parseInt(insured_yy) == 15 && parseInt(applcnt_mm) < parseInt(insured_mm)
			|| parseInt(applcnt_yy) - parseInt(insured_yy) == 15 && parseInt(applcnt_mm) == parseInt(insured_mm) && parseInt(applcnt_dd) < parseInt(insured_dd))
			&& relation_applcnt_insured.val() == "4"){
			relation_applcnt_insured.val("");
			alertMsg("要保人年齡和被保險人年齡相差歲數不到 15 歲, 請重新確認");
			return;
		}
		
	}
}

/**
 * check the value is Number and between min and max
 */
function validateNum(v) {
	var filter  = /^(-?)((\d+(\.\d*)?)|((\d*\.)?\d+))$/;
	if (filter.test(v)) {
		return true;
	}else{
		return false;
	}
}