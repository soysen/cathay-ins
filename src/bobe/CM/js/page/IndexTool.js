/**
 * 首頁
 * include from index.jsp
 * 
 * 1.收合功能
 */
var IndexTool = IndexTool || {
	/**首頁初始化設定**/
	init : function(){
		//processDiv 投保流程
		//this.autoClose(LayoutTool.viewDevice);
		
		$(".select2-selection__choice__remove").on("click",function(){
			this.showTravelDes($("#travel_country").val());
		})
	},
	/**
	 * 收合功能
	 * @param viewDevice M=手機/P=電腦
	 * 
	 * 6	投保流程：
		6.1	改為收合功能，手機版預設收合。
		6.2	可參考同業(富邦)。
		7	最新消息：
		7.1	改為收合功能，手機版預設收合。
		7.2	可參考同業(富邦)。
		8	QA：
		8.1	改為收合功能，手機版預設收合。
		8.2	可參考同業(富邦)。
		9	Footer：
		9.1	關於Bobe以下區塊，改為收合功能，手機版預設收合。
		9.2	可參考同業(富邦)。
	 */
	autoClose : function(viewDevice){
		if(viewDevice == 'M'){
			$("#processDiv").hide();
			$("#newsDiv").hide();
			$("#QADiv").hide();
			$("#aboutDiv").hide();	
		}
		/**投保流程收合**/
		$("#processTitle").on("click",function(){
			$("#processDiv").toggle();
		})
		/**最新消息收合**/
		$("#newsTitle").on("click",function(){
			$("#newsDiv").toggle();
		})
		/**QA收合**/
		$("#QATitle").on("click",function(){
			$("#QADiv").toggle();
		})
		/**關於Bobe收合**/
		$("#aboutTitle").on("click",function(){
			$("#aboutDiv").toggle();
		})
	},
	showTravelDes : function(travelCountry){
		if(!travelCountry){
			$("#travelWordingTw").show();
			$("#travelWordingForeign").show();
			$("#travelWordingSchengen").show();
			return;
		}
		$("#travelWordingTw").hide();
		$("#travelWordingForeign").hide();
		$("#travelWordingSchengen").hide();
		$.each(travelCountry, function( i, item){
			var arr = item.split('_');
			var val = arr[1];
			if(arr[1] == '1'){
				$("#travelWordingTw").show();
			}
			if($.inArray(arr[1],['2','3','4','5','6','7','9']) != -1){
				$("#travelWordingForeign").show();
			}
			if(arr[1] == 'A'){
				$("#travelWordingSchengen").show();
			}
		})
	}
}

