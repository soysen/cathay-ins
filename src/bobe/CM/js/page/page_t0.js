var _hasLogin = false;
// 用於比對是否登入人員(ID)等於被保人員姓名與生日檢核
var _tempId = "";
var _tempName = "";
var _tempBirthday = "";

var blockIdMsg = "親愛的顧客您好：\n您的資料須由專人為您處理\n請播打服務專線 0800-212-880，將有專人為您服務！\n\n服務時間：每週一至週五 08:30~17:30";

var underAge15Msg = "目前 bobe 網站無法受理未滿 15 歲之被保險人，若您有此需求，請您就近至本公司 <a href='https://www.cathayholdings.com/insurance/location/' target='_blank'>各服務據點</a> 直接辦理。";

// 登入時隱藏登入會員按鈕
function doCheckLoginStatusCallBackCustomer(status) {
	if (status == 'N') {
		$('.loginArea').show();
		$(".contentArea").hide();
		_hasLogin = false;
	} else {
		$('.loginArea').hide();
		$(".contentArea").show();
		_hasLogin = true;
		callAjax(ecDispatcher + 'ECM1_0101/checkloginStatus', '', doGetLoginInfoCallBack, false);
	}
}

doGetLoginInfoCallBack = function(data) {
	if (!isEmpty(data.error)) {
		return;
	}
	var loginLd = data.memberId;
	var loginName = data.memberName;
	var loginFlagArray = data.memberBirth.split("/");
	var sessionFlagArray = ("" != $("#pageT0SessionBirthday").val()) ? $("#pageT0SessionBirthday").val().split("/") : "";

	var id = $("#pageT0SessionId").val() || loginLd;
	var name = $("#pageT0SessionName").val() || loginName;

	$("#insured_u_id").val(id);
	$("#insured_name").val(name);
	$("#insured_yy").val(parseInt(loginFlagArray[0], 10));
	$("#insured_mm").val(parseInt(loginFlagArray[1], 10));
	//此method於 EBPT1000_T0.jsp
	getMonthDay('insured_');
	$("#insured_dd").val(parseInt(loginFlagArray[2], 10));

	if ("" != sessionFlagArray) {
		$("#insured_yy").val(parseInt(sessionFlagArray[0], 10));
		$("#insured_mm").val(parseInt(sessionFlagArray[1], 10));
		//此method於 EBPT1000_T0.jsp
		getMonthDay('insured_');
		$("#insured_dd").val(parseInt(sessionFlagArray[2], 10));
	}

	// 用於比對是否登入人員(ID)等於被保人員姓名與生日檢核
	$("#loginId").val(loginLd);
	_tempId = loginLd;
	_tempName = loginName;
	_tempBirthday = data.memberBirth;
	
	change_travel_conditions_html();

}

var setDefaultCountry = function(){
	var selectCountryStr = $("#fullCountryInfo").val();
	if( "" != selectCountryStr){
		$("#travel_country").val(selectCountryStr.split(',')).trigger("change");
	}
}

function getInsureWay() {
	var isSchengen = getIsSchengen($("#fullCountryInfo").val().toString());
	var numberOfPeople = $("#numberOfPeople").val();
	var startDate = $("#datepicker").val();
	var insured_u_id = $("#insured_u_id").val();
	var url = "../EBPT_1000_T0/getInsureWay?isSchengen=" + isSchengen + "&numberOfPeople=" + numberOfPeople + "&startDate=" + startDate + "&insured_u_id=" + insured_u_id;
	$.ajax({
		url : url, type : 'POST', success : function(data) {
			$("#insureWay").val(data['insureWay']);
			$("#isSchengen").val(isSchengen);
		}
	});
}

$(function() {
	
	$("#travel_country").select2({ maximumSelectionLength : 5 }).on("change", function(e) { countryCallBack($(this)); setCountryField(); getInsureWay(); change_travel_conditions_html(); });
	setDefaultCountry();
	
	$("#hasHere").val("true");

	var todayAddTwoHour = $("#serverDateTimeAddTwoHour").val();
	var d = new Date(todayAddTwoHour);
	var year = d.getFullYear();
	var month = d.getMonth() + 1;
	var day = d.getDate();
	var hour = d.getHours();
	var min = d.getMinutes();

	$("#tra_hour").val(fix2(hour));
	if (min >= 30) {
		$("#tra_min").val("30");
	} else {
		$("#tra_min").val("00");
	}

	var output = year + '/' + month + '/' + day;

	if ($("#start_date").val()) {
		var startDate = $("#start_date").val().replace(' ', 'T');
		var start_date_str = startDate.substring(0, 4) + '/' + startDate.substring(5, 7) + '/' + startDate.substring(8, 10);
		var s_hour = startDate.substring(11, 13);
		var s_min = startDate.substring(14, 16);

		$("#datepicker").datepicker("setDate", start_date_str);

		$("#tra_hour").val(s_hour);

		$("#tra_min").val(s_min);
	} else {
		$("#datepicker").datepicker("setDate", output);
	}

	$("#datepicker").datepicker("option", "minDate", new Date(output));
	$("#datepicker").datepicker("option", "maxDate", "+2m -1d");

	change_time("init");

	$("#insured_u_id").blur(function() {
		if (10 == $(this).val().length) {
			change_travel_conditions_html();
		}
	});
	doLoadSessionIdNameBirthday();
});

function doLoadSessionIdNameBirthday() {
	$.ajax({
		url : "../EBPT_1000_T0/doLoadSessionIdNameBirthday",
		type : 'POST',
		data : {},
		success : function(data) {
			if ("" != data.pageT0SessionId) {
				$("#pageT0SessionId").val(data.pageT0SessionId);
				$("#pageT0SessionName").val(data.pageT0SessionName);
				var temp = data.pageT0SessionBirthday
				var dateFlag = temp.replace(/\-/g, '\/');
				var array = dateFlag.split("/");
				$("#pageT0SessionBirthday").val(parseInt(array[0], 10) - 1911 + "/" + array[1] + "/" + array[2]);
				// 後續接著doCheckLoginStatusCallBackCustomer
			}
		}
	});
}

function change_product_list(fromPage) {
}

function isChangeInsureWay() {

	var newInsureWay = "web";
	var date1 = new Date();
	var date2 = new Date($("#datepicker").val().split(" ")[0]);
	var timeDiff = Math.abs(date2.getTime() - date1.getTime());
	var diffDay = Math.ceil(timeDiff / (1000 * 3600 * 24));

	var country = $("#travel_country").val();
	if (country) {
		var isSchengenFlag = getIsSchengen(country.toString()) == "true";
		var ppl = $("#numberOfPeople").val()
		var insuredUId = $("#insured_u_id").val();
		var memberUId = $("#loginId").val() || $("#member_u_id").val();

		if (diffDay >= $("#limitDay").val()) {
			newInsureWay = (ppl == 1 && !isSchengenFlag) ? "web" : "fax";
		} else {
			newInsureWay = (isSchengenFlag || ppl > 1) ? "fax" : "web";
		}
		if ("" != insuredUId && "" != memberUId && memberUId != insuredUId) {
			newInsureWay = "fax";
		}
	}

	return newInsureWay;
}

function change_travel_conditions_html() {
	// 投保條件文案隨著天地人變換
	var insureWay = isChangeInsureWay();
	var countryArrayStr = $("#countryArrayStr").val();
	var travel_conditions;

	// TODO 分辨
	if (countryArrayStr == "") {
		travel_conditions = "web";
		$("#travel_conditions_HTML").html($("#travel_conditions_HTML_web").val());
	} else if (insureWay == "fax") {
		travel_conditions = "fax";
		$("#travel_conditions_HTML").html($("#travel_conditions_HTML_fax").val());
	} else {
		travel_conditions = "web";
		$("#travel_conditions_HTML").html($("#travel_conditions_HTML_web").val());

	}

	$("#condition_li").closest('.tabs').find('li').removeClass('active');
	$("#condition_li").addClass('active');
	var liIndex = $("#condition_li").index() + 1;
	$("#condition_li").closest('.tabbed-content').find('.content>li').removeClass('active');
	$("#condition_li").closest('.tabbed-content').find('.content>li:nth-of-type(' + liIndex + ')').addClass('active');

}

function checkDate_nDays_for_fax_web() {
	var nDays = $("#nDays").val();
	var nDaysDate = new Date($("#nDaysDate").val());
	if ("2019-12-31" == nDaysDate) {
		alertMsg("function 不能用 nDaysDate=" + $("#nDaysDate").val())

	}

	var start_date = new Date($("#start_date").val())
	if (start_date > nDaysDate) {
		return "fax";
	} else {
		return "web";
	}

	serverDateTime = $("#serverDateTime").val();

	var now = new Date(serverDateTime);

	var mini_7day = 60 * 24 * nDays;
	var timeTavelFromNow = Math.round((start_date - now) / 1000 / 60);
	if (timeTavelFromNow < mini_7day) {
		return "web";
	} else {
		return "fax";
	}

}
function getPremium() {
	var insureWay = $("#insureWay").val();// 
	var PRODUCT_POLICY_CODE = $("#PRODUCT_POLICY_CODE").val();
	var travel_days = $("#travel_days").val();
	var amount_21 = $("#amount_21").val();
	var amount_22 = $("#amount_22").val();
	var amount_23 = $("#amount_23").val();
	var amount_61 = $("#amount_61").val();
	getPremiumApi(insureWay, PRODUCT_POLICY_CODE, travel_days, amount_21, amount_22, amount_23, amount_61);
}
function backPremium_from_getPremiumApi(data) {
	if (data) {
		var arr = data.split("__");
		var numberOfPeople = $('#numberOfPeople').val();
		var premium = arr[0] * numberOfPeople
		var showStr = arr[0] * $("#defaultPremium").html(arr[0] + " x " + numberOfPeople + "人 = " + premium + " 元起");
		$("#combo").val(arr[1]);
		var msg = checkTravelDate(arr[2]);
		alertErroMsg(msg);
	}
}
function alertErroMsg(msg) {
	if (msg != null && firstTime == false) {
		alertMsg(msg);
		return true;
	} else {
		return false;
	}
}
var isNeedLock = false;

function goSubmit(isCustom, product_id, key) {

	$("#product_id").val(product_id);

	if (isCustom == true) {
		data = $("#form1").serialize();
		if (key == "") {
			var url = "getTimeStamp_session?" + data;
		} else {
			var url = "getTimeStamp_session?" + data + "&key=" + key;
		}

		$.get(url, function(data, status) {
			$.blockUI();
			var url = "../EBPT_1200_T2/T2?key=" + data.key.trim();
			url += "&product_id=" + product_id;
			location.href = url;

		});

	} else {

		var originalName = $("#insured_name").val();
		$("#insured_name").val(encodeURIComponent(originalName));
		data = $("#form1").serialize();
		$("#insured_name").val(originalName);

		if (key == "") {
			var url = "getTimeStamp_session?" + data;
		} else {
			var url = "getTimeStamp_session?" + data + "&key=" + key;
		}

		$.get(url, function(data, status) {
			$.blockUI();
			location.href = "../EBPT_1100_T1/T1?key=" + data.key.trim();
		});
	}
}
function checkIfSelect() {
	if ($("#countryArrayStr").val() == "") {
		return false
	} else {
		return true;
	}
}

function hideProducts() {
	$("#part_0").attr("style", "display:block");
	$("#part_1").attr("style", "display:block");
	$("#travel_days").focus();
	$("#part_2").attr("style", "display:none");
}

function requiredInput() {

	var text = {
		"0" : "旅遊人數",
		"1" : "被保人姓名",
		"2" : "被保人身分證字號/居留證號碼",
		"3" : "被保人出生年",
		"4" : "被保人出生月",
		"5" : "被保人出生日"
	};

	var hasError = false;

	$(".requiredInput").each(function(index) {
		$this = $(this);
		thisVal = $this.val();
		if ([ "", "Default" ].indexOf(thisVal) != -1) {
			$this.focus();
			hasError = true;
			alertMsg("請輸入:\n" + text[index]);
			return false;
		}
	});

	if (!hasError && null == $("#travel_country").val()) {
		alertMsg("請選擇旅遊地點..");
		hasError = true
	}
	;

	return hasError;
}

function isDate(dateString) {
	return dateValidationCheck(dateString);
}

function dateValidationCheck(str) {
	  var re = new RegExp("^([0-9]{4})[.-]{1}([0-9]{1,2})[.-]{1}([0-9]{1,2})$");
	  var strDataValue;
	  var infoValidation = true;
	  if ((strDataValue = re.exec(str)) != null) {
	    var i;
	    i = parseFloat(strDataValue[1]);
	    if (i <= 0 || i > 9999) { /*年*/
	      infoValidation = false;
	    }
	    i = parseFloat(strDataValue[2]);
	    if (i <= 0 || i > 12) { /*月*/
	      infoValidation = false;
	    }
	    i = parseFloat(strDataValue[3]);
	    if (i <= 0 || i > 31) { /*日*/
	      infoValidation = false;
	    }
	  } else {
	    infoValidation = false;
	  }
	  return infoValidation;
	}

function toMore(){
	window.open($("#path").val()+"/html/CM/travel_desc.jsp");
}

// 檢核
function inputCheck(countppl, id, name, birthday) {

	var thisVal = "", $this = null, hasError = false;

	if (2 == countppl)
		return hasError;

	if (!hasError) {
		hasError = requiredInput();
	}

	if (!hasError) {
		hasError = checkId();
	}

	if (!hasError) {
		if (!isDate(birthday)) {
			hasError = true;
			alertMsg("被保人出生年月日非正確的日期");
		}
	}

	if (!hasError) {
		hasError = isUnder15YearOld(birthday.replace(/\-/g, '\/'));
	}

	if (!hasError) {
		hasError = isSameLoginPerson(id, name, birthday);
	}

	if (!hasError) {
		hasError = inputRulesCheck();
	}

	return hasError;
};

// 當被保人身分證字號/居留證號碼與登入ID相同時，驗證被保人姓名與出生年月日
function isSameLoginPerson(id, name, birthday) {

	if (id == _tempId && name != _tempName) {
		alertMsg("被保人姓名不符合");
		return true;
	}

	var temp = _tempBirthday.replace(/\//g, '-').split("-");
	temp = (parseInt(temp[0], 10) + 1911) + "-" + temp[1] + "-" + temp[2];

	if (id == _tempId && birthday != temp) {
		alertMsg("被保人出生年月日不符合");
		return true;
	}

	return false;
}

// 身份證檢核
function checkId() {
	checkInsureWay();
	var id = $("#insured_u_id").val();
	var error = false;
	if (!validatePersonalID(id) && !validateLiveID(id)) {
		$("#insured_u_id").focus();
		error = true;
		alertMsg("請輸入正確\n被保人身分證字號/居留證號碼");
	}
	return error;
}

// 年紀15歲檢核
function isUnder15YearOld(birthday) {
	var error = false;
	var today = new Date();
	var birthDate = new Date(birthday);
	var age = today.getFullYear() - birthDate.getFullYear();
	var m = today.getMonth() - birthDate.getMonth();
	if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
		age--;
	}
	if (15 > age) {
		error = true;
		alertMsg(underAge15Msg);
	}

	return error;
}

function getBirthday() {
	var year = $('#insured_yy').val();
	var month = $('#insured_mm').val();
	month = parseInt(month, 10) < 10 ? '0' + month : month;
	var date = $('#insured_dd').val();
	date = parseInt(date, 10) < 10 ? '0' + date : date;
	return (parseInt(year, 10) + 1911) + "-" + month + "-" + date;
}

function doNext(from, key) {

	var id = $("#insured_u_id").val();
	var name = $("#insured_name").val();
	var countppl = $("#numberOfPeople").val();
	var birthday = getBirthday();

	var hasInputError = inputCheck(countppl, id, name, birthday);
	if (hasInputError)
		return;

	if (_hasLogin) {
		
		var startDate = $("#datepicker").val().replace(/\//g, "-") + " " + $("#tra_hour").val() + ":" + $("#tra_min").val() + ":00";
		var insured_u_id = $("#insured_u_id").val();
		var endDate = $("#end_date").val();
		
		var url = "../EBPT_1000_T0/existBobeAmount?startDate=" + startDate + "&insured_u_id=" + insured_u_id + "&endDate="+endDate;
		
		$.ajax({
			url : url, type : 'POST', success : function(data) {
				
				if("T" == data.NOTEXIST){
					doShowProductList(from, key);
				}else if("F" == data.NOTEXIST){
					alert("親愛的顧客您好：\n被保人"+insured_u_id+ "已在本網站投保旅遊險，訂單無法重複！\n\n本交易無法完成。如有任何問題，請洽保戶服務中心 0800-212-880，謝謝您的惠顧！\n服務時間：每日上午 8:30 至晚上 9:00 \n");
				}else{
					alert("系統異常，請稍候再試。");
				}
			}
		});
	}
};

function getDate(pageT0SessionBirthday) {
	var temp = pageT0SessionBirthday.replace(/\//g, '-').split("-");
	return (parseInt(temp[0], 10) + 1911) + "-" + temp[1] + "-" + temp[2];
}

function isInteger(num) {
	return (num ^ 0) === num;
}

function doShowProductList(from, key) {
	var country = $("#travel_country").val().toString();
	var inputs = {
		"loginId" : _tempId,
		"endDate" : $("#end_date").val(),
		"startDate" : $("#datepicker").val().replace(/\//g, "-") + " " + $("#tra_hour").val() + ":" + $("#tra_min").val() + ":00",
		"numberOfPeople" : $("#numberOfPeople").val(),
		"country" : country,
		"isSchengen" : getIsSchengen(country),
		"travelType" : getTavelType(country),
		"isUsCa" : isUsCa(country),
		"rate" : getRate(country),
		"productPolicyCode" : getProductPolicyCode(country),
		"insured_u_id" : $("#insured_u_id").val(),
		"id" : ("" == $("#pageT0SessionId").val()) ? $("#insured_u_id").val() : $("#pageT0SessionId").val(),
		"name" : encodeURI($("#insured_name").val()),
		"datePicker" : $("#datepicker").val(),
		"travelDay" : $("#travel_days").val(),
		"birthday" : isInteger(parseInt($("#pageT0SessionBirthday").val().split("/")[0])) ? getDate($("#pageT0SessionBirthday").val()) : getBirthday(),
		"key" : key,
		"from" : from
	};
	var isSchengen = $("#isSchengen").val();
	var numberOfPeople = $("#numberOfPeople").val();
	var startDate = $("#datepicker").val();
	var insured_u_id = $("#insured_u_id").val();
	var url = "../EBPT_1000_T0/getInsureWay?isSchengen=" + isSchengen + "&numberOfPeople=" + numberOfPeople + "&startDate=" + startDate + "&insured_u_id=" + insured_u_id;
	$.ajax({
		url : url, type : 'POST', success : function(data) {
			$("#insureWay").val(data['insureWay']);
			inputs.insureWay = data['insureWay'];
			doCalAmountByIsCusEffect(inputs);
		}
	});
}

function  doCalAmountByIsCusEffect(inputs){
	$.ajax({
		url : "../EBPT_1000_T0/calAmountByIsCusEffect",
		type : 'POST',
		data : inputs,
		success : function(data) {
			inputs.maxValue = data.maxValue;
			inputs.isCusEffect = data.isCusEffect;
		},
		complete : function() {
			if (1 == inputs.numberOfPeople) {
				blockListCheck(inputs);
			} else {
				showProduct(inputs);
			}
		}
	});
	
}

function blockListCheck(inputs) {
	$.ajax({
		url : "../EBPT_1300_T3/blockCheck",
		type : 'POST',
		data : inputs,
		contentType : "application/x-www-form-urlencoded; charset=UTF-8",
		success : function(data) {
			var rejectCode = data.reject;
			("97" == rejectCode) && $("#is97BlockCode").val(rejectCode);
			if ("97" == rejectCode || "" == rejectCode) {
				showProduct(inputs);
			} else {
				alertMsg(blockIdMsg);
				return;
			}
		}
	});
}

function memberDoLogin(key) {
	var originalName = $("#insured_name").val();
	$("#insured_name").val(encodeURIComponent(originalName));
	$("#insured_name").val(originalName);
	var url = "getTimeStamp_session?" + $("#form1").serialize();
	$.get(url, function(data, status) {
		location.href = "../EBPT_1000_T0/T0?key=" + data.key.trim() + "&action=resum";
	});
}

function capitalize(textboxid, str) {
	if (str && str.length >= 1) {
		var firstChar = str.charAt(0);
		var remainingStr = str.slice(1);
		str = firstChar.toUpperCase() + remainingStr;
	}
	document.getElementById(textboxid).value = str;
}

function getIsSchengen(country) {
	if (0 != country.length) {
		var countryArray = country.split(",");
		var isSchengen = "false";
		for (var i = 0; i <= countryArray.length - 1; i++) {
			isSchengen = ("A" == (countryArray[i].split("_")[1])) ? "true" : isSchengen;
		}
		return isSchengen;
	}
}

function getTavelType(countryStr) {
	var travelType = "All";
	return ((countryStr) && 0 <= countryStr.indexOf("TW")) ? "I" : "A";
}

function getProductPolicyCode(countryStr) {
	var travelType = getTavelType(countryStr);
	return ("A" == travelType) ? "PC01002" : "PC01001";
}

function getRate(countryStr) {
	var countryArray = countryStr.split(",");
	var arrayFlag = null;
	var rate = "0";

	for (var i = 0; i <= countryArray.length - 1; i++) {
		arrayFlag = countryArray[i].split("_");
		rate = (parseInt(rate) <= parseInt(arrayFlag[2], 10)) ? arrayFlag[2] : rate;
	}
	return rate;
}

function isUsCa(countryStr) {
	if (0 <= countryStr.indexOf("US")) {
		return true;
	}
	if (0 <= countryStr.indexOf("CA")) {
		return true;
	}
	return false;
}

function showProductSuccessCallBack(data) {

	var resultJson = $.parseJSON(JSON.stringify(data));

	if ("" == data.getProductError) {

		$("#combo").val(resultJson.combo);

		var customTagHTML = "", frequentlyTagHTML = "", productId = "";

		var list = data.productObjs;

		for (var i = 0; i <= list.length - 1; i++) {

			customTagHTML = (list[i].custom) ? "<span class='label'>自選</span>" : "";
			// 自選不顯示
			if ("" == customTagHTML) {
				var isFrequentlyProductList = $("#isFrequentlyProductList").val();
				frequentlyTagHTML = (0 <= isFrequentlyProductList.indexOf(list[i].productId)) ? "<span class='label'>常用</span>" : "";

				var tagsObj = list[i].tags;
				var size = tagsObj.length;
				var tagTitleHTML = "";
				for (var j = 0; j <= size - 1; j++) {
					tagTitleHTML = tagTitleHTML + "<li><div class='btn btn-sm productTitle'>" + list[i].tags[j].title + "</div></li>";
				}

				var obj = "<div id='product_" + i + "' class='col-md-3 col-sm-6' style='display:block' productId='" + list[i].productId + "'iscustom='" + list[i].custom + "'> <div class='image-tile outer-title'> <img title='' alt='' src='" + $("#path").val() + "/" + list[i].thumbnail + "'>";
				obj = obj + customTagHTML + frequentlyTagHTML + "<div class='widget mb8'> <ul class='tags'> " + tagTitleHTML + " </ul> </div> <h5 class='uppercase mb0'>" + list[i].title + "</h5> <a class='title' href='#'> <span>" + list[i].summary + "</span> </a> </div> </div>";

				$(obj).on("click", function() {
					goSubmit($(this).attr("iscustom"), $(this).attr("productId"), $("#key").val());
				}).appendTo("#caseRecommendDiv");
			}
		}
		window.scrollTo(0, 0);

	} else {
		alertMsg(resultJson.getProductError);
	}
	$.unblockUI();
}

function showProductCompleteCallBack(inputs) {

	$("#loginId").val(inputs.loginId);
	$("#endDate").val(inputs.endDate);
	$("#startDate").val(inputs.startDate);
	$("#numberOfPeople").val(inputs.numberOfPeople);
	$("#country").val(inputs.country);
	$("#isSchengen").val(inputs.isSchengen);
	$("#travelType").val(inputs.travelType);
	$("#isUsCa").val(inputs.isUsCa);
	$("#rate").val(inputs.rate);
	$("#productPolicyCode").val(inputs.productPolicyCode);
	$("#insured_u_id").val(inputs.insured_u_id);
	$("#id").val(inputs.id);
	$("#insureWay").val(inputs.insureWay);
	$("#name").val(inputs.name);
	$("#datePicker").val(inputs.datePicker);
	$("#travelDay").val(inputs.travelDay);
	$("#birthday").val(inputs.birthday);
	$("#key").val(inputs.key);
	$("#from").val(inputs.from);

	var originalName = $("#insured_name").val();
	$("#insured_name").val(encodeURIComponent(originalName));
	var data = $("#form1").serialize();
	$("#insured_name").val(originalName);

	$.get("getTimeStamp_session?" + data + "&key=" + inputs.key, function(data, status) {
		$("#part_0").attr("style", "display:none");
		$("#part_1").attr("style", "display:none");
		$("#part_2").attr("style", "display:block");
	});
};

function showProduct(inputs) {

	$.ajax({
		url : "../EBPT_1000_T0/getProductList",
		type : 'POST',
		data : inputs,
		beforeSend : function() {
			$("div[id^='product_']").remove();
			$.blockUI();
		},
		success : showProductSuccessCallBack,
		complete : showProductCompleteCallBack(inputs)
	});
}

// 規則邏輯
function inputRulesCheck() {

	var now = new Date();
	var pickDate = new Date($("#start_date").val().replace(/-/g, "/"));
	var timeTavelFromNow = Math.round((pickDate - now) / 1000 / 60);
	var nDays = $("#nDays").val();
	var travelDays = $("#travel_days").val();

	var time90mins = 60 * 1.5;
	var time1day = 60 * 24;
	var timeNDays = 60 * 24 * nDays;

	var inputError1 = "出發時間距離現在時間須至少 1.5 小時，請重新選擇。"
	var inputError2 = "國內旅遊不得大於 60 天!";
	var inputError3 = "一天內出發之行程,國外旅遊期間不得少於於 2 天!";
	var inputError4 = "當被保險人非會員本人、或前往地區為歐洲申根國家，則旅遊出發時間距離現在須至少" + nDays + " 天 ！ ";
	var inputError5 = "本公司暫不提供旅遊地區為德國且保險期間超過90天之保障，造成不便敬請見諒。";
	
	//確定是否只有保德國
	var tmpcountry= $("#fullCountryInfo").val().toString()
	var countryArray = tmpcountry.split(",");
	var isDE= "false";
	if (countryArray.length==1 ){
		if (0 <= tmpcountry.indexOf("DE")){
		isDE= "true";}
	}
	var msg = "";
	var flag = $("#insureWay").val().toLowerCase() + "/" + getProductPolicyCode($("#travel_country").val().toString());
	switch (flag) {
	case "web/PC01001":
		if (timeTavelFromNow < time90mins) {
			msg = inputError1;
		} else if (travelDays > 60) {
			msg = inputError2;
		}
		break;
	case "web/PC01002":
		if (timeTavelFromNow < time90mins) {
			msg = inputError1;
		} else if (timeTavelFromNow < time1day) {
			if (travelDays < 2) {
				msg = inputError3;
			}
		}
		break;
	case "fax/PC01001":
		if (timeTavelFromNow < timeNDays) {
			msg = inputError4;
		} else if (travelDays > 60) {
			msg = inputError2;
		}
		break;
	case "fax/PC01002":
		if (timeTavelFromNow < timeNDays) {
			msg = inputError4;
		} else if (travelDays > 90){
			if(isDE== "true"){
				msg = inputError5;
			}
		}
		break;
	}

	if ("" != msg) {
		alertMsg(msg);
		return true;
	}
	return false;
}
