/**
 * 共用版型
 * include from footer.jsp
 * 共用入口版型都會用到的設定
 * 
 * 1.設定上方主要選單 (HTML 在header.jsp)
 * 
 */
var LayoutTool = LayoutTool || {
	/**主功能連結前置路徑**/
	path 			: "",
	/**AJAX連結前置路徑**/
	ecDispatcher 	: "",
	/**
	 * "M";//行動載具
	 * "P";//預設為電腦
	 */
	viewDevice 		: "P",
	projectId		: "",
	regFrom			: "",
	mrMemberID		: "",
	/**版型初始化**/
	init : function(ecDispatcher,path,viewDevice,projectId,regFrom,mrMemberID){
		this.ecDispatcher 	= ecDispatcher;
		this.path 			= path;
		this.viewDevice		= viewDevice;
		this.projectId		= projectId;
		this.regFrom		= regFrom;
		this.mrMemberID		= mrMemberID;
		this.doQueryMenu();
	},
	/**取得選單內容**/
	doQueryMenu : function(){
		var params = 'projectId=' + this.projectId;
		params = params + '&regFrom=' + this.regFrom;
		params = params + '&mrMemberID=' + this.mrMemberID;
		callAjax(LayoutTool.ecDispatcher + 'ECIE_1000/queryMenu',params,this.doQueryMenuCallBack,false);
	},
	/**
	 * 取得選單內容CallBack
	 * 解析選單資料組合成HTML並append至$('#mainMenuDiv')
	 */
	doQueryMenuCallBack : function(data){
		var template = '';
		var z = 0;
		var addMebmerButton = $('#addMemberButton').val();
		$.each(data.menuVo.menuPojoList, function( i, item){
			if(item.pageAddress){
				if(addMebmerButton == '0'){
					template = template + '<div id="joinMemberLi" class="module widget-handle left headerMainFunc" style="border:0px;cursor: pointer;">';
					template = template + '<span >';
					template = template + '<a href="'+LayoutTool.getHref(item.target,item.pageAddress)+'">'+item.pageName +'</a>	';			
					template = template + '</span>';
					template = template + '</div>';
				}
			}else{
				
				//新版menu
				template = template + '<li class="nav-item dropdown">';
				template = template + '<a href="#" class="nav-link" role="button" id="member-center-options" ';
				template = template + 'data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+item.pageName+'</a> ';
				
				if(item.menuPojoList && item.menuPojoList.length  > 0){
					template = template + '<div class="dropdown-menu bobe-dropdown-menu" aria-labelledby="member-center-options">';
					template = template + '<div class="bobe-sub-menu hidden d-lg-flex">';
					template = template + '<ul class="bobe-sub-link-group">';
					$.each(item.menuPojoList, function( i, item){
		 				if(item.programsub == 'ECM1_1000'){																	//傷害險要送出表單至導覽頁
		 					var programsub = "'"+item.programsub+"'";
		 					template = template + '<li>';
		 					template = template + '<a class="dropdown-item" href="javascript: return false;"  onclick="doLink2Trx('+programsub+');">';
		 	 				template = template + '<i class="far fa-user"></i>&nbsp;'+item.pageName;
		 	 				template = template + '</a>';  
		 	 				template = template + '</li>';  
		 				}else if(item.programsub == 'ECH1_0200'){																	//傷害險要送出表單至導覽頁
		 					var programsub = "'"+item.programsub+"'";
		 					template = template + '<li>';
		 					template = template + '<a class="dropdown-item" href="javascript: return false;"  onclick="doLink2Trx('+programsub+');">';
		 	 				template = template + '&nbsp;'+item.pageName;
		 	 				template = template + '</a>';  
		 	 				template = template + '</li>';  
		 				}else if(item.pageTarget == 'MASK'){
		 					var contentName = "'MASK_"+z+"'"; 
		 					var contentName2 = 'MASK_'+z; 
		 					template = template + '<li>';
		 					template = template + '		<a class="dropdown-item" onclick="openPageContent('+ contentName +')"  >&nbsp;'+item.pageName+'</a>';
		 	 				template = template + '</li>'; 
		 	 				template = template + ' <div id ="alertMask"  style="display:none" > <span id="'+ contentName2 +'">	'+ item.pageContent +'	</span>	</div>';
		 	 				z++;
		 				}else{
		 					template = template + '<li>';
		 	 				template = template + '	 <a class="dropdown-item" href="'+LayoutTool.getHref(item.target,item.pageAddress)+'">&nbsp;'+item.pageName+'</a>';
		 	 				template = template + '</li>';  
		 				}	
		            })
		            template = template + '</ul>';
		            template = template + '</div>';
		            template = template + '</div>';	
				}
	            template = template + '</li>';	
			}
		});
		$('#mainMenuDiv').prepend(template);
		$('.widget-handlel-toggle').click(function() {

			$(this).parent().toggleClass('toggle-widget-handle');
		});
	},
	/**
	 * 以是否開新頁來判斷連結組合模式
	 * @param target
	 * @param pageAddress
	 * @returns {String}
	 * 
	 * HREF 若無做用需代空連結字串 javascript:void(0);
	 */
	getHref : function(target,pageAddress){
		var hrefPath = 'javascript:void(0);';
		if(pageAddress){
			hrefPath = target == '_self' ? LayoutTool.path + pageAddress : pageAddress;
		}
		return hrefPath;
	}
}