var countryArrayStr = $("#countryArrayStr").val();
var isLoadCountries = false;
var hasLogin = false;

var travel_type = "All";
var last_travel_type = "null";
var firstTime = true;
var country_max = 5;
var selectObj = {
	maximumSelectionLength : country_max,
}

initSelectCountry();

$(function() {

	$("#travel_country").select2({ maximumSelectionLength : 5 }).on("change", function(e) { countryCallBack($(this)); setCountryField(); calPremium(); });
	
	$("#hasHere").val("true");

	var todayAddTwoHour = $("#serverDateTimeAddTwoHour").val();

	var d = new Date(todayAddTwoHour);
	var year = d.getFullYear();
	var month = d.getMonth() + 1;
	var day = d.getDate();
	var hour = d.getHours();
	var min = d.getMinutes();

	$("#tra_hour").val(fix2(hour));
	if (min >= 30) {
		$("#tra_min").val("30");
	} else {
		$("#tra_min").val("00");
	}

	var output = year + '/' + month + '/' + day;
	$("#datepicker").datepicker("setDate", output);
	$("#datepicker").datepicker("option", "minDate", new Date(output));
	$("#datepicker").datepicker("option", "maxDate", "+2m");

	change_time("init");


});

function change_product_list() {
}

function getPremium() {
	
	var insureWay = $("#insureWay").val();
	
	var fullCountryInfo = $("#fullCountryInfo").val();
	var PRODUCT_POLICY_CODE = ( "" == fullCountryInfo || "TW_1_100" == fullCountryInfo) ? "PC01001" : "PC01002";
	$("#PRODUCT_POLICY_CODE").val(PRODUCT_POLICY_CODE);
	var PRODUCT_POLICY_CODE = $("#PRODUCT_POLICY_CODE").val();
	
	var travel_days = $("#travel_days").val();
	
	if (fullCountryInfo.indexOf("_A") >= 0) {
		$("#isSchengen").val("true");
	}else{
		$("#isSchengen").val("false");
	}
	
	if ($("#isSchengen").val() == "true") {
		// 申根國家
		$("#amount_21").val(5000000);
		$("#amount_22").val(1200000);
		$("#amount_23").val(5000000);
		$("#amount_61").val(1000000);
	} else if ($("#countryArrayStr").val().indexOf("CA") >= 0 || $("#countryArrayStr").val().indexOf("US") >= 0 || $("#countryArrayStr").val().indexOf("UM") >= 0) {
		// 美加地區(CA、US、UM)
		$("#amount_21").val(3000000);
		$("#amount_22").val(100000);
		$("#amount_23").val(3000000);
		$("#amount_61").val(100000);
	} else {
		// 非申根國家
		$("#amount_21").val(2000000);
		$("#amount_22").val(100000);
		$("#amount_23").val(2000000);
		$("#amount_61").val(100000);
	}
	
	var rate = 100;
	if(""!=fullCountryInfo){
		$.each(fullCountryInfo.split(","),function(index,value){
			var array = value.split("_");
			rate = (parseInt(array[3],10) > rate) ?parseInt(array[3],10) : rate;
		});
		$("#rate").val(rate);
	}
	
	var amount_21 = $("#amount_21").val();
	var amount_22 = $("#amount_22").val();
	var amount_23 = $("#amount_23").val();
	var amount_61 = $("#amount_61").val();
	getPremiumApi(insureWay, PRODUCT_POLICY_CODE, travel_days, amount_21, amount_22, amount_23, amount_61)
}
function backPremium_from_getPremiumApi(data) {
	var arr = data.split("__");
	var numberOfPeople = $('#numberOfPeople').val();
	var premium = arr[0] * numberOfPeople
	var showStr = arr[0] * $("#defaultPremium").html("NT$ " + premium + " 起")
	$("#combo").val(arr[1])
	var msg = checkTravelDate(arr[2]);
}
function alertErroMsg(msg) {
	if (msg != null && firstTime == false) {
		alertMsg(msg);
		return true;
	} else {
		return false;
	}
}

function goSubmit() {
	firstTime = false;
//	var msg = checkTravelDate(serverDateTime);
//	var hasSomeError = alertErroMsg(msg);
//	if (hasSomeError) {
//		return;
//	}
	var result = checkTravelData();
	if(!result){
		return;
	}
	var url = "../EBPT_1000_T0/getTimeStamp_session?" + $("#form1").serialize();
	$.get(url, function(data, status) {
		location.href = "../EBPT_1000_T0/T0?key=" + data.key.trim();
	});
}
function goTrx(trx) {
	location.href = "../" + trx + "/prompt";
}