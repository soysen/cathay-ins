var ecDispatcher = 'INSEBWeb/servlet/HttpDispatcher/';
var notShowCarModelWinOnce = 'N'; 

$(document).ready(function(){
	doClearWarnMsg();
	
	var insCustomerType = '';
	var insCustomerId = '';
	var insBirthday = '';
	var insCompanyName = '';
	var madeDateY = '';
	var madeDateM = '';
	var originIssueDate = '';
	var vehicleKindNo = '';
	var engineExhaust = '';
	var maximumLoad = '';
	var modelBrand = '';
	var modelType = '';
	var modelFullNo = '';
	
	if('Y' == ''){ 
		doSelCustomerType(insCustomerType,'N');
		if(insCustomerType == '1'){
			$('#customerType1').prop('checked',true);
			$('#customerType1').parent().addClass('checked');
			$('#customerType3').parent().removeClass('checked');
			$('#customerId').val(insCustomerId);
			$('#birthday').val(insBirthday);
		}else{
			$('#customerType3').prop('checked',true);
			$('#customerType3').parent().addClass('checked');
			$('#customerType1').parent().removeClass('checked');
			$('#legalCustomerId').val(insCustomerId);
			$('#legalCustomerName').val(insCompanyName);
			$('#customerId').val('');
			$('#birthday').val('');
		}
		
 		$('#madeDateY').val(madeDateY);
		$('#madeDateM').val(madeDateM);
		$('#originIssueDate').val(originIssueDate);
		$('#vehicleKindNo').val(vehicleKindNo);
		
		$.ajaxSettings.async = false;
			doChgCarModelData('modelBrand','N');
			$('#modelBrand').val(modelBrand);
			
			doChgCarModelData('modelType','N');
			$('#modelType').val(modelType);
			
			doChgCarModelData('modelFull','N');
			$('#modelFull').val(modelFullNo);
			//乘載限制資料
			if(!isEmpty(modelFullJson)){
				$.each(modelFullJson, function( i, item){
					if(modelFullNo == item.MODEL_FULL_NO){
						$('#maximumLoad').val(parseInt(item.MAXIMUM_LOAD,10));
						$('#modelFullView').html(item.MODEL_FULL_NAME);
					}
				});
			}
			$.ajaxSettings.async = true;
			
		$('#engineExhaust').val(parseInt(engineExhaust,10));
		
	}else if('Y' == ''){ 
		doSelCustomerType(insCustomerType,'N');
		if(insCustomerType == '1'){
			$('#customerType1').prop('checked',true);
			$('#customerType1').parent().addClass('checked');
			$('#customerType3').parent().removeClass('checked');
			$('#customerId').val(insCustomerId);
			$('#birthday').val(insBirthday);
		}else{
			$('#customerType3').prop('checked',true);
			$('#customerType3').parent().addClass('checked');
			$('#customerType1').parent().removeClass('checked');
			$('#legalCustomerId').val(insCustomerId);
			$('#legalCustomerName').val(insCompanyName);
			$('#customerId').val('');
			$('#birthday').val('');
		}
 		$('#madeDateY').val(madeDateY);
		$('#madeDateM').val(madeDateM);
		$('#originIssueDate').val(originIssueDate);
		$('#vehicleKindNo').val(vehicleKindNo);
		doChgCarModelData('modelBrand','N');
		
	}else{
		doSelCustomerType('1','N');
		$('#customerType1').prop('checked',true);
		$('#customerType1').parent().addClass('checked');
		$('#customerType3').parent().removeClass('checked');
	}

	$('#originIssueDate').datepicker({
		onClose: function(date){
			doCheckOriginIssueDate();
		}
	});
	$('#birthday').datepicker({
		onClose: function(date){
			doCheckBirthday();
		},
		dateFormat:"yyy/mm/dd" 
	});
	
	var serviceMsg = '';
	if(!isEmpty(serviceMsg)){
		alertMsg(serviceMsg,showServiceAfter);
		return;
	}
	//使用民國年曆，使用的話，全頁面的日期都會變民國，無法單一指定
	useROCDate();
	freashDropDownList();
	
	$(".js-example-placeholder-single").select2({
		  allowClear: true
	});
	
});

/**
清除全部錯誤訊息
*/
function doClearWarnMsg(){
	clearWarnMsg('customerId_msg');
	clearWarnMsg('birthday_msg');
	clearWarnMsg('legalCustomerName_msg');
	clearWarnMsg('legalCustomerId_msg');
	clearWarnMsg('carNo_msg');
	clearWarnMsg('madeDate_msg');
	clearWarnMsg('originIssueDate_msg');
	clearWarnMsg('vehicleKindNo_msg');
	clearWarnMsg('carModel_msg');
	clearWarnMsg('engineExhaust_msg');
	clearWarnMsg('maximumLoad_msg');
}



/**
會員登入
*/
function doLoginThisPage(){
	commLoginSuccessContinue = '';
	doOpenCommLogin();
}

/**
加入會員/註冊會員
*/
function doJoinMemberThisPage(){
	var status = getLoginStatus();
	if(status == 'N'){
		doSubmitFormEC($('#form1'),ecDispatcher + 'ECM1_0400/prompt'); 
	}else{
		alertMsg('您已經為會員!');
	}
}

/**
身分別選擇
*/
function doSelCustomerType(val,isReloadVehicleKind){
	if('1' == val){ 
		$('#customerDiv').show();
		$('#legalCustomerDiv').hide();
	}
	if('3' == val){ 
		$('#customerDiv').hide();
		$('#legalCustomerDiv').show();
	}
	if('Y' == isReloadVehicleKind){
		doQueryVehicleKindList(val);
	}
}

//columns validation **********************************************************************************************

function doCheckCustomerId(){
	var customerId = $('#customerId').val();
	if(isEmpty(customerId)){
		setWarnMsg('customerId_msg','請輸入資料!');
		return false;
	}
	
	if(!validatePersonalID(customerId) && !validateLiveID(customerId)){
		setWarnMsg('customerId_msg','您的帳號有誤!');
		return false;
	}
	return true;
}


function doCheckBirthday(){
	clearWarnMsg('birthday_msg');
	var birthday = $('#birthday').val();
	if(isEmpty(birthday)){
		setWarnMsg('birthday_msg','請輸入資料!');
		return false;
	}
	if(!isDateROC(birthday)){
		setWarnMsg('birthday_msg','格式有誤請重新輸入!');
		return false;
	}
	
	var birthday = birthday.replace("/","").replace("/","");
	var currDatePre18Year = addDateMonth(-216,getCurrDate()); //18歲
	if((parseInt(birthday,10)+19110000) >= parseInt(currDatePre18Year,10)){
		setWarnMsg('birthday_msg','格式有誤請重新輸入!');
		return false;
	}
	return true;
}


function doCheckLegalCustomerId(){
	clearWarnMsg('legalCustomerId_msg');
	var legalCustomerId = $('#legalCustomerId').val();
	if(isEmpty(legalCustomerId)){
		return true;
	}
	
	if(!validateVATNO(legalCustomerId)){
		setWarnMsg('legalCustomerId_msg','您的帳號有誤!');
		return false;
	}
	return true;
}
	

function doCheckLegalCustomerName(){
	clearWarnMsg('legalCustomerName_msg');
	var legalCustomerName = $('#legalCustomerName').val();
	if(isEmpty(legalCustomerName)){
		setWarnMsg('legalCustomerName_msg','請輸入資料!');
		return false;
	}
	return true;
}


function doCheckCarNo(){
	clearWarnMsg('carNo_msg');
	var carNo1 = $('#carNo1').val();
	var carNo2 = $('#carNo2').val();
	if(isEmpty(carNo1) || isEmpty(carNo2)){
		setWarnMsg('carNo_msg','請輸入資料!');
		return false;
	}
	if(!chkEngNum(carNo1) || !chkEngNum(carNo2)){
		setWarnMsg('carNo_msg','格式有誤請重新輸入!');
		return false;
	}
	return true;
}


function doCheckEngineExhaust(){
	clearWarnMsg('engineExhaust_msg');
	var engineExhaust = $('#engineExhaust').val();
	if(isEmpty(engineExhaust)){
		setWarnMsg('engineExhaust_msg','請輸入資料!');
		return false;
	}
	if(!validateNumber(engineExhaust)){
		setWarnMsg('engineExhaust_msg','格式有誤請重新輸入!');
		return false;
	}
	if(parseInt(engineExhaust,10) < 1 || parseInt(engineExhaust,10) > 9999){
		setWarnMsg('engineExhaust_msg','格式有誤請重新輸入!');
		return false;
	}
	return true;
}


function doCheckMaximumLoad(){
	clearWarnMsg('maximumLoad_msg');
	var maximumLoad = $('#maximumLoad').val();
	if(isEmpty(maximumLoad)){
		setWarnMsg('maximumLoad_msg','請輸入資料!');
		return false;
	}
	if(!validateNumber(maximumLoad)){
		setWarnMsg('maximumLoad_msg','格式有誤請重新輸入!');
		return false;
	}
	return true;
}


function doCheckOriginIssueDate(){
	clearWarnMsg('originIssueDate_msg');
	var originIssueDate = $('#originIssueDate').val();
	if(isEmpty(originIssueDate)){
		setWarnMsg('originIssueDate_msg','請輸入資料!');
		return false;
	}
	if(!isDateROC(originIssueDate)){
		setWarnMsg('originIssueDate_msg','格式有誤請重新輸入!');
		return false;
	}
	
	var originIssueDate = originIssueDate.replace("/","").replace("/","");
	var currDatePre40Year = addDateMonth(-480,getCurrDate());
	if((parseInt(originIssueDate,10)+19110000) < parseInt(currDatePre40Year,10)){
		setWarnMsg('originIssueDate_msg','格式有誤請重新輸入(僅能為近40年日期)!');
		return false;
	}
	if((parseInt(originIssueDate,10)+19110000) > parseInt(getCurrDate(),10)){
		setWarnMsg('originIssueDate_msg','格式有誤請重新輸入!');
		return false;
	}
	return true;
}


function doCheckMadeDate(){
	clearWarnMsg('madeDate_msg');
	var madeDateY = $('#madeDateY').val();
	var madeDateM = $('#madeDateM').val();
	if(isEmpty(madeDateY) || isEmpty(madeDateM)){
		setWarnMsg('madeDate_msg','請選擇出廠年月!');
		return false;
	}
	if(parseInt(madeDateY+''+madeDateM,10) > parseInt(getCurrDate().substring(0,6),10)){
		setWarnMsg('madeDate_msg','格式有誤請重新輸入!');
		return false;
	}
	return true;
}


function doCheckVehicleKindNo(){
	clearWarnMsg('vehicleKindNo_msg');
	var vehicleKindNo = $('#vehicleKindNo').val();
	if(isEmpty(vehicleKindNo)){
		setWarnMsg('vehicleKindNo_msg','請選擇車輛種類!');
		return false;
	}
	return true;
}


function doCheckCarModel(isSubmit){
	clearWarnMsg('carModel_msg');
	var modelBrand = $('#modelBrand').val();
	var modelType = $('#modelType').val();
	var modelFull = $('#modelFull').val();
	if(isEmpty(modelBrand)){
		setWarnMsg('carModel_msg','請選擇廠牌名稱!');
		return false;
	}
	if(isEmpty(modelType)){
		setWarnMsg('carModel_msg','請選擇車系!');
		return false;
	}
	if(isEmpty(modelFull)){
		setWarnMsg('carModel_msg','請選擇車款!');
		return false;
	}
	
	//取得資料
	if(!isEmpty(modelFullJson)){
		$.each(modelFullJson, function( i, item){
			if(modelFull == item.MODEL_FULL_NO){
				if("N" == isSubmit){
					$('#engineExhaust').val(parseInt(item.ENGINE_EXHAUST,10));
				}
				$('#maximumLoad').val(parseInt(item.MAXIMUM_LOAD,10));
				clearWarnMsg('engineExhaust_msg');
				clearWarnMsg('maximumLoad_msg');
			}
		});
	}
	return true;
}

//columns validation END **********************************************************************************************

function doChgCarModelData(kind,openModelFullWin){
	
	clearWarnMsg('carModel_msg');
	$('#engineExhaust').val('');
	$('#maximumLoad').val('');
	$('#modelFull').val('');
	$('#modelFullView').html('');
	
		var vehicleKindNo = $("#vehicleKindNo").val(); //車輛種類
		var madeDateY = $("#madeDateY").val(); //出廠年
		var madeDateM = $("#madeDateM").val(); //出廠月
		var modelBrand = $("#modelBrand").val(); //廠牌
		var modelType = $("#modelType").val(); //車系
		
		$('#carModelList').empty();	
		
		if(isEmpty(madeDateY) || isEmpty(madeDateM) ){
			setWarnMsg('carModel_msg',"請先選擇車輛出廠年月!");
			return;
		}
		
		if(isEmpty(vehicleKindNo)){
			setWarnMsg('carModel_msg',"請先選擇車輛種類!");
			return;
		}
		
	if('modelType' == kind){
		if(isEmpty(modelBrand)){
			setWarnMsg('carModel_msg',"請先選擇車輛廠牌!");
 			return;
 		}
	}
	
	if('modelFull' == kind){
		if(isEmpty(modelType)){
			setWarnMsg('carModel_msg',"請先選擇車系!");
 			return;
 		}
		
	}
	
	if('modelBrand' == kind){
		$('#modelBrand').empty();
		$('#modelBrand').append('<option value="">請選擇</option>');
	}
	
	if('modelBrand' == kind || 'modelType' == kind){
		$('#modelType').empty();
		$('#modelType').append('<option value="">請選擇車系</option>');
	}
	
	
	//$('#modelFull').empty();
	//$('#modelFull').append('<option value="">請選擇車款</option>');
	
	
	var params = 'carDataKind=' + kind + '&vehicleKindNo=' + vehicleKindNo + '&madeDate=' + madeDateY + '-' + madeDateM + '-01' + '&modelBrand=' + modelBrand + '&modelType=' + modelType + '&openModelFullWin=' + openModelFullWin;
	callAjax(ecDispatcher + 'ECV1_0200/queryCarModelData',params,queryCarModelDataCallBack,false);
	
}

/**
汽車資料下拉選單
*/
var modelFullJson = '';
queryCarModelDataCallBack = function(data){
		if(!isEmpty(data.error)){
			doCheckLoginStatus();//檢核是否登入
			alertMsg(data.error);
			return;
		}
		
		if(!isEmpty(data.rtnData)){

			$.each(data.rtnData, function( i, item){
				var code = '';
				var text = '';
				if(data.carDataKind == 'modelBrand'){ //車廠品牌
					code = item.MODEL_BRAND; //廠牌代號
					text = item.MODEL_BRAND_NAME;
					$('#' + data.carDataKind).append("<option value='" + code + "'>" + text + "</option>");
				}else if(data.carDataKind == 'modelType'){ //車系
					code = item.MODEL_TYPE;
					text = item.MODEL_TYPE_NAME;
					$('#' + data.carDataKind).append("<option value='" + code + "'>" + text + "</option>");
				}
				else if(data.carDataKind == 'modelFull'){ //車款
					var modelFullStr = '';
					modelFullStr += '<a href="#" class="list-group-item col-xs-12 modelfullborder" onclick="doSelectCarModel(\'' + item.MODEL_FULL_NO + '\');return false;">';
					modelFullStr += '<span class="col-xs-2 col-sm-1">';
					modelFullStr += '</span>';
					modelFullStr += '<span class="col-xs-10 col-sm-5">' + item.MODEL_FULL_NAME + '</span>';
					modelFullStr += '<hr class="visible-xs hr-orange" />';
					modelFullStr += '<span class="col-xs-6 col-sm-3 text-right">' + parseInt(item.ENGINE_EXHAUST,10) + 'C.C</span>';
					modelFullStr += '<span class="col-xs-6 col-sm-3 text-right">' + (parseInt(item.CAR_PRICE)/10000) + '萬</span>';
					modelFullStr += '</a>';
				$('#carModelList').append(modelFullStr);
				}
			});
			
			if(data.carDataKind == 'modelFull'){
				modelFullJson = data.rtnData; // Keep ModelFull Data
			}else{
				modelFullJson = '';
			}
			freashDropDownList();
			$('#' + data.carDataKind).change();
			if(notShowCarModelWinOnce == 'Y'){ //強制不開啟車款選單
				notShowCarModelWinOnce = 'N';
			}else{
					if(data.openModelFullWin == 'Y'){ //是否自動開啟車款選單
						openCarModel();
					}
			}

		}
	}


function goCarOptionCaseTrx(){
	doSubmitFormEC($('#form1'),ecDispatcher + 'ECV1_0202/prompt');
}


function goCarNormalCaseTrx(planId){
	doSubmitFormEC($('#form1'),ecDispatcher + 'ECV1_0201/prompt?planId=' + planId);
}


function doSend(){
	
	doClearWarnMsg();
	
	var customerType = $('input[name=customerType]:checked').val();
	
	var errCnt = 0;
	if(customerType == '1') {
		if(!doCheckCustomerId()){ errCnt++; }
		if(!doCheckBirthday()){ errCnt++; }
	}
	if(customerType == '3') {
		if(!doCheckLegalCustomerId()){ errCnt++; }
		if(!doCheckLegalCustomerName()){ errCnt++; }
	}
	if(!doCheckCarNo()){ errCnt++; }
	if(!doCheckCarModel('Y')){ errCnt++; }
	if(!doCheckEngineExhaust()){ errCnt++; }
	if(!doCheckMaximumLoad()){ errCnt++; }
	if(!doCheckOriginIssueDate()){ errCnt++; }
	if(!doCheckMadeDate()){ errCnt++; }
	if(!doCheckVehicleKindNo()){ errCnt++; }
	
		if(errCnt > 0){
			alertMsg("檢核有誤請重新確認!",focusToWarnCallBack);
			return;
		}
	
		var customerId = $('#customerId').val();
	var birthday = $('#birthday').val();
	var legalCustomerId = $('#legalCustomerId').val();
	var legalCustomerName = $('#legalCustomerName').val();
	var carNo = $('#carNo1').val() + "-" + $('#carNo2').val();
	var vehicleKindNo = $('#vehicleKindNo').val();
	var madeDate = $('#madeDateY').val() + "-" + $('#madeDateM').val() + "-01";
	var originIssueDate = $('#originIssueDate').val();
	var modelBrand = $("#modelBrand").val(); 
	var modelBrandName = $("#modelBrand").find(":selected").text();
	var modelType = $('#modelType').val();
	var modelTypeName = $("#modelType").find(":selected").text();
	var modelFull = $('#modelFull').val();
	
	var engineExhaust = $('#engineExhaust').val();
	var maximumLoad = $('#maximumLoad').val();
	var depreciaRate = $('#depreciaRate').val();
	var engineNo = $('#engineNo').val();
	var trxUniKey = $('#trxUniKey').val();
	
	var originCode = ''; //產地別
	var carPrice = ''; //價格
	var loadUnit = ''; //乘載單位
	var engineExhaustUnit = '';//CC或馬力
	var modelFullName = ''; //車款全名
	if(!isEmpty(modelFullJson)){
		$.each(modelFullJson, function( i, item){
			if(modelFull == item.MODEL_FULL_NO){
				originCode = item.ORIGIN_CODE;
				carPrice = item.CAR_PRICE;
				loadUnit = item.MAXIMUM_LOAD_UNIT;
				engineExhaustUnit = item.ENGINE_EXHAUST_UNIT;
				modelFullName = item.MODEL_FULL_NAME;
			}
		});
	}
	
	var madeDateRoc = (parseInt($('#madeDateY').val(),10)-1911) + $('#madeDateM').val() + "01";
	if(parseInt(madeDateRoc,10) > parseInt(replaceAll(originIssueDate,'/',''),10)){
		setWarnMsg('originIssueDate_msg',"行照原發照日需大於車輛出廠年月!");
		alertMsg("行照原發照日需大於車輛出廠年月!");
		return;
	}
	
		var params = 'customerType=' + customerType;
		params += '&customerId=' + customerId;
		params += '&birthday=' + birthday;
		params += '&legalCustomerId=' + legalCustomerId;
		params += '&legalCustomerName=' + legalCustomerName;
		params += '&carNo=' + carNo;
		params += '&vehicleKindNo=' + vehicleKindNo;
		params += '&madeDate=' + madeDate;
		params += '&originIssueDate=' + originIssueDate;
		params += '&modelBrand=' + modelBrand;
		params += '&modelBrandName=' + modelBrandName;
		params += '&modelType=' + modelType;
		params += '&modelTypeName=' + modelTypeName;
		params += '&modelFull=' + modelFull;
		params += '&modelFullName=' + modelFullName;
		params += '&engineExhaust=' + engineExhaust;
		params += '&engineExhaustUnit=' + engineExhaustUnit;
		params += '&maximumLoad=' + maximumLoad;
		params += '&loadUnit=' + loadUnit;
		params += '&originCode=' + originCode;
		params += '&carPrice=' + carPrice;
		params += '&depreciaRate=' + depreciaRate;
		params += '&engineNo=' + engineNo;
		params += '&trxUniKey=' + trxUniKey;
		
	callAjax(ecDispatcher + 'ECV1_0200/sendData',params,sendDataCallBack,false);
}

sendDataCallBack = function(data){
		if(!isEmpty(data.error)){
			if(data.error == 'loginRequired'){
			commLoginSuccessContinue = 'send';
			doOpenCommLogin();
		}else{
			alertMsg(data.error);	
		}
			return;
		}
		
		if(data.isRenewal == 'Y'){ 
			doSubmitFormEC($('#form1'),ecDispatcher + 'ECV1_0202/prompt'); 
			return;
		}
		
		var haveCaseFlag = false;
		$('#caseRecommendDiv').empty();
		if(!isEmpty(data.carOptionCaseMap)){
			haveCaseFlag = true;
			genCarOptionCaseBlock(data.carOptionCaseMap);	
		}
		if(!isEmpty(data.carNormalCaseList)){
			haveCaseFlag = true;
			genCarNormalCaseBlock(data.carNormalCaseList);	
		}
		
		if(haveCaseFlag){
			$('#conditionDiv').hide();
			$('#programListDiv').show(300,function() {
				setTimeout(function(){
						$('html,body').animate({scrollTop:$('#programListDiv').offset().top},500);
					},300);
			  });
			
		}else{
			$('#programListDiv').hide();
			$('#conditionDiv').show();
		}
		$.unblockUI();
	}

function genCarOptionCaseBlock(data){
	var carOptionCaseTemplate = 
		'<div class="col-xs-12 col-md-3 col-sm-6">' +
		'    <div class="image-tile outer-title">' +
		'	   <img title="program@SEQ@" onclick="goCarOptionCaseTrx();return false;" style="cursor:pointer;" alt="program@SEQ@" src="https://w3.bobe.com.tw/INSEBWeb/html/CM/img/@THUMBNAIL@" >' +
		'	   <span class="label"></span>' +
		'	   <div class="widget mb8">' +
		'		   <ul class="tags">' +
		'			   @DETAIL@' +
		'		   </ul>' +
		'	   </div>' +
		'	   <h5 class="uppercase mb0" onclick="goCarOptionCaseTrx();return false;" style="cursor:pointer;">@CASE_TITLE@</h5>' +
		'	   <a class="title" href="#" onclick="goCarOptionCaseTrx();return false;">' +
		'		   <span>@SUMMARY@</span>' +
		'	   </a>' +
		'    </div>' +
		'</div>';
	carOptionCaseTemplate = carOptionCaseTemplate.replace(eval('/@SEQ@/g'),1);
	carOptionCaseTemplate = carOptionCaseTemplate.replace(eval('/@THUMBNAIL@/g'),data.THUMBNAIL);
	carOptionCaseTemplate = carOptionCaseTemplate.replace(eval('/@CASE_TITLE@/g'),data.TITLE);
	carOptionCaseTemplate = carOptionCaseTemplate.replace(eval('/@SUMMARY@/g'),data.SUMMARY);
	var deatilTemplate = '<li><div class="btn btn-sm">@TAG_TITLE@</div></li>';
	var detail = '';
	if(!isEmpty(data.TAG_TITLE_LIST) && !isEmpty(data.TAG_ID)){
		$.each(data.TAG_TITLE_LIST, function( i, item){
			var deatilTemplateNew = deatilTemplate.replace(eval('/@TAG_TITLE@/g'),item.TAG_TITLE);
			detail = detail + deatilTemplateNew;
		});
	}
	carOptionCaseTemplate = carOptionCaseTemplate.replace(eval('/@DETAIL@/g'),detail);	
	$('#caseRecommendDiv').append(carOptionCaseTemplate);
}

function genCarNormalCaseBlock(data){
	
	$.each(data, function( i, item){
		
		var carNormalCaseTemplate = 
			'<div class="col-xs-12 col-md-3 col-sm-6">' +
			'    <div class="image-tile outer-title">' +
			'	   <img title="program@SEQ@" onclick="goCarNormalCaseTrx(\'@PLAN_ID@\');return false;" style="cursor:pointer;" alt="program@SEQ@" src="https://w3.bobe.com.tw/INSEBWeb/html/CM/img/@THUMBNAIL@">';
			
			if(item.COMMON_CASE == 'Y'){
				carNormalCaseTemplate += '<span class="label">常用</span>';
			}
			
			carNormalCaseTemplate +=
			'	   <span class="label"></span>' +
			'	   <div class="widget mb8">' +
			'		   <ul class="tags">' +
			'			   @DETAIL@' +
			'		   </ul>' +
			'	   </div>' +
			'	   <h5 class="uppercase mb0" onclick="goCarNormalCaseTrx(\'@PLAN_ID@\');return false;" style="cursor:pointer;">@CASE_TITLE@</h5>' +
			'	   <a class="title" href="#" onclick="goCarNormalCaseTrx(\'@PLAN_ID@\');return false;">' +
			'		   <span>@SUMMARY@</span>' +
			'	   </a>' +
			'    </div>' +
			'</div>';
		carNormalCaseTemplate = carNormalCaseTemplate.replace(eval('/@SEQ@/g'),(i+2));
		carNormalCaseTemplate = carNormalCaseTemplate.replace(eval('/@THUMBNAIL@/g'),item.THUMBNAIL);
		carNormalCaseTemplate = carNormalCaseTemplate.replace(eval('/@CASE_TITLE@/g'),item.TITLE);
		carNormalCaseTemplate = carNormalCaseTemplate.replace(eval('/@PLAN_ID@/g'),item.PLAN_ID);
		carNormalCaseTemplate = carNormalCaseTemplate.replace(eval('/@SUMMARY@/g'),item.SUMMARY);
		var deatilTemplate = '<li><div class="btn btn-sm">@TAG_TITLE@</div></li>';
		var detail = '';
		if(!isEmpty(item.TAG_TITLE_LIST) && !isEmpty(item.TAG_ID)){
			$.each(item.TAG_TITLE_LIST, function( i2, item2){
				var deatilTemplateNew = deatilTemplate.replace(eval('/@TAG_TITLE@/g'),item2.TAG_TITLE);
				detail = detail + deatilTemplateNew;
			});
		}
		carNormalCaseTemplate = carNormalCaseTemplate.replace(eval('/@DETAIL@/g'),detail);	
		$('#caseRecommendDiv').append(carNormalCaseTemplate);
		
	});
	
}

/**
開啟常用車輛資料
*/
function doShowCommonUseCarData(){
	var carNo1 = $('#carNo1').val();
	var carNo2 = $('#carNo2').val();
	var carNo = '';
	if(isEmpty(carNo1) && isEmpty(carNo2)){
		carNo = carNo1 + '-' + carNo2;
	}
	callAjax(ecDispatcher + 'ECV1_0200/queryCommonUseCarData','',queryCommonUseCarDataCallBack,false);
}

var commonUseCarDataDetailJson = '';

queryCommonUseCarDataCallBack = function(data){
		if(!isEmpty(data.error)){
			if(data.error == 'loginRequired'){
			commLoginSuccessContinue = 'showCommonUseCar';
			doOpenCommLogin();
		}else{
			alertMsg(data.error);	
		}
			return;
		}
		
		$('#commonUseCarData').empty();
		
		if(isEmpty(data.rtnList)) {
			alertMsg('未設定常用車輛資料!');
			return;
		}
		
		$('#commonUseCarData').append('<h5>請選擇一輛常用車輛</h5>');

	$.each(data.rtnList, function( i, item){
				var template = 	'<div class="col-xs-12">' +
				'<div class="floatleft">' + 
				'   <div class="radio-option mr8">' +
				'       <div class="inner"></div>' +
				'           <input type="radio" id="commonUseCar' + i + '" name="commonUseCar" value="' + item.CAR_NO + '">' +
				'       </div>' +
				'       <span class="floatright"><label for="commonUseCar' + i + '">' + item.CAR_NO + '</label></span>' +
				'   </div>' +
				'</div>' +
				'</div>';
				$('#commonUseCarData').append(template);
	});
		$('#commonUseCarData').append('<input type=\"button\" class=\"btn btn-lg btn-filled\" onclick=\"doImportCommonUseData();\" value=\"確定\" />');
		
		commonUseCarDataDetailJson = data.rtnList;
		refreashUI();
	$.fancybox.open({
		href : '#commonUseCarData',
		maxWidth: 600,
		afterClose:function(){
			//關閉事件也可寫在此處
		}
	});
	}

/**
	開啟車款選擇視窗
*/
function openCarModel(){
	var modelBrand = $('#modelBrand').val();
	var modelType = $('#modelType').val();
	if(isEmpty(modelBrand) || isEmpty(modelType)){
		alertMsg("請先選擇汽車廠牌及車系!");
		return;
	}
	
		refreashUI();
	$.fancybox.open({
		href : '#carModelFullData',
		afterClose:function(){
			//關閉事件也可寫在此處
		}
	});
}

/**
	選擇車款
*/
function doSelectCarModel(carModelFullNo){
	//取得排氣量及乘載限制資料
	if(!isEmpty(modelFullJson)){
		$.each(modelFullJson, function( i, item){
			if(carModelFullNo == item.MODEL_FULL_NO){
				$('#engineExhaust').val(parseInt(item.ENGINE_EXHAUST,10));
				$('#maximumLoad').val(parseInt(item.MAXIMUM_LOAD,10));
				$('#modelFullView').html(item.MODEL_FULL_NAME);
				$('#modelFull').val(item.MODEL_FULL_NO);
				clearWarnMsg('carModel_msg');
			}
		});
	}
	$.fancybox.close();
}

function doImportCommonUseData(){
	var commonUseCar = $('input[name=commonUseCar]:checked').val();
	if(isEmpty(commonUseCar)){ alert("請選擇一筆車輛!"); return; }
	$.fancybox.close();
	doClearWarnMsg();
	$.blockUI();
	
	setTimeout(function(){
		$.each(commonUseCarDataDetailJson, function( i, item){
			if(item.CAR_NO == commonUseCar){
 					$('#vehicleKindNo').val(item.VEHICLE_KIND_NO);
 					$('#originIssueDate').val(item.ORIGIN_ISSUE_DATE.replace("-","/").replace("-","/"));
 					$('#maximumLoad').val(item.MAXIMUM_LOAD);
 					$('#engineExhaust').val(item.ENGINE_EXHAUST);
 					$('#madeDateY').val(item.MADE_DATE.split("-")[0]);
 					$('#madeDateM').val(item.MADE_DATE.split("-")[1]);
 					$('#carNo1').val(item.CAR_NO.split("-")[0]);
 					$('#carNo2').val(item.CAR_NO.split("-")[1]);
 					$('#engineNo').val(item.ENGINE_NO);
 				    $.ajaxSettings.async = false;
 					doChgCarModelData('modelBrand','N');
 					$('#modelBrand').val(item.MODEL_BRAND);
 					$('#modelBrand').change();
 					doChgCarModelData('modelType','N');
 					notShowCarModelWinOnce = 'Y';
 					$('#modelType').val(item.MODEL_TYPE);
 					$('#modelType').change();
 					doChgCarModelData('modelFull','N');
 					//取得排氣量及乘載限制資料
 					if(!isEmpty(modelFullJson)){
 						$.each(modelFullJson, function( i, item2){
 							if(item.MODEL_FULL_NO == item2.MODEL_FULL_NO){
 								$('#engineExhaust').val(parseInt(item2.ENGINE_EXHAUST,10));
 								$('#maximumLoad').val(parseInt(item2.MAXIMUM_LOAD,10));
 								$('#modelFullView').html(item2.MODEL_FULL_NAME);
 								$('#modelFull').val(item2.MODEL_FULL_NO);
 							}
 						});
 					}
 					freashDropDownList();
 					return false;
 				}
		});
		$.ajaxSettings.async = true;
	},500);
}

/**
登入或登出觸發的事件
*/
function doCheckLoginStatusCallBackCustomer(status){
	if(status == 'Y'){
		$('#commonUseCarDataDiv').show();
		$('#v1MemberNotLoginDiv').hide();
		$('#dataBlock').show();
	}else{
		$('#commonUseCarDataDiv').hide();
		$('#v1MemberNotLoginDiv').show();
		$('#dataBlock').hide();
	}
	if(commLoginSuccessContinue == 'send'){
		doSend();
		return;
	}
	if(commLoginSuccessContinue == 'showCommonUseCar'){
		doShowCommonUseCarData();
		return;
	}
	
	callAjax(ecDispatcher + 'ECM1_0101/checkloginStatus','',doGetLoginInfoCallBack,false);
}


doGetLoginInfoCallBack = function(data){
	if(!isEmpty(data.error)){
		return;
	}
	if('' != 'Y' && '' != 'Y'){
		$('#customerId').val(data.memberId);
		$('#birthday').val(data.memberBirth);
	}
}
/**
關閉方案區塊
*/
function hideProducts(){
	$('#programListDiv').hide();
	$('#conditionDiv').show();
}


	function doQueryVehicleKindList(customerType){
	doClearWarnMsg();
	var params = 'customerType=' + customerType;
	callAjax(ecDispatcher + 'ECV1_0200/queryVehicleTypeList',params,queryVehicleKindListCallBack,false);
}

/**
車種下拉選單
*/
queryVehicleKindListCallBack = function(data){
		if(!isEmpty(data.error)){
			alertMsg(data.error);
			return;
		}
		
		if(!isEmpty(data.vehicleTypeList)){
			$('#modelBrand').empty();
			$('#modelType').empty();
			//$('#modelFull').empty();
			$('#engineExhaust').val('');
			$('#maximumLoad').val('');
			$('#modelFull').val('');
			$('#modelFullView').html('');
			$('#vehicleKindNo').empty();
			$('#vehicleKindNo').append("<option value=''>請選擇</option>");
			$.each(data.vehicleTypeList, function( i, item){
				$('#vehicleKindNo').append("<option value='" + item.CODE + "'>" + item.TEXT + "</option>");
			});
			freashDropDownList();
		}
}

function toMore(){
	window.open("INSEBWeb/html/CM/car_desc.jsp");
}


focusToWarnCallBack = function focusToWarn(){
	$('i[id$=_msg]').each(function(){ 
			if(!isEmpty($(this).html())){
				var objId = replaceAll($(this).prop('id'),'_msg','');
			if(objId == 'carNo'){
				if(isEmpty($('#carNo1').val())){ $('#carNo1').focus(); document.body.scrollTop = Math.max( $(this).offset().top - 100 , 0 ); return false; }
				if(isEmpty($('#carNo2').val())){ $('#carNo2').focus(); document.body.scrollTop = Math.max( $(this).offset().top - 100 , 0 ); return false; }
			}else if(objId == 'madeDate'){
				if(isEmpty($('#madeDateY').val())){ $('#madeDateY').focus(); document.body.scrollTop = Math.max( $(this).offset().top - 100 , 0 ); return false; }
				if(isEmpty($('#madeDateM').val())){ $('#madeDateM').focus(); document.body.scrollTop = Math.max( $(this).offset().top - 100 , 0 ); return false; }
			}else if(objId == 'carModel'){
				if(isEmpty($('#modelBrand').val())){ $('#modelBrand').focus(); document.body.scrollTop = Math.max( $(this).offset().top - 100 , 0 ); return false; }
				if(isEmpty($('#modelType').val())){ $('#modelType').focus();   document.body.scrollTop = Math.max( $(this).offset().top - 100 , 0 ); return false; }
				if(isEmpty($('#modelFull').val())){ $('#modelFull').focus();   document.body.scrollTop = Math.max( $(this).offset().top - 100 , 0 ); return false; }
			}else{ 
				$('#'+objId).focus(); 
				document.body.scrollTop = Math.max( $(this).offset().top - 100 , 0 );
				return false;
			}
			}
		});
}

showServiceAfter = function doShowServiceAfter(){
	doSubmitFormEC($('#form1'),ecHomePage); //首頁
}