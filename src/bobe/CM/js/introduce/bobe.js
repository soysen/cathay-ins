$(function() {

	var Application = function(){

		var init = function(){

			initSlick();
			initFiltering();

			if($(window).width() > 480){

				initProduct();

			}

		}

		var initFiltering = function() {

			//init element attribute

			var triggrtElement = $(".condition-filtering-nav .option > a");
			var tags = $(".condition-chooser-tags > .option");
			var addtarget = $("#condition-filter > li");

			//console.log(tags);

			tags.not('.disabled').each(function(){

				var tagType = $(this).attr("data-tag-type");
				var tagsContainer = $(this).closest(addtarget);
				tagsContainer.addClass(tagType);
				
			}); 

			//console.log("initFiltering");

			var selected = [];
			triggrtElement.bind('click',function(){

				var $this = $(this);
				var type = $this.attr("data-protect-ta");
				var container = $("#condition-filter");

				//triggrtElement.removeClass("active");
				$this.toggleClass("active");
				var sidx = selected.indexOf(type);
				if (sidx == -1)
					selected.push(type);
				else {
					selected.splice(sidx, 1);
				}

				for(var i in selected)
				{
					var type = selected[i];
					container.find('> li').hide();
					container.find('li.' + type).show();

					console.log(type);
					// switch (type) {

					// 	case 'me':
					// 		if(target.hasClass(type)){
					// 			$("#condition-filter > li.me").show();
					// 		}
					// 		console.log(type);
					// 	break;

					// 	case 'my-passenger':
					// 		if(target.hasClass(type)){
					// 			$("#condition-filter > li.my-passenger").show();
					// 		}
					// 		console.log(type);
					// 	break;

					// 	case 'my-car':
					// 		if(target.hasClass(type)){
					// 			$("#condition-filter > li.my-car").show();
					// 		}
					// 		console.log(type);
					// 	break;

					// 	case 'you':
					// 		if(target.hasClass(type)){
					// 			$(".you").show();
					// 		}
					// 		console.log(type);
					// 	break;

					// 	case 'your-passenger':
					// 		if(target.hasClass(type)){
					// 			$("#condition-filter > li.your-passenger").show();
					// 		}
					// 		console.log(type);
					// 	break;

					// 	case 'your-car':
					// 		if(target.hasClass(type)){
					// 			target.hide();
					// 			$("#condition-filter > li.your-car").show();
					// 		}
					// 		console.log(type);
					// 	break;

					// 	case 'other':
					// 		if(target.hasClass(type)){
					// 			target.hide();
					// 			$("#condition-filter > li.other").show();
					// 		}
					// 		console.log(type);
					// 	break;
					// }

				}
				console.log(selected);

				

				//console.log("triggrtElemen=",triggrtElement,"type=",type);


			});





		}

		var initProduct = function(){
		    var maxH;
		    $(".example-cards-container").each(function(){
		        maxH = 0;
		        $(".example-card",$(this)).each(function(){
		            var h = $(this).height();
		            if(h>maxH){
		                maxH = h;
		            }
		        })
		        $(".example-card",$(this)).height(maxH);  
		        //
		        maxH = 0;
		        $(".example-card table",$(this)).each(function(){
		            var h = $(this).height();
		            if(h>maxH){
		                maxH = h;
		            }
		        })
		        $(".example-card table",$(this)).height(maxH);  


		        maxH = 0;
		        $(".example-card p",$(this)).each(function(){
		            var h = $(this).height();

		            if(h>maxH){
		                maxH = h;
		            }
		        })
		        $(".example-card p",$(this)).height(maxH);  
		    })
		}
		var initSlick = function(){

			$("#slick-condition-chooser").slick({

				slidesToShow:7,
				slidesToScroll:1,
				infinite:false,
				arrows:false,
				dots:false,
				responsive: [
					{
					  breakpoint: 1200,
					  settings: {
					    slidesToShow: 5,
						dots:false
					  }
					},
					{
					  breakpoint: 992,
					  settings: {
					    slidesToShow: 3,
						centerMode:true,
						infinite:true,
						centerPadding:'70px',
						focusOnSelect:true,
						dots:false
					  }
					},
					{
					  breakpoint: 800,
					  settings: {
					    slidesToShow: 3,
						centerMode:true,
						centerPadding:'110px',
						infinite:true,
						focusOnSelect:true,
						dots:false
					  }
					},
					{
					  breakpoint: 500,
					  settings: {
					    slidesToShow: 1,
						centerMode:true,
						centerPadding:'125px',
						infinite:true,
						focusOnSelect:true,
						dots:false
					  }
					},
					{
					  breakpoint: 400,
					  settings: {
					    slidesToShow: 1,
						centerMode:true,
						centerPadding:'105px',
						infinite:true,
						focusOnSelect:true,
						dots:false
					  }
					},
					{
					  breakpoint: 350,
					  settings: {
					    slidesToShow: 1,
						centerMode:true,
						centerPadding:'80px',
						infinite:true,
						focusOnSelect:true,
						dots:false
					  }
					}
				]

			})

		}

		init();

	}

	Application();

});