/*
 *  Document   : base_comp_treeview.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Tree View Page
 */

var BaseCompTreeview = function() {
    // Bootstrap Tree View, for more examples you can check out https://github.com/jonmiles/bootstrap-treeview

    // Init Tree Views
    var initTreeViews = function(){
        // Set default example tree data for all Tree Views
        var $treeData = [
            {
                text: '旅遊險',
                id:10,
                parent: 0,
                tags: ['4'],
                nodes: [
                    {
                        
                        
                        text: '一般事項',
                        id:11,
                        parent: 10,
                    },
                    {
                        text: '旅遊不便',
                        id:12,
                        parent: 10
                    },
                    {
                        text: '旅遊傷害保險',
                        id:13,
                        parent: 10
                       
                    }
                    ,
                    {
                        text: '海外急難救助',
                        id:14,
                        parent: 10
                    }
                    ,
                    {
                        text: '理賠申辦說明',
                        id:15,
                        parent: 10
                        
                    }
                    ,
                    {
                        text: '理賠應備文件',
                        id:16,
                        parent: 10
                    }
                ]
            },
            { 
                text: '汽車險',
                parent: '#parent3', 
                nodes: [
                    {
                        
                        
                        text: '強制汽車責任險－承保篇',
                        parent: '#child1',
                    },
                    {
                        text: '強制汽車責任險－理賠篇',
                        parent: '#child2'
                    },
                    {
                        text: '汽車任意險－承保篇',
                        parent: '#child3',
                        tags: ['2']
                    }
                    ,
                    {
                        text: '汽車任意險－理賠篇',
                        parent: '#child3',
                        tags: ['2']
                    }
                    
                ]
            },
            
            

    

    
    


            {
                text: '機車險',
                parent: '#parent4',
             nodes: [
                    {
                        
                        
                        text: '強制機車責任險－承保篇',
                        parent: '#child1',
                    },
                    {
                        text: '強制機車責任險－理賠篇',
                        parent: '#child2'
                    }
                    
                ]
            },
            {
                text: '火險',
                parent: '#parent5',
               
             nodes: [
                    {
                        
                        
                        text: '火險',
                        parent: '#child1',
                    }
                    
                ]
            }
            ,
            {
                text: '會員相關',
                parent: '#parent5'
            }
            ,
            {
                text: '投保流程',
                parent: '#parent5'
            }
        ];
/*
        // Init Simple Tree
        jQuery('.js-tree-simple').treeview({
            data: $treeData,
            color: '#555',
            expandIcon: 'fa fa-plus',
            collapseIcon: 'fa fa-minus',
            onhoverColor: '#f9f9f9',
            selectedColor: '#555',
            selectedBackColor: '#f1f1f1',
            showBorder: false,
            levels: 3
        });
*/
        // Init Icons Tree
        jQuery('.js-tree-icons').treeview({
            data: $treeData,
            color: '#555',
            expandIcon: 'fa fa-plus',
            collapseIcon: 'fa fa-minus',
            nodeIcon: 'fa fa-folder text-primary',
            onhoverColor: '#f9f9f9',
            selectedColor: '#555',
            selectedBackColor: '#f1f1f1',
            showBorder: false,
            levels: 3,
             onNodeSelected: function(event, data) {
    alert(data.text+" - "+data.id+" - "+data.parent)
  }
        });
/*
        // Init Alternative Icons Tree
        jQuery('.js-tree-icons-alt').treeview({
            data: $treeData,
            color: '#555',
            expandIcon: 'fa fa-angle-down',
            collapseIcon: 'fa fa-angle-up',
            nodeIcon: 'fa fa-file-o text-city',
            onhoverColor: '#f9f9f9',
            selectedColor: '#555',
            selectedBackColor: '#f1f1f1',
            showBorder: false,
            levels: 3
        });

        // Init Badges Tree
        jQuery('.js-tree-badges').treeview({
            data: $treeData,
            color: '#555',
            expandIcon: 'fa fa-plus',
            collapseIcon: 'fa fa-minus',
            nodeIcon: 'fa fa-folder text-primary',
            onhoverColor: '#f9f9f9',
            selectedColor: '#555',
            selectedBackColor: '#f1f1f1',
            showTags: true,
            levels: 3
        });

        // Init Collapsed Tree
        jQuery('.js-tree-collapsed').treeview({
            data: $treeData,
            color: '#555',
            expandIcon: 'fa fa-plus',
            collapseIcon: 'fa fa-minus',
            nodeIcon: 'fa fa-folder text-primary-light',
            onhoverColor: '#f9f9f9',
            selectedColor: '#555',
            selectedBackColor: '#f1f1f1',
            showTags: true,
            levels: 1
        });
*/
        // Set example JSON data for JSON Tree View
        var $treeDataJson = '[' +
          '{' +
            '"text": "Boot-140-strap",' +
            '"nodes": [' +
              '{' +
                '"text": "eLearning",' +
                '"nodes": [' +
                  '{' +
                    '"text": "Code"' +
                  '},' +
                  '{' +
                    '"text": "Tutorials"' +
                  '}' +
                ']' +
              '},' +
              '{' +
                '"text": "Templates"' +
              '},' +
              '{' +
                '"text": "CSS",' +
                '"nodes": [' +
                  '{' +
                    '"text": "Less"' +
                  '},' +
                  '{' +
                    '"text": "SaSS"' +
                  '}' +
                ']' +
              '}' +
            ']' +
          '},' +
          '{' +
            '"text": "Design"' +
          '},' +
          '{' +
            '"text": "Coding"' +
          '},' +
          '{' +
            '"text": "Marketing"' +
          '}' +
        ']';

        // Init Json Tree
        jQuery('.js-tree-json').treeview({
            data: $treeDataJson,
            color: '#555',
            expandIcon: 'fa fa-arrow-down',
            collapseIcon: 'fa fa-arrow-up',
            nodeIcon: 'fa fa-file-code-o text-flat',
            onhoverColor: '#f9f9f9',
            selectedColor: '#555',
            selectedBackColor: '#f1f1f1',
            showTags: true,
            levels: 3
        });
    };

    return {
        init: function () {
            // Init all Tree Views
            initTreeViews();
        }
    };
}();

// Initialize when page loads
jQuery(function(){ BaseCompTreeview.init(); });


