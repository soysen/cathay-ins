var isBobeAplus = $('#form1').hasClass('bobeForm') ? true : false;				/** aplus */
var isBobeAlbertlan = $('#form1').hasClass('bobeAlbertlanForm') ? true : false;	/** albertlan */
if(isBobeAplus){
	$.blockUI = lockScreen;
	$.unblockUI = unLockScreen;
	
	// 針對 A+ 新的BOBE版型微調
	// 警示視窗的按鈕調整為新UI
	$('#alertMsgTemplate > .btn-filled').addClass('bobebtn bobebtn--major');
	$('#alertMsgTemplate > .btn-filled').css('width','150px');
	$('#alertMsgTemplate > .btn-filled').removeClass('btn-filled btn');
	$('.popup__content').find('div').css('word-break','break-all');
	
	// 最後一層的麵包屑，連結移除
	var headerBreadLink = $('ol[class="breadcrumb breadcrumb--nomargin"]').find('li:last');
	if(headerBreadLink){
		$(headerBreadLink).find('a').attr('href','javascript:;');
		$(headerBreadLink).find('a').css('cursor','default');
		$(headerBreadLink).find('a').hover(function(){ $(this).css("color", "#f9ad19"); });
	}
	
}
if(isBobeAlbertlan){
	$.blockUI = lockScreen;
	$.unblockUI = unLockScreen;
}
$(document).ready(function(){
	$.unblockUI();
	//小日曆初始化
	try{
		$('[calendar=true]').datepicker();
	}catch(e){}
	//輸入框小寫轉大寫
	setObjUpperCase();
	//input內的keypress禁止使用Enter
	blockTextEnterSubmit();
	// a href 的 class 若有加 verifyBrowser 的class，會檢查瀏覽器是否可供執行下載動作
	verifyBrowserForDownload();
});

function sleep(ms){
	var sleepingTime = new Date();
		sleepingTime.setTime(sleepingTime.getTime() + ms);
	while (new Date().getTime() < sleepingTime.getTime());
}

function blockTextEnterSubmit(){
	$('input').keypress(function(e){
		var code = e.keyCode ? e.keyCode : e.which;
		if(code == 13){
			e.preventDefault();
		}
	});
}

/**
 * @param ecUrl 呼叫網址
 * @param param 其它參數(abc=123&def=456)
 * @param callBack 結束後呼叫的處理作業
 * @param notShowAlertMsg 不顯示嚴重錯誤(Y/N)
 * @returns data(JSON)
 */
function callAjaxNoBlock(ecUrl,param,callBack,notShowAlertMsg){
		//$.blockUI();
		//console.log("■url=" + ecUrl + "?" + param);
		var rtnStr = '';
		$.ajax({
			type: "POST",
			url : ecUrl,
			data: encodeURI(param),
			dataType: "JSON"
		}).done(function(resp) {
			rtnStr = preCallBack(resp,callBack,notShowAlertMsg);
			//$.unblockUI();
		});
		return rtnStr;
}

/**
 * @param ecUrl 呼叫網址
 * @param param 其它參數(abc=123&def=456)
 * @param callBack 結束後呼叫的處理作業
 * @param notShowAlertMsg 不顯示嚴重錯誤(Y/N)
 * @returns data(JSON)
 */
function callAjax(ecUrl,param,callBack,notShowAlertMsg){
		$.blockUI();
		//console.log("■url=" + ecUrl + "?" + param);
		var rtnStr = '';
		$.ajax({
			type: "POST",
			url : ecUrl,
			data: encodeURI(param),
			dataType: "JSON"
		}).done(function(resp) {
			rtnStr = preCallBack(resp,callBack,notShowAlertMsg);
			$.unblockUI();
		});
		return rtnStr;
}


/**
 * callBack前
 */
function preCallBack(data,callBack,notShowAlertMsg){
	
	if(data.ErrMsg != null){
		if(data.ErrMsg.returnCode != '0'){
			if('Y' != notShowAlertMsg){
				alertMsg(data.ErrMsg);
				return;
			}
		}
	}
	
	return callBack.call(null,data);
}

/**
 * 共用的 Submit Form
 * @param form
 * @param action
 * @param methodName
 * @param param
 */
function doSubmitFormEC(form,action){
	
	//console.log(form)
	//console.log(form.length)
	//console.log('action',action);
	
	/* 20180906*/
	if(form.length==0){
		location.href = action;
		return false;
	}
	/* 20180906 end*/
	
	setTimeout(function(){
		$.blockUI();
		if(!isEmpty(action)){
			$(form).attr('action',action);
		}
		$(form).attr('method','post');
		$(form).submit();
	},100);

}

/**
 * 取得系統日(YYYY/MM/DD)
 * @returns
 */
function getCurrDate(sp){
	var date = new Date();
	var d = date.getDate();
	var m = date.getMonth()+1;
	var y = date.getFullYear();
	if(!isEmpty(sp)){
		return (y) + sp + fm(m) + sp + fm(d);
	}else{
		return (y) + fm(m) + fm(d);
	}
	
}
function fy(v){
	return (v<100 ? '0' : '')+v;
}
function fm(v){
	return (v<10 ? '0' : '')+v;
}

/**
 * 判斷值是否為空白
 * @param val
 * @returns {Boolean}
 */
function isEmpty(val){
	return (val == '' || val == null || val == undefined || val == 'undefined');
}

/**
 * 警示訊息
 * @param msg 訊息
 */
//function alertMsg(msg){
//	alert(msg);
//}

/**
 * 取得網址參數(http:\\...?key1=value1&key2=value2)
 * @returns map=["key1":value1, "key2":value2]
 */
function getURLParam(){
	var map = [];

	var paramPair;
	var urlParam = location.search;
	if (urlParam.indexOf("?") != -1) {
		var param1 = urlParam.split("?");
		var param2 = (param1[1]).split("&");
		for ( var i = 0; i < param2.length; i++) {
			paramPair = (param2[i]).split("=");
			map[paramPair[0]] = paramPair[1];
		}
	}
	return map;
	
}


//***************************************************
// 檢核
//***************************************************
/**
 * 檢查是否為英文
 */
function chkEng( value ) {
	var regExp = /^[a-zA-Z ._]+$/;
	return regExp.test( value );
}

/**
 * 檢查是否為英數字
 * @param value
 * @returns
 */
function chkEngNum( value ) {
	var regExp = /^[\d|a-zA-Z]+$/;
	return regExp.test( value );
}

/**
 * 檢查是否為市話
 * @param value
 * @returns
 */
function validatePhoneNum(value) {
    value = new String(value);
    var RE = new RegExp("^0\\d{1,2}-\\d{6,8}(#\\d*)?$");
    return RE.test(value);
}

/**
 * 檢核是否為行動電話
 * @param value
 * @returns
 */
function validateCellPhone(value) {
    value = new String(value);
    var RE = new RegExp("^09\\d{8}$");
    return RE.test(value);
}

/**
 * 檢核身分證
 * @param value
 * @returns {Boolean}
 */
function validatePersonalID(value) {
    value = new String(value);
    value = value.toLowerCase();
    var RE = new RegExp("^[a-z][12][0-9]{8}$");
    if (RE.test(value)) {
        h = "abcdefghjklmnpqrstuvxywzio";
        x = 10 + h.indexOf(value.substring(0, 1));
        chksum = (x - (x % 10)) / 10 + (x % 10) * 9;
        for (i=1; i<9; i++) {
            chksum += value.substring(i, i+1) * (9 - i);
        } 
        chksum = (10 - chksum % 10) % 10;
        if (chksum == value.substring(9, 10))
            return true;
    }
    return false;
}

/**
 * 檢核居留證號
 */
function validateLiveID(id) {
  try{
	if(id==""){
		return false;
	}
	if (id.length != 10) return false;

	id = id.toUpperCase();

	if (isNaN(id.substring(2,10)) || (id.substring(0,1)<"A" ||id.substring(0,1)>"Z") || (id.substring(1,2)<"A" ||id.substring(1,2)>"Z")){
		return false;					
	}
	
	var head="ABCDEFGHJKLMNPQRSTUVXYWZIO";
	id = (head.indexOf(id.substring(0,1))+10) +''+ ((head.indexOf(id.substring(1,2))+10)%10) +''+ id.substring(2,10);
	s =parseInt(id.substring(0,1)) + 
	parseInt(id.substring(1,2)) * 9 + 
	parseInt(id.substring(2,3)) * 8 + 
	parseInt(id.substring(3,4)) * 7 + 			
	parseInt(id.substring(4,5)) * 6 + 
	parseInt(id.substring(5,6)) * 5 + 
	parseInt(id.substring(6,7)) * 4 + 
	parseInt(id.substring(7,8)) * 3 + 
	parseInt(id.substring(8,9)) * 2 + 
	parseInt(id.substring(9,10)) + 
	parseInt(id.substring(10,11));

	//判斷是否可整除
	if ((s % 10) != 0) return false;
	//居留證號碼正確		
	return true;
  }
  catch(err){
    	return false;
  }
	
}

/**
 * 檢核統一編號
 * @param value
 * @returns {Boolean}
 */
function validateVATNO( value ) {
	var tbNum = new Array(1,2,1,2,1,2,4,1);
	var temp = 0;
	var total = 0;
	if( value == '' ) {
		return false;
	} else if ( !value.match(/^\d{8}$/) ) {
		return false;
	} else {
		for( var i = 0; i < tbNum.length ;i ++ ) {
			temp = value.charAt(i) * tbNum[i];
			total += Math.floor(temp/10)+temp%10;
		}
		
		if( total%10==0 || (total%10==9 && value.charAt(6)==7) ) {
			return true;
		} else {
		    return false;
		}
	}
	
}

/**
 * 檢核是否為數字
 * @param value
 * @returns
 */
function validateNumber(value) {
    value = new String(value);
    var RE = new RegExp("^\\d\\d*$");
    return RE.test(value);
}

/**
 * 檢核是否為正整數
 * @param value
 * @returns
 */
function validateInt(input)  {  
     var RE = /^[1-9]+[0-9]*]*$/;
     return RE.test(input);
} 

/**
 * 檢核是否為數字(含小數2位)
 * @param value
 * @returns
 */
function validateNumberScale2(value) {
	value = new String(value);
	var RE = new RegExp("^[0-9]+(\.[0-9]{1,2})?$");
	return RE.test(value);
}

/**
 * 檢核是否為數字(含小數3位)
 * @param value
 * @returns
 */
function validateNumberScale3(value) {
	value = new String(value);
	var RE = new RegExp("^[0-9]+(\.[0-9]{1,3})?$");
	return RE.test(value);
}

/**
 * 檢查電子郵件格式
 * @param value
 * @returns
 */
function validateEmail(value) {
    value = new String(value);
    var RE = new RegExp("^(?:[\\w\\!\\#\\$\\%\\&\\'\\*\\+\\-\\/\\=\\?\\^\\`\\{\\|\\}\\~]+\\.)*[\\w\\!\\#\\$\\%\\&\\'\\*\\+\\-\\/\\=\\?\\^\\`\\{\\|\\}\\~]+@(?:(?:(?:[a-zA-Z0-9](?:[a-zA-Z0-9\\-](?!\\.)){0,61}[a-zA-Z0-9]?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9\\-](?!$)){0,61}[a-zA-Z0-9]?)|(?:\\[(?:(?:[01]?\\d{1,2}|2[0-4]\\d|25[0-5])\\.){3}(?:[01]?\\d{1,2}|2[0-4]\\d|25[0-5])\\]))$");
    return RE.test(value);
}

/**
 * 取得字串長度
 * @param str
 * @returns
 */
function getStringByteLength(str){
	
	var arr = str.match(/[^\x00-\xff]/ig);
	return  arr == null ? str.length : str.length + arr.length;
	
}

/**
 * 是否含有中文字
 * @param str
 * @returns
 */
function isHaveChinese(str){
	
	var arr = str.match(/[^\x00-\xff]/ig);
	return  arr == null ? false : true;
	
}

/**
 * 清除錯誤訊息
 * @param objName
 */
function clearWarnMsg(objName){ 
	$('#'+objName).html("").hide();
}

/**
 * 設定錯誤訊息
 * @param objName
 * @param msg
 */
function setWarnMsg(objName,msg){
	$('#'+objName).html(msg).show();
}

/**
 * 物件設定為，輸入小寫轉大寫
 */
function setObjUpperCase(){
	$('.toUpperFormat').keyup(function(event) {
		if(event.keyCode == 37) { return; }
		if(event.keyCode == 39) { return; }
		this.value = this.value.toUpperCase();
	});
	
	$('.toUpperFormat').blur(function() {
		this.value = this.value.toUpperCase();
	});
	
}

/**
 * 檢查密碼規則
 * @param value
 * @returns
 */
function chkPassword( value ) {
	var regExp = /^(?=^.{6,12}$)(?!.*[^\x30-\x39|^\x41-\x5A|^\x61-\x7A])((?=.*[0-9])(?=.*[a-z|A-Z]))^.*$/;
	return regExp.test( value );
}


/**
 * 檢查是否為正確日期
 * @param yyyymmdd
 * @returns {Boolean}
 */
function isDate(yyyymmdd){
	
	yyyymmdd = yyyymmdd + "";
	yyyymmdd = yyyymmdd.replace("/","");
	yyyymmdd = yyyymmdd.replace("/","");
	var ereg = /^(\d{4})(\d{2})(\d{2})$/;
	var r = yyyymmdd.match(ereg);
	if (r == null) {
		return false;
	}
	var d = new Date(r[1], r[2] - 1, r[3]);
	var result = (d.getFullYear() == r[1] && (d.getMonth() + 1) == r[2] && d.getDate() == r[3]);
	return (result);
	
}

/**
 * 檢查是否為正確民國日期
 * @param yyymmdd or yymmdd
 * @returns {Boolean}
 */
function isDateROC(ymd){
	if(isEmpty(ymd)){ return; }
	ymd = ymd + "";
	var yTmp = ymd.split('/')[0];
	var mTmp = ymd.split('/')[1];
	var dTmp = ymd.split('/')[2];
	ymd = (parseInt(yTmp,10)+1911) + mTmp + dTmp;
	return isDate(ymd);
}

/**
 * 取得該月的最後一天
 * @param year
 * @param month
 * @returns
 */
function getLastDayOfMonth(year,month){ 
	  var date=new Date(year,month,01); 
	  cdate=new Date(date.getTime()-1000*60*60*24);  
	  return cdate.getDate(); 
}

/**
 * 增加月
 * @param num (加或減)
 * @param sdate (19010101)
 * @returns {String}
 */
function addDateMonth(num,sdate) {
	
	if(isEmpty(sdate)){
		return;
	}
	
	var dateY = sdate.substring(0,4);
	var dateM = parseInt(sdate.substring(4,6),10);
	var dateD = sdate.substring(6,8);

	
	var d1 = new Date(dateY, parseInt(dateM-1,10)+num, dateD);//因為這邊加月，若日期是超過當月份~會自動加一個月
	var d2 = new Date(dateY, parseInt(dateM-1,10)+num, '01');//原相加或相減應該正確的月份
	var lastDate = false;

	if(d1.getFullYear() != d2.getFullYear()){//d1 加出來的年不等於
		lastDate = true;
	}
	
	if(d1.getMonth() != d2.getMonth()){//d1 加出來的月不等於 d2
		lastDate = true;
	}

	var returnDate = new Date();
	if(lastDate){
		//alert('lastDate');
		returnDate = new Date(d2.getFullYear(),d2.getMonth(),getLastDayOfMonth(d2.getFullYear(), d2.getMonth()+1));
	}else{
		returnDate = new Date(d2.getFullYear(),d2.getMonth(),dateD);
	}
	
	 var returnY = returnDate.getFullYear();  
	 var retyrnM = returnDate.getMonth()+1;  
	 var returnD = returnDate.getDate();  
	  if (retyrnM < 10) {  
	    	retyrnM = '0' + retyrnM;  
	 }  
	 if (returnD < 10) {  
		 returnD = '0' + returnD;  
	 }  
	    
	 return returnY + "" + retyrnM + "" + returnD;  
	
}

//移除日曆並唯讀
function removeDatePicker(objName,clearValue){
	$('#'+objName).datepicker('destroy');
	$('#'+objName).prop('readonly',true);
	if(clearValue){
		$('#'+objName).val('');
	}
}

//空物件(val == '' || val == null || val == undefined || val == 'undefined')轉預設字
function emptyToStr(oriVal,defaultVal){
	if(isEmpty(oriVal)){
		return defaultVal;
	}else{
		return oriVal;
	}
}

function addCommas(value) {
    value = new String(value);
    value1 = value.split(/\./)[0];
    value2 = value.split(/\./)[1];
    if( value2 != null && value2.length > 2 ) { value2 = value2.substring(0,2); }
    var RE  = new RegExp('(-?[0-9]+)([0-9]{3})');
    while (RE.test(value1)) {
        value1 = value1.replace(RE, '$1,$2');
    }
    if (value2 == undefined) {
        value = value1;
    } 
    else {
        value = value1 + '.' + value2;
    }
    
    return value;
}

function removeCommas(value) {
    value = new String(value);
    var RE = /,/g;
    return value.replace(RE,'');
}

function refreashUI(){
	//下列是copy愛現的程式，只是包一層而已
	
    // Checkboxes
	$('.checkbox-option').unbind("click").click(function() {
    		//jimmy
	    //console.log( $(this).prop("confirm")+" / "+$(this).attr("confirm"))
	    var confirmMsg=$(this).attr("confirm");
	    if(confirmMsg !=undefined){
		    	var r = confirm(confirmMsg);
		    	if (r == true) {
		    		// alert("改 "+isProposer)
		    	} else {
		    		return;
		    	}
	    }
    	
        $(this).toggleClass('checked');
        var checkbox = $(this).find('input');
        var func=checkbox.prop('onchange')
       
       
        if (checkbox.prop('checked') === false) {
            checkbox.prop('checked', true);
        } else {
            checkbox.prop('checked', false);
        }
        if(func !=null){
       	 func();
       }
    });
    
	$(".checkbox-option").find("input").each(function( index ) {
		  //console.log("scripts.js:"+ index + ": " + $( this ).val() +" / "+$( this ).prop("checked")+" / "+$( this ).parent().prop("class"));
		 if($( this ).prop("checked")==true){
			 $( this ).parent().addClass("checked");
		 }else{
			 $( this ).parent().removeClass("checked");
		 } 
	});
	
	 $('.radio-option').unbind("click").click(function() {
        var checked = $(this).hasClass('checked'); // Get the current status of the radio
        var disabled = $(this).hasClass('disabled'); // Get the current status of the radio
        if(disabled){
        	return;
        }
        var name = $(this).find('input').attr('name'); // Get the name of the input clicked

        if (!checked) {

            $('input[name="'+name+'"]').parent().removeClass('checked');

            $(this).addClass('checked');

            $(this).find('input').prop('checked', true);

        }
        var checkbox = $(this).find('input');
        var func=checkbox.prop('onclick')
        
      
        if(func !=null){
       	 func();
       }
    });
    
    
	 $('.accordion li').unbind("click").click(function() {
        if ($(this).closest('.accordion').hasClass('one-open')) {
            $(this).closest('.accordion').find('li').removeClass('active');
            $(this).addClass('active');
        } else {
            $(this).toggleClass('active');
        }
        if(typeof window.mr_parallax !== "undefined"){
            setTimeout(mr_parallax.windowLoad, 500);
        }
    });
    
    $('.tabbed-content').each(function() {
    	//jimmy
    	//alert($(this).attr("type"))
    	if($(this).attr("type")==undefined ||$(this).attr("type")=="undefined" || $(this).attr("type")!="v"){
    		//alert("hhh")
    		 $(this).append('<ul class="content"></ul>');
    	}else{
    		//alert("vvv")
    	}
       
    });

    $('.tabs li').each(function() {
        var originalTab = $(this),
            activeClass = "";
        if (originalTab.is('.tabs>li:first-child')) {
            activeClass = ' class="active"';
        }
        var tabContent = originalTab.find('.tab-content').detach().wrap('<li' + activeClass + '></li>').parent();
        originalTab.closest('.tabbed-content').find('.content').append(tabContent);
    });

    $('.tabs li').unbind("click").click(function() {
        $(this).closest('.tabs').find('li').removeClass('active');
        $(this).addClass('active');
        var liIndex = $(this).index() + 1;
        $(this).closest('.tabbed-content').find('.content>li').removeClass('active');
        $(this).closest('.tabbed-content').find('.content>li:nth-of-type(' + liIndex + ')').addClass('active');
    });
    
    
    $('select').each(function(){
	    if($(this).find('option:selected').val() == ""){
	        $(this).css('color','#999');
	        $(this).children().css('color','black');
	    }else {
	        $(this).css('color','black');
	        $(this).children().css('color','black');
	    }
    });
    
}

function freashDropDownList(){
    $('select').each(function(){
	    if($(this).find('option:selected').val() == ""){
	        $(this).css('color','#999');
	        $(this).children().css('color','black');
	    }else {
	        $(this).css('color','black');
	        $(this).children().css('color','black');
	    }
    });
}
/**
 * 警示訊息(fancybox已開啟中無法使用)
 * @param msg 訊息
 * @param afterCloseEvent 關閉後要做的事情(沒有的話轉 null 或不傳)
 * @param timeoutMS 延遲開啟(沒有的話轉 null 或不傳，預設值100ms)
 * @param title 標題(沒有的話轉 null 或不傳，預設值"提示訊息")
 */
function alertMsg(msg,afterCloseEvent,timeoutMS,title){
	$.unblockUI();
	setTimeout(function(){alertMsgRealCall(msg,afterCloseEvent,title);},isEmpty(timeoutMS) ? 100 : parseInt(timeoutMS,10));
}

function alertMsgRealCall(msg,afterCloseEvent,title){
	var alertObjId = (isBobeAplus ? 'aplusAlertMsg' :(isBobeAlbertlan ? 'albertlanAlertMsg' :'alertMsg')) + 'Template';
	var descObj = $('#' + alertObjId + 'Desc');
	var titleObj = $('#' + alertObjId + 'Title');
	$(descObj).html(filterSqlException(msg));
	
	if(titleObj) {
		$(titleObj).html("提醒您");
		var newTitle = filterAlertTitle(msg);
		if(isEmpty(newTitle)) {
			if(!isEmpty(title)) { $(titleObj).html(title); }
		}else{
			$(titleObj).html(newTitle);
		}
	}
	
	$.fancybox.open({
		minWidth:300,
		minHeight:150,
		maxWidth:500,
		href : '#' + alertObjId,
		autoDimensions: true,
		afterClose:function(){
			if(afterCloseEvent != null){
        		setTimeout(function(){afterCloseEvent();},100);
        	}
		},
		padding:(isBobeAplus || isBobeAlbertlan ? 0 : 15),
	});
	
}

function alertDivDataCall(alterId){
	
	$.fancybox.open({
		minWidth:300,
		minHeight:150,
		maxWidth:800,
		href : '#' + alterId,
		autoDimensions: true,
	});
	
}

function useROCDate(){
	//日曆用民國年
	(function($) {
	      $.extend($.datepicker, {
	         formatDate: function (format, date, settings) {
	                          var d = date.getDate();
	                          var m = date.getMonth()+1;
	                          var y = date.getFullYear();                   
	                          var fm = function(v){              
	                             return (v<10 ? '0' : '')+v;
	                          };
	                          var fmY = function(v){              
		                            return (v<10 ? '00' : (v<100 ? '0' : ''))+v;
		                      }; 
	                          return fmY(y-1911) +'/'+ fm(m) +'/'+ fm(d);
	         },
	         parseDate: function (format, value, settings) {
	             var v = new String(value);
	             var Y,M,D;
	             if(v.length==9){/*100/12/15*/
	                 Y = v.substring(0,3)-0+1911;
	                 M = v.substring(4,6)-0-1;
	                 D = v.substring(7,9)-0;
	                 return (new Date(Y,M,D));
	             }else if(v.length==8){/*98/12/15*/
	                 Y = v.substring(0,2)-0+1911;
	                 M = v.substring(3,5)-0-1;
	                 D = v.substring(6,8)-0;
	                 return (new Date(Y,M,D));
	             }
	             if(format == 'yyy/mm/dd'){ /* 特別指定此格式的話，就當生日用，取系統年前30年 */
	            	 var birthdayY = new Date().getFullYear()-1941;
	            	 var birthdayM = new Date().getMonth();
	            	 var birthdayD = new Date().getDate();
	            	 return (new Date(birthdayY,birthdayM,birthdayD));
	             }else{
	            	 return (new Date());
	             }
	         }
	      }); 
	      
	      
	})(jQuery);

	(function($)
			{
			    $.datepicker._baseGenerateMonthYearHeader = $.datepicker._generateMonthYearHeader;
			    $.datepicker._generateMonthYearHeader = function(inst, drawMonth, drawYear, minDate, maxDate,selectedDate, secondary, monthNames, monthNamesShort)
			    {
			       var result = $($.datepicker._baseGenerateMonthYearHeader(
			                                  inst, drawMonth, drawYear - 1911, minDate, maxDate,
			                                  selectedDate, secondary, monthNames, monthNamesShort)
			                     );
			       result.html('');
			       
			       var fm = function(v){              
                       return (v<10 ? '0' : '')+v;
                    };
                    var fmY = function(v){              
                          return (v<10 ? '00' : (v<100 ? '0' : ''))+v;
                    }; 
                   var yearNow = new Date().getFullYear();  
                    
			       var yearOverrite = "民國<select class=\"ui-datepicker-yearR\" data-handler=\"selectYear\" data-event=\"change\">";
			       
			       
			       minDate = inst.settings.minDate;
			       maxDate = inst.settings.maxDate;
			       
			       if(minDate){
			    	   if(drawYear > minDate.getFullYear()){
				    	   for(var i = drawYear - minDate.getFullYear() ; i > 0 ; i--){
					    	   yearOverrite +="<option value=\"" + (drawYear - i) + "\">" + fmY(drawYear - i - 1911) + "</option>";
					       }
				       }
			       }else{
			    	   for(var i = 30 ; i > 0 ; i--){
				    	   yearOverrite +="<option value=\"" + (drawYear - i) + "\">" + fmY(drawYear - i - 1911) + "</option>";
				       }
			       }
			       
			       yearOverrite +="<option value=\"" + drawYear + "\" selected>" + fmY(drawYear - 1911) + "</option>";
			       
			       if(maxDate){
			    	   if(maxDate.getFullYear() > drawYear){
				    	   for(var i = 1 ; i <= maxDate.getFullYear() - drawYear; i++){
					    	   yearOverrite +="<option value=\"" + (drawYear + i) + "\">" + fmY(drawYear + i - 1911) + "</option>";
					       }
				       }
			       }else{
			    	   for(var i = 1 ; i < 15 ; i++){
				    	   yearOverrite +="<option value=\"" + (drawYear + i) + "\">" + fmY(drawYear + i - 1911) + "</option>";
				       }
			       }     
			      
			       
			       yearOverrite += "</select>年";
			       
			       var monthOverrite = "<select class=\"ui-datepicker-monthR\" data-handler=\"selectMonth\" data-event=\"change\">";
			       for(var i = 0 ; i < 12 ; i++){
			    	   var selectedMon = (drawMonth == i) ? " selected " : "";
			    	   monthOverrite +="<option value=\"" + fm(i) + "\" " + selectedMon + ">" + fm(i+1) + "</option>";
			       }
			       monthOverrite += "</select>月";
			       //result.children(".ui-datepicker-year").remove();
			       result.append(yearOverrite);
			       result.append(monthOverrite);
			       result.removeClass('ui-datepicker-title');
			       result.addClass('ui-datepicker-titleR');
			       result.prop('align','center');
			       
			       return $("<div/>").append(result).html();
			    };
	})(jQuery);
}


function refreahSilder(){
    // Image Sliders
    if($('.slider-all-controls, .slider-paging-controls, .slider-arrow-controls, .slider-thumb-controls, .logo-carousel').length){
        $('.slider-all-controls').flexslider({
            start: function(slider){
                if(slider.find('.slides li:first-child').find('.fs-vid-background video').length){
                   slider.find('.slides li:first-child').find('.fs-vid-background video').get(0).play(); 
                }
            },
            after: function(slider){
                if(slider.find('.fs-vid-background video').length){
                    if(slider.find('li:not(.flex-active-slide)').find('.fs-vid-background video').length){
                        slider.find('li:not(.flex-active-slide)').find('.fs-vid-background video').get(0).pause();
                    }
                    if(slider.find('.flex-active-slide').find('.fs-vid-background video').length){
                        slider.find('.flex-active-slide').find('.fs-vid-background video').get(0).play();
                    }
                }
            }
        });
        $('.slider-paging-controls').flexslider({
            animation: "slide",
            directionNav: false
        });
        $('.slider-arrow-controls').flexslider({
            controlNav: false
        });
        $('.slider-thumb-controls .slides li').each(function() {
            var imgSrc = $(this).find('img').attr('src');
            $(this).attr('data-thumb', imgSrc);
        });
        $('.slider-thumb-controls').flexslider({
            animation: "slide",
            controlNav: "thumbnails",
            directionNav: true
        });
        $('.logo-carousel').flexslider({
            minItems: 1,
            maxItems: 4,
            move: 1,
            itemWidth: 200,
            itemMargin: 0,
            animation: "slide",
            slideshow: true,
            slideshowSpeed: 3000,
            directionNav: false,
            controlNav: false
        });
    }
}
/**
 * replaceAll
 * @param oldStr
 * @param removeStr
 * @param replaceStr
 * @returns {___anonymous_newStr}
 */
function replaceAll(oldStr, removeStr, replaceStr){
    var myPattern=new RegExp(removeStr,"g");
    newStr =oldStr.replace(myPattern,replaceStr);
    return newStr;
}

/**
 * 設定錯誤訊息(新版)
 * @param objName
 * @param msg
 */
function setEmWarnMsg(objName,msg){
	if(isBobeAlbertlan) {
		var objNameReal = objName.replace('_msg','');
		$('#'+objNameReal).addClass('is-invalid');
		$('#'+objName).html(msg);
		if(!isEmpty(msg)) { $('#'+objName).show() };
	}else{
		$('#'+objName).parent().addClass('error');
		$('#'+objName).html(msg).show();
	}
}

/**
 * 清除錯誤訊息(新版)
 * @param objName
 * @param msg
 */
function clearEmWarnMsg(objName){
	if(isBobeAlbertlan) {
		var objNameReal = objName.replace('_msg','');
		$('#'+objNameReal).removeClass('is-invalid');
		$('#'+objNameReal+'_chosen').removeClass('is-invalid');
		$('#'+objName).html("").hide();
	}else{
		$('#'+objName).parent().removeClass('error');
		$('#'+objName).html("").hide();
	}
}

/**
 * 鎖營幕
 */
function lockScreen(){
	$("div[id^='lockScreenDiv']").remove();
	$("#lockScreenOnload").css('opacity',0);
	$("body").append("<div id='lockScreenDiv' class='b-loading is-mask is-fixed'><img class='m-icon' style='height:55px;width:55px;' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAA5hSURBVHhe5VhpUxRZFk0QZBMEF+xNcWm3UkCBqqIKChAULdHG7qbHjumZ6Jme6JkfMN8nplXAggJk31dFoQR3OmJ+At/r/9w552YmXeAT0UYnJurDiZd58+XLd847975XZYlIUsMYTCYYg8kEYzCZYAwmE4zBZIIxmEwwBpMJxmAywRhMJhiDHxrB2HFPaPGol7i4WOQNLx3xmPp9DBiDHxIgHwZiwGrN4+JVkCdiQMuN5RMfXQhj8Pfis9mTHsBbNHNacfL+KUXZQknLxcfnVkBesPpSs1AkcICAPBG//uR0DCJQCO+3y4cV3z87qPjxxYdxiTH4viicKvEALUAMxFddgPwqyK+Wzh+NA4KVFwghl5+ckyag+WmpgLxcXjwkN5+ejUOAVQICrIL8KsgTMaBlu4UwBt8HIB0m8cP3vfHCmdNSOH1S4AKBAHJs7oRABIEIUrVwXkKPy6R2qQwilMqV5VJpWjonXz0pla+fFguIS/PSUQF5ufnkoEAA+ePy5/LT88MC8nFHiLBpDu8DY/BdUTDjD39237+SPQ7iUyVyaK5CIIQcm/fKqYfl4nlUJuceesW3UCH+xXIJxsohQrnULZXbToAIV5fOyzWI8O3TEvnmWQmdID88OyN/en5W/vrSI397cZoCQIgv2K788z+BbRHBGHwXZE/6wxBgJX3srOyfqRQIIYdmvXLogU+O4vrYvE9OPPBDBJ8UQ4zShTLxLlZAjDIJLp6XmliZ1C+flwaIEIYTmpZLNCW+flYq3z0vle9fFEOEYvnxJYR4dUZ+ghg/PDkgEGBbRDAGt4r0cV8YWLGGjsu+mYAK8Mn9Svl01iefQ4SDIH74AZ3ggxMq5BTas4/KpQzwLnrFDydUP0ZKQIQ6pEQjUwIuuLpcDDeclW+feOTmc4/8+YVH/gIn/PzytPz91Sn5x6sT8t1SzraIYAxuBS751PFzAhcIXCAFs36IEJBP5gLy2ZxPvsB9EYQ4DDd8+dAvxwEP0qJkASIAFUiNStSEqsVS7Aol0hg7I9eelknLy4DcfBGSP7wKScuLAOAHfBCjQn585Ze//xqUn38towBS/Di4Ary3CMbgZrBGAh6gJW0cth/3CVrZNRGUXdMByZ3yyx4IsX+2Ug7M+OTTuUr5ArXgIGrCEaTDUTjiONLg1HyFpoN30ScVi34JLFVL/bNmKYldRt8qOTLfIMcWLsuRh2E5OH9JPsf9p/O18sl8jRQ9qpcTi41SvAQ8DkpwuQau86/sny9rAd55hzAG3wSrPxAG+Zh170zcGiyRtLFKyZwMSdZ4QHZNBSVvulLypgKSDxH24HofhNB0uF8BYl45Og8B5gNwAMg9uiDHHzXLvrkmscaCsmMiJKmT1ZI6EZBUjLdjolIygEyMlzNVCYH9kjvtlV1wVe6MV/LhsAKMmztXhueniDgEiAHv5AZj0AQlPxRaAXmBCLJjrErSx6swyYBkwQFZk1UqQi6I58INu+GG/GmfFGKiRVi9w/MXUAuuwSGXZNdko6SMXhBruAoIijUKQIQ0YOdEleycDEjGVJVkgXzWNNw1iTFnKDBcNodUQ4rtnUXNQb0pfICUg7tAXNImDqsb9t2v2LIIxuBGKPn+wIrVew7kQ5hsNVAFEaplB1Yrg06ACJnjQcnmiqEm7MGEC+dqsfLXpGDiiuSMXRQIaL8/AsIjIK9jOYCYqePVEKEawgIQIRNjZ0GITIpAF2DM3RAhD27YDSHy4bB9IF/4gMXXjxTxwpEn5chCwwrutySCMZgIK1rhAfmY1V2M1ar5jQSvsYIUIX0UbhgLyE7Ug/zpOtSARimcaZJd442SNoC+/SA7CNGG8M4I2lHERnFNIXk/wmsAIlgTTAOMSUAAuoEiZE/SCbYQuSBOEQrUDX64wY9U8kGMCim8X460+ZJtDEK8tSYYg4mAAC1Y+ThEsAkoEVcEriJWDtbNnqyTvdNNkj8eloxhWLwfffvQh/0HcT0ADDvvDjmkR9GqA3jvXDtOUGGRDioCXJWGNMhCiuXQBbNVkjcbBPlKuCAgB+ZCKJZIs0cNXH05AEegjaf31reYOCXCGHShqx+tiFlRrH5/rT3xYYf4EGyM/E0fq5OC6bDsHrsqO4cuidWL533oN4D+/bhOFMB9n+KpCM69OoJx9KEIcJQF4qlIKRLPgbj75xolZ6JOcscbsLV+I0VT38ne0SZJ7b0gaUBm7yXJ7kOfvssC4mJFfcRbXWAMugB5L7BqdZ61CXHSJDJYJSlYzTxYPA/EMwdA/B7iJN6Hflz5PpKus4UbxD3hisBruoHCuE5SN7EgorCCcMZ4veyDowqnbkhGf6PsH/lKsnovi9WF4tlJNIjVDaKdGL8LYCyKsXjN9jbqVWfVKkTwmri5MAZdqP17gnG0DnEKgEo9ekn2TzZLztAVEMcHexHnapM8SRMqBCbVh+tBXCvQ5zURABZECJA7cVkKJptk9zAw0CQ7exokvQviduGdTgJj9RAYl+hmCxEUiEfdGL51F3P+PQKAuAeIWR1QkquI1U3B5HNHYffRa5JC4gQJcrV7Aea9K8AAJkLoc5BVF+C5ikgg14drJWPkItIHKz32tWRghdN6sFtESRaIYvxuvK+k0JIor+9h9TeCz1wB+E4nRI2G4mg3rQPGIGH1hOzV74EtQTB75JLkjVyXXQOwYTcG78EESZrWv4cP3iNZJ+YKMQioG9iCDF0CEdOGkbOjjZIPIXOx0ln3GjFhiELrknQU/UlCrwkKAIIm4i5UgMT+iLV7KcQK8MYt0RzsCYX3jN5YsTrOK6mcobDkIwfTezFoFybprn6iCL2YsAoBMEarE0wFCnIvJGmD9ZIzcgVjXZPMvrBaXFebedtNYOK6ign3tLM+59gOWSOYBglj6DXeaQWHKA5w7eZzweuBnhCsH4pZd8sxSAir9JVkD4bxAQzGQXXF0Sp5hzBXmLFeAh+nGygC77UwVksuiO8ZaYbNGyWVq0VSXRyTk2XLmAPe85l7rcLi2kjcBb/LcRLe49h0VbufiAGv7QjrbjQQrW3BKsetrqDABZLTD/KcrK66+xGScwUgScQJJc4WMSVfI1mDjbJnGCL2o2C6pJU4JskqrsT4Dkh0E7x2nmmh29BnUzj9197jd4B2bInt/jjwWj1Yf9OF1Y/WYvW9kjt4TfdVK4oVVAtyUAcqBmMARXBXXsnTIbA7tsa84etSMNgsKcxppo6b37S+kcA2Q0XGnFhQW8tsF7Su/8W4UQDs+7XYOmr1YKGTViuRsEPczU0qrGLgOUnTFewHEbg95g8zz1ncSJrk+Q6gq2+Y7IcAU0G/iTncLqUTViHAum1xvQDtPlsAWoYHDrUsQaK0FAfEPaE24zUI6T1+yPQ3yO4hVHamDePcv13ibmGioLS6acLbDa0HzhzvoBi2lm1FgJpV6w4LICcMwlqcMABbHRgDctVJ2m3hlIz+S7IPRS61C31Y2XUrc953oXmJcTdO9ENBv4n5cS6tOM904GAEjus4r7uhALRJGwTgqrkTTyxca6lgk0+ByjnYJbjyqbqtsS/6cMWjfHfDpD4m1G2YhzqghAK8pQa0+zw4QsasNhwjuYpKmAMliEBya/avkb1DzZI9cNUhzOfoS+Kq/P9YAM6X8+rgqbAujnbzXUAD7b4WK4ITIM/SzFUtgiCiLQbT2kCiDbqv5/Rhe+tkkWOMHwRodZJXAXDv1g8tmATGME2YWCuU6K/CO2NoSuL5G8Hn6MfVdt9zC2AbdgCufkfVFs4BEfwGiARjKTyaRnAM5qC66pwQBsOAKRg4v++65PSSvB2zyRLoG8WECJ0M38VzF3SGioHnJvA7a0dap79eM47nbwSes68zx7VdigvZgeNwh/k4/FpAg5FgGFix2nCWpggc0BEgBYTyBkG+D7ZX4iTsfPBNk2K/RLxNAL6jgIBKHt8wjp8I9mU/QB2K99q4m9W+kTxhDBKOCHRC3Lp1FoPx5yWOxsM3JAvnePsM73yQULIb4TxTFwCaPsCWBGALImz1O7zfDHhXBcO82lHEb3POvk3JE8agCwjAdGBNiIH4KgTADsE/SNByuyS6LtiIAt1EPa4BXredJ+IAV8ImommACZOsioPrjYgg/W4XE3Hgt29wbBP4bUUdtrlytJhXmw/F3LcpecIY3Ai4wAN4QZznBBtdF2xEnTbxPlrvBTn05zvVLQB+kvJIzfMBhQBJd7W1cFIUB6jYBfznpxvWjeDd7lqMVbf+G90cH9BvuXDmRbRhO2/zbfpXmAtjcLthRbCzMJUiyEk3JdQBFADXmueOAHcDPLsbt6wPAWNwu2F1qBPsI7buDCCvOU/LQwASZyHDj5YMFQXH8a7aTf/K2i4Yg9sNK1JpO+AuBOBK0/omB7BOMFXogM6a/08H6GnSPlJ7rYgfAPmO6pVMEu0AOa72Wg3g6rMGsEV94HVHpRwYusEdB3UjhAIcwBhAm3dLOf2uMAa3CuvWGQ/gVdwp9Vqt5S0gHrPavbS7jUhl3P5vjuRBmvu16wCmAo/Kuo3RBY4gkUr9EwNC4F1nnDYvzvEYv7Xca90usfGLh/hdwhiDWwFIh4EYsKpoxfbTWh7P5KpG+BcU9mISJxnuzSx+rPgkqAeVjXDIsx+F4PE6goLIusEzCN2D8fU7t0ts/OIhYta/PW/d7t4EY/BtsP513APEAAF5Z8/Gr607pZykfQLjxFn1KYACorwT3PcoAISkCBz7Fn7W6rfwzV88AvIERXgvJxiDb4MjwPTa6v/mAPsg4qZAxM8U2ADGNgEPWZGAA/RfSwEcwNxvrHfAKshPf1QBCEcEO/8J5qRdB2y0exMK4bugMgG45xhtGKutwh731rmNNcD7vuQJYzCZYAwmE4zBZIIxmEwwBpMJxmAywRhMJhiDyQRjMJlgDCYPxPov9xj9q/3kkX0AAAAASUVORK5CYII='></div>");
}

/**
 * 解鎖營幕
 */
function unLockScreen(){
	$("#lockScreenOnload").css('opacity',1);
	$("div[id^='lockScreenDiv']").remove();
	
}

/**
 * 鎖營幕(ONLOAD)
 */
function lockScreenOnLoad(){
	$("div[id^='lockScreenOnload']").remove();
	$("body").append("<div id='lockScreenOnload' class='b-loading is-mask is-fixed'><img class='m-icon' style='height:55px;width:55px;' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAA5hSURBVHhe5VhpUxRZFk0QZBMEF+xNcWm3UkCBqqIKChAULdHG7qbHjumZ6Jme6JkfMN8nplXAggJk31dFoQR3OmJ+At/r/9w552YmXeAT0UYnJurDiZd58+XLd847975XZYlIUsMYTCYYg8kEYzCZYAwmE4zBZIIxmEwwBpMJxmAywRhMJhiDHxrB2HFPaPGol7i4WOQNLx3xmPp9DBiDHxIgHwZiwGrN4+JVkCdiQMuN5RMfXQhj8Pfis9mTHsBbNHNacfL+KUXZQknLxcfnVkBesPpSs1AkcICAPBG//uR0DCJQCO+3y4cV3z87qPjxxYdxiTH4viicKvEALUAMxFddgPwqyK+Wzh+NA4KVFwghl5+ckyag+WmpgLxcXjwkN5+ejUOAVQICrIL8KsgTMaBlu4UwBt8HIB0m8cP3vfHCmdNSOH1S4AKBAHJs7oRABIEIUrVwXkKPy6R2qQwilMqV5VJpWjonXz0pla+fFguIS/PSUQF5ufnkoEAA+ePy5/LT88MC8nFHiLBpDu8DY/BdUTDjD39237+SPQ7iUyVyaK5CIIQcm/fKqYfl4nlUJuceesW3UCH+xXIJxsohQrnULZXbToAIV5fOyzWI8O3TEvnmWQmdID88OyN/en5W/vrSI397cZoCQIgv2K788z+BbRHBGHwXZE/6wxBgJX3srOyfqRQIIYdmvXLogU+O4vrYvE9OPPBDBJ8UQ4zShTLxLlZAjDIJLp6XmliZ1C+flwaIEIYTmpZLNCW+flYq3z0vle9fFEOEYvnxJYR4dUZ+ghg/PDkgEGBbRDAGt4r0cV8YWLGGjsu+mYAK8Mn9Svl01iefQ4SDIH74AZ3ggxMq5BTas4/KpQzwLnrFDydUP0ZKQIQ6pEQjUwIuuLpcDDeclW+feOTmc4/8+YVH/gIn/PzytPz91Sn5x6sT8t1SzraIYAxuBS751PFzAhcIXCAFs36IEJBP5gLy2ZxPvsB9EYQ4DDd8+dAvxwEP0qJkASIAFUiNStSEqsVS7Aol0hg7I9eelknLy4DcfBGSP7wKScuLAOAHfBCjQn585Ze//xqUn38towBS/Di4Ary3CMbgZrBGAh6gJW0cth/3CVrZNRGUXdMByZ3yyx4IsX+2Ug7M+OTTuUr5ArXgIGrCEaTDUTjiONLg1HyFpoN30ScVi34JLFVL/bNmKYldRt8qOTLfIMcWLsuRh2E5OH9JPsf9p/O18sl8jRQ9qpcTi41SvAQ8DkpwuQau86/sny9rAd55hzAG3wSrPxAG+Zh170zcGiyRtLFKyZwMSdZ4QHZNBSVvulLypgKSDxH24HofhNB0uF8BYl45Og8B5gNwAMg9uiDHHzXLvrkmscaCsmMiJKmT1ZI6EZBUjLdjolIygEyMlzNVCYH9kjvtlV1wVe6MV/LhsAKMmztXhueniDgEiAHv5AZj0AQlPxRaAXmBCLJjrErSx6swyYBkwQFZk1UqQi6I58INu+GG/GmfFGKiRVi9w/MXUAuuwSGXZNdko6SMXhBruAoIijUKQIQ0YOdEleycDEjGVJVkgXzWNNw1iTFnKDBcNodUQ4rtnUXNQb0pfICUg7tAXNImDqsb9t2v2LIIxuBGKPn+wIrVew7kQ5hsNVAFEaplB1Yrg06ACJnjQcnmiqEm7MGEC+dqsfLXpGDiiuSMXRQIaL8/AsIjIK9jOYCYqePVEKEawgIQIRNjZ0GITIpAF2DM3RAhD27YDSHy4bB9IF/4gMXXjxTxwpEn5chCwwrutySCMZgIK1rhAfmY1V2M1ar5jQSvsYIUIX0UbhgLyE7Ug/zpOtSARimcaZJd442SNoC+/SA7CNGG8M4I2lHERnFNIXk/wmsAIlgTTAOMSUAAuoEiZE/SCbYQuSBOEQrUDX64wY9U8kGMCim8X460+ZJtDEK8tSYYg4mAAC1Y+ThEsAkoEVcEriJWDtbNnqyTvdNNkj8eloxhWLwfffvQh/0HcT0ADDvvDjmkR9GqA3jvXDtOUGGRDioCXJWGNMhCiuXQBbNVkjcbBPlKuCAgB+ZCKJZIs0cNXH05AEegjaf31reYOCXCGHShqx+tiFlRrH5/rT3xYYf4EGyM/E0fq5OC6bDsHrsqO4cuidWL533oN4D+/bhOFMB9n+KpCM69OoJx9KEIcJQF4qlIKRLPgbj75xolZ6JOcscbsLV+I0VT38ne0SZJ7b0gaUBm7yXJ7kOfvssC4mJFfcRbXWAMugB5L7BqdZ61CXHSJDJYJSlYzTxYPA/EMwdA/B7iJN6Hflz5PpKus4UbxD3hisBruoHCuE5SN7EgorCCcMZ4veyDowqnbkhGf6PsH/lKsnovi9WF4tlJNIjVDaKdGL8LYCyKsXjN9jbqVWfVKkTwmri5MAZdqP17gnG0DnEKgEo9ekn2TzZLztAVEMcHexHnapM8SRMqBCbVh+tBXCvQ5zURABZECJA7cVkKJptk9zAw0CQ7exokvQviduGdTgJj9RAYl+hmCxEUiEfdGL51F3P+PQKAuAeIWR1QkquI1U3B5HNHYffRa5JC4gQJcrV7Aea9K8AAJkLoc5BVF+C5ikgg14drJWPkItIHKz32tWRghdN6sFtESRaIYvxuvK+k0JIor+9h9TeCz1wB+E4nRI2G4mg3rQPGIGH1hOzV74EtQTB75JLkjVyXXQOwYTcG78EESZrWv4cP3iNZJ+YKMQioG9iCDF0CEdOGkbOjjZIPIXOx0ln3GjFhiELrknQU/UlCrwkKAIIm4i5UgMT+iLV7KcQK8MYt0RzsCYX3jN5YsTrOK6mcobDkIwfTezFoFybprn6iCL2YsAoBMEarE0wFCnIvJGmD9ZIzcgVjXZPMvrBaXFebedtNYOK6ign3tLM+59gOWSOYBglj6DXeaQWHKA5w7eZzweuBnhCsH4pZd8sxSAir9JVkD4bxAQzGQXXF0Sp5hzBXmLFeAh+nGygC77UwVksuiO8ZaYbNGyWVq0VSXRyTk2XLmAPe85l7rcLi2kjcBb/LcRLe49h0VbufiAGv7QjrbjQQrW3BKsetrqDABZLTD/KcrK66+xGScwUgScQJJc4WMSVfI1mDjbJnGCL2o2C6pJU4JskqrsT4Dkh0E7x2nmmh29BnUzj9197jd4B2bInt/jjwWj1Yf9OF1Y/WYvW9kjt4TfdVK4oVVAtyUAcqBmMARXBXXsnTIbA7tsa84etSMNgsKcxppo6b37S+kcA2Q0XGnFhQW8tsF7Su/8W4UQDs+7XYOmr1YKGTViuRsEPczU0qrGLgOUnTFewHEbg95g8zz1ncSJrk+Q6gq2+Y7IcAU0G/iTncLqUTViHAum1xvQDtPlsAWoYHDrUsQaK0FAfEPaE24zUI6T1+yPQ3yO4hVHamDePcv13ibmGioLS6acLbDa0HzhzvoBi2lm1FgJpV6w4LICcMwlqcMABbHRgDctVJ2m3hlIz+S7IPRS61C31Y2XUrc953oXmJcTdO9ENBv4n5cS6tOM904GAEjus4r7uhALRJGwTgqrkTTyxca6lgk0+ByjnYJbjyqbqtsS/6cMWjfHfDpD4m1G2YhzqghAK8pQa0+zw4QsasNhwjuYpKmAMliEBya/avkb1DzZI9cNUhzOfoS+Kq/P9YAM6X8+rgqbAujnbzXUAD7b4WK4ITIM/SzFUtgiCiLQbT2kCiDbqv5/Rhe+tkkWOMHwRodZJXAXDv1g8tmATGME2YWCuU6K/CO2NoSuL5G8Hn6MfVdt9zC2AbdgCufkfVFs4BEfwGiARjKTyaRnAM5qC66pwQBsOAKRg4v++65PSSvB2zyRLoG8WECJ0M38VzF3SGioHnJvA7a0dap79eM47nbwSes68zx7VdigvZgeNwh/k4/FpAg5FgGFix2nCWpggc0BEgBYTyBkG+D7ZX4iTsfPBNk2K/RLxNAL6jgIBKHt8wjp8I9mU/QB2K99q4m9W+kTxhDBKOCHRC3Lp1FoPx5yWOxsM3JAvnePsM73yQULIb4TxTFwCaPsCWBGALImz1O7zfDHhXBcO82lHEb3POvk3JE8agCwjAdGBNiIH4KgTADsE/SNByuyS6LtiIAt1EPa4BXredJ+IAV8ImommACZOsioPrjYgg/W4XE3Hgt29wbBP4bUUdtrlytJhXmw/F3LcpecIY3Ai4wAN4QZznBBtdF2xEnTbxPlrvBTn05zvVLQB+kvJIzfMBhQBJd7W1cFIUB6jYBfznpxvWjeDd7lqMVbf+G90cH9BvuXDmRbRhO2/zbfpXmAtjcLthRbCzMJUiyEk3JdQBFADXmueOAHcDPLsbt6wPAWNwu2F1qBPsI7buDCCvOU/LQwASZyHDj5YMFQXH8a7aTf/K2i4Yg9sNK1JpO+AuBOBK0/omB7BOMFXogM6a/08H6GnSPlJ7rYgfAPmO6pVMEu0AOa72Wg3g6rMGsEV94HVHpRwYusEdB3UjhAIcwBhAm3dLOf2uMAa3CuvWGQ/gVdwp9Vqt5S0gHrPavbS7jUhl3P5vjuRBmvu16wCmAo/Kuo3RBY4gkUr9EwNC4F1nnDYvzvEYv7Xca90usfGLh/hdwhiDWwFIh4EYsKpoxfbTWh7P5KpG+BcU9mISJxnuzSx+rPgkqAeVjXDIsx+F4PE6goLIusEzCN2D8fU7t0ts/OIhYta/PW/d7t4EY/BtsP513APEAAF5Z8/Gr607pZykfQLjxFn1KYACorwT3PcoAISkCBz7Fn7W6rfwzV88AvIERXgvJxiDb4MjwPTa6v/mAPsg4qZAxM8U2ADGNgEPWZGAA/RfSwEcwNxvrHfAKshPf1QBCEcEO/8J5qRdB2y0exMK4bugMgG45xhtGKutwh731rmNNcD7vuQJYzCZYAwmE4zBZIIxmEwwBpMJxmAywRhMJhiDyQRjMJlgDCYPxPov9xj9q/3kkX0AAAAASUVORK5CYII='></div>");
}

/**
 * 解鎖營幕(ONLOAD)
 */
function unLockScreenOnLoad(){
	$("div[id^='lockScreenOnload']").remove();
}

/**
 * 要被保人姓名格式驗證
 * @param value
 */
function checkNameForTwIdOrLiveId(id,name,errorId){
	if(isEmpty(id)) { return true; } // 不檢核
	
	var twIdValid = validatePersonalID(id);
	var liveIdValid = validateLiveID(id);
	
	if(!twIdValid && !liveIdValid) { return true; } // 不檢核
	
	var nameByteLen = getStringByteLength(name);	// 依 byte 取得長度
	var nameLen = name.length;
	var chineseLen = nameByteLen - nameLen;			// 含有幾個全形字
	var engLen = nameLen - chineseLen;				// 含有幾個半形字
	
	if(nameByteLen > 32){	// 身分證 或 居留證 都限輸入 32個 byte 以內
//		console.log('欄位資料過長，請重新輸入!');
		setEmWarnMsg(errorId,'欄位資料過長，請重新輸入!');
		return false;
	}
	
	if(twIdValid) {	// 若為身分證，名稱必須全中文，不可有空白，不可有特殊符號
		var allChReg=/^[\u4E00-\u9FA5]+$/; 
		if(chkSpecialWords(name) || /\s/.test(name) ){
//			console.log('名稱中不可含有特殊字元!');
			setEmWarnMsg(errorId,'名稱中不可含有特殊字元!');
			return false;
		}
		if(!allChReg.test(name)){ 
//			console.log('姓名限中文!');
			setEmWarnMsg(errorId,'姓名限中文!');
			return false;
		} 
	}
	
	if(liveIdValid) {
		if(chkSpecialWords(name)){
//			console.log('欄名稱中不可含有特殊字元!');
			setEmWarnMsg(errorId,'名稱中不可含有特殊字元!');
			return false;
		}
		
		if( chineseLen >= 2 || engLen >= 3) { 
			// is ok
		}else {
//			console.log('名稱中至少2個中文字 或 至少3個英文字母!');
			setEmWarnMsg(errorId,'名稱中至少2個中文字 或 至少3個英文字母!');
			return false;
		}
	}
	
	return true;
}

/**
 * 檢查特殊字元
 * @param value
 * @returns
 */
function chkSpecialWords( value ) {
	var includeStrAry = ["!","@","#","$","%","^","&","*","(",")","'",";","-","_","=","+","[","]","{","}","\\","|",":","\"","<",",",">",".","?","/","`","~"];
	for(var i = 0 ; i < includeStrAry.length ; i++){
		if(value.indexOf(includeStrAry[i]) > -1){
			return true;
		}
	}
	return false;
}

function filterAlertTitle(msg) {
	if(!isEmpty(msg)){
		if(msg.indexOf('系統忙錄中') > -1) {
			return '很抱歉';
		}
	}
	return '';
}

function filterSqlException(msg){
	if(!isEmpty(msg)){
		if(msg.indexOf('SQL') > -1 || msg.indexOf('sql') > -1){
			return errMsgAjax + '<font color="#f0f0f0">(S)</font>';
		}
	}
	return msg;
}

function scrollToPosition(toObj) {
	
	try{
		var toTopPos = 0;
		if(toObj instanceof Object){
			toTopPos = Math.max($(toObj).offset().top - 100, 0);
		}else{
			toTopPos = Math.max($('#' + toObj).offset().top - 100, 0);
		}
		
		if(document.documentElement.scrollTop > 0) {
			document.documentElement.scrollTop = toTopPos;
		}else if(document.body.scrollTop > 0){
			document.body.scrollTop = toTopPos;
		}
		
		//$('html,body').animate({scrollTop:toTopPos},100);
	}catch(e){}
	
}

function verifyBrowserForDownload(){
	$('a').each(function (index, value){
		var isHasClass = $(this).hasClass('verifyBrowser');	// 如果有想定別的敘述，自行再定的 class
		if(isHasClass){
			var link = $(this).attr('href');
			if(startsWithStr(link,'http') || startsWithStr(link,'/')) {
				if(!chkDeviceVersion()){
					console.log('this can\'t download:' + $(this).html());
					$(this).prop('onclick','');
					$(this).attr('href','javascript:;');
					$(this).attr('target','');
					$(this).on('click', function(e) {
						chkDeviceVersionAndAlert();
					});
				}
			}
		}

	});
}

//檢核瀏覽器版本
function chkDeviceVersion() {
	try{
		var parser = new UAParser();
		if(userAgentStr) {
			parser.setUA(userAgentStr);
	    	var browser = parser.getBrowser();
	    	var engine = parser.getEngine();
	    	var os = parser.getOS();
	    	var device = parser.getDevice();
	    	if(console){
	    		console.log(browser.name + " | " + engine.name + " | " + os.name + " | " + browser.version);
	    	}
	    	if('Android' == os.name && 'Chrome' != browser.name){
	    		//alertMsg('由於您的瀏覽器版本較舊，下載電子文件可能會遇到問題，建議改用Google Chrome操作本服務。');
	    		return false;
	    	}
		}
	}catch(e) {}
	return true;
}

//核核瀏覽器版本，若錯誤直接拋出alertMsg提示 ( 下載要保書用 )
function chkDeviceVersionAndAlert() {
	if(!chkDeviceVersion()) {
		alertMsg('由於您的瀏覽器版本較舊，下載電子文件可能會遇到問題，請至您投保時所填寫的電子信箱中下載要保書。');
	}
}

function startsWithStr(str,condition){
	if (str.match("^"+condition)) {
		return true;
	}
	return false;
}

/**
 * 鎖營幕
 */
function lockScreen(a_msg){

	$("div[id^='lockScreenDiv']").remove();
	$("#lockScreenOnload").css('opacity',0);

	var html = "";
	if(a_msg==undefined || a_msg==""){
		html +="<div id='lockScreenDiv' class='b-loading is-mask is-fixed'>";
		html +="  <div>";
		html +="    <img style='position:absolute;top:0;left:0;right:0;bottom:0;margin:auto;z-index:1000;height:40px;width:40px;' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAA5hSURBVHhe5VhpUxRZFk0QZBMEF+xNcWm3UkCBqqIKChAULdHG7qbHjumZ6Jme6JkfMN8nplXAggJk31dFoQR3OmJ+At/r/9w552YmXeAT0UYnJurDiZd58+XLd847975XZYlIUsMYTCYYg8kEYzCZYAwmE4zBZIIxmEwwBpMJxmAywRhMJhiDHxrB2HFPaPGol7i4WOQNLx3xmPp9DBiDHxIgHwZiwGrN4+JVkCdiQMuN5RMfXQhj8Pfis9mTHsBbNHNacfL+KUXZQknLxcfnVkBesPpSs1AkcICAPBG//uR0DCJQCO+3y4cV3z87qPjxxYdxiTH4viicKvEALUAMxFddgPwqyK+Wzh+NA4KVFwghl5+ckyag+WmpgLxcXjwkN5+ejUOAVQICrIL8KsgTMaBlu4UwBt8HIB0m8cP3vfHCmdNSOH1S4AKBAHJs7oRABIEIUrVwXkKPy6R2qQwilMqV5VJpWjonXz0pla+fFguIS/PSUQF5ufnkoEAA+ePy5/LT88MC8nFHiLBpDu8DY/BdUTDjD39237+SPQ7iUyVyaK5CIIQcm/fKqYfl4nlUJuceesW3UCH+xXIJxsohQrnULZXbToAIV5fOyzWI8O3TEvnmWQmdID88OyN/en5W/vrSI397cZoCQIgv2K788z+BbRHBGHwXZE/6wxBgJX3srOyfqRQIIYdmvXLogU+O4vrYvE9OPPBDBJ8UQ4zShTLxLlZAjDIJLp6XmliZ1C+flwaIEIYTmpZLNCW+flYq3z0vle9fFEOEYvnxJYR4dUZ+ghg/PDkgEGBbRDAGt4r0cV8YWLGGjsu+mYAK8Mn9Svl01iefQ4SDIH74AZ3ggxMq5BTas4/KpQzwLnrFDydUP0ZKQIQ6pEQjUwIuuLpcDDeclW+feOTmc4/8+YVH/gIn/PzytPz91Sn5x6sT8t1SzraIYAxuBS751PFzAhcIXCAFs36IEJBP5gLy2ZxPvsB9EYQ4DDd8+dAvxwEP0qJkASIAFUiNStSEqsVS7Aol0hg7I9eelknLy4DcfBGSP7wKScuLAOAHfBCjQn585Ze//xqUn38towBS/Di4Ary3CMbgZrBGAh6gJW0cth/3CVrZNRGUXdMByZ3yyx4IsX+2Ug7M+OTTuUr5ArXgIGrCEaTDUTjiONLg1HyFpoN30ScVi34JLFVL/bNmKYldRt8qOTLfIMcWLsuRh2E5OH9JPsf9p/O18sl8jRQ9qpcTi41SvAQ8DkpwuQau86/sny9rAd55hzAG3wSrPxAG+Zh170zcGiyRtLFKyZwMSdZ4QHZNBSVvulLypgKSDxH24HofhNB0uF8BYl45Og8B5gNwAMg9uiDHHzXLvrkmscaCsmMiJKmT1ZI6EZBUjLdjolIygEyMlzNVCYH9kjvtlV1wVe6MV/LhsAKMmztXhueniDgEiAHv5AZj0AQlPxRaAXmBCLJjrErSx6swyYBkwQFZk1UqQi6I58INu+GG/GmfFGKiRVi9w/MXUAuuwSGXZNdko6SMXhBruAoIijUKQIQ0YOdEleycDEjGVJVkgXzWNNw1iTFnKDBcNodUQ4rtnUXNQb0pfICUg7tAXNImDqsb9t2v2LIIxuBGKPn+wIrVew7kQ5hsNVAFEaplB1Yrg06ACJnjQcnmiqEm7MGEC+dqsfLXpGDiiuSMXRQIaL8/AsIjIK9jOYCYqePVEKEawgIQIRNjZ0GITIpAF2DM3RAhD27YDSHy4bB9IF/4gMXXjxTxwpEn5chCwwrutySCMZgIK1rhAfmY1V2M1ar5jQSvsYIUIX0UbhgLyE7Ug/zpOtSARimcaZJd442SNoC+/SA7CNGG8M4I2lHERnFNIXk/wmsAIlgTTAOMSUAAuoEiZE/SCbYQuSBOEQrUDX64wY9U8kGMCim8X460+ZJtDEK8tSYYg4mAAC1Y+ThEsAkoEVcEriJWDtbNnqyTvdNNkj8eloxhWLwfffvQh/0HcT0ADDvvDjmkR9GqA3jvXDtOUGGRDioCXJWGNMhCiuXQBbNVkjcbBPlKuCAgB+ZCKJZIs0cNXH05AEegjaf31reYOCXCGHShqx+tiFlRrH5/rT3xYYf4EGyM/E0fq5OC6bDsHrsqO4cuidWL533oN4D+/bhOFMB9n+KpCM69OoJx9KEIcJQF4qlIKRLPgbj75xolZ6JOcscbsLV+I0VT38ne0SZJ7b0gaUBm7yXJ7kOfvssC4mJFfcRbXWAMugB5L7BqdZ61CXHSJDJYJSlYzTxYPA/EMwdA/B7iJN6Hflz5PpKus4UbxD3hisBruoHCuE5SN7EgorCCcMZ4veyDowqnbkhGf6PsH/lKsnovi9WF4tlJNIjVDaKdGL8LYCyKsXjN9jbqVWfVKkTwmri5MAZdqP17gnG0DnEKgEo9ekn2TzZLztAVEMcHexHnapM8SRMqBCbVh+tBXCvQ5zURABZECJA7cVkKJptk9zAw0CQ7exokvQviduGdTgJj9RAYl+hmCxEUiEfdGL51F3P+PQKAuAeIWR1QkquI1U3B5HNHYffRa5JC4gQJcrV7Aea9K8AAJkLoc5BVF+C5ikgg14drJWPkItIHKz32tWRghdN6sFtESRaIYvxuvK+k0JIor+9h9TeCz1wB+E4nRI2G4mg3rQPGIGH1hOzV74EtQTB75JLkjVyXXQOwYTcG78EESZrWv4cP3iNZJ+YKMQioG9iCDF0CEdOGkbOjjZIPIXOx0ln3GjFhiELrknQU/UlCrwkKAIIm4i5UgMT+iLV7KcQK8MYt0RzsCYX3jN5YsTrOK6mcobDkIwfTezFoFybprn6iCL2YsAoBMEarE0wFCnIvJGmD9ZIzcgVjXZPMvrBaXFebedtNYOK6ign3tLM+59gOWSOYBglj6DXeaQWHKA5w7eZzweuBnhCsH4pZd8sxSAir9JVkD4bxAQzGQXXF0Sp5hzBXmLFeAh+nGygC77UwVksuiO8ZaYbNGyWVq0VSXRyTk2XLmAPe85l7rcLi2kjcBb/LcRLe49h0VbufiAGv7QjrbjQQrW3BKsetrqDABZLTD/KcrK66+xGScwUgScQJJc4WMSVfI1mDjbJnGCL2o2C6pJU4JskqrsT4Dkh0E7x2nmmh29BnUzj9197jd4B2bInt/jjwWj1Yf9OF1Y/WYvW9kjt4TfdVK4oVVAtyUAcqBmMARXBXXsnTIbA7tsa84etSMNgsKcxppo6b37S+kcA2Q0XGnFhQW8tsF7Su/8W4UQDs+7XYOmr1YKGTViuRsEPczU0qrGLgOUnTFewHEbg95g8zz1ncSJrk+Q6gq2+Y7IcAU0G/iTncLqUTViHAum1xvQDtPlsAWoYHDrUsQaK0FAfEPaE24zUI6T1+yPQ3yO4hVHamDePcv13ibmGioLS6acLbDa0HzhzvoBi2lm1FgJpV6w4LICcMwlqcMABbHRgDctVJ2m3hlIz+S7IPRS61C31Y2XUrc953oXmJcTdO9ENBv4n5cS6tOM904GAEjus4r7uhALRJGwTgqrkTTyxca6lgk0+ByjnYJbjyqbqtsS/6cMWjfHfDpD4m1G2YhzqghAK8pQa0+zw4QsasNhwjuYpKmAMliEBya/avkb1DzZI9cNUhzOfoS+Kq/P9YAM6X8+rgqbAujnbzXUAD7b4WK4ITIM/SzFUtgiCiLQbT2kCiDbqv5/Rhe+tkkWOMHwRodZJXAXDv1g8tmATGME2YWCuU6K/CO2NoSuL5G8Hn6MfVdt9zC2AbdgCufkfVFs4BEfwGiARjKTyaRnAM5qC66pwQBsOAKRg4v++65PSSvB2zyRLoG8WECJ0M38VzF3SGioHnJvA7a0dap79eM47nbwSes68zx7VdigvZgeNwh/k4/FpAg5FgGFix2nCWpggc0BEgBYTyBkG+D7ZX4iTsfPBNk2K/RLxNAL6jgIBKHt8wjp8I9mU/QB2K99q4m9W+kTxhDBKOCHRC3Lp1FoPx5yWOxsM3JAvnePsM73yQULIb4TxTFwCaPsCWBGALImz1O7zfDHhXBcO82lHEb3POvk3JE8agCwjAdGBNiIH4KgTADsE/SNByuyS6LtiIAt1EPa4BXredJ+IAV8ImommACZOsioPrjYgg/W4XE3Hgt29wbBP4bUUdtrlytJhXmw/F3LcpecIY3Ai4wAN4QZznBBtdF2xEnTbxPlrvBTn05zvVLQB+kvJIzfMBhQBJd7W1cFIUB6jYBfznpxvWjeDd7lqMVbf+G90cH9BvuXDmRbRhO2/zbfpXmAtjcLthRbCzMJUiyEk3JdQBFADXmueOAHcDPLsbt6wPAWNwu2F1qBPsI7buDCCvOU/LQwASZyHDj5YMFQXH8a7aTf/K2i4Yg9sNK1JpO+AuBOBK0/omB7BOMFXogM6a/08H6GnSPlJ7rYgfAPmO6pVMEu0AOa72Wg3g6rMGsEV94HVHpRwYusEdB3UjhAIcwBhAm3dLOf2uMAa3CuvWGQ/gVdwp9Vqt5S0gHrPavbS7jUhl3P5vjuRBmvu16wCmAo/Kuo3RBY4gkUr9EwNC4F1nnDYvzvEYv7Xca90usfGLh/hdwhiDWwFIh4EYsKpoxfbTWh7P5KpG+BcU9mISJxnuzSx+rPgkqAeVjXDIsx+F4PE6goLIusEzCN2D8fU7t0ts/OIhYta/PW/d7t4EY/BtsP513APEAAF5Z8/Gr607pZykfQLjxFn1KYACorwT3PcoAISkCBz7Fn7W6rfwzV88AvIERXgvJxiDb4MjwPTa6v/mAPsg4qZAxM8U2ADGNgEPWZGAA/RfSwEcwNxvrHfAKshPf1QBCEcEO/8J5qRdB2y0exMK4bugMgG45xhtGKutwh731rmNNcD7vuQJYzCZYAwmE4zBZIIxmEwwBpMJxmAywRhMJhiDyQRjMJlgDCYPxPov9xj9q/3kkX0AAAAASUVORK5CYII='>";
		html +="  </div>"
		html +="</div>"
		$("body").append(html);
	}else{
		html +="<div id='lockScreenDiv' class='b-loading is-mask is-fixed'>";
		html +="  <div>";
		html +="    <img style='position:absolute;top:0;left:0;right:0;bottom:0;margin:auto;z-index:1000;height:40px;width:40px;' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAA5hSURBVHhe5VhpUxRZFk0QZBMEF+xNcWm3UkCBqqIKChAULdHG7qbHjumZ6Jme6JkfMN8nplXAggJk31dFoQR3OmJ+At/r/9w552YmXeAT0UYnJurDiZd58+XLd847975XZYlIUsMYTCYYg8kEYzCZYAwmE4zBZIIxmEwwBpMJxmAywRhMJhiDHxrB2HFPaPGol7i4WOQNLx3xmPp9DBiDHxIgHwZiwGrN4+JVkCdiQMuN5RMfXQhj8Pfis9mTHsBbNHNacfL+KUXZQknLxcfnVkBesPpSs1AkcICAPBG//uR0DCJQCO+3y4cV3z87qPjxxYdxiTH4viicKvEALUAMxFddgPwqyK+Wzh+NA4KVFwghl5+ckyag+WmpgLxcXjwkN5+ejUOAVQICrIL8KsgTMaBlu4UwBt8HIB0m8cP3vfHCmdNSOH1S4AKBAHJs7oRABIEIUrVwXkKPy6R2qQwilMqV5VJpWjonXz0pla+fFguIS/PSUQF5ufnkoEAA+ePy5/LT88MC8nFHiLBpDu8DY/BdUTDjD39237+SPQ7iUyVyaK5CIIQcm/fKqYfl4nlUJuceesW3UCH+xXIJxsohQrnULZXbToAIV5fOyzWI8O3TEvnmWQmdID88OyN/en5W/vrSI397cZoCQIgv2K788z+BbRHBGHwXZE/6wxBgJX3srOyfqRQIIYdmvXLogU+O4vrYvE9OPPBDBJ8UQ4zShTLxLlZAjDIJLp6XmliZ1C+flwaIEIYTmpZLNCW+flYq3z0vle9fFEOEYvnxJYR4dUZ+ghg/PDkgEGBbRDAGt4r0cV8YWLGGjsu+mYAK8Mn9Svl01iefQ4SDIH74AZ3ggxMq5BTas4/KpQzwLnrFDydUP0ZKQIQ6pEQjUwIuuLpcDDeclW+feOTmc4/8+YVH/gIn/PzytPz91Sn5x6sT8t1SzraIYAxuBS751PFzAhcIXCAFs36IEJBP5gLy2ZxPvsB9EYQ4DDd8+dAvxwEP0qJkASIAFUiNStSEqsVS7Aol0hg7I9eelknLy4DcfBGSP7wKScuLAOAHfBCjQn585Ze//xqUn38towBS/Di4Ary3CMbgZrBGAh6gJW0cth/3CVrZNRGUXdMByZ3yyx4IsX+2Ug7M+OTTuUr5ArXgIGrCEaTDUTjiONLg1HyFpoN30ScVi34JLFVL/bNmKYldRt8qOTLfIMcWLsuRh2E5OH9JPsf9p/O18sl8jRQ9qpcTi41SvAQ8DkpwuQau86/sny9rAd55hzAG3wSrPxAG+Zh170zcGiyRtLFKyZwMSdZ4QHZNBSVvulLypgKSDxH24HofhNB0uF8BYl45Og8B5gNwAMg9uiDHHzXLvrkmscaCsmMiJKmT1ZI6EZBUjLdjolIygEyMlzNVCYH9kjvtlV1wVe6MV/LhsAKMmztXhueniDgEiAHv5AZj0AQlPxRaAXmBCLJjrErSx6swyYBkwQFZk1UqQi6I58INu+GG/GmfFGKiRVi9w/MXUAuuwSGXZNdko6SMXhBruAoIijUKQIQ0YOdEleycDEjGVJVkgXzWNNw1iTFnKDBcNodUQ4rtnUXNQb0pfICUg7tAXNImDqsb9t2v2LIIxuBGKPn+wIrVew7kQ5hsNVAFEaplB1Yrg06ACJnjQcnmiqEm7MGEC+dqsfLXpGDiiuSMXRQIaL8/AsIjIK9jOYCYqePVEKEawgIQIRNjZ0GITIpAF2DM3RAhD27YDSHy4bB9IF/4gMXXjxTxwpEn5chCwwrutySCMZgIK1rhAfmY1V2M1ar5jQSvsYIUIX0UbhgLyE7Ug/zpOtSARimcaZJd442SNoC+/SA7CNGG8M4I2lHERnFNIXk/wmsAIlgTTAOMSUAAuoEiZE/SCbYQuSBOEQrUDX64wY9U8kGMCim8X460+ZJtDEK8tSYYg4mAAC1Y+ThEsAkoEVcEriJWDtbNnqyTvdNNkj8eloxhWLwfffvQh/0HcT0ADDvvDjmkR9GqA3jvXDtOUGGRDioCXJWGNMhCiuXQBbNVkjcbBPlKuCAgB+ZCKJZIs0cNXH05AEegjaf31reYOCXCGHShqx+tiFlRrH5/rT3xYYf4EGyM/E0fq5OC6bDsHrsqO4cuidWL533oN4D+/bhOFMB9n+KpCM69OoJx9KEIcJQF4qlIKRLPgbj75xolZ6JOcscbsLV+I0VT38ne0SZJ7b0gaUBm7yXJ7kOfvssC4mJFfcRbXWAMugB5L7BqdZ61CXHSJDJYJSlYzTxYPA/EMwdA/B7iJN6Hflz5PpKus4UbxD3hisBruoHCuE5SN7EgorCCcMZ4veyDowqnbkhGf6PsH/lKsnovi9WF4tlJNIjVDaKdGL8LYCyKsXjN9jbqVWfVKkTwmri5MAZdqP17gnG0DnEKgEo9ekn2TzZLztAVEMcHexHnapM8SRMqBCbVh+tBXCvQ5zURABZECJA7cVkKJptk9zAw0CQ7exokvQviduGdTgJj9RAYl+hmCxEUiEfdGL51F3P+PQKAuAeIWR1QkquI1U3B5HNHYffRa5JC4gQJcrV7Aea9K8AAJkLoc5BVF+C5ikgg14drJWPkItIHKz32tWRghdN6sFtESRaIYvxuvK+k0JIor+9h9TeCz1wB+E4nRI2G4mg3rQPGIGH1hOzV74EtQTB75JLkjVyXXQOwYTcG78EESZrWv4cP3iNZJ+YKMQioG9iCDF0CEdOGkbOjjZIPIXOx0ln3GjFhiELrknQU/UlCrwkKAIIm4i5UgMT+iLV7KcQK8MYt0RzsCYX3jN5YsTrOK6mcobDkIwfTezFoFybprn6iCL2YsAoBMEarE0wFCnIvJGmD9ZIzcgVjXZPMvrBaXFebedtNYOK6ign3tLM+59gOWSOYBglj6DXeaQWHKA5w7eZzweuBnhCsH4pZd8sxSAir9JVkD4bxAQzGQXXF0Sp5hzBXmLFeAh+nGygC77UwVksuiO8ZaYbNGyWVq0VSXRyTk2XLmAPe85l7rcLi2kjcBb/LcRLe49h0VbufiAGv7QjrbjQQrW3BKsetrqDABZLTD/KcrK66+xGScwUgScQJJc4WMSVfI1mDjbJnGCL2o2C6pJU4JskqrsT4Dkh0E7x2nmmh29BnUzj9197jd4B2bInt/jjwWj1Yf9OF1Y/WYvW9kjt4TfdVK4oVVAtyUAcqBmMARXBXXsnTIbA7tsa84etSMNgsKcxppo6b37S+kcA2Q0XGnFhQW8tsF7Su/8W4UQDs+7XYOmr1YKGTViuRsEPczU0qrGLgOUnTFewHEbg95g8zz1ncSJrk+Q6gq2+Y7IcAU0G/iTncLqUTViHAum1xvQDtPlsAWoYHDrUsQaK0FAfEPaE24zUI6T1+yPQ3yO4hVHamDePcv13ibmGioLS6acLbDa0HzhzvoBi2lm1FgJpV6w4LICcMwlqcMABbHRgDctVJ2m3hlIz+S7IPRS61C31Y2XUrc953oXmJcTdO9ENBv4n5cS6tOM904GAEjus4r7uhALRJGwTgqrkTTyxca6lgk0+ByjnYJbjyqbqtsS/6cMWjfHfDpD4m1G2YhzqghAK8pQa0+zw4QsasNhwjuYpKmAMliEBya/avkb1DzZI9cNUhzOfoS+Kq/P9YAM6X8+rgqbAujnbzXUAD7b4WK4ITIM/SzFUtgiCiLQbT2kCiDbqv5/Rhe+tkkWOMHwRodZJXAXDv1g8tmATGME2YWCuU6K/CO2NoSuL5G8Hn6MfVdt9zC2AbdgCufkfVFs4BEfwGiARjKTyaRnAM5qC66pwQBsOAKRg4v++65PSSvB2zyRLoG8WECJ0M38VzF3SGioHnJvA7a0dap79eM47nbwSes68zx7VdigvZgeNwh/k4/FpAg5FgGFix2nCWpggc0BEgBYTyBkG+D7ZX4iTsfPBNk2K/RLxNAL6jgIBKHt8wjp8I9mU/QB2K99q4m9W+kTxhDBKOCHRC3Lp1FoPx5yWOxsM3JAvnePsM73yQULIb4TxTFwCaPsCWBGALImz1O7zfDHhXBcO82lHEb3POvk3JE8agCwjAdGBNiIH4KgTADsE/SNByuyS6LtiIAt1EPa4BXredJ+IAV8ImommACZOsioPrjYgg/W4XE3Hgt29wbBP4bUUdtrlytJhXmw/F3LcpecIY3Ai4wAN4QZznBBtdF2xEnTbxPlrvBTn05zvVLQB+kvJIzfMBhQBJd7W1cFIUB6jYBfznpxvWjeDd7lqMVbf+G90cH9BvuXDmRbRhO2/zbfpXmAtjcLthRbCzMJUiyEk3JdQBFADXmueOAHcDPLsbt6wPAWNwu2F1qBPsI7buDCCvOU/LQwASZyHDj5YMFQXH8a7aTf/K2i4Yg9sNK1JpO+AuBOBK0/omB7BOMFXogM6a/08H6GnSPlJ7rYgfAPmO6pVMEu0AOa72Wg3g6rMGsEV94HVHpRwYusEdB3UjhAIcwBhAm3dLOf2uMAa3CuvWGQ/gVdwp9Vqt5S0gHrPavbS7jUhl3P5vjuRBmvu16wCmAo/Kuo3RBY4gkUr9EwNC4F1nnDYvzvEYv7Xca90usfGLh/hdwhiDWwFIh4EYsKpoxfbTWh7P5KpG+BcU9mISJxnuzSx+rPgkqAeVjXDIsx+F4PE6goLIusEzCN2D8fU7t0ts/OIhYta/PW/d7t4EY/BtsP513APEAAF5Z8/Gr607pZykfQLjxFn1KYACorwT3PcoAISkCBz7Fn7W6rfwzV88AvIERXgvJxiDb4MjwPTa6v/mAPsg4qZAxM8U2ADGNgEPWZGAA/RfSwEcwNxvrHfAKshPf1QBCEcEO/8J5qRdB2y0exMK4bugMgG45xhtGKutwh731rmNNcD7vuQJYzCZYAwmE4zBZIIxmEwwBpMJxmAywRhMJhiDyQRjMJlgDCYPxPov9xj9q/3kkX0AAAAASUVORK5CYII='>";
		html +="    <div id='lockScreenDivMsg'>" + a_msg + "</div>";
		html +="  </div>"
		html +="</div>"
		$("body").append(html);
		$("#lockScreenDivMsg").css('top',($(window).height() - $("#lockScreenDivMsg").outerHeight())/2+60);
		$("#lockScreenDivMsg").css('left',($(window).width() - $("#lockScreenDivMsg").outerWidth())/2);
	}

}
