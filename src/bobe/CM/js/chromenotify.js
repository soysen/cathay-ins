(function(window){
 /**
  * @brief Notifier 
  *
  * @param type 1. 一直提示 2，更新提示内容  3， 控制顯示上限。超過個数删除最早的  4，超時更新
  * @param param  本參数在type為3，4時才有效， type為3表示可以最多顯示通知的個數  4， 表示多少秒後删除了
  *
  * @return 
  */
    function Notifier() {};


    window.Notifier = Notifier;
      

    type = 1;
    queue = [];
    t = 5;
    c = 3;
    _notifier = undefined ;

    if(undefined !==  window.webkitNotifications) {
      _notifier = window.webkitNotifications;

    }else if (undefined !== window.Notification)  {
      _notifier = window.Notification;
    } else {
      console.log('error not found notification!')
    }

    window.Notifier.ModelAll = function() {
        type = 1;
    }

    window.Notifier.ModelUpdate =  function() {
        type = 2;
    }

    window.Notifier.ModelCount = function(ct) {
        if(ct !== undefined) c = ct;
        type = 3;
    }

    window.Notifier.ModelTimeout = function(timeout) {
        if(timeout !== undefined) t = timeout;
        type = 4;
    }



    window.Notifier.HasSupport = function() {
       if(undefined === _notifier) {
          return false;
       }

       return true;
    }

    window.Notifier.GetPermission = function() {
        return _notifier.checkPermission();
    }

    window.Notifier.IsGetPermission = function() {
        return (_notifier === 0);
    }

    window.Notifier.Disable = function() {
        return (_notifier.checkPermission === 2);
    }

    window.Notifier.RequestPermission = function(cb) {
      _notifier.requestPermission(function() {
            if(cb) {cb(_notifier.checkPermission() == 0)}
        });
    }



    //type = 1;關才上一個
    window.Notifier.Close = function(type) {
        if(type = 1) {
            tmp = queue.pop();
        } else {
          tmp = queue.shift();
        }
        _closeItem(tmp);
    }

    window.Notifier.ClosePre = function () {
      tmp = queue.pop();
      _closeItem(tmp);
    }

    window.Notifier.CloseLast = function () {
      tmp = queue.shift();
      _closeItem(tmp);
    }

    window.Notifier.CloseAll = function() {
        while(queue.length > 0) {
          var tmp =  queue.shift();
          _closeItem(tmp);
        }
    }


    window.Notifier.Notify = function(icon, title, body) {
      if (this.IsGetPermission() == 0) {


        var popup = _createNotificationAndShow(icon, title, body);
        if(undefined == popup) {
          return false;
        }

        switch(type) {
          case 2:
            if(queue.length > 0) {
              tmp = queue.pop();
              _closeItem(tmp);
            }
            break;
          case 3:
            while(queue.length >= c) {
              tmp = queue.shift();
              _closeItem(tmp);
            }
            break;
          case 4:
            setTimeout(function(){_closeItem(popup);},  t*1000);
            break;
        }

        var q = queue;
        popup.onclose = function(){
            var cur = q.indexOf(popup);
            if(cur >= 0) {
                q.splice(cur, 1);
            }
        };


        popup.onclick = function(){};

        queue.push(popup);
        return true;
      } else {
		    RequestPermission();
		
	    }

      return false;
    }

    function _createNotificationAndShow(icon, title, body) {
      if(undefined != window.webkitNotifications && _notifier.name ===  window.webkitNotifications.name) {
        var n =  _notifier.createNotification(icon, title, body);
        n.show();
        return n;

      }else if (undefined !== window.Notification && _notifier.name ===  window.Notification.name)  {
        return  new _notifier(title, {icon:icon, body: body});
      } else {
        console.log('error not found notification!')
        alert(title +"\n\n"+body);
        return undefined;
      }
    }

    function _closeItem(n) {
      if(undefined == n) {
        return
      }
      if(n.cancel) {
        n.cancel();
      } else {
        n.close();
      }
    }

})(window);

/**
 * 
 
    function support() {
        if(window.Notifier.HasSupport()) {
            alert("你的浏览器支持桌面通知功能！");
        } else {
            alert("你的浏览器不支持桌面通知同能，请你使用chrome浏览器！");
        }
    }
    
    function isGetPermission() {
        if(window.Notifier.IsGetPermission()) {
            alert("网站已经获取浏览器允许发送桌面通知权限！");
        } else {
            alert("需要获取浏览器允许发送桌面通知权限 ！");
        }
    }
    function disable(){
        if(window.Notifier.Disable()) {
            alert("网站被禁止发送桌面通知！");
        } else {
            alert("未被禁止发送桌面通知 ！");
        }
    }
    function requestPermission(){
        window.Notifier.RequestPermission();
        alert("已经发送请求，在可以获取权限的情况下会显示获取权限提示！");
    }
    
    var i = 0;
    function notify() {
        Notifier.Notify("", "通知"+i, "内容" + i++);
    }
    function fall() {
        var obj = document.getElementById("model");
        obj.innerHTML = "<strong>显示所有通知</strong>，但是chrome最多会在屏幕上显示三个，其他处于隐藏状态";
        Notifier.ModelAll();
    }
    function update() {
        var obj = document.getElementById("model");
        obj.innerHTML = "<strong>更新模式</strong>，在上个通知位置显示本次通知";
        Notifier.ModelUpdate();
    }
    function count() {
        var obj = document.getElementById("model");
        obj.innerHTML = "<strong>限制个数模式</strong>，默认是三个，你可以同ModelCount参数改变的，现在为2个";
        Notifier.ModelCount(2);
    }
    function timeout() {
        var obj = document.getElementById("model");
        obj.innerHTML = "<strong>超过时间消失</strong>，现在设置5秒，";
        Notifier.ModelTimeout(2);
    }
    
    function  closePre() {
        Notifier.ClosePre();
    }
    function  closeLast() {
        Notifier.CloseLast();
    }
    function closeAll() {
        Notifier.CloseAll();
    }
*/