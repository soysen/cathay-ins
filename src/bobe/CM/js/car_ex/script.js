jQuery(document).ready(function($) {
	jQuery('.car-type .tab').on('click', 'a', function(){
		if(jQuery(this).hasClass('current')){
			return
		}else{
			var tabName = jQuery(this).data('type')
			console.log(tabName)
			jQuery('.car-type .tab a,.car-type .block,.btn-calc a').removeClass('current')
			jQuery('.car-type .tab .' + tabName + ',.car-type .block.' + tabName + ',.btn-calc .' + tabName).addClass('current')
		}
	})
});