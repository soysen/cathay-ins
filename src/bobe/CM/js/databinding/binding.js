//******************* binding *******************
var Binding = {
    enhance: function (model, autoBinding, options) {
        Object.extend(model, {
            init : function(autoBinding, options) {
                this.registeredBinders = new Array();
                this.registeredListeners = new Array();
                if (autoBinding) {
                    for (var propertyName in model) {
                        var property = model[propertyName];
                        if (!Object.isFunction(property) && !Object.isArray(property)) {
                            var target = options && options[propertyName] && options[propertyName]['target'] ? options[propertyName]['target'] : propertyName;
                            model.bind(propertyName, target, options ? options[propertyName] : null);
                        }
                    }
                }
            },
            setBoundValue : function (propertyName, propertyValue) {
                var oldValue = this[propertyName];
                var binders = this.registeredBinders[propertyName];
                if (binders) {
                    binders.each(function (binder) {
                        var target = binder.target;
                        var accessor = binder.accessor;
                        var transformer = binder.transformer;
                        model[propertyName] = propertyValue;
                        var transformedValue = Object.isFunction(transformer.format) ? transformer.format(propertyValue) : propertyValue;
                        accessor.setValue($(target), transformedValue);
                    });
                }
                var listeners = this.registeredListeners[propertyName];
                if (listeners) {
                    var propertyChangeEvent = new PropertyChangeEvent(this, propertyName, oldValue, propertyValue);
                    listeners.each(function (listener) {
                        var handler = listener.handler;
                        var args = listener.args;
                        if (propertyChangeEvent.newValue != propertyChangeEvent.oldValue) {
                            handler.apply(this, [propertyChangeEvent].concat(args));
                        }
                    });
                }
            },
            bind : function (propertyName, target, options) {
                if (options && options['byName']) {
                    $$('[name="' + target + '"]').each(function (item) {
                        var targetName = item.id ? item.id : target;
                        model.bindById(propertyName, targetName, options);
                    });
                }
                else {
                    model.bindById(propertyName, target, options);
                }
            },
            bindById : function (propertyName, target, options) {
                var registeredBinder = new Binder(propertyName, target, options);
                var binders = this.registeredBinders[propertyName];
                if (!binders) {
                    binders = new Array();
                    this.registeredBinders[propertyName] = binders;
                }
                binders.push(registeredBinder);

                var targetEventName = registeredBinder.targetEventName;
                var oneWay = registeredBinder.oneWay;
                var accessor = registeredBinder.accessor;
                var transformer = registeredBinder.transformer;

                if (!oneWay) {
                    Event.observe($(target), targetEventName,
                            this.changeBindingData.bindAsEventListener($(target), propertyName, model, accessor, transformer));
                }
                var defaultValue = this[propertyName];
                this.setBoundValue(propertyName, Object.isUndefined(defaultValue) ? '' : defaultValue);
            },
            addPropertyChangeListener : function (propertyName, propertyChangeListener) {
                if (!this.registeredListeners[propertyName]) {
                    this.registeredListeners[propertyName] = new Array();
                }
                if (propertyChangeListener) {
                    var args = arguments.length > 2 ? $A(arguments).slice(2) : new Array();
                    this.registeredListeners[propertyName].push(new PropertyChangeListener(propertyChangeListener, args));
                }
            },
            changeBindingData : function (event, propertyName, model, accessor, transformer) {
                var target = event.target;
                var value = accessor.getValue(target);
                var transformedValue = Object.isFunction(transformer.parse) ? transformer.parse(value) : value;
                model.setBoundValue(propertyName, transformedValue);
            },
            toJSON : function () {
                var results = [];
                for (var property in this) {
                    if (property != 'registeredBinders' && property != 'registeredListeners') {
                        var value = Object.toJSON(this[property]);
                        if (value !== undefined)
                            results.push(property.toJSON() + ': ' + value);
                    }
                }

                return '{' + results.join(', ') + '}';
            }});
        model.init(autoBinding, options);

        return model;
    }
};

var PropertyChangeListener = Class.create({
    initialize:function (handler, args) {
        this.handler = handler;
        this.args = args;
    }
});

var PropertyChangeEvent = Class.create({
    initialize:function (source, propertyName, oldValue, newValue) {
        this.source = source;
        this.propertyName = propertyName;
        this.oldValue = oldValue;
        this.newValue = newValue;
    },
    toString: function () {
        var results = new Array();
        for (var property in this) {
            var value = this[property];
            results.push(property + ': ' + value);
        }
        return '{' + results.join(', ') + '}';
    }
});

var Binder = Class.create({
    initialize:function (propertyName, target, options) {
        this.propertyName = propertyName;
        this.target = target;
        this.oneWay = options ? options['oneWay'] : null;
        this.targetEventName = options && options['targetEventName'] ? options['targetEventName'] : getDefaultTargetEventName(target);
        this.accessor = options && options['accessor'] ? options['accessor'] : htmlAccessor;
        this.transformer = options && options['transformer'] ? options['transformer'] : dummyTransformer;
        this.toString = function () {
            return 'propertyName = ' + this.propertyName
                    + ', target = ' + this.target
                    + ', oneWay = ' + this.oneWay
                    + ', targetEventName = ' + this.targetEventName;
        };
    }
}
        );

function getDefaultTargetEventName(target) {
    var targetEventName;
    if (getTagName(target) == 'input' && getType(target).match('checkbox|radio')) {
        targetEventName = 'click';
    }
    else {
        targetEventName = 'change';
    }
    return targetEventName;
}

//******************* accessor *******************

var PropertyAccessor = Class.create({
    initialize : function (propertyName) {
        this.propertyName = propertyName;
    },
    setValue: function (target, value) {
        target[this.propertyName] = value;
    },
    getValue: function (target) {
        return target[this.propertyName];
    }
});

var ValuePropertyAccessor = Class.create(PropertyAccessor, {
    initialize: function () {
        this.propertyName = 'value';
    }
});

var InnerHTMLPropertyAccessor = Class.create(PropertyAccessor, {
    initialize: function () {
        this.propertyName = 'innerHTML';
    }
});

var CheckedPropertyAccessor = Class.create(PropertyAccessor, {
    initialize: function () {
        this.propertyName = 'checked';
    }
});

var RadioPropertyAccessor = Class.create(ValuePropertyAccessor, {
    setValue: function (target, value) {
        if (target.value == value) {
            target.checked = true;
        }
        else {
            target.checked = false;
        }
    }
});

var htmlAccessor = {
    setValue: function (target, value) {
        var tagName = getTagName(target);
        if (tagName == 'input') {
            var type = getType(target);
            if (type == 'checkbox') {
                new CheckedPropertyAccessor().setValue(target, value);
            }
            else if (type == 'radio') {
                new RadioPropertyAccessor().setValue(target, value);
            }
            else {
                new ValuePropertyAccessor().setValue(target, value);
            }
        }
        else if (tagName == 'select') {
            new ValuePropertyAccessor().setValue(target, value);
        }
        else if (tagName == 'textarea') {
                new ValuePropertyAccessor().setValue(target, value);
            }
            else {
                new InnerHTMLPropertyAccessor().setValue(target, value);
            }
    },
    getValue: function (target) {
        var tagName = getTagName(target);
        if (tagName == 'input') {
            var type = getType(target);
            if (type == 'checkbox') {
                return new CheckedPropertyAccessor().getValue(target);
            }
            else if (type == 'radio') {
                return new RadioPropertyAccessor().getValue(target);
            }
            else {
                return new ValuePropertyAccessor().getValue(target);
            }
        }
        else if (tagName == 'select') {
            return new ValuePropertyAccessor().getValue(target);
        }
        else if (tagName == 'textarea') {
            return new ValuePropertyAccessor().getValue(target);
        }
        else {
            return new InnerHTMLPropertyAccessor().getValue(target);
        }
    }
};

function getTagName(elementId) {
    if (!$(elementId)) {
        alert(elementId + 'is not exist');
    }
    return $(elementId).tagName.toLowerCase();
}

function getType(elementId) {
    if (!$(elementId)) {
        alert(elementId + 'is not exist');
    }
    return $(elementId).type.toLowerCase();
}

//******************* tranformer *******************
var dummyTransformer = {
    parse:function (value) {
        return value;
    },
    format:function (value) {
        return value;
    }
};