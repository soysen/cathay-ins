var inputDateTransformer = {
    parse:function (value) {
        return DateUtils.toY2K(value);
    },
    format:function (value) {
        return DateUtils.toInputROC(value);
    }
};

var displayDateTransformer = {
    parse:function (value) {
        return DateUtils.toY2K(value);
    },
    format:function (value) {
        return DateUtils.toROC(value);
    }
};

var amountTransformer = {
    parse:function (value) {
        var x = new String(value).split(',');
        var str = '';
        for (var i = 0; i < x.length; i++) {
            str += x[i];
        }
        return parseFloat(str);
    },
    format:function (value) {
        if (!value || isNaN(value)) {
            value = 0;
        }

        var x = new String(value).split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    }
};

var defaultAmountTransformer = {
    parse:function (value) {
        if (!value || isNaN(value)) {
            value = 0;
        }
        return parseFloat(value);
    },
    format:function (value) {
        if (!value || isNaN(value)) {
            value = 0;
        }
        return parseFloat(value);
    }
};