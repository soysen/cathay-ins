<%@ page contentType="text/html;charset=Big5" language="java" %>
<%@ page import="org.swato.json_rpc.JSONConverter" %>
<%@ page import="com.igsapp.wibc.dataobj.Context" %>
<%@ page import="java.util.HashMap" %>
<%
    java.util.Map outDatas = new HashMap();
    Context.ResponseContext context = (Context.ResponseContext) request.getAttribute("$Attribute.responseContext");
    for (java.util.Enumeration names = context.getOutputDataNames(); names.hasMoreElements();) {
        java.lang.String _name = (java.lang.String) names.nextElement();
        if (!"menuObject".equals(_name)) {
            outDatas.put(_name, context.getOutputData(_name));
        }
    }
    Object resp = (new JSONConverter()).marshall(outDatas);
%>
<html>
    <head>
        <script type="text/javascript" src="<%=request.getContextPath()%>/html/CM/js/ajax/prototype.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/html/CM/js/ajax/CSRUtil.js"></script>

        <script type="text/javascript">
            var FakeAjaxFileUpload = {
                hidePageCover : function () {
                    parent['_frontCover'].hide();
                    var keys = parent.hiddenComboBox.keys();
                    keys.each(function (id) {
                        var cb = parent.hiddenComboBox.unset(id);
                        cb.source.style.visibility = cb.visibility;
                    });
                }
            }


            var resp = <%=resp%>;
            try {
                CSRUtil.defaultAjaxHandler.onComplete(null, null, resp);
            } catch(e) {
                FakeAjaxFileUpload.hidePageCover();
            }

            if (resp && resp.callback) {
                try {
                    parent[resp.callback](resp);
                } catch(e) {
                    alert('Can not find callback [' + resp.callback + ']');
                }
            } else {
                try {
                    // default callback
                    parent.onFileUploadComplete(resp);
                } catch(e) {
                }
            }

        </script>
        
    </head>
    <body>
    </body>
<%@ include file="/html/CM/msgDisplayer.jsp" %>
</html>