$(function () {
	//e.preventDefault();
	//e.stopPropagation();
	
	function watchForHover() {
		var hasHoverClass = false;
		var container = document.body;
		var lastTouchTime = 0;
	
		function enableHover() {
			// filter emulated events coming from touch events
			if (new Date() - lastTouchTime < 500) return;
			if (hasHoverClass) return;
	
			container.className += ' hasHover';
			hasHoverClass = true;
		}
	
		function disableHover() {
			if (!hasHoverClass) return;
	
			container.className = container.className.replace(' hasHover', '');
			hasHoverClass = false;
		}
	
		function updateLastTouchTime() {
			lastTouchTime = new Date();
		}
	
		document.addEventListener('touchstart', updateLastTouchTime, true);
		document.addEventListener('touchstart', disableHover, true);
		document.addEventListener('mousemove', enableHover, true);
	
		enableHover();
	}
	
	watchForHover();

	//dollar
	$('.dollar').each(function () {
		var string = $(this).text();
		var number = Number(string);
		var dollar = (number).toLocaleString('en-US');
		$(this).text(dollar);
	});

	//check click to toggle block
	$('[data-togglecontrol]').on('change',function(){
		var name = $(this).data('togglecontrol');
		var target = $('[data-toggleblock="' + name + '"]');
		if( $(this).prop('checked')){
			target.addClass('showControlOpen');
		}else {
			target.removeClass('showControlOpen');
		}
	});

	//check click to change block
	$('[data-changecontrol]').on('change',function(){
		var name = $(this).data('changecontrol');
		var show = $('[data-changeshowblock="' + name + '"]');
		var hide = $('[data-changehideblock="' + name + '"]');
		if( $(this).prop('checked')){
			show.show();
			hide.hide();
		}else {
			show.hide();
			hide.show();
		}
	});
	
	//radio click to changeblock
	$('[data-radiochangecontrol]').on('change',function(){
		var item =$(this).data('radiochangecontrol');
		var name = $(this).data('radiochangename');
		var inputgroup = $('[data-radiochangegroup="' + name + '"]');
		var inputblock = $('[data-radiochangeblock="' + item + '"]');
		if( $(this).prop('checked')){
			inputgroup.addClass('hidden');
			inputblock.removeClass('hidden');
		}
	});
	
});