var TravelInsurance = (function(window){

    //國家
    var COUNTRIES;
    // var COUNTRIES = '<option value="">請選擇國籍</option><option value="AD">AD安道爾</option><option value="AE">AE阿拉伯聯合酋長國</option><option value="AF">AF阿富汗</option><option value="AG">AG安提瓜及巴布達</option><option value="AI">AI英屬安吉拉</option><option value="AL">AL阿爾巴尼亞</option><option value="AM">AM亞美尼亞</option><option value="AO">AO安哥拉</option><option value="AQ">AQ南極洲</option><option value="AR">AR阿根廷</option><option value="AS">AS美屬薩摩亞</option><option value="AT">AT奧地利</option><option value="AU">AU澳大利亞</option><option value="AW">AW阿魯巴</option><option value="AX">AX奧蘭群島</option><option value="AZ">AZ亞塞拜然</option><option value="BA">BA波斯尼亞及黑塞哥維那</option><option value="BB">BB巴巴多斯 (巴貝多)</option><option value="BD">BD孟加拉</option><option value="BE">BE比利時</option><option value="BF">BF布吉納法索</option><option value="BG">BG保加利亞</option><option value="BH">BH巴林</option><option value="BI">BI布隆迪 (蒲隆地)</option><option value="BJ">BJ貝寧 (貝南)</option><option value="BL">BL聖巴瑟米</option><option value="BM">BM百慕達</option><option value="BN">BN汶萊</option><option value="BO">BO玻利維亞</option><option value="BQ">BQ波奈、聖佑達修斯及沙巴</option><option value="BR">BR巴西</option><option value="BS">BS巴哈馬</option><option value="BT">BT不丹</option><option value="BV">BV波維特島</option><option value="BW">BW博茨瓦納 (波札那)</option><option value="BY">BY白俄羅斯</option><option value="BZ">BZ貝里斯</option><option value="CA">CA加拿大</option><option value="CC">CC可可斯群島</option><option value="CG">CG剛果</option><option value="CH">CH瑞士</option><option value="CI">CI象牙海岸</option><option value="CK">CK庫克群島</option><option value="CL">CL智利</option><option value="CM">CM喀麥隆</option><option value="CN">CN中國大陸</option><option value="CO">CO哥倫比亞</option><option value="CR">CR哥斯大黎加</option><option value="CU">CU古巴</option><option value="CV">CV佛得角 (維德角)</option><option value="CW">CW古拉索</option><option value="CX">CX聖誕島</option><option value="CY">CY賽普路斯</option><option value="CZ">CZ捷克共和國</option><option value="DE">DE德國</option><option value="DJ">DJ吉布地</option><option value="DK">DK丹麥</option><option value="DM">DM多米尼克</option><option value="DO">DO多米尼加共和國</option><option value="DZ">DZ阿爾及利亞</option><option value="EC">EC厄瓜多</option><option value="EE">EE愛沙尼亞</option><option value="EG">EG埃及</option><option value="EH">EH西撒哈拉</option><option value="ER">ER厄立特里亞</option><option value="ES">ES西班牙</option><option value="ET">ET衣索比亞</option><option value="FI">FI芬蘭</option><option value="FJ">FJ斐濟</option><option value="FK">FK福克蘭群島</option><option value="FM">FM密克羅尼西亞聯邦</option><option value="FO">FO法羅群島</option><option value="FR">FR法國</option><option value="GA">GA加彭</option><option value="GB">GB英國</option><option value="GD">GD格瑞納達</option><option value="GE">GE喬治亞</option><option value="GF">GF法屬圭亞那</option><option value="GG">GG根息島</option><option value="GH">GH迦納</option><option value="GI">GI直布羅陀</option><option value="GL">GL格陵蘭</option><option value="GM">GM甘比亞</option><option value="GN">GN幾內亞</option><option value="GP">GP瓜地洛普</option><option value="GQ">GQ赤道幾內亞</option><option value="GR">GR希臘</option><option value="GS">GS南喬治亞島及南三明治群島</option><option value="GT">GT瓜地馬拉</option><option value="GU">GU關島</option><option value="GW">GW幾內亞比索</option><option value="GY">GY圭亞那</option><option value="HK">HK香港</option><option value="HM">HM赫德島及麥當勞群島</option><option value="HN">HN宏都拉斯</option><option value="HR">HR克羅埃西亞</option><option value="HT">HT海地</option><option value="HU">HU匈牙利</option><option value="ID">ID印尼</option><option value="IE">IE愛爾蘭</option><option value="IL">IL以色列</option><option value="IM">IM英屬曼島</option><option value="IN">IN印度</option><option value="IO">IO英屬印度洋地區</option><option value="IR">IR伊朗</option><option value="IS">IS冰島</option><option value="IT">IT義大利</option><option value="JE">JE澤西島</option><option value="JM">JM牙買加</option><option value="JO">JO約旦</option><option value="JP">JP日本</option><option value="KE">KE肯尼亞 (肯亞)</option><option value="KG">KG吉爾吉斯斯坦</option><option value="KH">KH柬埔寨</option><option value="KI">KI吉里巴斯</option><option value="KM">KM科摩羅</option><option value="KN">KN聖基茨及尼維斯</option><option value="KR">KR韓國</option><option value="KW">KW科威特</option><option value="KY">KY開曼群島</option><option value="KZ">KZ哈薩克斯坦</option><option value="LA">LA寮國</option><option value="LC">LC聖露西亞</option><option value="LI">LI列支敦斯登</option><option value="LK">LK斯里蘭卡</option><option value="LR">LR賴比瑞亞</option><option value="LS">LS賴索托</option><option value="LT">LT立陶宛</option><option value="LU">LU盧森堡</option><option value="LV">LV拉脫維亞</option><option value="MA">MA摩洛哥</option><option value="MC">MC摩納哥</option><option value="MD">MD摩爾多瓦共和國</option><option value="ME">ME蒙特內哥羅</option><option value="MF">MF聖馬丁 (法屬)</option><option value="MG">MG馬達加斯加</option><option value="MH">MH馬紹爾群島</option><option value="MK">MK馬其頓共和國</option><option value="ML">ML馬利</option><option value="MM">MM緬甸</option><option value="MN">MN蒙古</option><option value="MO">MO澳門</option><option value="MP">MP北馬里亞納群島</option><option value="MQ">MQ法屬馬丁尼克</option><option value="MR">MR茅利塔尼亞</option><option value="MS">MS蒙哲臘</option><option value="MT">MT馬爾他</option><option value="MU">MU模里西斯</option><option value="MV">MV馬爾地夫</option><option value="MW">MW馬拉威</option><option value="MX">MX墨西哥</option><option value="MY">MY馬來西亞</option><option value="MZ">MZ莫三比克</option><option value="NA">NA納米比亞</option><option value="NC">NC新喀里多尼亞</option><option value="NE">NE尼日</option><option value="NF">NF諾福克島</option><option value="NG">NG尼日利亞 (奈及利亞)</option><option value="NI">NI尼加拉瓜</option><option value="NL">NL荷蘭</option><option value="NO">NO挪威</option><option value="NP">NP尼泊爾</option><option value="NR">NR諾魯</option><option value="NU">NU紐埃</option><option value="NZ">NZ紐西蘭</option><option value="OM">OM阿曼</option><option value="PA">PA巴拿馬</option><option value="PE">PE秘魯</option><option value="PF">PF法屬玻里尼西亞</option><option value="PG">PG巴布亞紐幾內亞</option><option value="PH">PH菲律賓</option><option value="PK">PK巴基斯坦</option><option value="PL">PL波蘭</option><option value="PM">PM聖皮埃及密克隆群島</option><option value="PN">PN皮特康島</option><option value="PR">PR波多黎各</option><option value="PS">PS巴勒斯坦</option><option value="PT">PT葡萄牙</option><option value="PW">PW帛琉</option><option value="PY">PY巴拉圭</option><option value="QA">QA卡塔爾 (卡達)</option><option value="RE">RE留尼旺</option><option value="RO">RO羅馬尼亞</option><option value="RS">RS塞爾維亞</option><option value="RU">RU俄羅斯聯邦</option><option value="RW">RW盧旺達 (盧安達)</option><option value="SA">SA沙烏地阿拉伯</option><option value="SB">SB索羅門群島</option><option value="SC">SC塞席爾</option><option value="SE">SE瑞典</option><option value="SG">SG新加坡</option><option value="SH">SH聖赫勒拿、阿森松及特里斯坦達庫尼亞</option><option value="SI">SI斯洛維尼亞</option><option value="SJ">SJ斯瓦爾巴及揚馬延</option><option value="SK">SK斯洛伐克</option><option value="SL">SL塞拉利昂 (獅子山)</option><option value="SM">SM聖馬利諾</option><option value="SN">SN塞內加爾</option><option value="SR">SR蘇利南</option><option value="ST">ST聖多美及普林西比</option><option value="SV">SV薩爾瓦多</option><option value="SX">SX聖馬丁 (荷屬)</option><option value="SZ">SZ史瓦濟蘭</option><option value="TC">TC土克斯及開科斯群島</option><option value="TD">TD查德</option><option value="TF">TF法屬南部屬地</option><option value="TG">TG多哥</option><option value="TH">TH泰國</option><option value="TJ">TJ塔吉克斯坦</option><option value="TK">TK托克勞</option><option value="TL">TL東帝汶</option><option value="TM">TM土庫曼斯坦</option><option value="TN">TN突尼西亞</option><option value="TO">TO東加</option><option value="TR">TR土耳其</option><option value="TT">TT特立尼達及多巴哥</option><option value="TV">TV吐瓦魯</option><option value="TZ">TZ坦桑尼亞聯合共和國</option><option value="UA">UA烏克蘭</option><option value="UG">UG烏干達</option><option value="UM">UM美屬邊疆群島</option><option value="US">US美國</option><option value="UY">UY烏拉圭</option><option value="UZ">UZ烏玆別克斯坦</option><option value="VA">VA教廷</option><option value="VC">VC聖文森及格瑞納丁斯</option><option value="VE">VE委內瑞拉</option><option value="VG">VG英屬維京群島</option><option value="VI">VI美屬維京群島</option><option value="VN">VN越南</option><option value="VU">VU瓦努阿圖 (萬那杜)</option><option value="WF">WF瓦利斯群島及富圖那群島</option><option value="WS">WS薩摩亞</option><option value="XA">XA其他亞洲國家</option><option value="XE">XE其他歐洲國家</option><option value="XM">XM其他美洲國家</option><option value="XO">XO其餘國家</option><option value="YT">YT馬約特</option><option value="ZA">ZA南非</option><option value="ZM">ZM尚比亞</option><option value="ZW">ZW津巴布韋 (辛巴威)</option>';

    if(window.SYS_COUNTRIES!=undefined){
        COUNTRIES = window.SYS_COUNTRIES;
    }else{
       // alert(window.SYS_COUNTRIES +"不存在");
    }
    //投保額度
    var AMT = {};
    AMT.min = 100
    AMT.max = 500
    AMT.step = 100;

    //保障期間最多180天
    var ISSUE_MAX_DAYS = 180;

    //可承保年齡
    var ISSUE_AGE = {};
    ISSUE_AGE.min = 20;
    ISSUE_AGE.max = 100;

    var TYPE = [];
    TYPE.push({'value':'twid','text':'身份證字號','selected':true});
    TYPE.push({'value':'arcid','text':'居留證'});
    TYPE.push({'value':'taxid','text':'統一編號'});
    TYPE.push({'value':'passportid','text':'護照'});

    var NATIONAL = [];
    NATIONAL.push({'value':'tw','text':'台灣','selected':true});
    NATIONAL.push({'value':'hk','text':'香港'});
    NATIONAL.push({'value':'jp','text':'日本'});
    NATIONAL.push({'value':'kr','text':'韓國'});

    //關係
    var RLTN = [];
    RLTN.push({'value':'1','text':'法定繼承人'});
    RLTN.push({'value':'2','text':'本人'});
    RLTN.push({'value':'3','text':'朋友'});
    RLTN.push({'value':'4','text':'配偶'});

    //IT AJAX
    var FORM_ENV = {};
    FORM_ENV.trial_url = "some.php";
    FORM_ENV.trial_method = "POST";

    var UTILS = function(){
        return {
            numberWithCommas:function(x) {
                x = x.toString();
                var pattern = /(-?\d+)(\d{3})/;
                while (pattern.test(x))
                x = x.replace(pattern, "$1,$2");
                return x;
            },
            trim:function(a_str){
                return a_str.replace(/\s/g, "")
            }
        };
    }();

    var INS = function(){
        return{
            //地址
            initAddress:function(a_target,a_onCountySelect,a_onDistrictSelect){
                var target;
                if(a_target == undefined || a_target.length==0){
                    alert('initAddress: '+a_target +' is not exist');
                    return false;
                }
                target = a_target;
                //
                target.twzipcode({
                    'onCountySelect':a_onCountySelect,
                    'onDistrictSelect':a_onDistrictSelect
                });
                $('select',target).prop("required",true);
                $('select',target).trigger("chosen:updated");
                $('select',target).bind("change",function(e){
                    $('select',target).trigger("chosen:updated");
                })
                
            },
            initAMT:function(a_target,a_max,a_min){
                var target;
                if(a_target == undefined){
                    target = $('*[role="AMT"]');
                }else{
                    target = $('*[role="AMT"]',a_target);
                }
                if(target.length==0)return false;
                
                if(a_max == undefined){
                    a_max = AMT.max;
                }
                
                if(a_min == undefined){
                    a_min = AMT.min;
                }

                target.each(function(){
                    var scope = $(this);
                    var val = $(this).attr("data-val");
                    scope.empty();
                    scope.append('<option value="">請選擇投保額度</option>');
                    for(var i=a_max;i>=a_min;i-=AMT.step){
                        scope.append('<option value="'+i+'">'+UTILS.numberWithCommas(i)+'萬' +'</option>');
                    }
                    if(val!=undefined){
                        scope.val(val);
                    }
                    scope.trigger("chosen:updated");
                })
            },
            initCountries:function(a_target){
                if(window.SYS_COUNTRIES==undefined){
                    alert(window.SYS_COUNTRIES +"不存在");
                }

                var target;
                if(a_target == undefined){
                    target = $('*[role="COUNTRIES"]');
                }else{
                    target = $('*[role="COUNTRIES"]',a_target);
                }
                if(target.length==0)return false;

                target.each(function(){
                    var scope = $(this);
                    scope.empty();
                    scope.append('<option value="">其他國家</option>');
                    scope.html(COUNTRIES);
                    // scope.val(value);
                    scope.trigger("chosen:updated");
                })
            },
            //預產期
            initDueDay:function(a_target){
                 mobiscroll.settings = {
                     theme: 'bootstrap'
                 };
                var target;
                if(a_target == undefined || a_target.length==0){
                    alert('initDueDay: '+a_target +' is not exist');
                    return false;
                }
                target = a_target;
                var now = new Date();
                var minDate = moment(now);
                var maxDate = moment(now).add(280,'d');
                a_target.mobiscroll().date({
                   theme: 'ios',
                   lang: 'zh',
                   dateFormat:'yyyy/mm/dd',
                   max:new Date(maxDate.format('YYYY'),maxDate.format('MM')-1,maxDate.format('DD')),
                   min:new Date(minDate.format('YYYY'),minDate.format('MM')-1,minDate.format('DD')),

                    responsive: {
                        small: {
                            display: 'bottom'
                        },
                        medium: {
                            display: 'bubble'
                        },
                        large: {
                            display: 'bubble'
                        }
                    },
                    onSet: function (event, inst) {
                        var today = moment(new Date());
                        var due_date = moment(event.valueText);
                        
                        var duration = moment.duration(due_date.diff(today));
                        var days = duration.asDays();

                        var error_target = $(a_target.attr("data-error-target"));
                        if(days <= 28*7){
                            error_target.removeClass("d-none");
                        }else{
                            error_target.addClass("d-none");
                        }
                    }
                });

                return false;
            },
            //出生年月日
            initBday:function(a_target,a_min,a_max){
                 mobiscroll.settings = {
                     theme: 'bootstrap'
                 };
                var target;
                if(a_target == undefined || a_target.length==0){
                    alert('initBday: '+a_target +' is not exist');
                    return false;
                }
                target = a_target;
                if(a_min != undefined && !isNaN(a_min)){
                    var ISSUE_MIN_AGE = moment().subtract(a_min, 'year');
                }else{
                    var ISSUE_MIN_AGE = moment().subtract(ISSUE_AGE.min, 'year');
                }

                if(a_max != undefined && !isNaN(a_max)){
                    var ISSUE_MAX_AGE = moment().subtract(a_max, 'year');
                }else{
                    var ISSUE_MAX_AGE = moment().subtract(ISSUE_AGE.max, 'year');
                    
                }
                
                var ISSUE_MAX_MONTH;
                var ISSUE_MAX_DATE;          
                var scope = target;      
                var target_y;
                var target_m;
                var target_d;

                // console.log('ISSUE_MIN_AGE',ISSUE_MIN_AGE.format('YYYY'),ISSUE_MIN_AGE.format('MM'),ISSUE_MIN_AGE.format('DD'));
                // console.log('ISSUE_MAX_AGE',ISSUE_MAX_AGE.format('YYYY'),ISSUE_MAX_AGE.format('MM'),ISSUE_MAX_AGE.format('DD'));
                a_target.mobiscroll().date({
                   theme: 'ios',
                   lang: 'zh',
                   dateFormat:'yyyy/mm/dd',
                   max:new Date(ISSUE_MIN_AGE.format('YYYY'),ISSUE_MIN_AGE.format('MM')-1,ISSUE_MIN_AGE.format('DD')),
                   min:new Date(ISSUE_MAX_AGE.format('YYYY'),ISSUE_MAX_AGE.format('MM')-1,ISSUE_MAX_AGE.format('DD')),

                    responsive: {
                        small: {
                            display: 'bottom'
                        },
                        medium: {
                            display: 'bubble'
                        },
                        large: {
                            display: 'bubble'
                        }
                    }
                });

                return false;
            }
        }
    }();



    var init = function(){

    }

    init();
    return{
        addNewInsured:function(a_target){

            INS.initBday($('*[role="INSBDAY"]',a_target),0,79);
            INS.initBday($('*[role="ASSBDAY"]',a_target));
            INS.initDueDay($('*[role="DUEDAY"]',a_target));
            
            //INS.initAddress($('*[role="twzipcode"]',a_target));
            //INS.initCountries(a_target);
            //INS.initAMT(a_target);

        },
       // updateAMT:function(a_target,a_max,a_min){
    //        INS.initAMT(a_target,a_max,a_min);
      //  },
        initBday:function(a_target){
            INS.initBday(a_target);
        },
        initDueDay:function(a_target){
            INS.initDueDay(a_target);
        },        
       // initAddress:function(a_target,a_onCountySelect,a_onDistrictSelect){
       //     INS.initAddress(a_target,a_onCountySelect,a_onDistrictSelect);
      //  },
       // initCountries:function(){
       //     INS.initCountries();
       // },
       // initAMT:function(){
       //     INS.initAMT();
       // }
    };
})(window);