this.AjaxPagination = function(){

	var element = $("[role='data-loader-container']");
	var loader = element.next().find($("[role='data-loader']"));
	var pagination = $("[role='content-pagination']");
	var _currentPage;
	var _prevURL;
	var _nextURL;
	var _source = loader.attr("data-loader-source");
	var _target = loader.attr("data-loader-target");

    var initBgImgPreplacer = function(){

		var source = $('*[role="bg-img-source"]');
    	var target = $('*[role="bg-img-display"]');

        var url = source.map(function(){
                return this.src
            });

        for(var i=0; i<url.length; i++){
        	$(target[i]).css("background-image","url("+url[i]+")");
        }

    }

	var getCurrentPage = function(){
		//
		var _pagesItem = pagination.find('.page-item');
		var _pagesTotal = _pagesItem.last().find('.page-link').html();
		var _pageActive = pagination.find('.page-item.active .page-link').html();

		_currentPage = parseInt(_pageActive);

		_prevURL = pagination.find('.page-prev a').attr("href");
		_nextURL = pagination.find('.page-next a').attr("href");

		console.log("Pagination:",_currentPage ,"/",_pagesTotal,_prevURL,_nextURL)

		loader.show();
		if(_nextURL == "javascript:;" || _nextURL == "#" || _nextURL == ""){
			console.log("last page")
			loader.hide();
			$(".page-next").addClass("disabled");
		}


		if(_prevURL == "javascript:;" || _prevURL == "#" || _prevURL == ""){
			console.log("first page")
			$(".page-prev").addClass("disabled");
		}
	}
    

    var addEventListener = function(){

		loader.unbind("click").bind("click",function(e){
			e.preventDefault();
			loadMore();
		});

		$(".pagination a").unbind("click").bind("click",function(e){
			e.preventDefault();
			_nextURL = $(this).attr("href");
			switchPage();
			return false;
		});
	};

	var switchPage = function(){

		var _object = _nextURL+" "+_source;

		$("body").append("<div class='loadData d-none'></div>");

		$( ".loadData" ).load( _object , function( response, status, xhr ) {
			//console.log(status);

		  if ( status == "error" ) {
		    var msg = "Sorry but there was an error: ";
		    $( "#error" ).html( msg + xhr.status + " " + xhr.statusText );
		  }else{
		    //console.log(msg);
		    var data = $( ".loadData .row" ).html();
		    var pageData = $( ".loadData .pagination").html();
		    $(""+_target+" .row").html(data);
		    $(_target).find(pagination).html(pageData);

		    getCurrentPage();
		  	initBgImgPreplacer();
		    addEventListener();
		  }

		  $(".loadData").remove();

		});
	}

    var loadMore = function(){

		var _object = _nextURL+" "+_source;

		console.log('_object',_object);
		$("body").append("<div class='loadData d-none'></div>");

		$( ".loadData" ).load( _object , function( response, status, xhr ) {
			//console.log(status);

		  if ( status == "error" ) {
		    var msg = "Sorry but there was an error: ";
		    $( "#error" ).html( msg + xhr.status + " " + xhr.statusText );
		  }else{
		    //console.log(msg);
		    var data = $( ".loadData .row" ).html();
		    var pageData = $( ".loadData .pagination").html();
		    $(""+_target+" .row").append(data);
		    $(_target).find(pagination).html(pageData);

		    getCurrentPage();
		    addEventListener();
		  }

		  initBgImgPreplacer();

		  $(".loadData").remove();

		});
    }


	var init = function(){
		console.log('%cGetData initialized.','background: #222; color: #bada55')
   		addEventListener();
		getCurrentPage();
		$(".pagination").addClass("d-none");
		loader.parent().removeClass("d-none");
	}

	init();
}();