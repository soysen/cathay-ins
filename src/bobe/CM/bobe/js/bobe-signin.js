function doUpdChgPwdTime(loginKind){
    var isNeedToCallCPT =  $('#isNeedToCallCPT').val();

    if(isNeedToCallCPT == 'N' || isNeedToCallCPT == undefined){
        $.fancybox.close();
        return;
    }
    var params = 'loginKind=' + loginKind;
    if(loginKind == 'header'){ params += '&toTrx=' + $('#loginForwardTrx').val(); } 
    
    callAjax(window.ecDispatcher + 'ECM1_0100/updChgPwdTime',params,updChgPwdTimeCallBack,false);
}

function updChgPwdTimeCallBack(data){
    if(!isEmpty(data.error)){
        alertMsg(data.error);
        return;
    }
    $.fancybox.close();
}

function doChgPwd(){
    doSubmitFormEC($('#form1'),window.ecDispatcher + 'ECM1_1000/prompt?tab=7');
}

this.BobeSignIn = function(){

    var ecDispatcher = window.ecDispatcher;
    var form;
    var formData;
	var signin_btn = $('[role="bobe-signin-btn"]');
	var logout_btn = $('[role="bobe-logout-btn"]');
    var submit_btn = $('[role="bobe-signin-form-submit"]');
    var msg_area = $('[role="bobe-signin-form-msg"]');
	var MemberID;
    var MemberPWD;
    var RememberMe;
	var submit_status_text = $(".sending-status",form);
    var isV3 = $("header").hasClass("bobe-bs-v3-compatibity");

    //tracking
    var addTextChangeForTD = function(){
        var TextChangeObj = new Object();
        TextChangeObj.name = 'Bobe';

        switch(formData.type){
            case "laptop":
                TextChangeObj.id = 'Bobe_Menu_Login';
            break;
            case "mobile":
                TextChangeObj.id = 'Bobe_Menu_Login';
            break;
            case "comm":
                TextChangeObj.id = 'Bobe_FancyBox_Login';
            break;

            default:
                return false;
            break;
        }
        
        TextChangeObj.value = MemberID; //帳號欄位的值
        TextChangeObj.tagName = 'input';
        TextChangeObj.type = 'text';
        if(window.cubcsatextchange) {
            window.cubcsatextchange (TextChangeObj);
        }
    }
    var addClickForTD = function (){
        var AddClickObj = new Object();
        AddClickObj.name = 'Bobe';
        AddClickObj.id = 'Bobe_Login_Success';
        AddClickObj.title = 'Bobe會員登入成功';
        AddClickObj.tagName = 'SUBMIT';
        if(window.cubcsaclick) {
            window.cubcsaclick(AddClickObj);
        }
    }

    var addClickForTDcall = function(call_name,call_id,call_title){
        var AddClickObj = new Object();
        AddClickObj.name = call_name;
        AddClickObj.id = call_id;
        AddClickObj.title = call_title;
        AddClickObj.tagName = 'SUBMIT';
        if(window.cubcsaclick) {
            window.cubcsaclick(AddClickObj);
        }
    }

    var displayMsg = function(a_msg){
        msg_area.show();
        msg_area.html(a_msg);
    }
    var doLogin = function(){
        var needRemeber = false;
        var type = form.attr("data-type");

        console.log('type',type);
        formData = {};
        formData.memberId = MemberID.val();
        formData.memberPwd = MemberPWD.val();
        formData.rememberMe = "";
        formData.loginKind = type; //common,laptop,mobile
        if(RememberMe.prop("checked")){
            formData.rememberMe = RememberMe.val();
        }

        switch(type){
            case "laptop":
            case "mobile":
            case "comm":
            break;

            default:
                return false;
            break;
        }

        if(formData.rememberMe){
            //記住使用者ID, 寫入Cookie
            $.cookie.raw = true;
            $.cookie('memberIdCookie', $.base64.encode(formData.memberId), { path:'/', expires: 7 });
        }else{
            //不記住使用者ID, 清除Cookie
            try{
                $.removeCookie('memberIdCookie',{ path:'/'});
            }catch(err){}
        }

        var params = 'loginKind='+formData.loginKind;
        params += '&' + 'memberId=' +formData.memberId;
        params += '&' + 'memberPwd=' + formData.memberPwd;
        params += '&' + 'viewDevice=P';
        
        switch(formData.loginKind){
            case 'laptop':
            case 'mobile':
                params += '&toTrx=' + $('#loginForwardTrx').val();
            break;

            case 'comm':
            break;
        }

        console.log('params',params);
        callAjax(ecDispatcher + 'ECM1_0100/login',params,loginCallBack,false);

        //for demo
        loginCallBack();
    }


    var loginCallBack = function(data){

        //demo password error
        var data = {"ErrMsg":{"returnCode":0,"displayException":"","displayMsgDescs":"","msgDesc":"","sysid":"","length":1,"msgid":"","msgDescs":[""],"type":"","url":""},"error":"您的帳號或密碼有誤，請再次確認並填入正確帳號密碼再登入!","loginKind":"header"}
        

        //demo guest 您尚未加入會員，是否立即加入?
        var data  = {"ErrMsg":{"returnCode":0,"displayException":"","displayMsgDescs":"","msgDesc":"","sysid":"","length":1,"msgid":"","msgDescs":[""],"type":"","url":""},"loginKind":"header","isJoinMember":"Y"};


        //demo success
        var data = "";


        //demo msg
        var data = {'msg':'someerror',isNeedToCallCPT:0};


        if(!isEmpty(data.error)){
            switch(formData.loginKind){
                case 'laptop':
                case 'mobile':
                    displayMsg(data.error);
                break;

                case 'comm':
                    $.fancybox.close();
                    alertMsg(data.error,doOpenCommLogin);
                break;
            }
            lockSubmit(false);
            $.unblockUI();
            return false;
        }

        if(!isEmpty(data.isJoinMember) && data.isJoinMember == 'Y'){
            lockSubmit(false);
            $.unblockUI();
            if(!confirm("您尚未加入會員，是否立即加入?")){
                return false;
            }
            doSubmitFormEC($('#form1'),ecDispatcher + 'ECM1_0400/prompt'); 
            return false;
        }

        if(!isEmpty(data.toTrx)){
            if(data.toTrx.indexOf('/') > -1){
                doSubmitFormEC($('#form1'),ecDispatcher + data.toTrx);
            }else{
                doSubmitFormEC($('#form1'),ecDispatcher + data.toTrx + '/prompt');
            }
            return;
        }


        //login success
        addTextChangeForTD();
        addClickForTD();


        switch(formData.loginKind){
            case 'laptop':
            case 'mobile':
               $("#memberCollapse").collapse('hide');
               $('[role="bobe-user-signin-wrap"]').hide();
               $('[role="bobe-user-logout-wrap"]').show();

                var tmp_form = new Form(form);
                tmp_form.reset();
                form.removeClass("was-validated");

                signin_btn.removeClass("show");
                submit_btn.removeClass("form-sent").removeClass("disabled");
                submit_status_text.text("登入");

                var isV3 = $("header").hasClass("bobe-bs-v3-compatibity");
                if(isV3){
                    var collapseElement = $(".bobe-collapsing");
                    var collapseTrigger = $(".bobe-collapsing-trigger",collapseElement);

                    collapseTrigger.removeClass("show");
                    var target = collapseTrigger.attr("data-target");
                    $(target).removeClass("show");
                }
            break;

            case 'comm':
                $.fancybox.close();
            break;
        }


        //
        $('#commChkPwdTemplateContent').html('');
        if(!isEmpty(data.msg)){
            if(!isEmpty(data.isNeedToCallCPT)){
                $('#isNeedToCallCPT').val(data.isNeedToCallCPT);
            }
            
            $('#commChkPwdTemplateContent').html(data.msg);
            doOpenChkPwd();
            return;
        }

        doCheckLoginStatus();//重新檢核登入狀態
    }

    

    var doOpenChkPwd = function(){
        $.unblockUI();
        $.fancybox.open({
            href : '#commChkPwdTemplate',
            autoSize: false,
            width: "500",
            minHeight: "200",
            maxHeight: "250",
            closeBtn  : false,
            helpers     : { overlay : {closeClick: false} },
        }); 
    }

    var doCheckLoginStatus = function(){
        callAjaxNoBlock(ecDispatcher + 'ECM1_0101/checkloginStatus','',doCheckLoginStatusCallBack,false);
    }

    var doCheckLoginStatusCallBack = function(data){
        if(!isEmpty(data.error)){
            return;
        }
        if("Y" == data.status){ 
            $('#headerloginDiv').hide();  
            $('#headerlogOutDiv').show(); 
            $('#memberCenterLi').show();  
            $('#joinMemberLi').hide();    
            $('#addMemberButton').val('1'); 
        }else{
            $('#headerlogOutDiv').hide();
            $('#headerloginDiv').show();
            $('#memberCenterLi').hide();
            $('#joinMemberLi').show();
            $('#addMemberButton').val('0');
        }
        setTimeout(function(){
            doCheckLoginStatusCallBackCustomer(data.status);//登入或登出自訂事件，請覆寫此 Method
        },300);
    }

    var logout = function(){
        $('[role="bobe-user-signin-wrap"]').show();
        $('[role="bobe-user-logout-wrap"]').hide();
    }

    var validate = function(){
    	form.addClass("was-validated");
    	if(submit_btn.hasClass('disabled')){
    		return false;
    	}
        lockSubmit(true);
    	//
        var tmp_form = new Form(form);

        var isValid = tmp_form.validate();
        console.log('tmp_form.validate',isValid);
        if(isValid){
           doLogin();
        }
        else{
            lockSubmit(false);
        }
        return false;
    }

    var lockSubmit = function(a_boolean){
        if(a_boolean || a_boolean == undefined){
            submit_btn.addClass("form-sent").addClass("disabled");
            submit_status_text.text("登入中 ")

        }else{
            submit_btn.removeClass("form-sent").removeClass("disabled");
            submit_status_text.text("登入");
        }
    }
    var setForm = function(a_form){
        form = a_form;
        MemberID = $('input[name="MemberId"]',form);
        MemberPWD = $('input[name="MemberPwd"]',form);
        RememberMe = $('input[name="RememberMe"]',form);
    }

	var addEventListener = function(){
        console.log(submit_btn.length);
		submit_btn.bind("click",function(e){
			e.preventDefault();
            form = $(this).closest($('[role="bobe-signin-form"]'));
            setForm(form);
			validate();
			return false;
		})

		logout_btn.bind("click",function(e){
			e.preventDefault();
			logout();
		})
	}

    var doOpenCommLogin = function(){

        
        addEventListener();

    }

	var init = function(){
        // doOpenCommLogin();
        $.unblockUI();

        $.fancybox.open({
            href : '#commLoginTemplate',
            autoSize: false,
            width: "400",
            height: "auto",
            beforeShow:function(){
                var isV3 = $("header").hasClass("bobe-bs-v3-compatibity");
                var fancyboxWrap = $(".fancybox-wrap");

                if(isV3){
                   fancyboxWrap.addClass("bobe-bs-v3-compatibity");
                }
            }
        }); 

        var descObj = $('#alertMsgTemplateDesc');
        $(descObj).html("系統發生異常!!");   
        $.fancybox.open({
            href : '#alertMsgTemplate',
            autoSize: false,
            width: "400",
            height: "auto",
            beforeShow:function(){
                var isV3 = $("header").hasClass("bobe-bs-v3-compatibity");
                var fancyboxWrap = $(".fancybox-wrap");

                if(isV3){
                   fancyboxWrap.addClass("bobe-bs-v3-compatibity");
                }
            }
        }); 


        $.fancybox.open({
            href : '#commChkPwdTemplate',
            autoSize: false,
            width: "400",
            height: "auto",
            beforeShow:function(){
                var isV3 = $("header").hasClass("bobe-bs-v3-compatibity");
                var fancyboxWrap = $(".fancybox-wrap");

                if(isV3){
                   fancyboxWrap.addClass("bobe-bs-v3-compatibity");
                }
            }
        }); 
        
        
        addEventListener();
	}


	init();

}


var bobesignin = new BobeSignIn();