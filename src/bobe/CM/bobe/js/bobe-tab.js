this.BobeTab = function(){

	var url = window.location.href;
	var tab = $("[role='bobe-tab-container']");
	var tabNav = tab.find($('.bobe-nav-tabs'));
	var tabNavItem = tabNav.find($('.nav-item'));
	var tabLink = tabNav.find($('.nav-link'));
	var tabContent = tabNav.next('.tab-content');


	var getUrlParams = function ( prop ) {
		var params = {};
		var search = decodeURIComponent( window.location.href.slice( window.location.href.indexOf( '?' ) + 1 ) );
		var definitions = search.split( '&' );

		definitions.forEach( function( val, key ) {
		    var parts = val.split( '=', 2 );
		    params[ parts[ 0 ] ] = parts[ 1 ];
		} );

		// console.log('params[ prop ]',params[ prop ]);
		// console.log('prop && prop in params',prop && prop in params);
		// console.log('params',params)
		// console.log('---------');
		return ( prop && prop in params ) ? params[ prop ] : undefined;
	}

	var getParamsByURL = function ( a_url,prop ) {
		var params = {};
		var search = decodeURIComponent( a_url.slice( a_url.indexOf( '?' ) + 1 ) );
		var definitions = search.split( '&' );

		definitions.forEach( function( val, key ) {
		    var parts = val.split( '=', 2 );
		    params[ parts[ 0 ] ] = parts[ 1 ];
		} );

		// console.log('params[ prop ]',params[ prop ]);
		// console.log('prop && prop in params',prop && prop in params);
		// console.log('params',params)
		// console.log('---------');
		return ( prop && prop in params ) ? params[ prop ] : undefined;
	}

	function addParamsToURL(param){
	    _url = location.href;
	    _url += (_url.split('?')[1] ? '&':'?') + param;
	    return _url;
	}

	var updateURL = function(a_str){
		if (window.history.replaceState) {
		   //prevents browser from storing history with each change:
		   // window.history.replaceState("", "", a_str);
		   window.history.pushState("aa"+a_str, "bb"+a_str, a_str);  
		}
	}

	var scrollTab = function(id){

		if($(window).width() >= 576){
			return false;
		}
		var page_w = $(window).width();
		var tabs_w =  tabNav.width();
		var target_w = tabNavItem.eq(id).width();
		var total = tabNavItem.length;
		var i=0;
		var left = 0;
		for(i=0;i<id;i++){
			var tmp_w = tabNavItem.eq(i).width();
			left +=  tmp_w;
		}
		var gap = (page_w - target_w)/2;
		left -= gap;
		var ratio = (left/tabs_w);
		var pos_x = page_w*ratio;
	    tabNav.animate({scrollLeft: pos_x}, 300);
	}

	var updateTab = function(a_tabID){
		// console.log(a_tabID);
		$('a#'+a_tabID+'-tab').tab('show');
		$(document).scrollTop(0);
	}

	var addEventListener = function(){

		if(tabNav.length<1)
			return false;
		var threshhold = tabNav.offset().top - 55;

		tabLink.on('click',function(){
			//$(document).scrollTop(285);
			var idx = $(this).parent().index();
			scrollTab(idx);
			var new_tabID = $(this).attr("href").replace("#","");
			var tabID = getUrlParams("tab");
			//
			

			if(tabID==undefined){
				var newUrl = addParamsToURL("tab="+new_tabID)
			}
			else{
				var newUrl = location.href.replace("tab="+tabID, "tab="+new_tabID);
			}

			if(!tabLink.parents(".bobe-tab-container").hasClass("bobe-tab-container--no-history")){
				updateURL(newUrl);
			}

			//
			var st = $(window).scrollTop();
			var _windowH = $(window).height();

			//threshhold = threshhold - 55;

			if(st>threshhold){
				 setTimeout(function(){
				 	$(window).scrollTop(threshhold)
				 },200)
			}

		})


		if($(window).width() < 576){


			$(window).scroll(function(){

				var st = $(this).scrollTop();

	          	if (st > threshhold){

					if(!tabNav.parents(".bobe-tab-container").hasClass("bobe-tab-container--no-fix")){
						tabNav.addClass('fixed');
						tabContent.addClass('fixed');
	          		}
				}else{
					tabNav.removeClass('fixed');
					tabContent.removeClass('fixed');
				}

				//console.log(st,_windowH);
			})
			
		}


		window.addEventListener("popstate", function(e) {

			var state = e.state;
			if(state!=null){
				var tabID = getParamsByURL(state,"tab");
				updateTab(tabID);
			}
		});
		
	}

	var init = function(){

		addEventListener();			

		var tabID = getUrlParams("tab");
		if(tabID!=undefined){
			updateTab(tabID);
		}
	}

	if(tab.length){
		init();
	}

}


var bobetab = new BobeTab();