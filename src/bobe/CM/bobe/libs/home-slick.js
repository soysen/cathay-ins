(function ($) {
    //update banner size

    $.fn.autoImageCenter = function (options) {
        var w = $(window).width();
        var ori_w = 2000;
        var ori_h = 400;
        var banner_h = 400;
        var banner_w = 2000;
        var ratio = w / ori_w;
        var breakpointTablet = 992;
        var breakpointMobile = 576;

        var w_h_ratio = ori_h / ori_w;


        if (w > ori_w) {

            banner_w = ratio * ori_w;
            banner_h = ratio * ori_h;

        }else{


            if(  w < breakpointTablet ){

                    banner_w = ori_w * 0.75;
                    banner_h = ori_h * 0.75;

            }


            if( w < breakpointMobile){

                    banner_w = ori_w * 0.5;
                    banner_h = ori_h * 0.5;
            }

        }

        this.height(banner_h);

        this.find('img').each(function (idx, elm) {

            $(elm).width(banner_w);
            $(elm).height(banner_h);
            var _w = banner_w;
            if (w > banner_w) {
                var diffW = 0;
            } else {
                var diffW = w - _w;
            }
            $(elm).css('margin-left', diffW / 2);
        });

        //console.log("banner_w=",banner_w,"banner_h=",banner_h,"ratio=",ratio,"w=",w,"breakpointTablet=",breakpointTablet);

    }

    $.fn.homeslick = function (options) {
        if (typeof $.fn.slick === 'undefined')
            return;

        var $container = this,
            $btns = null,
            slickOptions = {
                draggable: false
            };

        if (options.autoplay)
            slickOptions.autoplay = options.autoplay;

        if (options.dots) {
            slickOptions.dots = options.dots;
        }

        $container.on('init', function () {
            $container.autoImageCenter();
            $container.fadeTo(1000, 1);
        });

        $container.slick(slickOptions);

        if (options.btns) {
            $btns = $(options.btns);

            $container.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                $btns.removeClass('active').eq(nextSlide).addClass('active');
            });

            //btns click event
            $btns.on('click', function (e) {
                e.stopPropagation();
                var idx = $btns.index(this);
                $container.slick('slickGoTo', idx);
            });
        }
        
        
        //resize event
        $(window).resize(function () {
            $container.autoImageCenter();
        });


        return this;
    }

})(jQuery);