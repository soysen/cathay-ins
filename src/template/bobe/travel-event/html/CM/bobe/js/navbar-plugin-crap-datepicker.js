this.NavbarPlugin = function(){

	var bs_collapse = $('.bs_collapse');
	var bs_collapse_trigger = $('[data-toggle="collapse"]');
	var bs_nav_collapse = $(".navbar-collapse");

	//dropdown
	var dropdownElement = $('#navbarSupportedContent .dropdown');

	//collapsing
	var collapseElement = $(".bobe-collapsing");
	var collapseTrigger = $(".bobe-collapsing-trigger",collapseElement);


	var dateRangePicker = function(){


		var input = $('.input-date input');
		input.closest('.custom-input-daterange').prepend('<div class="input-date-wrap"><hr><div class="datepicker"/></div></div>');

		var datepicker = $('.custom-input-daterange .datepicker');

		var wrap;
		var start='';
		var end='';
		input.on('focus',function(){

			var $this = $(this);
			wrap = $this.parent().siblings('.input-date-wrap');
			console.log($this,wrap)
			wrap.show()

		})

		// $('#datetimepicker6').datetimepicker();
  //       $('#datetimepicker7').datetimepicker({
  //           useCurrent: false //Important! See issue #1075
  //       });
  //       $("#datetimepicker6").on("dp.change", function (e) {
  //           $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
  //       });
  //       $("#datetimepicker7").on("dp.change", function (e) {
  //           $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
  //       });

  		var now = moment().format('MM/DD/YYYY');
		datepicker.datetimepicker({
			inline:true,
			format:'YYYY/MM/DD',
			minDate:now
		})
		$(".day").removeClass("active");
		datepicker.on("dp.change", function (e) {
			//console.log(e.oldDate.format('YYYY/MM/DD'),e.date.format('YYYY/MM/DD'));

			if(start ==""){
				start = e.date.format('MM/DD/YYYY');
				end = '';
				$(this).data("DateTimePicker").minDate(e.date);
				var targetA = $('.datepicker-days .day[data-day="'+start+'"]')
				targetA.css('background-color','#ff0000');
				console.log('targetA',targetA.length)
			}else{
				$(this).data("DateTimePicker").minDate(now);
				start = e.oldDate.format('MM/DD/YYYY');
				end = e.date.format('MM/DD/YYYY');
				var targetA = $('.datepicker-days .day[data-day="'+start+'"]')
				var targetB = $('.datepicker-days .day[data-day="'+end+'"]')

				targetA.css('background-color','#ff0000');
				targetB.css('background-color','#dd0000');

                var now = moment(e.date);
                var end = moment(e.oldDate);
                var duration = moment.duration(now.diff(end));
                var days = duration.asDays();
                

				for(var i=1;i<days;i++){
					var day = moment(e.oldDate).add(i,"d").format('MM/DD/YYYY');
					var target = $('.datepicker-days .day[data-day="'+day+'"]')
					target.css('background-color','#00dd00');
				}
				start="";
				end="";
				// $(this).parent().hide();
			}
			// target.
			//start
			//$(this).data("DateTimePicker").minDate(e.date);
			//

			//

			//end
			//$(this).parent().hide();
        });

		 // $('#startDate').datetimepicker({
   //          useCurrent: false;
   //      });
		 // $('#startDate').on("dp.change", function (e) {
   //         alert(e.date);
   //      });
		// var wrap = $('.custom-input-daterange');



	}();

	var selectPlugin = function(){

		var body = $('body');

		//initial html and data
		var select = $('.custom-select');

		select.each(function(){

			var $this = $(this);

			var options = $('option',$this);
			var value = $.map(options ,function(option) {
			    return option.value;
			});
			var text = $.map(options ,function(option) {
			    return option.text;
			});

			$this.wrap('<div class="custom-fake-select"></div>')
			var wrap = $this.parent($('.custom-fake-select'))
			wrap.prepend('<div class="custom-select" tabindex="0"><span></span></div><ul class="custom-fake-select-list"></ul>')
			var selectedwrap = wrap.find('span')
			var list = wrap.find('.custom-fake-select-list')

			for( var i=0; i < options.length; i++){
				list.append('<li data-value='+value[i]+'>'+text[i]+'</li>')
			}

			var listOptions = wrap.find($('.custom-fake-select-list li'))

			//set default value
			selectedwrap.text(text[0]).attr('data-value',value[0])
			$(listOptions[0]).addClass('selected')

		})

		var fakeselect = select.siblings('div');
		var list = select.siblings('ul');
		var option = list.find('li')

		fakeselect.on('click',function(e){

			var $this = $(this);

			list.hide()
			$this.addClass('onchange');
			var nowlist = $this.next('.custom-fake-select-list')
			nowlist.show()
			e.stopPropagation()

		})

		option.on('click',function(e){

			var $this = $(this);
			option.removeClass('selected');
			$this.addClass('selected')

			var t = $this.text();
			var v = $this.attr('data-value');

			$this.parent().prev().find('span').text(t).attr('data-value',v)

			list.hide();

			console.log(t,v)

		})


		body.on('click',function(e){
	      if(e.target.id !== fakeselect){
	        list.hide();
	      }    
		})

	}();


	// $(window).resize(initDropdownhover());

	var addEventListener = function(){

		if($(window).width() > 992){

		    dropdownElement.hover(function() {
		      $(this).find('a.nav-link').attr('aria-expanded','true');
		      $(this).find('.dropdown-menu').addClass('show');
		    }, function() {
		      $(this).find('.dropdown-menu').removeClass('show');
		      $(this).find('a.nav-link').attr('aria-expanded','false');
		    });

		}

		collapseTrigger.bind('click',function(){
			$(this).toggleClass("show");
			var target = $(this).attr("data-target");
			$(target).toggleClass("show");
		})	

		bs_collapse_trigger.click(function(){
			bs_collapse.collapse('hide');
		})

		bs_nav_collapse.on('show.bs.collapse',function(){
			$(this).addClass('collapsed');
			$('body').addClass("body-locked");
		}).on('hide.bs.collapse hidden.bs.collapse',function(){
			$(this).removeClass('collapsed');
			$('body').removeClass("body-locked");
		})

		var _pos;
		var _body = $('body');

		bs_collapse.on('show.bs.collapse',function(){

			_pos = $(window).scrollTop();
			_body.css({'position':'fixed','top': -_pos});
			_body.addClass("body-locked");


		}).on('hide.bs.collapse',function(){

			_body.removeClass("body-locked");
			_body.css({'position':'relative','top': 'auto'});

			$(window).scrollTop(_pos);

		})

	}

	var init = function(){
		addEventListener();
		console.log('%cNavbar Plugin initialized.','background: #222; color: #bada55')
	}

	return{
		init:init
	}
}

