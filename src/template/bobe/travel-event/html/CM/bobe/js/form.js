var Form = (function(){

  var type;
  var isOldVersion = false;
  function Form(a_form){
    this._form = a_form;
  }

  function isNotEmpty(a_str){
    return ($.trim(a_str)!="");
  }

  function toggleDisable(a_elm,a_boolean){
      if(a_boolean){
          a_elm.addClass("disabled");
      }else{
          a_elm.removeClass("disabled");
      }
  }

  function toggleError(a_elm,a_boolean,a_customMsg){

   // console.log('toggleError',type,isOldVersion);
   // console.log('a_customMsg',a_customMsg);

    // if(a_customMsg!=undefined){

    //   switch(type){
    //     case "laptop":
    //     case "mobile":
    //     case "header":
    //       if(isOldVersion){
    //       }
    //       else{

    //       }
    //     break;

    //     case "comm":
    //     break;

    //   }
    //   a_elm.attr("data-msg","");
    // }

    switch(type){
      case "laptop":
      case "mobile":
      case "header":

        //console.log('isOldVersion',isOldVersion);
        if(isOldVersion){
          var group = a_elm.parents(".form-row");
          group.removeClass("has-error").removeClass("has-danger");
          group.find(".help-block").html(a_elm.attr("data-error"));
          if(a_customMsg!=undefined){
            a_elm.attr("data-error",a_customMsg)
          }
          if(a_boolean){
              if(a_customMsg!=undefined){
                a_elm.parent(".form-row").find(".invalid-feedback").html(a_customMsg)
              }
              group.addClass("has-error").addClass("has-danger");
          }else{
              group.removeClass("has-error").removeClass("has-danger");
          }
        }else{
          a_elm.removeClass('is-valid');
          a_elm.removeClass('is-invalid');
          if(a_boolean){
              if(a_customMsg!=undefined){
                a_elm.parent(".form-row").find(".invalid-feedback").html(a_customMsg)
              }
              a_elm.addClass("is-invalid");
          }else{
              a_elm.removeClass("is-invalid");
          }
        }
      break;

      case "comm":
          a_elm.removeClass('is-valid');
          a_elm.removeClass('is-invalid');
          if(a_boolean){
              if(a_customMsg!=undefined){
                a_elm.parent(".form-row").find(".invalid-feedback").html(a_customMsg)
              }
              a_elm.addClass("is-invalid");
          }else{
              a_elm.removeClass("is-invalid");
          }
      break;

      default:
          a_elm.removeClass('is-valid');
          a_elm.removeClass('is-invalid');
          
          var custom_target = a_elm.attr("data-error");
          var custom_display_style = a_elm.attr("data-error-display-style");
          if(a_boolean){

              if(a_customMsg!=undefined){
                a_elm.parents(".form-row").find(".invalid-feedback").html(a_customMsg)
              }
              a_elm.addClass("is-invalid");
              if(custom_target!=undefined){
                if(custom_display_style!=undefined){
                  $('*[role='+custom_target+']').addClass(custom_display_style);
                }else{
                  $('*[role='+custom_target+']').show();
                }
              }
          }else{
              a_elm.removeClass("is-invalid");
              if(custom_target!=undefined){
                if(custom_display_style!=undefined){
                  $('*[role='+custom_target+']').removeClass(custom_display_style);
                }else{
                  $('*[role='+custom_target+']').hide();
                }
              }
          }
      break;
    }
  }

  function isEmail(a_str){
    a_str = $.trim(a_str);
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(a_str);
  }
  function isChecked(a_elm){
    return a_elm.prop("checked")
  }

  function isTWPhone(a_num){
    if ( !a_num.match(/^09/) ) 
    { 
      return false; 
    }
    return isNumeric(a_num);
  }

  function isValidPwd(a_value){
    return chkPassword(a_value);
  }

  function isSelected(a_elm){
    
    var val = a_elm.val();
    if($.trim(val)==""){
      return false;
    }
    return true;
  }
  function isRadioSelected(a_name){
    return $('input[name='+a_name+']:checked').val();
  }
  function isNumeric(a_str){
    a_str = $.trim(a_str);
    return !isNaN(a_str) && ($.trim(a_str)!="");
  }
  function isDate(a_str){
    if(a_str.length!=8)
      return false;
    var yyyy = a_str.substr(0,4);
    var mm = a_str.substr(4,2);
    var dd = a_str.substr(6,2);
    var date = yyyy+'/'+mm+'/'+dd;
    var r = (new Date(date) !== "Invalid Date") && !isNaN(new Date(date)); 
    return r
  }

  function isValidIdLength(value){
    if(value.length!=10)
      return false;
    return true;
  }

  function isTWId(value){
    value = new String(value);
    value = value.toLowerCase();
    var RE = new RegExp("^[a-z][12][0-9]{8}$");

    if (RE.test(value)) {
        h = "abcdefghjklmnpqrstuvxywzio";
        x = 10 + h.indexOf(value.substring(0, 1));
        chksum = (x - (x % 10)) / 10 + (x % 10) * 9;
        for (i=1; i<9; i++) {
            chksum += value.substring(i, i+1) * (9 - i);
        } 
        chksum = (10 - chksum % 10) % 10;
        if (chksum == value.substring(9, 10))
            return true;
    }
    return isResideId(value)
    // return false;
  }

  function isResideId(id){
    try{
      if(id==""){
        return false;
      }
      if (id.length != 10) return false;

      id = id.toUpperCase();

      if (isNaN(id.substring(2,10)) || (id.substring(0,1)<"A" ||id.substring(0,1)>"Z") || (id.substring(1,2)<"A" ||id.substring(1,2)>"Z")){
        return false;         
      }
      
      var head="ABCDEFGHJKLMNPQRSTUVXYWZIO";
      id = (head.indexOf(id.substring(0,1))+10) +''+ ((head.indexOf(id.substring(1,2))+10)%10) +''+ id.substring(2,10);
      s =parseInt(id.substring(0,1)) + 
      parseInt(id.substring(1,2)) * 9 + 
      parseInt(id.substring(2,3)) * 8 + 
      parseInt(id.substring(3,4)) * 7 +       
      parseInt(id.substring(4,5)) * 6 + 
      parseInt(id.substring(5,6)) * 5 + 
      parseInt(id.substring(6,7)) * 4 + 
      parseInt(id.substring(7,8)) * 3 + 
      parseInt(id.substring(8,9)) * 2 + 
      parseInt(id.substring(9,10)) + 
      parseInt(id.substring(10,11));

      //判斷是否可整除
      if ((s % 10) != 0) return false;
      //居留證號碼正確   
      return true;
      }
      catch(err){
          return false;
      }
  }
  function isValidate(a_elm,a_boolean,a_customMsg){
    toggleError(a_elm,!a_boolean,a_customMsg);
  }
  
  function validate(a_target){
    if($("header").hasClass("bobe-bs-v3-compatibity")){
      isOldVersion = true;
    }
    var status = true;
    a_target.removeClass('is-invalid');
    var required = a_target.prop('required');
    var name = a_target.attr("name");
    var custom_msg = a_target.attr("data-msg");

    if(required){
      var type = a_target.attr("type");
      var option = a_target.attr("type");
      if(a_target.is('input')){
        switch(type){
          case "text":
          case "tel":
          case "email":
          case "password":
            var val = a_target.val();
            var format = a_target.attr("data-format");

            switch(format){
              
              case "twid":

                if(isNotEmpty(val)){
                  if(isTWId(val)){
                    isValidate(a_target,true);
                  }else{
                    status = false;
                    isValidate(a_target,false,"身份證字號錯誤！");
                  }

                }else{
                  status = false;
                  isValidate(a_target,false,"請輸入身份證字號！");
                }

                
              break;

              
              case "pwd":
                if(isNotEmpty(val)){
                  if(isValidPwd(val)){
                    isValidate(a_target,true);
                  }else{
                    status = false;
                    isValidate(a_target,false,"密碼需為6~12位英數混合之半形字元");
                  }
                  
                }else{
                  status = false;
                  isValidate(a_target,false,"請輸入密碼！");
                }
              break;

              case "repwd":
                var t = $(a_target.attr("data-target"));
                if(isNotEmpty(val)){
                  if(isValidPwd(val)){
                    if(t.val() == val){
                      isValidate(a_target,true);  
                    }else{
                    status = false;
                      isValidate(a_target,false,"密碼與第1次的不符");  
                    }
                  }else{
                    status = false;
                    isValidate(a_target,false,"密碼需為6~12位英數混合之半形字元");
                  }
                }else{
                  status = false;
                  isValidate(a_target,false,"請輸入密碼！");
                }
              break;

              case "validid":
                if(isValidIdLength(val)){
                  isValidate(a_target,true);
                }else{
                  status = false;
                  isValidate(a_target,false);
                }
              break;

              case "numeric-limit":
                if(isNumeric(val)){
                  var max = parseInt(a_target.attr("data-max"));
                  var min = parseInt(a_target.attr("data-min"));

                  if(val<min || val>max ){
                    status = false;
                    isValidate(a_target,false);
                  }else{
                    isValidate(a_target,true);
                  }
                }else{
                    status = false;
                    isValidate(a_target,false);
                }
              break;

              case "numeric":
                if(isNotEmpty(val)){
                  if(isNumeric(val)){
                    isValidate(a_target,true);
                  }else{
                    status = false;
                    isValidate(a_target,false,"驗證碼為4碼數字");
                  }
                }
                else{
                  status = false;
                  isValidate(a_target,false);
                }
              break;

              case "twPhone":
                if(isTWPhone(val)){
                  isValidate(a_target,true);
                }else{
                  status = false;
                  isValidate(a_target,false);
                }

              break;
              case "date":
                if(isDate(val)){
                  isValidate(a_target,true);
                }else{
                  status = false;
                  isValidate(a_target,false);
                }
              break;

              case "email":
                var val = a_target.val();
                if(isNotEmpty(val)){
                  if(isEmail(val)){
                    isValidate(a_target,true);
                  }else{
                    status = false;
                    isValidate(a_target,false,"Email格式錯誤！");
                  }
                }else{
                  status = false;
                  isValidate(a_target,false,"請輸入Email");
                }
              break;

              default:
                if(isNotEmpty(val)){
                  isValidate(a_target,true);
                }else{
                  status = false;
                  isValidate(a_target,false);
                }
              break;
            }
          break;

          case "date":
            var val = a_target.val();
            if(isNotEmpty(val)){
              isValidate(a_target,true);
            }else{
              status = false;
              isValidate(a_target,false);
            }
          break;

          // case "email":
          //   var val = a_target.val();
          //   // console.log('email',val)
          //   // console.log('isEmail(val)',isEmail(val));
          //   if(isEmail(val)){
          //     isValidate(a_target,true);
          //   }else{
          //     status = false;
          //     isValidate(a_target,false);
          //   }
          // break;

          case "checkbox":
            if(isChecked(a_target)){
              isValidate(a_target,true);
            }else{
              status = false;
              isValidate(a_target,false);
            }
          break;

          case "radio":
            if(isRadioSelected(name)){
              console.log('custom_msg',custom_msg);
              isValidate(a_target,true,custom_msg);
            }else{
              status = false;
              isValidate(a_target,false);
            }
          break;
        }
      }

      if(a_target.is('select')){
          if(isSelected(a_target)){
            isValidate(a_target,true);
          }else{
            status = false;
            isValidate(a_target,false);
          }
      }

    }

    return status;
  }
  Form.prototype.reset = function(){
  // console.log(this._form);
  // console.log(this._form.reset);
  //   console.log(this._form[0].reset);
  //   // this._form[0].reset();

    $("input",this._form).val("");
    $("input",this._form).removeClass('is-valid');
    $("input",this._form).removeClass('is-invalid');
  }

  Form.prototype.getData = function(){
    // console.log(this._form.serialize());
    // console.log(this._form.serializeArray());
    $("input,select",this._form).each(function(){
      if($(this).attr("data-form-name")!=undefined){
        $(this).attr("ori-name",$(this).attr("name"))
        $(this).attr("name",$(this).attr("data-form-name"));
      }
    })

    var result = this._form.serializeArray();

    $("input,select",this._form).each(function(){
      if($(this).attr("ori-name")!=undefined){
        $(this).attr("name",$(this).attr("ori-name"))
      }
    })

    return result;
  }

  Form.prototype.validateItem = function(a_elm){

    var status = true;
    type = $(this._form).attr('data-type');
    if($("header").hasClass("bobe-bs-v3-compatibity")){
      isOldVersion = true;
    }
    var target = $(a_elm,this._form);

    $(this._form).addClass("was-validated");
    var result = validate(target);

    return result;
  }


  Form.prototype.validate = function(){
    var status = true;
    type = $(this._form).attr('data-type');
    if($("header").hasClass("bobe-bs-v3-compatibity")){
      isOldVersion = true;
    }
    $(this._form).addClass("was-validated");
    // $("input,select",this._form).removeClass('is-invalid');
    $("input,select",this._form).each(function(){
      var target = $(this);
      var result = validate(target);
      if(!result){
        status = false; 
      }
    });
    
    return status;
  }

  return Form;
})();