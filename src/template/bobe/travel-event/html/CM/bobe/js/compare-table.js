//最多保180天
var ISSUE_MAX_DAYS= 180;

//單一保單最多5個國家
var REGION_MAX_LENGTH = 5;

//日期格式
var dateformat = 'YYYY/MM/DD';

//最快收件出發前3小時
var ISSUE_MIN_HOURS = 3;

var BREAK_POINT_MD = 576;

$(function(){

    var formScope = $('form[role="ins_form"]');
    var submit = $('*[role="form_submit"]');
    var form = new Form(formScope);

    var now = moment();
    var ISSUE = moment(now).add(ISSUE_MIN_HOURS, 'hours');
    var INS_START_Y = ISSUE.format(dateformat+'/HH').split('/')[0];
    var INS_START_M = ISSUE.format(dateformat+'/HH').split('/')[1];
    var ISSUE_D = ISSUE.format(dateformat+'/HH').split('/')[2];
    var START_TIME = ISSUE.format(dateformat+'/HH').split('/')[3];

    var ISSUE_MAX = moment(now).add(ISSUE_MAX_DAYS, 'd');

    var ISSUE_MAX_Y = ISSUE_MAX.format(dateformat+'/HH').split('/')[0];
    var ISSUE_MAX_M = ISSUE_MAX.format(dateformat+'/HH').split('/')[1];
    var ISSUE_MAX_D = ISSUE_MAX.format(dateformat+'/HH').split('/')[2];
    var mydrp;


    var initDateRangePicker = function(){
        now = moment();
        ISSUE = moment(now).add(ISSUE_MIN_HOURS, 'hours'); 
        INS_START_Y = ISSUE.format(dateformat+'/HH').split('/')[0];
        INS_START_M = ISSUE.format(dateformat+'/HH').split('/')[1];
        ISSUE_D = ISSUE.format(dateformat+'/HH').split('/')[2];
        START_TIME = ISSUE.format(dateformat+'/HH').split('/')[3];

        ISSUE_MAX = moment(now).add(ISSUE_MAX_DAYS, 'd');

        ISSUE_MAX_Y = ISSUE_MAX.format(dateformat+'/HH').split('/')[0];
        ISSUE_MAX_M = ISSUE_MAX.format(dateformat+'/HH').split('/')[1];
        ISSUE_MAX_D = ISSUE_MAX.format(dateformat+'/HH').split('/')[2];
        
        var _container = '.custom-input-daterange';
        var _datepicker = '.datepicker';
        var _input = 'init-daterangepicker';
        var _datepicker = '.datepicker';
        var active = 'active';

        var wrap_tmpl = 
            '<div class="input-date-wrap">' +
                '<div class="input-date-info d-block d-md-none"><input type="text" class="form-control d-inline" data-form-name="f_id" placeholder="請選擇旅遊期間" role="INS_DATE"></div>' +
                '<div class="datepicker"></div>' +
                // '<div class="input-date-footer d-md-block p-3"><a href="javascript:;" class="btn bobe-btn-3d bobe-btn-3d-primary d-block mx-auto">OK</a></div>' +
            '</div>';


        var container = $(_container);
        var $input = $('.init-daterangepicker',container);

        if($('.init-daterangepicker').length==0)
            return false;

        if( $('.init-daterangepicker').daterangepicker==undefined){
            alert("please include /INSEBWeb/html/CM/bobe/libs/daterangepicker.js into your page");
            return false;
        }

        $input.closest(_container).prepend(wrap_tmpl);
        var datepicker = $(_datepicker);

        $('input[role="INS_DATE"]',container).attr("autocomplete", "off");

        mydrp = $('.init-daterangepicker').daterangepicker(
        {
            autoUpdateInput: false,
            autoApply: true,
            parentEl:datepicker,
            opens:'embed',
            minDate:ISSUE,
            maxDate:ISSUE_MAX,
            pickTime: false,
            locale: {
              format: dateformat
            },
        });

        mydrp.on('show.daterangepicker',function(ev, picker){
            container.addClass(active);
        })

        mydrp.on('hide.daterangepicker',function(ev, picker){
            container.removeClass(active);
            picker.minDate = ISSUE;
            picker.maxDate = ISSUE_MAX; 
        })

        mydrp.on('setStartDate.daterangepicker',function(ev, picker){
            var date = picker.startDate.format(dateformat);
            var minDate = moment(date);
            var maxDate = moment(minDate).add(ISSUE_MAX_DAYS-1, 'd');

            $('input[role="INS_DATE"]',container).val(date);
            $('input[name="INS_START_Y"]',container).val(date.split('/')[0]);
            $('input[name="INS_START_M"]',container).val(date.split('/')[1]);
            $('input[name="INS_START_D"]',container).val(date.split('/')[2]);

            picker.minDate = minDate;
            picker.maxDate = maxDate;

            var target = $('select[role="INS_START_TIME"]');
            target.empty();
            target.append('<option value="">出發時間</option>');
            target.prop("disabled",true);
            target.trigger("chosen:updated");

        })

        mydrp.on('setEndDate.daterangepicker',function(ev, picker){
            var end_date = picker.endDate.format(dateformat);
            var start_date = $('input[role="INS_DATE"]',container).val();

            $('input[name="INS_END_Y"]',container).val(end_date.split('/')[0]);
            $('input[name="INS_END_M"]',container).val(end_date.split('/')[1]);
            $('input[name="INS_END_D"]',container).val(end_date.split('/')[2]);
            // 
            $('input[role="INS_DATE"]',container).val(start_date+" - "+end_date);
            //
            var now = moment(end_date);
            var end = moment(start_date);
            var duration = moment.duration(now.diff(end));
            var days = duration.asDays();
            $("#DISPLAY_INS_DAYS_VAL").html(days)
            $("#DISPLAY_INS_DAYS").removeClass("d-none");
            $("#DISPLAY_INS_INFO").removeClass("d-none");
            $('input[name="INS_DAYS"]').val(days);
            
            form.validateItem($('input[role="INS_DATE"]',container));
            picker.minDate = ISSUE;
            picker.maxDate = ISSUE_MAX; 

            
            if($(window).width()<BREAK_POINT_MD){

            }
            //update time
            var target = $('select[role="INS_START_TIME"]');

            var ISSUE_START_H;
            var ISSUE_START_M;

            if(start_date == ISSUE.format(dateformat)){
                //同1天,3小時後
                ISSUE_START_H = START_TIME;
                ISSUE_START_M = 0;
                var HH = parseInt(ISSUE.format('HH'));
                var mm = parseInt(ISSUE.format('mm'));
                if(mm<30){
                    ISSUE_START_M = 30;
                }else{
                    ISSUE_START_H = HH+1
                }
            }
            else{
                //不同天,00:00 開始
                ISSUE_START_H = 0; 
                ISSUE_START_M = 0;
            }
            target.empty();
            target.append('<option value="">出發時間</option>');
            for(var i=ISSUE_START_H;i<=23;i++){

                switch(ISSUE_START_M){
                    case 0:
                        target.append('<option value="'+UTILS.pad(i)+':'+UTILS.pad(ISSUE_START_M)+'">'+UTILS.pad(i)+':'+UTILS.pad(ISSUE_START_M)+'</option>');
                        ISSUE_START_M=30;
                        target.append('<option value="'+UTILS.pad(i)+':'+UTILS.pad(ISSUE_START_M)+'">'+UTILS.pad(i)+':'+UTILS.pad(ISSUE_START_M)+'</option>');
                        ISSUE_START_M=0;
                    break;

                    case 30:
                        target.append('<option value="'+UTILS.pad(i)+':'+UTILS.pad(ISSUE_START_M)+'">'+UTILS.pad(i)+':'+UTILS.pad(ISSUE_START_M)+'</option>');
                        ISSUE_START_M=0;
                        break;
                    break;
                }
            };

            target.off("change").on("change",function(){
                var end_dae = $("#INS_END_Y").val()+ '/' +$("#INS_END_M").val()+ '/' +$("#INS_END_D").val();
                if($(this).val()!=""){
                    $("#DISPLAY_INS_PERIOD_VAL").html(end_dae+" "+UTILS.pad($(this).val())+":00")
                    $("#DISPLAY_INS_PERIOD").removeClass("d-none");
                    $("#DISPLAY_INS_INFO").removeClass("d-none");
                    //
                    var val = $(this).val().split(":");
                    $("#INS_START_TIME_H").val(val[0]);
                    $("#INS_START_TIME_M").val(val[1]);
                }else{
                    $("#DISPLAY_INS_PERIOD_VAL").html(end_dae)
                }
            })
            target.prop("disabled",false);
            target.trigger("chosen:updated");
       
        })

        if($(window).width()<BREAK_POINT_MD){
            container.addClass('is-mobile')
        }else{
            container.removeClass('is-mobile')
            container.find('.input-date-wrap').css({"width":"550px"})
        }
    }

    var sendData = function(a_data){
        // location.href="2.html";
        console.log(a_data);
        var alert_msg = '請開啟console看form的值\n';
        for(var i in a_data){
            alert_msg += "變數:"+a_data[i].name+'   值:'+a_data[i].value+'\n'
        }
        alert(alert_msg)
        $.unblockUI();
    }

    var validate = function(){
        var result = form.validate();
        if(result){
            var data = form.getData();
            $.blockUI();
            setTimeout(sendData,1000,data);
        }
        else{
            if($(".is-invalid").length>0){
                var scrollPos = $(".is-invalid").eq(0).parent().offset().top;
                UTILS.scrollPage(scrollPos);
            }
        }
    }

    var addEventListener = function(){
        
        $(document).on( "focus", 'input', function(){
          $(this).on("keyup",function(){
            form.validateItem($(this));
          });
        });

        $(document).on( "blur", 'input', function(){
         $(this).off("keyup");
        });

        $(document).on( "change", 'select', function(){
           if($(this).val()!=""){
           	$(this).addClass("hasValue");
           }else{
           	$(this).removeClass("hasValue");
           }
           form.validateItem($(this));
        });


        submit.off().on("click",validate);
    }

	var initChosenSelect = function(){
        $(".chosen-select").chosen({
            disable_search:true,
            inherit_select_classes:true
        });
	}

	

	var initBobeCompare = function(a_scope,a_mode){
		var scope = a_scope
		var mode;
		if(a_mode==undefined){
			a_mode = 'table';
		}

		if($(window).width()<=425){
			a_mode = 'card';
		}

		$(window).resize(function(){
			if($(window).width()<=425){
				switchMode('card');
			}
		})
		default_mode = a_mode;

		var switchMode = function(a_mode){
			if(a_mode!="table" && a_mode!= "card"){
				a_mode = "table";
			}
			mode = a_mode;
			var target = $(".bobe-compare__toolbar",scope);
			$("a",target).removeClass("active");
			$(".bobe-compare__toolbar-"+mode,target).addClass("active");

			switch(mode){
				case "table":
					$(scope).addClass("thumb");
					$(".w-ins").removeClass("d-none");
					$(".w-product-item").removeClass("d-none");
					//

					if($(".plan-item--highlight .shadow",$(".bobe-compare__card")).length==0){
						$(".plan-item--highlight",$(".bobe-compare__card")).append('<div class="shadow""></div>');
					}
					var shadow_h = $(".bobe-compare__card",scope).height()+$(".bobe-compare__table",scope).height();
					$( ".shadow",scope).height(shadow_h);
				break;

				case "card":
					$(scope).removeClass("thumb");
					$(".w-ins").addClass("d-none");
					$(".w-product-item").addClass("d-none");
				break;
			}
		}
		var initCompareToolbar = function(a_scope){

			$(".bobe-compare__toolbar a",scope).on("click",function(e){
				var val = $(this).attr("data-value");
				switchMode(val);
			})
		}

		var initCompareCard = function(a_scope){
			var scope = $(".bobe-compare__card",a_scope);
		}

		var initCompareTable = function(a_scope){
			var scope = $(".bobe-compare__table",a_scope);
			var o = this;
			var updateCompareTable = function(){
				var content_h = $(".bobe-compare__table-content",scope).height();
				var scroll_h = parseInt($(".bobe-compare__table-content",scope).attr("data-scroll-h"));
				if(content_h>scroll_h){
					$(".bobe-compare__table-content").addClass("bobe-compare__table-content--scroll");
					
					
					setTimeout(function(){
						$('.bobe-compare__table-content--scroll').unbind('scroll').bind('scroll',function(){
						var tp = $(this).scrollTop();
						var gap_ary = [0]; 
							$('.product-item').each(function(){
							gap_ary.push($(this).height());
						})
						var total = gap_ary.length;
						for(var i=0;i<gap_ary.length;i++){
							if(tp>=gap_ary[i]){
								if(i==total-1){
									$(".bobe-compare__table-hint>div").addClass("d-none");
									$(".bobe-compare__table-hint>div").last().removeClass("d-none");
								}else if(i==0){
									$(".bobe-compare__table-hint>div").addClass("d-none");
									$(".bobe-compare__table-hint>div").first().removeClass("d-none")
								}else{
									$(".bobe-compare__table-hint>div").addClass("d-none");
									$(".bobe-compare__table-hint>div").eq(i).removeClass("d-none");
								}
							}
						}
					}).trigger("scroll");
					},400);
				}else{
					 $(".bobe-compare__table-content").removeClass("bobe-compare__table-content--scroll");
				}
			}
		
			$('.more-product-spec',scope).on('show.bs.collapse', function () {
				var target = $( ".shadow",scope);
				var target_h = parseInt(target.attr("max-h"));
				target.height(target_h)

				var target = $(".product-item",scope).last();
				$(">.cell",target).last().removeClass("border-b-none");
				$(".more-product-spec>.cell",target).last().addClass("border-b-none");

				var target = $(".product-item",scope);
				target.each(function(){
					$(">.cell",$(this)).last().removeClass("border-b-none");
				})

				var target = $(".product-spec",scope);
				target.each(function(){
					$(">.cell",$(this)).last().removeClass("border-b-none");
				})

			});

			$('.more-product-spec',scope).on('shown.bs.collapse', function () {
				//resizeCompareTable();

			});
			$('.more-product-spec',scope).on('hide.bs.collapse', function () {
				var target = $( ".shadow",scope);
				var target_h = parseInt(target.attr("min-h"));
				target.height(target_h);
			});

			$('.more-product-spec',scope).on('hidden.bs.collapse', function () {
				//resizeCompareTable();

				var target = $(".product-item",scope);
				target.each(function(){
					$(">.cell",$(this)).last().addClass("border-b-none");
				})
				var target = $(".product-spec",scope);
				target.each(function(){
					$(">.cell",$(this)).last().addClass("border-b-none");
				})
			});
			
			var target = $(".product-item",scope);
			target.each(function(){
				$(">.cell",$(this)).last().addClass("border-b-none");
			})
			var target = $(".product-spec",scope);
			target.each(function(){
				$(">.cell",$(this)).last().addClass("border-b-none");
			})

			var target = $(".product-item",scope);
			target.each(function(){
				$(".more-product-spec>.cell",$(this)).last().addClass("border-b-none");
			})
			var target = $(".product-spec",scope);
			target.each(function(){
				$(".more-product-spec>.cell",$(this)).last().addClass("border-b-none");
			})

			$(".collapse",scope).collapse('show');
			updateCompareTable();
		}

			
		
        initCompareToolbar(scope);
        initCompareCard(scope);
        initCompareTable(scope);
		switchMode(default_mode);
	}

	

    var init = function(){
        initDateRangePicker();
        initChosenSelect();
        initBobeCompare($("#bobe-compare-official"),"table");
        addEventListener();

    }

    init();

})