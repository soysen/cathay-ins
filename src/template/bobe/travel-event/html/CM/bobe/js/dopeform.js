$(function(){

	/**
	* @version: 3.0.3
	* @author: Dan Grossman http://www.dangrossman.info/
	* @copyright: Copyright (c) 2012-2018 Dan Grossman. All rights reserved.
	* @license: Licensed under the MIT license. See http://www.opensource.org/licenses/mit-license.php
	* @website: http://www.daterangepicker.com/
	*/
	// Following the UMD template https://github.com/umdjs/umd/blob/master/templates/returnExportsGlobal.js

	//========================================//

	//usage html
	//  <div class="custom-input-daterange">
	//   <div class="input-date">
	//     <input type="text" class="form-control d-inline init-daterangepicker" name="startDate" id="startDate" placeholder="" required readonly>
	//   </div>
	//   <span> - </span>
	//   <div class="input-date">
	//     <input type="text" class="form-control d-inline" name="endDate" id="endDate" placeholder="" required readonly>
	//   </div>
	// </div>

	//========================================//



	function dateRangePicker(){

		var window_w = $(window).width()
		var isDesktop = window_w > 992;
		var isMobile = window_w < 576;

		//ClassName
		var _container = '.custom-input-daterange';
		var _datepicker = '.datepicker';
		var _wrap = '.input-date-wrap';
		var _input = 'init-daterangepicker';
		var _startDate = '.input-date input#startDate';
		var _endDate = '.input-date input#endDate';
		var _datepicker = '.datepicker';

		var dateformat = 'MM/DD/YYYY';
		var active = 'active';

		var wrap_tmpl = 
			'<div class="input-date-wrap">' +
				'<hr class="d-none d-sm-block">' +
				'<div class="datepicker"/></div>' +
			'</div>';

		var locale_settings = {
			    "format": "MM/DD/YYYY",
			    "separator": " - ",
			    "applyLabel": "確認",
			    "cancelLabel": "取消",
			    "fromLabel": "From",
			    "toLabel": "To",
			    "customRangeLabel": "Custom",
			    "weekLabel": "W",
			    "daysOfWeek": [
			        "日",
			        "一",
			        "二",
			        "三",
			        "四",
			        "五",
			        "六"
			    ],
			    "monthNames": [
			        "一月",
			        "二月",
			        "三月",
			        "四月",
			        "五月",
			        "六月",
			        "七月",
			        "八月",
			        "九月",
			        "十月",
			        "十一月",
			        "十二月"
			    ],
			    "firstDay": 1
			}

		//jQuery Objects
		var container = $(_container);
		var $input = $('.init-daterangepicker',container);
		var $startDate = $(_startDate);
		var $endDate = $(_endDate)


		function initDateRangepicker(){

			$input.closest(_container).prepend(wrap_tmpl);

			var datepicker = $(_datepicker);

			console.log($input.length)

			$input.daterangepicker({
				parentEl:datepicker,
				opens:'embed',
				autoUpdateInput: false,
				"locale": locale_settings
			})

			console.log('input=',$input)

			if(isMobile){

				container.addClass('is-mobile')


			}else{

				container.removeClass('is-mobile')

			}


		}

		function addEventListener(){

			var _pos;
			var _body = $('body');

			$input.on('focus',function(){

				var $this = $(this);
				$this.trigger('blur');
				$this.closest(_container).addClass(active)

				console.log($this)

				_pos = $(window).scrollTop();
				_body.css({'position':'fixed','top': -_pos});
				_body.addClass("body-locked");


			})

			$input.on('hide.daterangepicker',function(ev, picker){

				var $this = $(this);
				$this.closest(_container).removeClass(active)
				_body.removeClass("body-locked");
				_body.css({'position':'relative','top': 'auto'});

				$(window).scrollTop(_pos);

			})

			$input.on('setStartDate.daterangepicker',function(ev, picker){

				console.log('setStartDate',picker)
				var startDate = picker.startDate.format(dateformat)
				$(this).closest(_container).find($startDate).val(startDate)

			})

			$input.on('setEndDate.daterangepicker',function(ev, picker){

				console.log('setEndDate',picker)
				var endDate = picker.endDate.format(dateformat)
				$(this).closest(_container).find($endDate).val(endDate)
				 
			})

		}

		function init(){

			initDateRangepicker()
			addEventListener()

		}

		$(window).resize(init())


	}

	dateRangePicker()


	/*!
	 * Datepicker for Bootstrap v1.8.0 (https://github.com/uxsolutions/bootstrap-datepicker)
	 *
	 * Licensed under the Apache License v2.0 (http://www.apache.org/licenses/LICENSE-2.0)
	 */


	function datepicker(){
		return;
       var inputdate = $('input.input-datepicker');
        
        if($(window).width()<576){

          $('body').append(

            '<div class="datepicker-container">' +
              '<div class="datepicker-buttons">' +
                '<a class="close-datepicker-wrap" href="javascript:;">關閉</a>' +
              '</div>' +
              '<div class="datepicker-wrap"></div>' +
            '</div>'

          )

          $('.datepicker-wrap').datepicker({
            todayHighlight:true
          })

          var datepickerInlineWrap = $('.datepicker-container');
          var Btnclose = $('.close-datepicker-wrap');
          var _pos;
          var _body = $('body');

          inputdate.on('focus',function(){

            var offset = $(this).offset();
            _pos = offset.top - 100;
            $(this).addClass('datepickerOnchange').blur();
      
            
            console.log("_pos=",_pos)
            _body.css({'position':'fixed','top': -_pos});
            _body.addClass("body-locked");

            datepickerInlineWrap.addClass('active');

            $('.datepicker-wrap').on('changeDate',function(e){

              var value = moment(e.date).format('MM-DD-YYYY')
              $('input.datepickerOnchange').val(value)

            })



          })

          Btnclose.on('click',function(){

            $(this).closest(datepickerInlineWrap).removeClass('active');
            $('input.datepickerOnchange').removeClass('datepickerOnchange')

            _body.removeClass("body-locked");
            _body.css({'position':'relative','top': 'auto'});

            $(window).scrollTop(_pos);

          })


        }else{
			
			alert(inputdate.length)
          inputdate.datepicker({

            orientation:'bottom',
            autoclose:'true'

          })

        }

	}

	datepicker()
	
})


