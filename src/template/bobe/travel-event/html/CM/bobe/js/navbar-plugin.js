this.NavbarPlugin = function(){

	var bs_collapse = $('.bs_collapse');
	var bs_collapse_trigger = $('[data-toggle="collapse"]');
	var bs_nav_collapse = $(".navbar-collapse");

	//dropdown
	var dropdownElement = $('#navbarSupportedContent .dropdown');

	//collapsing
	var collapseElement = $(".bobe-collapsing");
	var collapseTrigger = $(".bobe-collapsing-trigger",collapseElement);

	var customformPlugin = function(){

		var formlistcheck = $('.formlistcheck')

		formlistcheck.on('change',function(){

			var $this = $(this)

			var status = $this.prop('checked');

			if( status == true){

				$this.closest('.form-unit').removeClass('disabled').addClass('enabled')

			}else{

				$this.closest('.form-unit').removeClass('enabled').addClass('disabled')
				$this.closest('.form-unit').find('.collapse').collapse('hide')

			}

		})



		var formtoggle = $('[data-toggle="collapse"]');

		formtoggle.bind('click',function(){

			var status = $(this).attr('aria-expanded')
			status = ( status == 'true' ) ? false : true


			if( status == true){

				$(this).closest('.form-unit').addClass('form-toggled')

			}else{

				$(this).closest('.form-unit').removeClass('form-toggled')

			}

			//console.log("status",status)
		})
		

	}();


	// $(window).resize(initDropdownhover());

	var addEventListener = function(){

		if($(window).width() > 992){

		    dropdownElement.hover(function() {
		      $(this).find('a.nav-link').attr('aria-expanded','true');
		      $(this).find('.dropdown-menu').addClass('show');
		    }, function() {
		      $(this).find('.dropdown-menu').removeClass('show');
		      $(this).find('a.nav-link').attr('aria-expanded','false');
		    });

		}

		collapseTrigger.bind('click',function(){
			$(this).toggleClass("show");
			var target = $(this).attr("data-target");
			$(target).toggleClass("show");
		})	

		bs_collapse_trigger.click(function(){
			bs_collapse.collapse('hide');
		})

		bs_nav_collapse.on('show.bs.collapse',function(){
			$(this).addClass('collapsed');
			$('body').addClass("body-locked");
		}).on('hide.bs.collapse hidden.bs.collapse',function(){
			$(this).removeClass('collapsed');
			$('body').removeClass("body-locked");
		})

		var _pos;
		var _body = $('body');

		bs_collapse.on('show.bs.collapse',function(){

			_pos = $(window).scrollTop();
			_body.css({'position':'fixed','top': -_pos});
			_body.addClass("body-locked");


		}).on('hide.bs.collapse',function(){

			_body.removeClass("body-locked");
			_body.css({'position':'relative','top': 'auto'});

			$(window).scrollTop(_pos);

		})

	}

	var init = function(){
		addEventListener();
		console.log('%cNavbar Plugin initialized.','background: #222; color: #bada55')
	}

	return{
		init:init
	}
}

