/** 
 * Import gulp and gulp plugin
 * */
import gulp from 'gulp'
import glob from 'glob'
import fs from 'fs'
import path from 'path'
import archiver from 'archiver'
import vue from 'rollup-plugin-vue2';
import buble from 'rollup-plugin-buble'
import rollupEach from 'gulp-rollup-each'
import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import replace from 'rollup-plugin-replace'
import babel from 'rollup-plugin-babel'
import concat from 'gulp-concat'
import clean from 'gulp-clean'
import compass from 'gulp-compass'
import cleanCSS from 'gulp-clean-css'
import imagemin from 'gulp-imagemin'
import jade from 'gulp-jade'
import merge from 'merge-stream'
import plumber from 'gulp-plumber'
import uglify from 'gulp-uglify'
import watch from 'gulp-watch'
import template from 'gulp-template-html'
const browserSync = require('browser-sync').create();

/** 
 * Import package and directories setting
 * */
const pkg = require('./package.json');

const assetPath = pkg['f2e-configs'].assets;
const scriptsPath = pkg['f2e-configs'].scripts;
const cssPath = pkg['f2e-configs'].styles;
const useJade = pkg['f2e-configs'].useJade;

const srcPath = {
    root: 'src/',
    sass: 'src/sass',
    css: 'src/css',
    js: 'src/js',
    html: "src/template/**/*." + (useJade ? 'jade' : 'html'),
    partials: "src/partials/**/*.jade",
    img: 'src/img',
    font: 'src/fonts'
};

const distPath = {
    font: 'dist/fonts',
    style: 'dist/css',
    js: 'dist/js',
    html: 'dist',
    img: 'dist/img'
};

const archivePath = 'archive'

const date = new Date();
const month = date.getMonth() + 1;
const today = `${date.getFullYear()}${(month < 10 ? '0' : '') + month}${(date.getDate() < 10 ? '0' : '') + date.getDate()}`;


/** 
 * Compile asset
 * */
gulp.task('copy:bobe-travel', done => {
    return gulp.src(srcPath.root + '/template/bobe/travel-event/**/*.*')
    .pipe(gulp.dest(distPath.html + '/bobe/travel-event'))
    .pipe(browserSync.stream())
})
gulp.task('copy:bobe-event', done => {
    return gulp.src(srcPath.root + '/template/bobe/event/*.html')
    .pipe(template('src/partials/bobe-travel.html'))
    .pipe(gulp.dest(distPath.html + '/bobe/event'))
    .pipe(browserSync.stream())
})
gulp.task('copy:bobe-html', done => {
    return gulp.src(srcPath.root + '/template/bobe/!(travel-event|event)/*.html')
    .pipe(template('src/partials/bobe.html'))
    .pipe(gulp.dest(distPath.html + '/bobe'))
    .pipe(browserSync.stream())
})

gulp.task('copy:bobe-assets', done => {
    return gulp.src('src/bobe/CM/**/*.*')
    .pipe(gulp.dest('dist/CM'))
})

gulp.task('copy:bobe-js', done => {
    const files = glob.sync('bobe/js/*.*', {
        'cwd': 'src',
        'dot': true // include hidden files
    });
    let bobePath = []

    files.forEach((file) => {
        if (file.indexOf('.DS_Store') == -1) {
            bobePath.push(path.resolve('src', file))
        }
    })

    return gulp.src(bobePath).pipe(concat('bobe-lib.js'))
    .pipe(gulp.dest(distPath.js))
})

gulp.task('copy:library', done => {
    let mergeObj = []
    if (assetPath.length)
        mergeObj.push(
            gulp.src(assetPath).pipe(gulp.dest(distPath.font))
        )

    if (cssPath.length)
        mergeObj.push(
            gulp.src(cssPath).pipe(concat('lib.css')).pipe(gulp.dest(distPath.style))
        )
    if (scriptsPath.length)
        mergeObj.push(
            gulp.src(scriptsPath).pipe(concat('lib.js')).pipe(gulp.dest(distPath.js))
        )

    if (mergeObj.length)
        return merge(...mergeObj).pipe(browserSync.stream())
    else
        done()
});

/** 
 * Setting build task
 * */
// JS
gulp.task('clean:js', () => {
    return gulp.src(distPath.js, { allowEmpty: true }).pipe(clean())
})

gulp.task('compile:js', gulp.series('clean:js', 'copy:library', () => {
    return gulp.src([
        srcPath.js + '/*.js'
    ]).pipe(gulp.dest(distPath.js))
}))

// Compass 
gulp.task('clean:img', () => {
    return gulp.src(distPath.img, { read: false, allowEmpty: true }).pipe(clean())
})

gulp.task('compass', () => {
    return gulp.src([
            srcPath.sass + '/*.sass',
            srcPath.sass + '/*.scss'
        ])
        .pipe(plumber({
            errorHandler(err) {
                console.log(err.message)
            }
        }))
        .pipe(compass({
            project: path.join(__dirname, '/src'),
            css: 'css',
            image: 'img',
            sass: 'sass'
        }))
        .on('error', err => {
            console.log(err.message)
        })
        .pipe(gulp.dest('css'))
})

gulp.task('compile:css', gulp.series('clean:img', 'compass', () => {
    let css = gulp.src(srcPath.css + '/**/!(lib).css')
        .pipe(gulp.dest(distPath.style));

    let img = gulp.src([
        srcPath.img + "/**/*.png",
        srcPath.img + "/**/*.jpg",
        srcPath.img + "/**/*.gif",
        srcPath.img + "/**/*.svg",
        srcPath.img + "/!(icons|icons-2x)/*.png"
    ]).pipe(gulp.dest(distPath.img));

    return merge(css, img).pipe(browserSync.stream())
}))

gulp.task('compile:asset', done => {
    return gulp.src(srcPath.font).pipe(gulp.dest(distPath.font));
})

// Jade
gulp.task('clean:html', () => {
    return gulp.src(distPath.html + '/**/*.html', { allowEmpty: true }).pipe(clean());
});

gulp.task('template:p-html', gulp.series('clean:html', () => {
    return gulp.src(srcPath.root + 'template/personal-ins/*.html')
    .pipe(template('src/partials/personal.html'))
    .pipe(gulp.dest(distPath.html + '/personal-ins'))
    .pipe(browserSync.stream())
}))

gulp.task('template:c-html', gulp.series(() => {
    return gulp.src(srcPath.root + 'template/commercial-ins/*.html')
    .pipe(template('src/partials/commercial.html'))
    .pipe(gulp.dest(distPath.html + '/commercial-ins'))
    .pipe(browserSync.stream())
}))

gulp.task('template:h-html', gulp.series(() => {
    return gulp.src(srcPath.root + 'template/helper/*.html')
    .pipe(template('src/partials/helper.html'))
    .pipe(gulp.dest(distPath.html + '/helper'))
    .pipe(browserSync.stream())
}))

gulp.task('template:guideline', done => {
    return gulp.src(srcPath.root + 'template/guideline/*.html')
    .pipe(template('src/partials/guideline.html'))
    .pipe(gulp.dest(distPath.html + '/guideline'))
    .pipe(browserSync.stream())
})

gulp.task('compile:html', gulp.series('clean:html', () => {
    if (useJade) 
        return gulp.src(srcPath.html)
            .pipe(plumber({
                errorHandler(err) {
                    console.log(err.message)
                }
            })).pipe(jade({
                pretty: true
            }).on('error', err => {
                console.log(err)
            })).pipe(gulp.dest(distPath.html)).pipe(browserSync.stream());
    else 
        return gulp.src(srcPath.html)
            .pipe(plumber({
                errorHandler(err) {
                    console.log(err.message)
                }
            })).pipe(gulp.dest(distPath.html)).pipe(browserSync.stream())
}));

/**
 * Archive
 */
gulp.task('archive:create-dir', done => {
    if (!fs.existsSync(archivePath)) {
        fs.mkdirSync(path.resolve(archivePath), '0755');
    }

    if (fs.existsSync(archivePath + `/${pkg.name}_${today}.zip`)) {
        fs.unlinkSync(archivePath + `/${pkg.name}_${today}.zip`);
    }

    done();
});

gulp.task('archive:zip', done => {
    const archiveName = path.resolve(archivePath, `${pkg.name}-${today}.zip`);
    const zip = archiver('zip');
    const files = glob.sync('**/*.*', {
        'cwd': 'dist',
        'dot': true // include hidden files
    });
    const output = fs.createWriteStream(archiveName);

    zip.on('error', (error) => {
        throw error;
    });

    output.on('close', done);

    files.forEach((file) => {

        const filePath = path.resolve('dist', file);

        // `zip.bulk` does not maintain the file
        // permissions, so we need to add files individually
        zip.append(fs.createReadStream(filePath), {
            'name': file,
            'mode': fs.statSync(filePath).mode
        });

    });

    zip.pipe(output);
    zip.finalize();

    done()
});

/** 
 * Setting minify task
 * */
gulp.task('min:css', done => {
    return gulp.src(distPath.style + '/**/!(lib).css')
        .pipe(cleanCSS())
        .pipe(gulp.dest(distPath.style))
})

gulp.task('min:js', done => {
    return gulp.src(distPath.js + '/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest(distPath.js))
})

gulp.task('min:image', done => {
    var img = gulp.src([
        distPath.img + "/**/*.png",
        distPath.img + "/**/*.jpg",
        distPath.img + "/**/*.gif",
        distPath.img + "/**/*.svg",
        distPath.img + "/!(icons|icons-2x)/*.png"
    ]).pipe(imagemin({
        verbose: true
    })).pipe(gulp.dest(distPath.img));

    return merge(img);
})

/** 
 * Gulp watch setting
 * */
gulp.task('server', function () {
    browserSync.init({
        server: {
            baseDir: "./dist"
        }
    })

    watch([
        srcPath.sass + '/**/*.sass',
        srcPath.img + '/**/*.jpg',
        srcPath.img + '/**/*.png',
        srcPath.img + '/**/*.gif',
        srcPath.img + '/**/*.svg'
    ], gulp.series('compile:css', done => done()));

    if (useJade) {
        watch([
            srcPath.html,
            "src/partials/**/*.jade"
        ], gulp.series('compile:html', done => done()));
    } else {
        watch([
            srcPath.html,
            "src/partials/**/*.html"
        ], gulp.series('template:p-html', 'template:c-html', 'template:guideline', 'template:h-html', 'copy:bobe-html', 'copy:bobe-event', 'copy:bobe-travel', done => done()));
    }

    watch([
        srcPath.js + '/**/*.js',
        srcPath.js + '/**/*.vue',
    ], gulp.series('compile:js', done => done()));

    watch([
        srcPath.font + '/*'
    ], gulp.series('compile:asset', done => done()));
});

// Setting default command

gulp.task('archive', gulp.series('archive:create-dir', 'archive:zip', done => done()));

gulp.task('default', gulp.series('compile:css', 'compile:js', 'template:p-html', 'template:c-html', 'template:h-html', 'template:guideline', 'copy:bobe-html', 'copy:bobe-travel', 'copy:bobe-event', 'copy:bobe-assets', done => done()));

gulp.task('watch', gulp.series('compile:css', 'compile:js', 'template:p-html', 'template:c-html', 'template:h-html', 'template:guideline', 'copy:bobe-html', 'copy:bobe-travel', 'copy:bobe-event', 'copy:bobe-assets', 'server', done => done()));

gulp.task('minify', gulp.series('min:css', 'min:image', 'min:js', done => done()));
