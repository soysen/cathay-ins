$(function(){

	var isLocalhost = function(){
    	//console.log('index.js location.host',location.host);
		if(location.host == 'localhost:9998' || location.host == 'bobe.albertlan.net'){
			return true;
		}
		return false;
	}

	var initSlick = function(){
		//banner
        $("#banner-btns a:first").addClass("active");

        var banner = $("[role='banner-carousel']");
        var fakelink = $('<a>fake link</a>').hide().insertAfter(banner);
        var bannerurl = banner.find('a');

        banner.homeslick({
            btns: '#banner-btns a',
            autoplay: 1
        }).on('beforeChange', function (event, slick, currentSlide, nextSlide) {

            var $nextSlide = bannerurl.eq(nextSlide);
            fakelink.attr('href', $nextSlide.attr('href'));

            if ($nextSlide.attr('target') != undefined) {
                fakelink.attr('target', $nextSlide.attr('target'));
            }
            else {
                fakelink.removeAttr('target');
            }

            //add data-id
            if ($nextSlide.data('id') != '') {
                fakelink.attr('data-id', $nextSlide.data('id'));
                fakelink.attr('onclick', $nextSlide.attr('onclick'));
            }
        });
        var next = bannerurl.eq(0);
        fakelink.attr('href', next.attr('href'));

        if (next.attr('target') != undefined) {
            fakelink.attr('target', next.attr('target'));
        }
	}
	var renderBanner = function(){
		var html = '';
		var page_html = '';
    	$.each(bannerLst, function(index,item){
            var template = $("#template-banner-item").html();
			var url = item.URL;
			//console.log('banner url',url);
			var title = item.SUBJECT;
			var img = bannerPath+item.PIC;
            template = template.replace(/%BANNER_TITLE%/gi,title);
            template = template.replace(/%BANNER_URL%/gi,url);
            template = template.replace(/%BANNER_IMG%/gi,img);
            html += template;

            //
            var page_template = $("#template-banner-item-btn").html();
            page_template = page_template.replace(/%BANNER_TITLE%/gi,title);
            page_html += page_template;
		});
		$("#banner-container").html(html);
		$("#banner-btns").html(page_html);
		$("#banner-btns a").eq(0).addClass("active");
		
		initSlick();
	}

	var renderNews = function(){
		var html = '';
		//console.log('latestNewsLst',latestNewsLst);
    	$.each(latestNewsLst, function(index,item){
    		if(index>4){
    			return false;
    		}
            var template = $("#template-news-item").html();
			var url = '/INSEBWeb/servlet/HttpDispatcher/ECA2_0200/prompt?SERIAL_NO='+item.SERIAL_NO;
			var title = item.SUBJECT;
			var date = item.EFFECT_DATE.replace(eval('/-/g'),"/").substr(0,10);
            template = template.replace(/%NEWS_TITLE%/gi,title);
            template = template.replace(/%NEWS_URL%/gi,url);
            template = template.replace(/%NEWS_DATE%/gi,date);
            html += template;
		});
		$('[role="bobe-news-holder"]').html(html);
	}

	var initBgImgPreplacer = function(){

		var source = $('*[role="bg-img-source"]');
    	var target = $('*[role="bg-img-display"]');

        var url = source.map(function(){
                return this.src
            });

        for(var i=0; i<url.length; i++){
        	$(target[i]).css("background-image","url("+url[i]+")");
        }


    }
    
	var renderTopic = function(resp){
		var html = '';
    	$.each(resp, function(index,item){
    		if(index>3){
    			return false;
    		}
            var template = $("#template-topic-item").html();
			var url = '/INSEBWeb/servlet/HttpDispatcher/ECA2_0401/prompt?SERIAL_NO='+item.SERIAL_NO+'&type=&pageFlag=';
			var title = item.SUBJECT;
			var date = item.EFFECT_DATE.replace(eval('/-/g'),"/").substr(0,10);
			var img = item.PIC;
            template = template.replace(/%TOPIC_TITLE%/gi,title);
            template = template.replace(/%TOPIC_URL%/gi,url);
            template = template.replace(/%TOPIC_DATE%/gi,date);
            template = template.replace(/%TOPIC_IMG%/gi,img);
            html += template;
		});
		$('[role="bobe-topic-holder"]').html(html);
		initBgImgPreplacer();
	}

	var getTopic = function(){
		
		var params = 'type=';
		var type = "POST";
		var url = ecDispatcher + 'ECA2_0400/getPageInfoLst';
		
		if(isLocalhost()){
			url += '.json';
			type = 'GET';
		}
		$.ajax({
			type: type,
			url : url,
			data: encodeURI(params),
			dataType: "JSON"
		}).done(function(resp) {
			renderTopic(resp.pageInfo);
		});
	}
	renderBanner();
	renderNews();
	getTopic();
})