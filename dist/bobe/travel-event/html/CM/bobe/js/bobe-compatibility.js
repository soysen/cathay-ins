this.NavbarPlugin = function(){

	//breadcrumb adjustment
	var breadcrumb = $('.breadcrumb');
	if( breadcrumb.length >0){
		breadcrumb.wrap( "<div class='breadcrumb-container'><div class='container'></div></div>" );
	}


	var bs_collapse = $('.bs_collapse');
	var bs_collapse_trigger = $('[data-toggle="collapse"]');
	var bs_nav_collapse = $(".navbar-collapse");

	//dropdown
	var dropdownElement = $('#bobe-hamburger-menu .dropdown');
	var bs3dropdownElement = $("#bobe-hamburger-menu a.nav-link");

	//collapsing
	var collapseElement = $(".bobe-collapsing");
	var collapseTrigger = $(".bobe-collapsing-trigger",collapseElement);

	// $(window).resize(initDropdownhover());
	console.log(bs_collapse_trigger);

	var addEventListener = function(){

		var _body = $('body');
		var _html = $('html');

		if($(window).width() > 992){

		    dropdownElement.hover(function() {
		      $(this).find('a.nav-link').attr('aria-expanded','true');
		      $(this).find('.dropdown-menu').addClass('show');
		    }, function() {
		      $(this).find('.dropdown-menu').removeClass('show');
		      $(this).find('a.nav-link').attr('aria-expanded','false');
		    });


		    bs3dropdownElement.attr("data-toggle","");

		}

		collapseTrigger.bind('click',function(){
			$(this).toggleClass("show");
			var target = $(this).attr("data-target");
			$(target).toggleClass("show");
		})	

		bs_collapse_trigger.click(function(){
			
			bs_collapse.collapse('hide');

		})

		var _pos;

		bs_nav_collapse.on('show.bs.collapse',function(){
			$(this).addClass('collapsed');
		}).on('hide.bs.collapse',function(){
			$(this).removeClass('collapsed');
		})

		var _pos;

		bs_collapse.on('show.bs.collapse',function(){

			_pos = $(window).scrollTop();
			_body.css({'position':'fixed','top': -_pos});
			_body.addClass("body-locked");


		}).on('hide.bs.collapse',function(){

			_body.removeClass("body-locked");
			_body.css({'position':'relative','top': 'auto'});

			$(window).scrollTop(_pos);

		})

	}

	var init = function(){
		addEventListener();
		console.log('%cNavbar Plugin initialized.','background: #222; color: #bada55')
	}

	return{
		init:init
	}

}

