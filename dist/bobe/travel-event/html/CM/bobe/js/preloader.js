$(function(){

	$.blockUI = lockScreen;
	$.unblockUI = unLockScreen;
	
	var path = "/INSEBWeb/html/CM/bobe/";
	var manifest = [
		
		// {id:"vendor.min.css",src:path+"/css/vendor.css"},	
		// {id:"bobe-official.min.css",src:path+"/css/bobe-official.css"},
		// {id:"ScrollMagic.min.js", src:"http://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.5/ScrollMagic.min.js"},
		// {id:"animation.gsap.min.js", src:path+"/libs/plugins/animation.gsap.min.js"},
		// {id:"vendor.js", src:path+"js/vendor.js"},
		// {id:"vendor.js", src:path+"libs/popper.min.js"},
		// {id:"vendor.js", src:path+"libs/bootstrap.min.js"},
		//{id:"form.js", src:path+"js/form.js"},
		{id:"navbar-plugin.js", src:path+"js/navbar-plugin.js"},
		// {id:"dopeform.js", src:path+"js/dopeform.js"},
		{id:"bobe-app.js", src:path+"js/bobe-app.js"}
	];

	//component
	var _BobeNewsLoader = $("[role='data-loader-container']");
	var _HomeSlick = $("[role='banner-carousel']");
	var _BobeTab = $("[role='bobe-tab-container']");

	var preload = function(){

		queue = new createjs.LoadQueue(true, null, true);	
		queue.on("fileload", handeFileLoad, this);					 
		queue.on ("complete", handleComplete, this);		 
		queue.on("error", handleError, this);
		queue.loadManifest(manifest);


		if(_HomeSlick.length){
			// queue.loadFile({id:"slick.min.js", src:path+"/libs/slick.min.js"});
			// queue.loadFile({id:"home-slick.js", src:path+"/libs/home-slick.js"});
			// queue.loadFile({id:"bobe-homeslick.js", src:path+"/js/bobe-homeslick.js"});
		//	console.log("Found "+ _HomeSlick.length+" %c BannerCarousl ","background:#aaaaaa;color:#fff","element:",_HomeSlick)
		}

		if(_BobeTab.length){
			queue.loadFile({id:"bobe-tab.js", src:path+"/js/bobe-tab.js"});
		//	console.log("Found "+ _BobeTab.length+" %c BobeTab ","background:#aaaaaa;color:#fff","element:",_BobeTab)
		}


		if(_BobeNewsLoader.length){
			queue.loadFile({id:"ajax-pagination.js", src:path+"/js/ajax-pagination.js"});
		//	console.log("Found "+ _BobeNewsLoader.length+" %c BobeNewsLoader ","background:#aaaaaa;color:#fff","element:",_BobeNewsLoader)
		}

	}

	var handleError = function( e ){

		console.log( "error loading " + e.data.src);

	}

	var handeFileLoad = function( e ){

		//console.log('preload handling...');


	}

	var handleComplete = function(){

		//console.log('preload completed!');
		$(".loading-animation").remove();
	    		
	    var init = function(){
	    }

	    init();


	}

	var init = function(){

		preload();

        var fancybox = $('.fancybox')
        fancybox.bind('click',function(){
        $(this).fancybox()
          // var target = $(this).attr('data-fancybox');
          // console.log($(target))
          // $(target).fancybox()
        })

	}

	init();

});
