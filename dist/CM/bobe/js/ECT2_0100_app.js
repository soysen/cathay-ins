//最多保180天
var ISSUE_MAX_DAYS= 180;

//單一保單最多5個國家
var REGION_MAX_LENGTH = 5;

//日期格式
var dateformat = 'YYYY/MM/DD';

//最快收件出發前1小時
var ISSUE_MIN_HOURS = 1;

var BREAK_POINT_MD = 576;

$(function(){

    var formScope;
    var submit;
    var form;

    var now = moment();
    var ISSUE = moment(now).add(ISSUE_MIN_HOURS, 'hours');
    var INS_START_Y = ISSUE.format(dateformat+'/HH').split('/')[0];
    var INS_START_M = ISSUE.format(dateformat+'/HH').split('/')[1];
    var ISSUE_D = ISSUE.format(dateformat+'/HH').split('/')[2];
    var START_TIME = ISSUE.format(dateformat+'/HH').split('/')[3];

    var ISSUE_MAX = moment(now).add(ISSUE_MAX_DAYS, 'd');

    var ISSUE_MAX_Y = ISSUE_MAX.format(dateformat+'/HH').split('/')[0];
    var ISSUE_MAX_M = ISSUE_MAX.format(dateformat+'/HH').split('/')[1];
    var ISSUE_MAX_D = ISSUE_MAX.format(dateformat+'/HH').split('/')[2];


    var isLocalhost = function(){
        //console.log('index.js location.host',location.host);
        if(location.host == 'localhost:9998' || location.host == 'bobe.albertlan.net'){
            return true;
        }
        return false;
    }

    var initBgImgPreplacer = function(){

        var source = $('*[role="bg-img-source"]');
        var target = $('*[role="bg-img-display"]');

        var url = source.map(function(){
                return this.src
            });

        for(var i=0; i<url.length; i++){
            $(target[i]).css("background-image","url("+url[i]+")");
        }


    }
    
    /**
    var renderTopic = function(resp){
        var html = '';
        $.each(resp, function(index,item){
            if(index>3){
                return false;
            }
            var template = $("#template-topic-item").html();
            var url = '/INSEBWeb/servlet/HttpDispatcher/ECA2_0401/prompt?SERIAL_NO='+item.SERIAL_NO+'&type=&pageFlag=';
            var title = item.SUBJECT;
            var date = item.EFFECT_DATE.replace(eval('/-/g'),"/").substr(0,10);
            var img = item.PIC;
            template = template.replace(/%TOPIC_TITLE%/gi,title);
            template = template.replace(/%TOPIC_URL%/gi,url);
            template = template.replace(/%TOPIC_DATE%/gi,date);
            template = template.replace(/%TOPIC_IMG%/gi,"https://w3.bobe.com.tw/INSEBWeb/html/CM/bobe/img/events/event__3.jpg");
            html += template;
        });
        $('[role="bobe-topic-holder"]').html(html);
        initBgImgPreplacer();
    }

    var getTopic = function(){
        
        var params = 'type=';
        var type = "POST";
        var url = ecDispatcher + 'ECA2_0400/getPageInfoLst';
        
        if(isLocalhost()){
            url += '.json';
            type = 'GET';
        }
        $.ajax({
            type: type,
            url : url,
            data: encodeURI(params),
            dataType: "JSON"
        }).done(function(resp) {
            renderTopic(resp.pageInfo);
        });
    }    
     */
    
    /*
    var onDateRangePickerComplete = function(a_startDate,a_endDate){
    	var groupId = $(formScope).attr('data-value');
        //alert("start:"+moment(a_startDate).format(dateformat)+" end:"+moment(a_endDate).format(dateformat));
    	doCheckTravelDatePeriod(groupId,'N');
    }*/
    
    var initDateRangePicker = function(a_target){
        if(a_target.length==0){
            alert(a_target +' is not exist!');
            return false;
        }   

        var onDateRangePickerComplete = function(a_startDate,a_endDate,inst){
            display_target.val(moment(a_startDate).format(dateformat)+" - "+moment(a_endDate).format(dateformat));
            restoreDateRangePickerVal(inst);
            
            var groupId = $(formScope).attr('data-value');
            //alert("start:"+moment(a_startDate).format(dateformat)+" end:"+moment(a_endDate).format(dateformat));
        	doCheckTravelDatePeriod(groupId,'N');
        }
        var stringToDate = function(a_str){
            var date = a_str.split("/");
            var y = parseInt(date[0]);
            var m = parseInt(date[1])-1;
            var d = parseInt(date[2]);
            return new Date(y,m,d);
        }

        var validateDateRangePicker = function(a_inst){
            //console.log('----validateDateRangePicker----');
            if(start_date == undefined){
                //console.log('start_date == undefined valid false');
                $(".bobe-drp-submit").addClass("mbsc-disabled");
                return false;
            }
            if(end_date == undefined){
                //console.log('end_date == undefined valid false');
                $(".bobe-drp-submit").addClass("mbsc-disabled");
                return false;
            }

            $(".bobe-drp-submit").removeClass("mbsc-disabled");
            //console.log('valid true');
            //console.log("start:"+moment(start_date).format(dateformat)+" end:"+moment(end_date).format(dateformat));
        }

        var toggleDisplay = function(a_boolean){


            // display_target.removeClass("d-none").addClass("d-inline");
            // target.removeClass("d-none").addClass("d-inline");
            // return false;
            if(a_boolean){
                // target.css("border-color",'#333');
                // display_target.css("border-color",'#ff0000');
                display_target.removeClass("d-none").addClass("d-inline");
                // target.removeClass("d-inline").addClass("d-none");
            }else{
                // target.css("border-color",'#ff0000');
                // display_target.css("border-color",'#333');
                // target.removeClass("d-none").addClass("d-inline");
                display_target.removeClass("d-inline").addClass("d-none");
            }
        }

        var createDisplayElm = function(a_target){
            var elm = a_target.clone();
            var id = a_target.attr("id")+"_DISPLAY";
            var name = a_target.attr("name")+"_DISPLAY";
            var data_form_name = a_target.attr("data-form-name")+"_DISPLAY";
            elm.attr("id",id);
            elm.attr("name",name);
            elm.attr("data-form-name",data_form_name);
            elm.attr("role","");
            elm.attr("readonly",false);
            elm.attr("required",false);
            elm.css('position','absolute');
            elm.css('top',0);
            elm.css('left',0);
            elm.removeClass("d-inline").addClass("d-none");
            a_target.attr("data-display-target","#"+id);
            a_target.after(elm);
            return elm;
        }

        var restoreDateRangePickerVal = function(inst){

            var tmp_date = display_target.val().split(" - ");

            //console.log('restoreDateRangePickerVal',tmp_date);
            if(tmp_date[0]==undefined || tmp_date[1] == undefined){
                inst.clear();
                return false;
            }
            setTimeout(function(){
                inst.option({
                    min: new Date(INS_START_Y,parseInt(INS_START_M)-1, ISSUE_D),
                    max: undefined
                });
                inst.setVal([new Date(stringToDate(tmp_date[0])),stringToDate(tmp_date[1])],true,true,true);    
            },0)
            
        }
        var target = a_target;
        var display_target = createDisplayElm(target);
        var instance;

        now = moment();
        ISSUE = moment(now).add(ISSUE_MIN_HOURS, 'hours'); 
        INS_START_Y = ISSUE.format(dateformat+'/HH').split('/')[0];
        INS_START_M = ISSUE.format(dateformat+'/HH').split('/')[1];
        ISSUE_D = ISSUE.format(dateformat+'/HH').split('/')[2];
        START_TIME = ISSUE.format(dateformat+'/HH').split('/')[3];

        ISSUE_MAX = moment(now).add(ISSUE_MAX_DAYS, 'd');

        ISSUE_MAX_Y = ISSUE_MAX.format(dateformat+'/HH').split('/')[0];
        ISSUE_MAX_M = ISSUE_MAX.format(dateformat+'/HH').split('/')[1];
        ISSUE_MAX_D = ISSUE_MAX.format(dateformat+'/HH').split('/')[2];

        var start_date;
        var end_date;
        var min_date;
        var max_date;
        var process_ary = new Array();
        if(target.hasClass('mbsc-comp')){
            return false;
        }
        instance = target.mobiscroll().range({ 
            theme: 'material',
            lang: 'zh',
            display: 'bottom',
            dateFormat:'yyyy/mm/dd',
            min: new Date(INS_START_Y,parseInt(INS_START_M)-1, ISSUE_D),
            buttons: [ 
                { 
                    text: '取消',
                    handler: 'cancel',
                    cssClass: 'mbsc-fr-btn-e mbsc-fr-btn', 
                    handler: function (event, inst) {
                        inst.clear(); 
                        inst.hide();
                        restoreDateRangePickerVal(inst);
                        toggleDisplay(false);
                    } 
                },
                { 
                    text: '送出',
                    cssClass: 'mbsc-fr-btn-e mbsc-fr-btn mbsc-disabled bobe-drp-submit', 
                    handler: function (event, inst) {
                        //alert('Custom button clicked!'+start_date+"  "+end_date); 
                        onDateRangePickerComplete(start_date,end_date,inst);
                        // inst.clear();
                        inst.hide();
                        toggleDisplay(false);
                    } 
                }
            ],
            responsive: {
                small: {
                    display: 'bottom'
                },
                custom: { // Custom breakpoint
                    breakpoint: 600
                },
                large: {
                    display: 'bubble',
                    months: '2'
                }
            },
            onDayChange: function (event, inst) {
                process_ary.push('onDayChange');
                //console.log('onDayChange',event.active,moment(event.date).format(dateformat));
                switch(event.active){
                    case 'start':
                        start_date = event.date;
                        end_date = undefined;
                        min_date = moment(start_date);
                        max_date = moment(min_date).add(ISSUE_MAX_DAYS-1, 'd');
                        /////////////////////
                        inst.option({
                            min: min_date,
                            max: max_date
                        });

                        inst.setVal([event.date,undefined], false, false, true); // set the start value 
                        inst.setActiveDate('end')
                        //target.val(start_date.format(dateformat) +' - ');
                        //target.val('');
                        // console.log($(".mbsc-fr-btn1").length);
                        $(".bobe-drp-submit").addClass("mbsc-disabled");
                    break;

                    case 'end':
                        end_date = event.date;
                        //inst.setVal([start_date,end_date]);
                        //target.val(start_date.format(dateformat) +' - ' + end_date.format(dateformat));

                        //target.val('');
                        $(".bobe-drp-submit").removeClass("mbsc-disabled");
                    break;
                }
                
                validateDateRangePicker();
                toggleCompareTable(false);
            }
            ,
            onInit: function(event, inst){
                process_ary.push('onInit');                
            },
            onPageLoaded:function(event, inst){
                // console.log('onPageLoaded',start_date,end_date);
                if(start_date == undefined && end_date == undefined){
                    process_ary = [];
                    //console.log('onPageLoaded All New');
                    toggleDisplay(true);
                }
                process_ary.push('onPageLoaded');

                if(start_date != undefined && end_date == undefined){
                    //console.log('onPageLoaded Clicked Start Date')
                }

                if(start_date != undefined && end_date != undefined){
                    //console.log('onPageLoaded All Set')
                    toggleDisplay(true);
                }

            },
            onCancel: function (event, inst) {
                process_ary.push('onCancel');
                if(start_date == undefined && end_date == undefined){
                    //console.log('onCancel 什麼事都沒做 就關掉')
                }

                if(start_date != undefined && end_date == undefined){
                    //console.log('onCancel 點了開始日 就關掉')
                }

                if(start_date != undefined && end_date != undefined){
                   //console.log('onCancel 點了開始+結束日 就關掉');
                }
                restoreDateRangePickerVal(inst);
            }
            ,
            onClose: function (event, inst) {
                process_ary.push('onClose');
                
                //console.log('onClose',process_ary);

                if(process_ary[process_ary.length-2] == "onDayChange"){
                    //console.log('A');
                    // toggleDisplay(false);
                }else if(process_ary[process_ary.length-2] == "onCancel"){
                    //console.log('B');
                    toggleDisplay(false);
                    process_ary = [];
                }else{
                    //console.log('C');
                    toggleDisplay(true);
                }
            }
        });   
    }
    
    var initDateRangePickerOld = function(a_target){
        if(a_target.length==0){
            alert(a_target +' is not exist!');
            return false;
        }
        
       	var target = a_target;
        now = moment();
        ISSUE = moment(now).add(ISSUE_MIN_HOURS, 'hours'); 
        INS_START_Y = ISSUE.format(dateformat+'/HH').split('/')[0];
        INS_START_M = ISSUE.format(dateformat+'/HH').split('/')[1];
        ISSUE_D = ISSUE.format(dateformat+'/HH').split('/')[2];
        START_TIME = ISSUE.format(dateformat+'/HH').split('/')[3];

        ISSUE_MAX = moment(now).add(ISSUE_MAX_DAYS, 'd');

        ISSUE_MAX_Y = ISSUE_MAX.format(dateformat+'/HH').split('/')[0];
        ISSUE_MAX_M = ISSUE_MAX.format(dateformat+'/HH').split('/')[1];
        ISSUE_MAX_D = ISSUE_MAX.format(dateformat+'/HH').split('/')[2];

        var start_date;
        var end_date;
        var min_date;
        var max_date;

        target.mobiscroll().range({ 
            theme: 'material',
            lang: 'zh',
            display: 'bottom',
            dateFormat:'yyyy/mm/dd',
            min: new Date(INS_START_Y,parseInt(INS_START_M)-1, ISSUE_D),
            responsive: {
                small: {
                    display: 'bottom'
                },
                custom: { // Custom breakpoint
                    breakpoint: 600
                },
                large: {
                    display: 'bubble',
                    months: '2'
                }
            },
            onSetDate: function(event,inst){
            },
            onDayChange: function (event, inst) {
                
                switch(event.active){
                    case 'start':
                        start_date = event.date;
                        min_date = start_date = moment(start_date);
                        max_date = moment(min_date).add(ISSUE_MAX_DAYS-1, 'd');
                        /////////////////////
                        inst.option({
                            min: min_date,
                            max: max_date
                        });

                        inst.setVal([event.date], false, false, true); // set the start value 
                        inst.setActiveDate('end')
                    break;

                    case 'end':
                        end_date = moment(event.date);
                        inst.setVal([start_date,end_date]);
                    break;
                }
                //console.log('onDayChange',event.date);
                
                toggleCompareTable(false);
            }
            ,
            onChange: function (event, inst) {
                // console.log('onChange');
            }
            ,
            onBeforeShow: function(event, inst){
                // console.log('onBeforeShow');
            }
            ,
            onClose: function (event, inst) {
                // console.log('onClose');
                if(start_date != undefined && end_date != undefined){
                    //reset 
                    inst.option({
                        min: new Date(INS_START_Y,parseInt(INS_START_M)-1, ISSUE_D),
                        max: undefined
                    });
                    onDateRangePickerComplete(start_date,end_date);
                }
            }
        });   
    }

    var sendData = function(a_data){
        // location.href="2.html";
        toggleCompareTable(false);
        var groupId = $(formScope).attr('data-value');
        callAjax(ecDispatcher + 'ECT2_0100/getRecommandCase',getParams(groupId),sendDataComplete,'Y');
        /**
        var alert_msg = '請開啟console看form的值\n';
        for(var i in a_data){
            alert_msg += "變數:"+a_data[i].name+'   值:'+a_data[i].value+'\n'
        }

        //demo
        var url = window.location.pathname;
        var filename = url.substring(url.lastIndexOf('/')+1);
        switch(filename){
            case 'cobrand.html':
                var data = {};
                data.result = []
                data.result.push("1");
            break;

            default:
                var data = {};
                data.result = []
                data.result.push("1");
                data.result.push("2");
                data.result.push("3");
            break;
        }
        */
        //sendDataComplete(data);
        //demo end
    }

    var toggleCompareTable = function(a_boolean,a_mode){

        var target;
        switch(a_boolean){
            case true:
                switch(a_mode){
                    case "single":
                        target = $($("#bobe-compare-item-"+a_mode).html());
                        $("#bobe-compare-result").append(target);
                    break;

                    case "multi":
                        target = $($("#bobe-compare-item-"+a_mode).html());
                        $("#bobe-compare-result").append(target);
                    break;
                }
            break;

            case false:
                $("#bobe-compare-result").empty();
            break;
        }
        if(a_boolean){
            return target;
        }
    }

    var sendDataComplete = function(data){
        $.unblockUI();
        // $( document).scrollTop( 1000 );

        if(!isEmpty(data.error)) {
            if(!isEmpty(data.deathDisablerInsuredWEB)) {
                $('#' + 'deathDisablerInsured' + data.groupId).val(data.deathDisablerInsuredWEB);
                repaintUI('deathDisablerInsured' + data.groupId);
            }
            alertMsg(data.error);
            return;
        }
        
        //console.log('a_obj.result',a_obj.result.length);
        //if(a_obj.result.length==1){
        //    toggleCompareTable(true,"single");
        //}else{
        

            var target = toggleCompareTable(true,"multi");
            
            // 大版不便險，三包集合的項目清單，先Loop出來
            if(data.showConvnte == "Y") {
                var seqCnt = 0;
                $.each(data.convnteAllItemMap, function(key, val) {
                    seqCnt++;
                    if(seqCnt <= 5) {
                        // $('#travelConvnteTableSubject').append();
                        $('<div class="cell" id="' + key + '">' + val + '</div>').insertBefore($("#travelConvnteTableSubjectMore"));
                    }else{
                        $('#travelConvnteTableSubjectMore').append('<div class="cell" id="' + key + '">' + val + '</div>');
                    }
                });
            }else{
                $('#lv1TravelConvnteDiv').remove();
                $('#lv2TravelConvnteDiv').remove();
                $('#lv3TravelConvnteDiv').remove();
                $('#travelConvnteTableContent').remove();
                $('.bobe-compare__table-footer').remove();
            }
            
            
            var hasDeathDisablerInsured = false;
            var hasRmbsmnt = false;
            var hasSuddenlyCmplnt = false;
            var hasDeathSpecific = false;
            
            if(isEmpty(data.trialList)) {
            	alertMsg('系統忙錄中，請稍候...');
            	return;
            }
            
            $.each(data.trialList, function(i, item){
                
                var tagSeq = 'lv' + item.SORT_NO;
                
                if(item.SORT_NO == '3' && item.BTN_DISABLED == 'Y') {
                    $('#lv3BtnInsure').removeClass('bobe-btn-orange');
                    $('#lv3BtnInsure').addClass('bobe-btn-white');
                    $('#lv3BtnInsure').attr('onclick','doShow7DaysTip();');
                }
                
                // 方案名稱
                $('#' + tagSeq + 'PlanNameMobile').html(item.TITLE);
                $('#' + tagSeq + 'PlanNamePC').html(item.TITLE);

                // 優惠保費
                $('#' + tagSeq + 'PricePC').html(item.DISCOUNT_PREMIUM-item.TERRORSM_PREMIUM);
                $('#' + tagSeq + 'PriceMobile').html(item.DISCOUNT_PREMIUM-item.TERRORSM_PREMIUM);

                // 小版(卡片式的內容) -----------------------------------------------------------------------------------------------
                $('#' + tagSeq + 'TravelInsuranceContent').empty();
                if(!isEmpty(item.DEATH_DISABLER_INSURED_TT) && parseInt(item.DEATH_DISABLER_INSURED_TT,10) > 0) {
                    $('#' + tagSeq + 'TravelInsuranceContent').append('<li>意外事故身故失能保險金 - 限額<span class="plan-detail__amt">' + item.DEATH_DISABLER_INSURED_TT + '萬</span></li>');
                    hasDeathDisablerInsured = true;
                }
                if(!isEmpty(item.RMBSMNT_TT) && parseInt(item.RMBSMNT_TT,10) > 0) {
                    $('#' + tagSeq + 'TravelInsuranceContent').append('<li>傷害醫療實支實付型保險金 - 限額<span class="plan-detail__amt">' + item.RMBSMNT_TT + '萬</span></li>');
                    hasRmbsmnt = true;
                }
                if(!isEmpty(item.SUDDENLY_CMPLNT_TT) && parseInt(item.SUDDENLY_CMPLNT_TT,10) > 0) {
                    $('#' + tagSeq + 'TravelInsuranceContent').append('<li>海外突發疾病及燒燙傷保險金 - 限額<span class="plan-detail__amt">' + item.SUDDENLY_CMPLNT_TT + '萬</span></li>');
                    hasSuddenlyCmplnt = true;
                }
                if(!isEmpty(item.DEATH_SPECIFIC_TT) && parseInt(item.DEATH_SPECIFIC_TT,10) > 0) {
                    $('#' + tagSeq + 'TravelInsuranceContent').append('<li>特定意外死殘保額 - 限額<span class="plan-detail__amt">' + item.DEATH_SPECIFIC_TT + '萬</span></li>');
                    hasDeathSpecific = true;
                }
                if(!isEmpty(item.convnteViewList)) {
                    $('#' + tagSeq + 'TravelConvnteContent').empty();
                    $.each(item.convnteViewList, function(j, convnteItem) {
                        $('#' + tagSeq + 'TravelConvnteContent').append('<li>' + convnteItem.ITEM2 +'<span class="plan-detail__amt">' + convnteItem.ITEM3 + '</span></li>');
                    });
                }else {
                	// 小版 - 沒有不便險，要移除不便險 title
                	$('#' + tagSeq + 'TravelConvnteDiv').remove();
                }
                
                // 大版(Table) -----------------------------------------------------------------------------------------------
                $('#travelInsuranceTableSubject').empty();
                $('#travelInsuranceTableSubject').append('<div class="cell">意外事故身故失能保險金 - 限額</div>');
                $('#travelInsuranceTableSubject').append('<div class="cell">傷害醫療實支實付型保險金 - 限額</div>');
                $('#travelInsuranceTableSubject').append('<div class="cell">海外突發疾病及燒燙傷保險金 - 限額</div>');
                if(hasDeathSpecific) {
                    $('#travelInsuranceTableSubject').append('<div class="cell">特定意外死殘保額 - 限額</div>');
                }
                
                $('#travelInsuranceTableSubjectMore').empty().hide();   // 目前不需要隱藏旅平的部份，因為項目只有三個
                
                // $('#' + tagSeq + 'TravelInsuranceTableAmount').append('<div class="cell">' + (hasDeathDisablerInsured ? item.DEATH_DISABLER_INSURED_TT + '萬' : '-') + '</div>');
                // $('#' + tagSeq + 'TravelInsuranceTableAmount').append('<div class="cell">' + (hasRmbsmnt ? item.RMBSMNT_TT + '萬' : '-') + '</div>');
                // $('#' + tagSeq + 'TravelInsuranceTableAmount').append('<div class="cell">' + (hasSuddenlyCmplnt ? item.SUDDENLY_CMPLNT_TT + '萬' : '-') + '</div>');
               
                $('<div class="cell">' + (hasDeathDisablerInsured ? item.DEATH_DISABLER_INSURED_TT + '萬' : '-') + '</div>').insertBefore($('#' + tagSeq + 'TravelInsuranceTableAmountMore'));
                $('<div class="cell">' + (hasRmbsmnt ? item.RMBSMNT_TT + '萬' : '-') + '</div>').insertBefore($('#' + tagSeq + 'TravelInsuranceTableAmountMore'));
                $('<div class="cell">' + (hasSuddenlyCmplnt ? item.SUDDENLY_CMPLNT_TT + '萬' : '-') + '</div>').insertBefore($('#' + tagSeq + 'TravelInsuranceTableAmountMore'));
                

                if(hasDeathSpecific) {
                    $('<div class="cell">' + item.DEATH_SPECIFIC_TT + '萬' + '</div>').insertBefore($('#' + tagSeq + 'TravelInsuranceTableAmountMore'));
                    // $('#' + tagSeq + 'TravelInsuranceTableAmount').append('<div class="cell">' + item.DEATH_SPECIFIC_TT + '萬' + '</div>');

                }
                
                $('#' + tagSeq + 'TravelInsuranceTableAmountMore').empty();   // 目前不需要隱藏旅平的部份，因為項目只有三個
                $('#' + tagSeq + 'TravelInsuranceTableAmountMore').hide();    // 目前不需要隱藏旅平的部份，因為項目只有三個
                // $('#' + tagSeq + 'TravelInsuranceTableAmountMore').remove();  // 目前不需要刪除旅平結構的部份，因為項目只有三個 by ald

                if(data.showConvnte == "Y") {
                    var seqCnt = 0;

                    $.each(data.convnteAllItemMap, function(key, val) {
                        seqCnt++;
                        var amountValTmp = '-';
                        if(!isEmpty(item.convnteAmountMap)) {
                            $.each(item.convnteAmountMap, function(amountKey, amountVal) {
                                if(amountKey == key) {
                                    amountValTmp = amountVal;
                                }
                            });
                        }
                        if(seqCnt <= 5) {
                            $('<div class="cell">' + amountValTmp + '</div>').insertBefore($('#' + tagSeq + 'TravelConvnteTableAmountMore'));
                            // $('#' + tagSeq + 'TravelConvnteTableAmount').append('<div class="cell">' + amountValTmp + '</div>');
                        }else{
                            $('#' + tagSeq + 'TravelConvnteTableAmountMore').append('<div class="cell">' + amountValTmp + '</div>');
                        }
                    });
                }
                
                
                $('#' + tagSeq + 'BtnInsure').attr('data-amount-id', item.AMOUNT_ID);
                $('#' + tagSeq + 'BtnInsure').attr('data-group-id', item.GROUP_ID);
                
            });
            
            //console.log('sendDataComplete Finish');
            initBobeCompare(target,"table");
            
        //}
        
    }

    var validate = function(){
		var errCnt = 0;
		var groupId = $(formScope).attr('data-value');
		//console.log('groupId=' + groupId);
		var assuredCountType = $('input[name ^= assuredCountType' + groupId + ']:checked').val();
		if(!doCheckTravelArea(groupId, 'Y')) { errCnt++; }
		if(!doCheckPersonType(assuredCountType, groupId, 'Y')) { errCnt++; }
		if(!doCheckTravelDatePeriod(groupId,'Y')) { errCnt++; }
		if(!doCheckDeathDisablerInsured(groupId)) { errCnt++; }
		
		if(errCnt > 0) {
			if($(".is-invalid").length>0){
				var scrollPos = $(".is-invalid").eq(0).parent().offset().top;
				UTILS.scrollPage(scrollPos);
			}
			return;
		}
    	
		$.blockUI();
        setTimeout(sendData,100);
    }

    var addEventListener = function(){

    }

	var initChosenSelect = function(){

        $(".chosen-select").chosen({
            disable_search:true,
            inherit_select_classes:true
        });
	}

	

	var initBobeCompare = function(a_scope,a_mode){
		var scope = a_scope
		var mode;
		if(a_mode==undefined){
			a_mode = 'table';
		}
        
		if($(window).width()<=425 && a_mode == 'table'){
			a_mode = 'card';
		}

		$(window).resize(function(){
			if($(window).width()<=425 && a_mode == 'table'){
				switchMode('card');
			}else{
				setTimeout(resizeCompareTable,100);
			}
		})
		default_mode = a_mode;

		var switchMode = function(a_mode){
			if(a_mode!="table" && a_mode!= "card" && a_mode!= "single"){
				a_mode = "table";
			}
			mode = a_mode;
			var target = $(".bobe-compare__toolbar",scope);
			$("a",target).removeClass("active");
			$(".bobe-compare__toolbar-"+mode,target).addClass("active");

			switch(mode){
				case "table":

					$(scope).addClass("thumb");

                    if($(window).width()<=768){
                        $(".w-ins").addClass("d-none");
                    }else{
                        $(".w-ins").removeClass("d-none");
                    }
					$(".w-product-item").removeClass("d-none");
					//

					if($(".plan-item--highlight .shadow",$(".bobe-compare__card")).length==0){
						$(".plan-item--highlight",$(".bobe-compare__card")).append('<div class="shadow""></div>');
					}
					var shadow_h = $(".bobe-compare__card",scope).height()+$(".bobe-compare__table",scope).height();
					// $( ".shadow",scope).height(shadow_h);
                    $( ".shadow",scope).remove();
				break;

                case "card":
                    $(scope).removeClass("thumb");
                    $(".w-ins").addClass("d-none");
                    $(".w-product-item").addClass("d-none");
                break;

                case "single":
                break;
			}
		}

		var resizeCompareTable = function(){
            
            // console.log('resizeCompareTable');
            
            var isShow = $(".more-product-spec").hasClass('show');
            $(".more-product-spec").addClass('show');
            var array = new Array($(".d-flex.bg-green"),$(".d-flex.bg-blue"))
            //
            if($(window).width()<=768){
                $(".w-ins").addClass("d-none");
            }else{
                $(".w-ins").removeClass("d-none");
            }
            
            var minH = 45;
            for(var g=0;g<array.length;g++){
                target = array[g];
                target.each(function(){
                    var total = $(this).length;
                    var i;
                    for(i=0;i<total;i++){
                        var total_source_cell = $(".product-item>.cell",$(this)).length;
                        for(var j=0;j<total_source_cell;j++){
                            var max_h = 0;
                            var target_ary = new Array();
                            var source_cell = $(".product-item>.cell",$(this)).eq(j);
                            target_ary.push(source_cell);
                            source_cell.css('height','auto');
                            var h = source_cell.height();
                            source_cell.attr("data-h",h);
                            if(h<minH){
                                h = minH+10;
                            }else{
                                h+=20
                            }
                            if(h>max_h){
                                max_h = h;
                            }
                            $(".product-spec",$(this)).each(function(){
                                var child_cell = $(">.cell",$(this)).eq(j)
                                child_cell.css('height','auto');
                                var h = child_cell.height();
                                child_cell.attr("data-h",h);
                                if(h<minH){
                                h = minH+10;
                                }else{
                                    h+=20
                                }
                                target_ary.push(child_cell);
                                if(h>max_h){
                                    max_h = h;
                                }
                            })
                            for(var k in target_ary){
                                target_ary[k].css('height',max_h);
                            } 
                        }
                        /////////////////////////more item
                        var total_source_cell = $(".product-item>.more-product-spec>.cell",$(this)).length;
                        for(var j=0;j<total_source_cell;j++){
                            var max_h = 0;
                            var target_ary = new Array();
                            var source_cell = $(".product-item>.more-product-spec>.cell",$(this)).eq(j);
                            target_ary.push(source_cell);
                            source_cell.css('height','auto');
                            source_cell.attr("data-h",h);
                            var h = source_cell.height();
                            if(h<minH){
                                h = minH+10;
                            }else{
                                h+=20
                            }
                            if(h>max_h){
                                max_h = h;
                            }
                            $(".product-spec>.more-product-spec",$(this)).each(function(){
                                var child_cell = $(">.cell",$(this)).eq(j)
                                child_cell.css('height','auto');
                                var h = child_cell.height();
                                child_cell.attr("data-h",h);
                                if(h<minH){
                                h = minH+10;
                                }else{
                                    h+=20
                                }
                                target_ary.push(child_cell);
                                if(h>max_h){
                                    max_h = h;
                                }
                            })
                            for(var k in target_ary){
                                target_ary[k].css('height',max_h);
                            } 
                        }
                               
                    }
                })
            }
            // console.log('isShow',isShow)
            if(isShow){
                $(".more-product-spec").addClass("show");
            }else{
                $(".more-product-spec").removeClass("show");
            }
        }
		
		var initCompareToolbar = function(a_scope){
            

            if($("html").hasClass("is-mobile") && $(window).width()<768){
                $(".bobe-compare__toolbar",a_scope).remove();
                return false;
            }
			$(".bobe-compare__toolbar a",scope).on("click",function(e){
				var val = $(this).attr("data-value");
				switchMode(val);
			})


		}

		var initCompareCard = function(a_scope){
			var scope = $(".bobe-compare__card",a_scope);

            var card_slick;
            $(".plan-more",scope).bind("click",function(e){
                var window_w = $(window).width();
                if(window_w>=768){
                    switchMode('table');
                    e.preventDefault();
                    return false;
                }
                else{
                }
            })

            $('.plan-collapse').on('shown.bs.collapse', function () {
                setTimeout(function(){
                    //console.log('aaa',$(".slick-track").height());
                    $(".slick-list").height($(".slick-track").height())
                },1000)
            })

            $('.plan-collapse').on('hidden.bs.collapse', function () {
                setTimeout(function(){
                    //console.log('aaa',$(".slick-track").height());
                   $(".slick-list").height($(".slick-track").height())
                },1000)
            })
            
            if($("html").hasClass("is-mobile") && $(window).width()<768){
                $(".plan-item-gap",scope).remove();
                $(scope).slick({
                  infinite: false,
                  slidesToShow: 1,
                  slidesToScroll: 1,
                  dots: true,
                  centerPadding:'0px',
                  centerMode: true,
                  focusOnSelect: true,
                  arrows:true,
                  adaptiveHeight:false,
                  afterChange:function(){
                    //console.log('afterChange');
                    $(".slick-list").height($(".slick-track").height())
                  },
                  init:function(){
                    $('.bobe-compare__card').slick('setPosition');
                  }
                });
                setTimeout(function(){
                  $('.bobe-compare__card').slick('setPosition');
                },100)
            }
		}

		var initCompareTable = function(a_scope){
			var scope = $(".bobe-compare__table",a_scope);


            
            var target = $(".product-item",scope);
            target.each(function(){
                $(">.cell",$(this)).last().addClass("border-b-none");
            })
            var target = $(".product-spec",scope);
            target.each(function(){
                $(">.cell",$(this)).last().addClass("border-b-none");
            })
            $(".bobe-compare__table-footer .w-product-item").addClass("border-l-none");
           
            var target = $(".product-item",scope);
            target.each(function(){
                $(".more-product-spec>.cell",$(this)).last().addClass("border-b-none");
            })
            var target = $(".product-spec",scope);
            target.each(function(){
                $(".more-product-spec>.cell",$(this)).last().addClass("border-b-none");
            })


            if($("html").hasClass("is-mobile")){

                if($(window).width()<768){
                    $(".bobe-compare__table",a_scope).remove();
                    $(".bobe-compare__table-hint",a_scope).remove();
                }
                return false;
            }

			var o = this;
			var updateCompareTable = function(){
				var content_h = $(".bobe-compare__table-content",scope).height();
				var scroll_h = parseInt($(".bobe-compare__table-content",scope).attr("data-scroll-h"));
				if(content_h>scroll_h){
					$(".bobe-compare__table-content").addClass("bobe-compare__table-content--scroll");
					
					
					setTimeout(function(){
						$('.bobe-compare__table-content--scroll').unbind('scroll').bind('scroll',function(){
						var tp = $(this).scrollTop();
						var gap_ary = [0]; 
							$('.product-item').each(function(){
							gap_ary.push($(this).height());
						})
						var total = gap_ary.length;
						for(var i=0;i<gap_ary.length;i++){
							if(tp>=gap_ary[i]){
								if(i==total-1){
									$(".bobe-compare__table-hint>div").addClass("d-none");
									$(".bobe-compare__table-hint>div").last().removeClass("d-none");
								}else if(i==0){
									$(".bobe-compare__table-hint>div").addClass("d-none");
									$(".bobe-compare__table-hint>div").first().removeClass("d-none")
								}else{
									$(".bobe-compare__table-hint>div").addClass("d-none");
									$(".bobe-compare__table-hint>div").eq(i).removeClass("d-none");
								}
							}
						}
					}).trigger("scroll");
					},400);
				}else{
					 $(".bobe-compare__table-content").removeClass("bobe-compare__table-content--scroll");
				}
			}
		
			$('.more-product-spec',scope).on('show.bs.collapse', function () {
				var target = $( ".shadow",scope);
                var target_h = parseInt(target.attr("max-h"));
                target.height(target_h)

                var target = $(".product-item",scope).last();
                $(">.cell",target).last().removeClass("border-b-none");
                $(".more-product-spec>.cell",target).last().addClass("border-b-none");

                
                var target = $(".product-item",scope);
                target.each(function(){
                    if($(".collapse .cell",$(this)).length>0){
                       $(">.cell",$(this)).last().removeClass("border-b-none");
                    }
                })

                var target = $(".product-spec",scope);
                target.each(function(){
                    if($(".collapse .cell",$(this)).length>0){
                      $(">.cell",$(this)).last().removeClass("border-b-none");
                    }
                })
			});

			$('.more-product-spec',scope).on('shown.bs.collapse', function () {
				//resizeCompareTable();

			});
			$('.more-product-spec',scope).on('hide.bs.collapse', function () {
				var target = $( ".shadow",scope);
				var target_h = parseInt(target.attr("min-h"));
				target.height(target_h);
			});

			$('.more-product-spec',scope).on('hidden.bs.collapse', function () {
				//resizeCompareTable();

				var target = $(".product-item",scope);
				target.each(function(){
					$(">.cell",$(this)).last().addClass("border-b-none");
				})
				var target = $(".product-spec",scope);
				target.each(function(){
					$(">.cell",$(this)).last().addClass("border-b-none");
				})
			});
			

            setTimeout(resizeCompareTable,100);

		}

        initCompareCard(scope);
        initCompareToolbar(scope);
        initCompareTable(scope);
        
		switchMode(default_mode);
	}

    var setForm = function(a_target){
        formScope = a_target.find("form");
        submit = $('*[role="form_submit"]',formScope);

        // console.log(formScope.attr('role'));
        // console.log(a_target.find("form").length);
        // console.log('submit',submit.length);
        form = new Form(formScope); 
        submit.off().on("click",validate);

        initDateRangePicker($("[role='INS_DATE']",formScope));
    }
    
    var displayLightbox = function(e){
        var section = $.trim($(this).attr("role"));
        var obj = {};
        var modalMode = false;
        var windowW = $(window).width();
        var maxW = 9999;
        var minW =  100;
        var minH = 100;
        var maxH = 9999;
        switch (section) {
            case 'show-supported-creaditcard':
                var target = "#supportedCreaditCard";
                maxW = 400;
            break;
            case 'show-supported-creaditcard-for-personal':
                var target = "#supportedCreaditCardForPersonal";
                maxW = 400;
            break;

        }    
        $.fancybox.open({  
            href: target, 
            padding:0,
            modal:modalMode,
            maxWidth: maxW,
            minWidth: minW,
            maxHeight: maxH,
            minHeight: minH,
            autoResize:true,
            beforeShow: function() {
            }
        });
    }
    
    var init = function(){

        var mobiscroll_css = '<link id="bobe_mobiscroll_css" href="/INSEBWeb/html/CM/bobe/css/mobiscroll.jquery.min.css" rel="stylesheet" />'
       	$(mobiscroll_css).insertBefore($('#bobe_official_css'))
        var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
        if (isMobile) {
            $("html").addClass("is-mobile");
        }
        
        initChosenSelect();
        $('[data-toggle="tooltip"]').tooltip();
        $('#getPlanTab a').on('shown.bs.tab', function(){
            $(".chosen-select").chosen("destroy");
            initChosenSelect();
            setForm($($(this).attr("href")));
        });


        $(document).on("click", '[role="show-supported-creaditcard"]', displayLightbox);
        $(document).on("click", '[role="show-supported-creaditcard-for-personal"]', displayLightbox);
        setForm($($('#getPlanTab a').eq(0).attr("href")));
        addEventListener();

        //getTopic();

    }

    init();

})