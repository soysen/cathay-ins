/**
 * 共用版型
 * include from footer.jsp
 * 共用入口版型都會用到的設定
 * 
 * 1.設定上方主要選單 (HTML 在header.jsp)
 * 
 */
 
var isLocalhost = function(){
    //console.log('location.host',location.host);
    
    if(location.host == 'localhost:9998' || location.host == 'bobe.albertlan.net'){
        return true;
    }
    return false;
}

var isMember = false;
var BobeUtils = BobeUtils ||{
    initBday:function(a_target,a_min,a_max){
        mobiscroll.settings = {
             theme: 'bootstrap'
         };
        var target;
        if(a_target == undefined || a_target.length==0){
            alert('initBday: '+a_target +' is not exist');
            return false;
        }
        target = a_target;
        if(a_min != undefined && !isNaN(a_min)){
            var ISSUE_MIN_AGE = moment().subtract(a_min, 'year');
        }else{
            var ISSUE_MIN_AGE = 0;
        }

        if(a_max != undefined && !isNaN(a_max)){
            var ISSUE_MAX_AGE = moment().subtract(a_max, 'year');
        }else{
            var ISSUE_MAX_AGE =100;
            
        }
        
        var ISSUE_MAX_MONTH;
        var ISSUE_MAX_DATE; 
        // console.log('ISSUE_MIN_AGE',ISSUE_MIN_AGE.format('YYYY'),ISSUE_MIN_AGE.format('MM'),ISSUE_MIN_AGE.format('DD'));
        // console.log('ISSUE_MAX_AGE',ISSUE_MAX_AGE.format('YYYY'),ISSUE_MAX_AGE.format('MM'),ISSUE_MAX_AGE.format('DD'));
        target.mobiscroll().date({
           theme: 'ios',
           lang: 'zh',
           dateFormat:'yyyy/mm/dd',
           max:new Date(ISSUE_MIN_AGE.format('YYYY'),ISSUE_MIN_AGE.format('MM')-1,ISSUE_MIN_AGE.format('DD')),
           min:new Date(ISSUE_MAX_AGE.format('YYYY'),ISSUE_MAX_AGE.format('MM')-1,ISSUE_MAX_AGE.format('DD')),

            responsive: {
                small: {
                    display: 'bottom'
                },
                medium: {
                    display: 'bubble'
                },
                large: {
                    display: 'bubble'
                }
            }
        });

        return false;
    }
    ,
    initBgImgPreplacer:function(){

        var source = $('*[role="bg-img-source"]');
        var target = $('*[role="bg-img-display"]');

        var url = source.map(function(){
                return this.src
            });

        for(var i=0; i<url.length; i++){
            $(target[i]).css("background-image","url("+url[i]+")");
        }
    }
    ,
    renderTopic:function(resp){
        var html = '';
        $.each(resp, function(index,item){
            if(index>3){
                return false;
            }
            var template = 
                '<div class="col-6 col-lg-3">'+
                '   <a class="issue-item read-more-link" href="%TOPIC_URL%" title="%TOPIC_TITLE%">'+
                '        <div class="thumb-outer">'+
                '           <div class="thumbnail" role="bg-img-display">'+
                '               <img src="%TOPIC_IMG%" role="bg-img-source" alt="%TOPIC_TITLE%">'+
                '           </div>'+
                '        </div>'+
                '       <div class="issue-date">%TOPIC_DATE%</div>'+
                '       <div class="issue-title">'+
                '           <p>%TOPIC_TITLE%</p>'+
                '        </div>'+
                '       <span>了解更多 &nbsp;<i class="far fa-angle-right"></i></span>'+
                '   </a>'+
                '</div>';
            var url = '/INSEBWeb/servlet/HttpDispatcher/ECA2_0401/prompt?SERIAL_NO='+item.SERIAL_NO+'&type=&pageFlag=';
            var title = item.SUBJECT;
            var date = item.EFFECT_DATE.replace(eval('/-/g'),"/").substr(0,10);
            var img = item.PIC;
            template = template.replace(/%TOPIC_TITLE%/gi,title);
            template = template.replace(/%TOPIC_URL%/gi,url);
            template = template.replace(/%TOPIC_DATE%/gi,date);
            template = template.replace(/%TOPIC_IMG%/gi,img);
            html += template;
        });
        $('[role="bobe-topic-holder"]').html(html);
        BobeUtils.initBgImgPreplacer();
    }
    ,
    getTopic:function(){
        
        var params = 'type=';
        var type = "POST";
        var url = ecDispatcher + 'ECA2_0400/getPageInfoLst';
        
        if(isLocalhost()){
            url += '.json';
            type = 'GET';
        }
        $.ajax({
            type: type,
            url : url,
            data: encodeURI(params),
            dataType: "JSON"
        }).done(function(resp) {
            BobeUtils.renderTopic(resp.pageInfo);
        });
    }

}
var LayoutTool = LayoutTool || {
    /**主功能連結前置路徑**/
    path            : "",
    /**AJAX連結前置路徑**/
    ecDispatcher    : "",
    /**
     * "M";//行動載具
     * "P";//預設為電腦
     */
    viewDevice      : "P",
    projectId       : "",
    regFrom         : "",
    mrMemberID      : "",
    /**版型初始化**/
    init : function(){
    //init : function(ecDispatcher,path,viewDevice,projectId,regFrom,mrMemberID){
        // this.ecDispatcher    = ecDispatcher;
        // this.path            = path;
        // this.viewDevice      = viewDevice;
        // this.projectId       = projectId;
        // this.regFrom     = regFrom;
        // this.mrMemberID      = mrMemberID;
        this.ecDispatcher   = window.ecDispatcher;
        this.path           = window.path;
        this.viewDevice     = window.viewDevice;
        this.projectId      = window.projectId;
        this.regFrom        = window.regFrom;
        this.mrMemberID     = window.mrMemberID;
        this.doQueryMenu();

    },
    /**取得選單內容**/
    doQueryMenu : function(){
        var params = 'projectId=' + this.projectId;
        params = params + '&regFrom=' + this.regFrom;
        params = params + '&mrMemberID=' + this.mrMemberID;
        
        var url = LayoutTool.ecDispatcher + 'ECIE_1000/queryMenu';
        var type = 'POST';
        var dataType = 'JSON';

        //demo

        // console.log('doQueryMenu',isLocalhost())

        if (isLocalhost()) {
            url += ".json";
            type = 'GET';
        }
        //demo end

        //console.log('LayoutTool.ecDispatcher',LayoutTool.ecDispatcher);
        //console.log('url',url);
        
        
        $.ajax({
            type: type,
            url : url,
            data: encodeURI(params),
            dataType: "JSON"
        }).done(this.doQueryMenuCallBack);
    },

    /**
     * 取得選單內容CallBack
     * 解析選單資料組合成HTML並append至$('#mainMenuDiv')
     */
    doQueryMenuCallBack : function(data){
        var template = '';
        var z = 0;
        var html = '';
        var isMemberCenter = true;
        var idx = 0;
        

        //var addMebmerButton = $('#addMemberButton').val();


        $.each(data.menuVo.menuPojoList, function( i, item){

   //       console.log('item.',item.pageName,item.pageAddress);
            
            isMemberCenter = false;
            if(item.pageName=="會員中心"){
                isMemberCenter = true;
                template = template + '<li class="nav-item dropdown bobe-member-area d-lg-none">'

                html += '<div class="dropdown-menu" aria-labelledby="member-center-options">';
                html += '    <div class="bobe-sub-menu">';
                html += '        <ul class="bobe-sub-link-group">';
                
            }else{
                template = template + '<li class="nav-item dropdown">'
            }
            
            template = template + ' <a class="nav-link" href="'+LayoutTool.getHref(item.target,item.pageAddress)+'" role="button" id="insurance-get-started" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+item.pageName+'<h4 class="hidden-seo-element">'+item.pageName+'</h4></a>';
            template = template + '     <div class="dropdown-menu bobe-dropdown-menu"  aria-labelledby="insurance-get-started">';
            template = template + '         <div class="bobe-sub-menu">';
            template = template + '             <ul class="bobe-sub-link-group">';

            
            if(item.menuPojoList){$.each(item.menuPojoList, function( i, item){
                
                template = template + '<li><a class="dropdown-item" href="'+LayoutTool.getHref(item.target,item.pageAddress)+'">'+item.pageName+'<h5 class="hidden-seo-element">'+item.pageName+'</h5></a></li>';       
                    if(isMemberCenter){


                        if(idx%4==0 && idx != 0){
                            html += '        </ul>';
                            html += '        <ul class="bobe-sub-link-group">';
                        }
                        html += '            <li><a class="dropdown-item" href="'+LayoutTool.getHref(item.target,item.pageAddress)+'">'+item.pageName+'</a></li>';
                        idx++;
                    }
                    /*
                if(item.programsub == 'ECH1_0200'){   
                    
                }else if(item.pageTarget == 'MASK'){

                }else{
                    
                    
                }   */                
            })}

            template = template + '             </ul>';
            template = template + '         </div>';
            template = template + '     </div>';
            template = template + '</li>';
        });

        html += '        </ul>';
        html += '     </div>'
        html += '</div>'

       // $('[role="member-center-nav-holder"]').append(html);
 
        $('#mainMenuDiv').html(template);

        var navbarPlugin = new NavbarPlugin();
        navbarPlugin.init();
        doCheckLoginStatus();
    },
    /**
     * 以是否開新頁來判斷連結組合模式
     * @param target
     * @param pageAddress
     * @returns {String}
     * 
     * HREF 若無做用需代空連結字串 javascript:void(0);
     */
    getHref : function(target,pageAddress){
        var hrefPath = 'javascript:void(0);';
        if(pageAddress){
            hrefPath = target == '_self' ? LayoutTool.path + pageAddress : pageAddress;
        }
        return hrefPath;
    }
}



var BobeApp = (function (window) {

    var ecDispatcher    = window.ecDispatcher;
    var ecDispatcherF   = window.ecDispatcherF;
    var contextPath     = window.contextPath;
    var viewDeviceF     = window.viewDeviceF;
    var projectId       = window.projectId;
    var regFrom         = window.regFrom;
    var mrMemberID      = window.mrMemberID;

    var form;
    var formData;
    var signin_btn = $('[role="bobe-signin-btn"]');
    var logout_btn = $('[role="bobe-logout-btn"]');
    var submit_btn = $('[role="bobe-signin-form-submit"]');
    var member_center_btn = $('[role="member-center-btn"]');
    var msg_area = $('[role="bobe-signin-form-msg"]');
    var MemberID;
    var MemberPWD;
    var RememberMe;
    var submit_status_text = $(".sending-status",form);
    var isV3 = $("header").hasClass("bobe-bs-v3-compatibity");

    var document_pos = {};
    var otp_timer_int;
    //tracking
    window.addTextChangeForTD = function(){
        var TextChangeObj = new Object();
        TextChangeObj.name = 'Bobe';

        switch(formData.type){
            case "laptop":
                TextChangeObj.id = 'Bobe_Menu_Login';
            break;
            case "mobile":
                TextChangeObj.id = 'Bobe_Menu_Login';
            break;
            case "comm":
                TextChangeObj.id = 'Bobe_FancyBox_Login';
            break;

            default:
                return false;
            break;
        }
        
        TextChangeObj.value = MemberID; //帳號欄位的值
        TextChangeObj.tagName = 'input';
        TextChangeObj.type = 'text';
        if(window.cubcsatextchange) {
            window.cubcsatextchange (TextChangeObj);
        }
    }

    window.addClickForTD = function (){
        var AddClickObj = new Object();
        AddClickObj.name = 'Bobe';
        AddClickObj.id = 'Bobe_Login_Success';
        AddClickObj.title = 'Bobe會員登入成功';
        AddClickObj.tagName = 'SUBMIT';
        if(window.cubcsaclick) {
            window.cubcsaclick(AddClickObj);
        }
    }

    window.addClickForTDcall = function(call_name,call_id,call_title){
        var AddClickObj = new Object();
        AddClickObj.name = call_name;
        AddClickObj.id = call_id;
        AddClickObj.title = call_title;
        AddClickObj.tagName = 'SUBMIT';
        if(window.cubcsaclick) {
            window.cubcsaclick(AddClickObj);
        }
    }

    window.doUpdChgPwdTime = function(loginKind){

        var isNeedToCallCPT =  $('#isNeedToCallCPT').val();

        if(isNeedToCallCPT == 'N' || isNeedToCallCPT == undefined){
            $.fancybox.close();
            return;
        }
        var params = 'loginKind=' + loginKind;
        if(loginKind == 'header'){ params += '&toTrx=' + $('#loginForwardTrx').val(); } 
        
        callAjax(window.ecDispatcher + 'ECM1_0100/updChgPwdTime',params,updChgPwdTimeCallBack,false);
    }

    window.doLink2Trx = function(trxCode){
        $('#trxUniKey').val('');
        doSubmitFormEC($('#form1'),window.ecDispatcher + trxCode + '/prompt'); 
    }

    window.updChgPwdTimeCallBack = function (data){
        if(!isEmpty(data.error)){
            alertMsg(data.error);
            return;
        }
        $.fancybox.close();
    }

    window.doChgPwd = function (){
        var url = window.ecDispatcher + 'ECM1_1000/prompt?tab=7';
        doSubmitFormEC($('#form1'),url);
    }
    
    window.doLogin = function(){
        var needRemeber = false;
        var type = form.attr("data-type");
        var login_type = form.attr("login-type");

        formData = {};
        switch(login_type){
            case "otp":
                //stopTimer();

                switch(parseInt(MemberOTPStep.val())){
                    case 1:
                        formData.memberId = MemberID.val();
                        formData.memberBday = MemberBday.val()
                    break;

                    case 2:
                        formData.otpCode = MemberOTPCode.val();
                    break;
                }
                formData.otpStep = MemberOTPStep.val();
                formData.loginKind = type; //common,laptop,mobile
                formData.isOTP = 1;
            break;

            default:
                formData.memberId = MemberID.val();
                formData.memberPwd = MemberPWD.val();
                formData.rememberMe = "";
                formData.loginKind = type; //common,laptop,mobile
                formData.isOTP = 0;
            break;
        }

        if(RememberMe.prop("checked")){
            formData.rememberMe = RememberMe.val();
        }

        switch(type){
            case "laptop":
            case "mobile":
            case "comm":
            case "common-login":
            break;

            default:
                return false;
            break;
        }

        if(formData.rememberMe){
            //記住使用者ID, 寫入Cookie
            $.cookie.raw = true;
            $.cookie('memberIdCookie', $.base64.encode(formData.memberId), { path:'/', expires: 7 });
        }else{
            //不記住使用者ID, 清除Cookie
            try{
                $.removeCookie('memberIdCookie',{ path:'/'});
            }catch(err){}
        }

        var params = 'loginKind='+formData.loginKind;
        params += '&' + 'memberId=' +formData.memberId;
        params += '&' + 'memberPwd=' + formData.memberPwd;
        //params += '&' + 'viewDevice=P';
        params += '&' + 'viewDevice='+window.viewDevice;
        params += '&' + 'isOTP=' + formData.isOTP;
        params += '&' + 'otpCode=' + formData.otpCode;
        params += '&' + 'otpStep=' + formData.otpStep;
        //console.log('parms=' + params);
        
        switch(formData.loginKind){
            case 'laptop':
            case 'mobile':
                params += '&toTrx=' + $('#loginForwardTrx').val();
            break;

            case 'comm':
            case 'common-login':
            break;
        }

        //demo
        if(formData.isOTP){
        	msg_area.hide();
            $.blockUI();
            switch(parseInt(formData.otpStep)){
                case 1:
                    setTimeout(otpLoginCallBack,1500);
                break;

                case 2:
                    setTimeout(otpStep2LoginCallBack,1500);
                break;
            }
        }else{
            callAjax(ecDispatcher + 'ECM1_0100/login',params,loginCallBack,false);
        }

        //正式
    }

    var displayMsg = function(a_msg){
        msg_area.show();
        msg_area.html(a_msg);
    }

    ////
    var startTimer = function (duration,display,a_timeoutEent) {
        var timer = duration, minutes, seconds;
        var o = this;
        var _display = display;
        var updateTimer = function(){
            minutes = parseInt(timer / 60, 10)
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;
            _display.html(minutes + ":" + seconds);
            if (--timer < 0) {
                timer = duration;
                stopTimer();
                a_timeoutEent();
            }
        }
        stopTimer();
        otp_timer_int = setInterval(updateTimer, 1000);
        updateTimer();
    }

    var stopTimer = function(){
        clearInterval(otp_timer_int);
    }

    var otpStep2LoginCallBack = function(data){
        //驗證OTP及登入
//          doClearWarnMsg();
//          if(!doCheckOTPCode()){ return false; }
        var params = "";
        //loginOtpMemberIdGlobal
        //loginOtpCodeGlobal
        //loginOtpPreGlobal
        params += 'otpCode='+ MemberOTPCode.val() ;
        params += '&otpPre=' + $('#loginOtpPreGlobal').val();
        params += '&viewDevice=' + window.viewDevice;
        
        callAjax(ecDispatcher + 'ECM1_0100/loginOTPStep2',params,loginCallBack,false);
    }
    var otpLoginCallBack = function(data){
        
        msg_area.hide();
        var params = "";
        params += 'memberId='+ MemberID.val();
        var birthday = replaceAll(MemberBday.val(),'/','-');
        params += '&birthday=' + birthday;
        params += '&viewDevice=' + window.viewDevice;
        
        function leftPad(v){
            return (v<10 ? '0' : '')+v;
        };
        
        $.ajax({
            type: "POST",
            url : ecDispatcher + 'ECM1_0100/loginOTPStep1',
            data: encodeURI(params),
            dataType: "JSON",
            async: false
        }).done(function(data) {
        
            lockSubmit(false);
            $.unblockUI();
            if(!isEmpty(data.error)){
                displayMsg(data.error);// TODO
                //alertMsg('提醒您：'+data.error);
                return;
            }
            
            $(".otp-step-scope-1").addClass("d-none");
            $(".otp-step-scope-2").removeClass("d-none");

            var a_data = {};
            a_data.otp_validate_minutes = data.otpSecTime;
           
            startTimer(a_data.otp_validate_minutes*60,$("*[role='opt-time']"),otpOutofDate);
            $("[role='opt-phone']").html(data.cellPhone);
            $("[role='opt-time']").html(data.otpSecTime+':00');
            $("[role='resend-otp']").addClass("text-mute");
            $("[role='resend-otp']").addClass("disabled");
            $("[role='resend-otp']").removeClass("text-green");
            $("[role='resend-otp']").removeClass("link-green");
            $("[role='resend-otp']").css("cursor","default");
            
            
            // 詢問是否加入會員，是的話導至加入會員交易
            if(!isEmpty(data.isJoinMember) && data.isJoinMember == 'Y'){
                if(!confirm("您尚未加入會員，是否立即加入?")){
                    $('#loginOtpMemberIdGlobal').val('');
//                      $('#memberPwd').val('');
                    return;
                }
                doSubmitFormEC($('#form1'),ecDispatcher + 'ECM1_0400/prompt'); 
                return;
            }
            
            $('#loginOtpPreGlobalSpan').html('');
            $('#loginOtpCodeGlobal').val('');
            $('#loginOtpPreGlobal').val(data.verifyCodePre);
            $('#loginOtpPreGlobalSpan').html(data.verifyCodePre+' - ');
//              $('#cellPhone').html(data.cellPhone);
            $('#loginOtpCodeGlobal').val(data.verifyCode); //測試環境才會帶
        });
    }
    
//    reTry = function doReTry(){
//      
//      //清空otp-step-one區塊資料,並顯示
//      $('#divMemberID').show();
//      $('#memberId').val('');
//      $('#divBirthday').show();
//      
//      
//      //清空otp-step-two區塊資料,並隱藏
//      $("#divInputCode").hide();
//      $('#otpPreSpan').html('');
//      $('#otpCode').val('');//<%-- 測試環境才會帶 --%>
//      $('#cellPhone').html('');
//      $("#divResend").hide();
//      $('#sendCode').show();
//      $('#login').hide();
//      
//  }

    var otpOutofDate = function(){
         //alert("驗證碼已失效");
         $("[role='resend-otp'] [role='opt-time'").html('');
         $("[role='resend-otp']").addClass("text-green");
         $("[role='resend-otp']").addClass("link-green");
            $("[role='resend-otp']").removeClass("text-mute");
            $("[role='resend-otp']").removeClass("disabled");
         $("[role='resend-otp']").css("cursor","pointer");
    }

    var resendOTP = function(){
        if($(this).hasClass('disabled')){
            return false;
        }
        $.blockUI();
        setTimeout(otpLoginCallBack,1500);
    }

    var loginCallBack = function(data){

        //demo password error
        //var data = {"ErrMsg":{"returnCode":0,"displayException":"","displayMsgDescs":"","msgDesc":"","sysid":"","length":1,"msgid":"","msgDescs":[""],"type":"","url":""},"error":"您的帳號或密碼有誤，請再次確認並填入正確帳號密碼再登入!","loginKind":"header"}
        

        //demo guest 您尚未加入會員，是否立即加入?
        //var data  = {"ErrMsg":{"returnCode":0,"displayException":"","displayMsgDescs":"","msgDesc":"","sysid":"","length":1,"msgid":"","msgDescs":[""],"type":"","url":""},"loginKind":"header","isJoinMember":"Y"};


        //demo success
        //var data = "";


        //demo msg
        //var data = {'msg':'someerror',isNeedToCallCPT:0};
    	//console.log(data.error);
    	//console.log(formData.loginKind);
        if(!isEmpty(data.error)){
            switch(formData.loginKind){
                case 'laptop':
                case 'mobile':
                case 'common-login':
                    displayMsg(data.error);
                break;
                case 'comm':
                    // $.fancybox.close();
                    alertMsg(data.error,doOpenCommLogin);
                break;
            }
            lockSubmit(false);
            $.unblockUI();
            return false;
        }

    	stopTimer();
    	
        if(!isEmpty(data.isJoinMember) && data.isJoinMember == 'Y'){
            lockSubmit(false);
            $.unblockUI();
            if(!confirm("您尚未加入會員，是否立即加入?")){
                return false;
            }
            doSubmitFormEC($('#form1'),ecDispatcher + 'ECM1_0400/prompt'); 
            return false;
        }

        if(!isEmpty(data.toTrx)){
            if(data.toTrx.indexOf('/') > -1){
                doSubmitFormEC($('#form1'),ecDispatcher + data.toTrx);
            }else{
                doSubmitFormEC($('#form1'),ecDispatcher + data.toTrx + '/prompt');
            }
            return;
        }


        //login success
        addTextChangeForTD();
        addClickForTD();

        $.fancybox.close();

        //
        $('#commChkPwdTemplateContent').html('');
        if(!isEmpty(data.msg)){
            if(!isEmpty(data.isNeedToCallCPT)){
                $('#isNeedToCallCPT').val(data.isNeedToCallCPT);
            }
            
            $('#commChkPwdTemplateContent').html(data.msg);
            doOpenChkPwd();
          // return;
        }

        doCheckLoginStatus();//重新檢核登入狀態
    }

    window.doOpenChkPwd = function(){
        $.unblockUI();
        $.fancybox.open({
            href : '#commChkPwdTemplate',
            autoSize: false,
            width: "400",
            height: "auto",
            beforeShow:function(){
                var isV3 = $("header").hasClass("bobe-bs-v3-compatibity");
                var fancyboxWrap = $(".fancybox-wrap");

                if(isV3){
                   fancyboxWrap.addClass("bobe-bs-v3-compatibity");
                }
            }
        }); 
    }

    window.doCheckLoginStatus = function(){
        
        //console.log('window.doCheckLoginStatus');
        var r = new Date().getTime();

        var url = ecDispatcher + 'ECM1_0101/checkloginStatus';
        if(isLocalhost()){
            url += '.json';
            type = 'GET';
        }
        url += '?r='+r
 
        // //for demo

        // console.log('doCheckLoginStatus',isLocalhost())
        if (isLocalhost()) {
            doCheckLoginStatusCallBack();
        }else{

            callAjaxNoBlock(url,'',doCheckLoginStatusCallBack,false);
        }
    }

    var switchMode = function(a_mode){

        switch(a_mode){
            case 'guest':
                isMember = false;
                //member_center_btn.addClass('d-none');
                member_center_btn.removeClass('d-none');
                $('[role="member-center-options"]').removeClass('d-none');
                $('[role="bobe-user-signin-wrap"]').show();
                $('[role="bobe-user-logout-wrap"]').hide();

                $('.bobe-guest-area').show();
                $('.bobe-member-area').hide();
            break;

            case 'member':
                isMember = true;
                // isMember = false;
                $("#memberCollapse").collapse('hide');
                $('[role="bobe-user-signin-wrap"]').hide();
                $('[role="bobe-user-logout-wrap"]').show();
                signin_btn.removeClass("show");
                $('[role="member-center-options"]').removeClass('d-none');
                member_center_btn.removeClass('d-none');
                submit_btn.removeClass("form-sent").removeClass("disabled");
                submit_status_text.text("登入");

                var isV3 = $("header").hasClass("bobe-bs-v3-compatibity");
                if(isV3){
                    var collapseElement = $(".bobe-collapsing");
                    var collapseTrigger = $(".bobe-collapsing-trigger",collapseElement);

                    collapseTrigger.removeClass("show");
                    var target = collapseTrigger.attr("data-target");
                    $(target).removeClass("show");
                }
                $('.bobe-guest-area').hide();
                $('.bobe-member-area').show();
            break;
        }
    }

    window.doCheckLoginStatusCallBack = function(data){

        //demo

        if (isLocalhost()) {
           var data = {}
            data.error="";
            data.status = 'Y';
            data.status = 'N';
        }
        
        // console.log('doCheckLoginStatusCallBack',isLocalhost())
        if(!isEmpty(data.error)){
            return;
        }
        if("Y" == data.status){ 

            switchMode('member');
            // $('#headerloginDiv').hide();  
            // $('#headerlogOutDiv').show(); 
            // $('#memberCenterLi').show();  
            // $('#joinMemberLi').hide();    
            // $('#addMemberButton').val('1'); 
        }else{
            switchMode('guest');
            // $('#headerlogOutDiv').hide();
            // $('#headerloginDiv').show();
            // $('#memberCenterLi').hide();
            // $('#joinMemberLi').show();
            // $('#addMemberButton').val('0');
        }
        
        
        setTimeout(function(){
            try{
                doCheckLoginStatusCallBackCustomer(data.status);//登入或登出自訂事件，請覆寫此 Method
            }
            catch(err){
                //console.log(err);
            }
        },300);
    }

     
    var validate = function(){
        form.addClass("was-validated");
        if(submit_btn.hasClass('disabled')){
            return false;
        }
        lockSubmit(true);
        //
        var tmp_form = new Form(form);

        var isValid = tmp_form.validate();

        if(isValid){
            doLogin();
        }
        else{
            lockSubmit(false);
        }
        return false;
    }

    var lockSubmit = function(a_boolean){
        if(a_boolean || a_boolean == undefined){
            submit_btn.addClass("form-sent").addClass("disabled");
            submit_status_text.text("登入中 ")

        }else{
            submit_btn.removeClass("form-sent").removeClass("disabled");
            submit_status_text.text("登入");
        }
    }

    window.doLogOut = function(){
        if(confirm("確定執行登出?")){
            callAjax(ecDispatcher + 'ECM1_0101/logout','',logoutCallBack,false);
        }
    }
    
    
    window.logoutCallBack = function(data){
        if(!isEmpty(data.error)){
            alertMsg(data.error);
            return;
        }
        switchMode('guest');
        
        location.href = ecDispatcher + data.loginUrl;
        //doSubmitFormEC($('#form1'),ecDispatcher + data.loginUrl); 
    }
    
    var setForm = function(a_form){
        form = a_form;


        var loginType = $(form).attr('login-type');
        //console.log(loginType);
        if(loginType == 'regular') {
            MemberID = $('input[name="MemberId"]',form);
            MemberPWD = $('input[name="MemberPwd"]',form);
            RememberMe = $('input[name="RememberMe"]',form);
        } else if(loginType == 'otp') {
             //otp step1
            MemberID = $('input[name="loginOtpMemberIdGlobal"]',form); // 原本為:OTP_MemberId
            RememberMe = $('input[name="RememberMe"]',form);
            MemberBday = $('input[name="OTP_BDAY"]',form);
            MemberOTPStep = $('input[name="OTP_Step"]',form);
            //opt step2
            MemberOTPCode = $('input[name="loginOtpCodeGlobal"]',form);// 原本為:OTP_Code
        }
    }
    

    /*OTP*/
    var initOTPLogin = function(){
        $(document).on("click", '[role="resend-otp"]', resendOTP);
    }
    var checkJqueryDatepickerBoundary = function(e, target) {
        var b = {};
        b.x1 = target.offset().left;
        b.y1 = target.offset().top;
        b.x2 = b.x1 + $("#ui-datepicker-div").width();
        b.y2 = b.y1 + $("#ui-datepicker-div").height() + target.outerHeight();
        b.x3 = b.x1 + target.outerWidth();
        b.y3 = b.y1 + target.outerHeight();

        switch (e.type) {
            case 'touchstart':
                e.pageX = e.originalEvent.changedTouches[0].pageX;
                e.pageY = e.originalEvent.changedTouches[0].pageY;
                break;
            case 'touchend':
                e.pageX = e.originalEvent.changedTouches[0].pageX;
                e.pageY = e.originalEvent.changedTouches[0].pageY;
                break;
            case 'mousedown':
                e.pageX = e.originalEvent.pageX;
                e.pageY = e.originalEvent.pageY;
                break;
        }
        console.log(e.pageX, e.pageY, b);
        var hide = false;
        var log = 'in datepicker ' + e.pageX + '   ' + e.pageY + '   ' + e;
        if (e.pageX < b.x1) {
            log = "太左邊，關"
            hide = true;
        }
        if (e.pageY < b.y1) {
            log = "太上邊，關" + "  " + e.pageY, b.y1
            hide = true;
        }
        if (e.pageY > b.y2) {
            log = "太下邊，關" + "  " + e.pageY + "  " + b.y2
            hide = true;
        }

        if (hide == false) {
            if (e.pageX > b.x2) {
                //log = ("日曆的右邊")
                if (e.pageX > b.x3) {
                    log = ("太右邊，關");
                    hide = true;
                } else {
                    if (e.pageY > b.y2 && e.pageY < b.y3) {
                        log = ("正中日曆輸入框");
                    }
                    if (e.pageY < b.y1) {
                        log = ("超出文字框上邊，關")
                        hide = true;
                    }
                    if (e.pageY > b.y3) {
                        log = ("超出文字框下邊，關")
                        hide = true;
                    }
                }
            }
        }

        var msg = {};
        msg.result = hide;
        msg.log = log;
        console.log(log);
        return msg;
    }

    var addEventListener = function(){
        submit_btn.bind("click",function(e){
            e.preventDefault();
            form = $(this).closest($('[role="bobe-signin-form"]'));
            setForm(form);
            validate();
            return false;
        })
   
        $('input[name="MemberId"],input[name="MemberPwd"]').unbind();
        //
        $('input[name="MemberId"],input[name="MemberPwd"]').bind("focusin",function(e){
            form = $(this).closest($('[role="bobe-signin-form"]'));
        })

        $('input[name="MemberId"],input[name="MemberPwd"]').bind("focusout",function(e){
            form = $(this).closest($('[role="bobe-signin-form"]'));
            var tmp_form = new Form(form);
            tmp_form.validateItem($(this));
        })

        $(document).unbind("keypress").bind("keypress",function(e){
            if (e.which == 13 || event.keyCode == 13) {
                e.preventDefault();
                console.log('enter form',form);
                submit_btn.trigger("click");
            }
        });


        logout_btn.unbind().bind("click",function(e){
            e.preventDefault();
            doLogOut();
        })


        $('[role="btn-forget-password"]').unbind().bind("click",function(e){
            e.preventDefault();
            doLink2Trx('ECM1_0300');
        })

        $('[role="btn-signup"]').unbind().bind("click",function(e){
            e.preventDefault();
            doLink2Trx('ECM1_0400');
        })

        $('[role="member-center-options"]').unbind().bind("click",function(e){
            
            if(!isMember){
                e.preventDefault();
                doOpenCommLogin();
                return false;
            }
        })


        $('[role="doUpdChgPwdTime-btn"]').unbind().bind("click",function(e){
            e.preventDefault();
            doUpdChgPwdTime('comm');
            return false;
        })

        $('[role="doOpenCommLogin-btn"]').unbind().bind("click",function(e){
            e.preventDefault();
            doOpenCommLogin('comm');
            return false;
        })

        $('[role="doChgPwd-btn"]').unbind().bind("click",function(e){
            e.preventDefault();
            doChgPwd();
            return false;
        })

        //check jquery datepicker touch externalarea
        document_pos = {};
        $( document ).on("touchstart touchend mousedown", function(e) {  
          var need_to_check = false;
          switch(e.type){
            case 'touchstart':
             document_pos.x = e.originalEvent.changedTouches[0].clientX;
             document_pos.y = e.originalEvent.changedTouches[0].clientY;
            break;
            case 'touchend':
             var diff_x = Math.abs(e.originalEvent.changedTouches[0].clientX - document_pos.x);
             var diff_y = Math.abs(e.originalEvent.changedTouches[0].clientY - document_pos.y);
             if(diff_y<10){
               need_to_check = true;
             }
            break;

            case 'mousedown':
              need_to_check = true;
            break;
          }
          
          if(need_to_check){
              $(".hasDatepicker.active").each(function(){
                var msg = checkJqueryDatepickerBoundary(e,$(this));  

                if(msg.result){
                   $(this).datepicker("hide"); 
                   $(this).blur();
                }
            });
          }
        });
        
    }

    window.doOpenCommLogin = function(){

        $.unblockUI();
        $.fancybox.open({
            href : '#commLoginTemplate',
            autoSize: false,
            width: "430",
            padding: "0",
            height: "auto",
            helpers   : { 
               overlay : {closeClick: false} // prevents closing when clicking OUTSIDE fancybox 
            },
            beforeShow:function(){
                var isV3 = $("header").hasClass("bobe-bs-v3-compatibity");
                var fancyboxWrap = $(".fancybox-wrap");

                if(isV3){
                   fancyboxWrap.addClass("bobe-bs-v3-compatibity");
                }
            }
        }); 
    }

    //only for demo
    var demoOpenAlertMsg = function(){
        var descObj = $('#alertMsgTemplateDesc');
        $(descObj).html("重要訊息公告");   
        $.fancybox.open({
            minWidth:300,
            minHeight:150,
            maxWidth:500,
            href : '#alertMsgTemplate',
            autoDimensions: true,
            beforeShow:function(){
                var isV3 = $("header").hasClass("bobe-bs-v3-compatibity");
                var fancyboxWrap = $(".fancybox-wrap");

                if(isV3){
                   fancyboxWrap.addClass("bobe-bs-v3-compatibity");
                }
            },
            afterClose:function(){
                if(afterCloseEvent != null){
                    setTimeout(function(){afterCloseEvent();},100);
                }
            }
        });
    }

    var checkMemberCookie = function(){
        
        try{
            $('input[name="MemberId"]').val($.base64.decode($.cookie('memberIdCookie')));
            if(!isEmpty($.cookie('memberIdCookie'))){
                $('input[name="RememberMe"]').prop('checked',true);
            }
        }catch(err){
            //console.log('err',err);
        }
    }

    


    var init = function(){

        $.blockUI = lockScreen;
        $.unblockUI = unLockScreen;

       addEventListener();
       checkMemberCookie();
        //site nav
        LayoutTool.init();        

        initOTPLogin();
        
        var datetimeLocal = $('input[type="date"]');

        datetimeLocal.on('focus blur',function(){
            $(this).addClass('input-active')
        })
        
       if(window.onBobeAppComplete!=undefined)
        window.onBobeAppComplete();
    }


    init();
})(window);
