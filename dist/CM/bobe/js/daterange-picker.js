var DaterangePicker = (function(window){

	var window_w = $(window).width()
	var isDesktop = window_w > 992;
	var isMobile = window_w < 576;

	//ClassName
	var _container = '.custom-input-daterange';
	var _datepicker = '.datepicker';
	var _wrap = '.input-date-wrap';
	var _input = 'init-daterangepicker';
	var _startDate = '.input-date input#startDate';
	var _endDate = '.input-date input#endDate';
	var _datepicker = '.datepicker';

	var dateformat = 'MM/DD/YYYY';
	var active = 'active';

	var wrap_tmpl = 
		'<div class="input-date-wrap">' +
			'<div class="input-holder">' +
			'<div class="datepicker"/></div>' +
		'</div>';

	var locale_settings = {
		    "format": "MM/DD/YYYY",
		    "separator": " - ",
		    "applyLabel": "確認",
		    "cancelLabel": "取消",
		    "fromLabel": "From",
		    "toLabel": "To",
		    "customRangeLabel": "Custom",
		    "weekLabel": "W",
		    "daysOfWeek": [
		        "日",
		        "一",
		        "二",
		        "三",
		        "四",
		        "五",
		        "六"
		    ],
		    "monthNames": [
		        "一月",
		        "二月",
		        "三月",
		        "四月",
		        "五月",
		        "六月",
		        "七月",
		        "八月",
		        "九月",
		        "十月",
		        "十一月",
		        "十二月"
		    ],
		    "firstDay": 1
		}

	//jQuery Objects
	var container = $(_container);
	var $input = $('.init-daterangepicker',container);
	var $startDate = $(_startDate);
	var $endDate = $(_endDate)


	var addEventListener = function(){

		$input.on('focus',function(){

			var $this = $(this);
			$this.trigger('blur');
			$this.closest(_container).addClass(active)

			console.log($this)

		})


		$input.on('show.daterangepicker',function(ev, picker){

			var $this = $(this);
			// $this.trigger('blur');
			// $this.closest(_container).addClass(active)

			//console.log($this)

		})

		$input.on('hide.daterangepicker',function(ev, picker){

			var $this = $(this);
			$this.closest(_container).removeClass(active)

		})

		$input.on('setStartDate.daterangepicker',function(ev, picker){

			console.log('setStartDate',picker)
			var startDate = picker.startDate.format(dateformat)
			$(this).closest(_container).find($startDate).val(startDate)

		})

		$input.on('setEndDate.daterangepicker',function(ev, picker){

			console.log('setEndDate',picker)
			var endDate = picker.endDate.format(dateformat)
			$(this).closest(_container).find($endDate).val(endDate)
			 
		})


	}


	var init = function(){

		$input.closest(_container).prepend(wrap_tmpl);

		var datepicker = $(_datepicker);

		console.log($input.length)

		$input.daterangepicker({
			parentEl:datepicker,
			opens:'embed',
			autoUpdateInput: false,
			"locale": locale_settings
		})

		console.log('input=',$input)

		if(isMobile){

			container.addClass('is-mobile')


		}else{

			container.removeClass('is-mobile')

		}
		addEventListener();
    }

    return{
        init:init
    };

})(window);