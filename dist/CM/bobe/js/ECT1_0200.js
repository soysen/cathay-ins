$(function(){

	var isLocalhost = function(){
    	//console.log('index.js location.host',location.host);
		if(location.host == 'localhost:9998' || location.host == 'bobe.albertlan.net'){
			return true;
		}
		return false;
	}

	var initBgImgPreplacer = function(){

		var source = $('*[role="bg-img-source"]');
    	var target = $('*[role="bg-img-display"]');

        var url = source.map(function(){
                return this.src
            });

        for(var i=0; i<url.length; i++){
        	$(target[i]).css("background-image","url("+url[i]+")");
        }


    }
    
	var renderTopic = function(resp){
		var html = '';
    	$.each(resp, function(index,item){
    		if(index>3){
    			return false;
    		}
            var template = $("#template-topic-item").html();
			var url = '/INSEBWeb/servlet/HttpDispatcher/ECA2_0401/prompt?SERIAL_NO='+item.SERIAL_NO+'&type=&pageFlag=';
			var title = item.SUBJECT;
			var date = item.EFFECT_DATE.replace(eval('/-/g'),"/").substr(0,10);
			var img = item.PIC;
            template = template.replace(/%TOPIC_TITLE%/gi,title);
            template = template.replace(/%TOPIC_URL%/gi,url);
            template = template.replace(/%TOPIC_DATE%/gi,date);
            template = template.replace(/%TOPIC_IMG%/gi,"https://w3.bobe.com.tw/INSEBWeb/html/CM/bobe/img/events/event__3.jpg");
            html += template;
		});
		$('[role="bobe-topic-holder"]').html(html);
		initBgImgPreplacer();
	}

	var getTopic = function(){
		
		var params = 'type=';
		var type = "POST";
		var url = ecDispatcher + 'ECA2_0400/getPageInfoLst';
		
		if(isLocalhost()){
			url += '.json';
			type = 'GET';
		}
		$.ajax({
			type: type,
			url : url,
			data: encodeURI(params),
			dataType: "JSON"
		}).done(function(resp) {
			renderTopic(resp.pageInfo);
		});
	}
	getTopic();
})