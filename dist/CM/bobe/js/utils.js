var UTILS = function(window){
    return {
        numberWithCommas:function(x) {
            x = x.toString();
            var pattern = /(-?\d+)(\d{3})/;
            while (pattern.test(x))
            x = x.replace(pattern, "$1,$2");
            return x;
        },
        getAge:function(dateString) {
            var today = new Date();
            var birthDate = new Date(dateString);
            var age = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age;
        },
        setCookie:function(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays*24*60*60*1000));
            var expires = "expires="+ d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        },
        getCookie:function(cname){
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for(var i = 0; i <ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        },
        getUrlParams:function ( prop ) {
            var params = {};
            var search = decodeURIComponent( window.location.href.slice( window.location.href.indexOf( '?' ) + 1 ) );
            var definitions = search.split( '&' );

            definitions.forEach( function( val, key ) {
                var parts = val.split( '=', 2 );
                params[ parts[ 0 ] ] = parts[ 1 ];
            } );
            return ( prop && prop in params ) ? params[ prop ] : undefined;
        },
        scrollPage:function(a_pos){
            var body = $("html");
            var dest_y = Math.round(a_pos-370);
            if (navigator.userAgent.match(/(iPod|iPhone|iPad|Android)/)) {    
                window.setTimeout(function() {window.scrollTo(0,dest_y);}, 0);
            }else{
                body.animate({scrollTop:dest_y}, 300, 'swing', function() {}); 
            }
        },
        pad:function(d) {
           return (d < 10) ? '0' + d.toString() : d.toString();
        },
        isMobile:function(){
            var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
            if (isMobile) {
              return true;
            }
            return false;
        }
    };
}(window);