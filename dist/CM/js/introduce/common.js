$(document).ready(function(){
    
    $('#login_btn').click(function(e){
        $(this).parent().children('.function').toggleClass('active');
    });
    $('.headerFunction').click(function(e){
        if (window.matchMedia('(max-width: 990px)').matches) {
            $(this).parent().children('.function').toggleClass('active');
        }
    });
    
    doCheckLoginStatus();
    doClearLoginWarnMsg('header');
    doClearLoginWarnMsg('comm');
    try{
        $('#headerMemberId').val($.base64.decode($.cookie('memberIdCookie')));
        if(!isEmpty($.cookie('memberIdCookie'))){
            doRemeberMe1Click();
            $('#remeberMeFlagDiv1').addClass('checked',true);
            $('#remeberMeFlag1').prop('checked',true);
        }
    }catch(err){}
    
    try{
        $('#commMemberId').val($.base64.decode($.cookie('memberIdCookie')));
        if(!isEmpty($.cookie('memberIdCookie'))){
            $('#commRemeberMeFlagDiv').addClass('checked',true);
            $('#commRemeberMeFlag').prop('checked',true);
        }
    }catch(err){}
    
});
            
            
function doCheckLoginStatus(){
    callAjaxNoBlock(ecDispatcher + 'ECM1_0101/checkloginStatus','',doCheckLoginStatusCallBack,false);
}

doCheckLoginStatusCallBack = function(data){
    if(!isEmpty(data.error)){
        return;
    }
    if("Y" == data.status){ 
        $('#headerloginDiv').hide();  
        $('#headerlogOutDiv').show(); 
        $('#memberCenterLi').show();  
        $('#joinMemberLi').hide();    
        
    }else{
        $('#headerlogOutDiv').hide();
        $('#headerloginDiv').show();
        $('#memberCenterLi').hide();
        $('#joinMemberLi').show();
        
    }
    setTimeout(function(){
        doCheckLoginStatusCallBackCustomer(data.status);//登入或登出自訂事件，請覆寫此 Method
    },300);
    
}

function getLoginStatus(){
    var status = 'N';
    $.ajaxSettings.async = false;
    $.ajax({
        type: "POST",
        url : ecDispatcher + 'ECM1_0101/checkloginStatus',
        data: '',
        dataType: "JSON"
    }).done(function(resp) {
        if(resp.ErrMsg != null){
            if(resp.ErrMsg.returnCode != '0'){
                if('Y' != notShowAlertMsg){
                    alertMsg(resp.ErrMsg);
                    return status;
                }
            }
            status = resp.status;
        }
    });
    $.ajaxSettings.async = true;
    return status;
}


function doRemeberMe1Click(){
    var checked = $('#remeberMeFlagDiv1').hasClass('checked');
    if(!checked){
        $('#remeberMeFlagDiv2').addClass('checked',checked);
    }else{
        $('#remeberMeFlagDiv2').removeClass('checked');
    }
    $('#remeberMeFlag2').prop('checked',!checked);
}

/**
清除全部錯誤訊息
*/
function doClearLoginWarnMsg(loginKind){
    clearWarnMsg(loginKind+'MemberId_msg');
    clearWarnMsg(loginKind+'MemberPwd_msg');
}


function doCheckLoginMemberId(loginKind){
    clearWarnMsg(loginKind+'MemberId_msg');
    var memberId = $('#' + loginKind+ 'MemberId').val();
    if(isEmpty(memberId)){
        setWarnMsg(loginKind+'MemberId_msg','<font color="red" size="3">請輸入資料!</font>');
        return false;
    }
    
    if(!validatePersonalID(memberId) && !validateLiveID(memberId)){
        setWarnMsg(loginKind+'MemberId_msg','<font color="red" size="3">您的帳號或密碼有誤!</font>');
        return false;
    }
    return true;
}


function doCheckLoginMemberPwd(loginKind){
    clearWarnMsg(loginKind+'MemberPwd_msg');
    var memberPwd = $('#'+loginKind+'MemberPwd').val();
    if(isEmpty(memberPwd)){
        setWarnMsg(loginKind+'MemberPwd_msg','<font color="red" size="3">請輸入資料!!</font>');
        return false;
    }
    
    
    //if(!chkPassword(memberPwd)){
    //  setWarnMsg(loginKind+'MemberPwd_msg','<font color="red" size="3">請輸入正確資訊!</font>');
    //  return false;
    //}
    return true;
}


function doLogin(loginKind){
    
    if(loginKind == 'header') { commLoginSuccessContinue = ''; }
    
    doClearLoginWarnMsg(loginKind);
    
    if(!doCheckLoginMemberId(loginKind)){ return; }
    if(!doCheckLoginMemberPwd(loginKind)){ return; }
    if(loginKind == 'header'){
        if($('#remeberMeFlag1').prop('checked')){
            $.cookie.raw = true;
            $.cookie('memberIdCookie', $.base64.encode($('#headerMemberId').val()), { path:'/', expires: 7 });
        }else{
            try{
                $.removeCookie('memberIdCookie',{ path:'/'});
            }catch(err){}
        }
    }
    if(loginKind == 'comm'){
        if($('#commRemeberMeFlag').prop('checked')){
            $.cookie.raw = true;
            $.cookie('memberIdCookie', $.base64.encode($('#commMemberId').val()), { path:'/', expires: 7 });
        }else{
            try{
                $.removeCookie('memberIdCookie',{ path:'/'});
            }catch(err){}
        }
    }
    
    var params = 'loginKind=' + loginKind + '&memberId='+ $('#' + loginKind + 'MemberId').val() + '&memberPwd=' + $('#' + loginKind + 'MemberPwd').val() + '&viewDevice=P';
    if(loginKind == 'header'){ params += '&toTrx=' + $('#loginForwardTrx').val(); } 
    
    callAjax(ecDispatcher + 'ECM1_0100/login',params,loginCallBack,false);
    
}


loginCallBack = function(data){
    if(!isEmpty(data.error)){
        if(data.loginKind == 'header'){
            alertMsg(data.error);
            $('#memberId').val('');
            $('#memberPwd').val('');
        }else if(data.loginKind == 'comm'){
            $.fancybox.close();
            alertMsg(data.error,reOpenCommLogin);
        }
        return;
    }
    if(data.loginKind == 'comm'){
        $.fancybox.close();
    }
    
    if(!isEmpty(data.isJoinMember) && data.isJoinMember == 'Y'){
        if(!confirm("您尚未加入會員，是否立即加入?")){
            return;
        }
        doSubmitFormEC($('#form1'),ecDispatcher + 'ECM1_0400/prompt'); 
        return;
    }
    
    if(!isEmpty(data.toTrx)){
        if(data.toTrx.indexOf('/') > -1){
            doSubmitFormEC($('#form1'),ecDispatcher + data.toTrx);
        }else{
            doSubmitFormEC($('#form1'),ecDispatcher + data.toTrx + '/prompt');
        }
        return;
    }
    
    $('#commChkPwdTemplateContent').html('');
    if(!isEmpty(data.msg)){
        $('#commChkPwdTemplateContent').html(data.msg);
        doOpenChkPwd();
    }
    doCheckLoginStatus();//重新檢核登入狀態
}


function doLogOut(){
    if(confirm("確定執行登出?")){
        callAjax(ecDispatcher + 'ECM1_0101/logout','',logoutCallBack,false);
    }
}


logoutCallBack = function(data){
    if(!isEmpty(data.error)){
        alertMsg(data.error);
        return;
    }
    doSubmitFormEC($('#form1'),ecDispatcher + data.loginUrl); 
}


function doForgotPwd(){
    doSubmitFormEC($('#form1'),ecDispatcher + 'ECM1_0300/prompt'); 
}

function doCheckLoginStatusCallBackCustomer(status){
    //請覆寫此Method在自已的JSP
}
 
function doLink2Trx(trxCode){
    $('#trxUniKey').val('');
    doSubmitFormEC($('#form1'),ecDispatcher + trxCode + '/prompt'); 
}

function doLink2Url(url){
    $('#trxUniKey').val('');
    doSubmitFormEC($('#form1'),url); 
}

function doLink2TrxAndMethod(trxCode,initMethod){
    doSubmitFormEC($('#form1'),ecDispatcher + trxCode + '/' + initMethod); 
}

function doLink2HomePage(){
    doSubmitFormEC($('#form1'),ecHomePage); //首頁 
}


reOpenCommLogin = function doReOpenCommLogin(){
    doOpenCommLogin();
}


function doOpenCommLogin(){
    $.unblockUI();
    $.fancybox.open({
        href : '#commLoginTemplate',
        autoSize: false,
        width: "600",
        height: "430"
    }); 
}


$(document).ready(function(){
    $("body").attr("onpageshow","onPageShowMethod();");
});

function onPageShowMethod(){
    $.unblockUI();
}


function doOpenChkPwd(){
    $.unblockUI();
    $.fancybox.open({
        href : '#commChkPwdTemplate',
        autoSize: false,
        width: "500",
        minHeight: "200",
        maxHeight: "250",
        closeBtn  : false,
        helpers     : { overlay : {closeClick: false} },
    }); 
    
}


function doChgPwd(){
    doSubmitFormEC($('#form1'),ecDispatcher + 'ECM1_1000/prompt?tab=7');
}


function doUpdChgPwdTime(loginKind){
    var params = 'loginKind=' + loginKind;
    if(loginKind == 'header'){ params += '&toTrx=' + $('#loginForwardTrx').val(); } 
    
    callAjax(ecDispatcher + 'ECM1_0100/updChgPwdTime',params,updChgPwdTimeCallBack,false);
}


updChgPwdTimeCallBack = function(data){
    if(!isEmpty(data.error)){
        alertMsg(data.error);
        return;
    }
    $.fancybox.close();
}