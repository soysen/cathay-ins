$(function() {

    var offset = $("#step").offset();
    
    /*var topPadding = 15;
    $(window).scroll(function() {
        if ($(window).scrollTop() > offset.top) {
            $("#step").stop(true, true ).animate({
                marginTop: $(window).scrollTop() - offset.top + topPadding
            },400);
        } else {
            $("#step").stop(true, true ).animate({
                marginTop: 0
            });
        };
    });*/

    $('#goout').on('click', function(){
        $(".modal").modal("hide")
    });

    $(".btn-goopt1").click(function(){
        //$(this).fadeOut();
        $(".opt1").fadeIn();
        $("html, body").animate({ scrollTop: $('.opt1').offset().top }, 1000);
    })

    $(".btn-goopt2").click(function(){
        //$(".opt1").hide();
        $(".opt2").fadeIn();
        $("html, body").animate({ scrollTop: $('.opt2').offset().top }, 1000);
    })

    $('.gotop').click(function(){
        $('html, body').animate({
            scrollTop: 0
        }, 400);
        return false;
    }).focus(function(){
        $(this).blur();
    });
    //20150122
    $('.gotab').click(function(){
        $('html, body').animate({
            scrollTop: 0
        }, 400,function(){
            $('.nav-tabs li').eq(1).find('a').trigger('click');
        });   
    })
    $(".choise-area").click(function(){
        $("#area-main").show();
    })
    $("#area-main .out-close, #area-main .save, #area-main .cancle").click(function(){
        $("#area-main").hide();
    })
    $(".go-deep").click(function(){
        $("#deep-box").show();
    })
    $("#deep-box .deep-close").click(function(){
        $("#deep-box").hide();
    })
    $("#deep-box input:checkbox").click(function(){
        $("#deep-box").hide();
    })

    $('.navbar-sub .dropdown').hover(function() {
        $(this).find('.dropdown-menu').first().stop(true, true).fadeIn(200);
    }, function() {
        $(this).find('.dropdown-menu').first().stop(true, true).fadeOut(200);
    });

    $(".qa-nav .panel:eq(1)").click();
    /*$(".qa-nav .panel:eq(1)").children(".panel-collapse").addClass("in");*/

    $(".radio-visa").click(function(){
        $("#visa").show();
        $("#gomem").hide();
    });
    $(".radio-gomem").click(function(){
        $("#gomem").show();
        $("#visa").hide();
    });
    

    $(".ihave").click(function(){
        if($(this).is(':checked')){
            $( ".status" ).removeAttr( "disabled" )
        }else{
            $( ".status" ).attr( "disabled","disabled" )
        }
    })
    $(".idont").click(function(){
        if($(this).is(':checked')){
            $( ".status" ).attr( "disabled","disabled" )
        }else{
            $( ".status" ).removeAttr( "disabled" )
        }
    })

    $(".iadd").click(function(){
        if($(this).is(':checked')){
            $( ".add" ).removeAttr( "disabled" )
        }else{
            $( ".add" ).attr( "disabled","disabled" )
        }
    })
    $(".ipeople").click(function(){
        if($(this).is(':checked')){
            $( ".people" ).removeAttr( "disabled" )
        }else{
            $( ".people" ).attr( "disabled","disabled" )
        }
    })


	$(".go-count").on("click", function(){
		$(".result-box, .line").fadeIn();
		$("html, body").animate({ scrollTop: $('.result-box').offset().top }, 1400);
	})
	$( ".radio-color .radio" ).on("click", function(){
		$(this).siblings(".radio").removeClass("thisthis").end().addClass("thisthis");
	})

    

    $( "#datepicker" ).datepicker();
    $( "#datepicker02" ).datepicker();

    $.datepicker.regional['zh-TW'] = {
        closeText: '關閉',
        prevText: '&#x3C;上月',
        nextText: '下月&#x3E;',
        currentText: '今天',
        monthNames: ['一月','二月','三月','四月','五月','六月',
        '七月','八月','九月','十月','十一月','十二月'],
        monthNamesShort: ['一月','二月','三月','四月','五月','六月',
        '七月','八月','九月','十月','十一月','十二月'],
        dayNames: ['星期日','星期一','星期二','星期三','星期四','星期五','星期六'],
        dayNamesShort: ['周日','周一','周二','周三','周四','周五','周六'],
        dayNamesMin: ['日','一','二','三','四','五','六'],
        weekHeader: '周',
        dateFormat: 'yy/mm/dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: true,
        yearSuffix: '年'};
    $.datepicker.setDefaults($.datepicker.regional['zh-TW']);

    
    selectedEl = $('select').find('option:selected');
    if(selectedEl.val()==""){
        $('select').css('color','#999');
        $('select').children().css('color','black');
    }

    $('body').on('change','select', function (){
        if($(this).find('option:selected').val() == ""){
            $(this).css('color','#999');
            $(this).children().css('color','black');
        }
        else {
            $(this).css('color','black');
            $(this).children().css('color','black');
        }
    });


   

    var Browser = {
        IsIe: function () {
            return navigator.appVersion.indexOf("MSIE") != -1;
        },
        Navigator: navigator.appVersion,
        Version: function() {
            var version = 999; // we assume a sane browser
            if (navigator.appVersion.indexOf("MSIE") != -1)
                // bah, IE again, lets downgrade version number
                version = parseFloat(navigator.appVersion.split("MSIE")[1]);
            return version;
        }
    };

    if (Browser.IsIe() && Browser.Version() <= 9) {
         // Placeholder fix for IE


                setInterval(function () {
                    var i = $("input:text");
                    if(i.val() == i.attr('placeholder')) {
                      $(this).addClass('placeholder').val($(this).attr('placeholder'));    
                    }  
                }, 1000);


              $('input').focus(function() {
                var i = $(this);
                if(i.val() == i.attr('placeholder')) {
                  i.val('').removeClass('placeholder');
                  if(i.hasClass('password')) {
                    i.removeClass('password');
                    this.type='password';
                  }     
                }
              }).blur(function() {
                var i = $(this);  
                if(i.val() == '' || i.val() == i.attr('placeholder')) { 
                  if(this.type=='password') {
                    i.addClass('password');
                    this.type='text';
                  }
                  i.addClass('placeholder').val(i.attr('placeholder'));
                }
              }).blur().parents('form').submit(function() {
                //if($(this).validationEngine('validate')) { // If using validationEngine
                  $(this).find('[placeholder]').each(function() {
                    var i = $(this);
                    if(i.val() == i.attr('placeholder'))
                      i.val('');
                      i.removeClass('placeholder');

                  })
                //}
              });




       $('#top1_mem_pass, #u_password, #u_password2, .change_to_password').focus(function() {

       }).blur(function(){
            if( $(this).val() == $(this).attr('placeholder') ) {
                $(this).attr('type','text');
            }else{
                $(this).attr('type','password');
            }
       });
       /*
       $('#u_password').attr('type','password');
       $('#u_password2').attr('type','password');
       $('.change_to_password').attr('type','password');*/

    }

    ///////////0326新增
    //國家頁國家radio切換頁籤
    $('input[name="country_radio"]').click(function () {
        $(this).parents('.checkbox').addClass('text-primary').siblings().removeClass('text-primary');
        $(this).tab('show');
    });
    //國家頁籤
    $('#myTab a').click(function (e) {
        e.preventDefault()
        $(this).tab('show');
     });    
    //刪除
    $('.country_choose a').click(function () {
         $(this).parents('.country_div').fadeOut(0);
     });
    //國家五大洲更多更少 
    $(".more-box01").hide();
    $(".go-less01").hide();
    $('.go-more01').each(function(){
                
                $(this).click(
                   function(){     
                    $(this).fadeOut(0);
                    $(this).siblings('.go-less01').fadeIn(0);
                    $(this).parent().siblings('.more-box01').fadeIn();
                  }
               );
    });
    $('.go-less01').each(function(){
                $(this).click(
                   function(){   
                    $(this).fadeOut(0);
                    $(this).siblings('.go-more01').fadeIn(0);
                    $(this).parent().siblings('.more-box01').fadeOut();
                  }
               );

      });



        // $('.popmsg').popover(
        // {
        // 'trigger': 'hover',
        // 'placement': 'right',
        // 'html': true,
        // });


//doc結束
});


$(function () {

            var WHAT = navigator.userAgent;

            if( WHAT.match(/(Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile)/)){
                // alert("手機成功");


                // ipad
                $('.popmsg').popover(
                {
                'trigger': 'mouseover',
                'placement': 'right',
                'html': true
                 });
               
                $('.popmsg').click(
                    function(){   
                             $(".popover-bg").fadeIn(0);
                             // alert("test!");
                              // $('body').append('<div class="popover-bg"></div>');
                    }
                );

                $('.popover-bg').click(
                    function(){   
                             $('.popover').hide();
                             $(".popover-bg").fadeOut(0);
                              // alert("test!");
                    }
                ); 
               

            }else{
                //電腦
                $('.popmsg').popover(
                {
                'trigger': 'hover',
                'placement': 'right',
                'html': true
                }); 
                                
                   
            };

//doc結束
});


//tab  頁籤用第二按鈕切換
$(function () {

    $('.gototab1').on('click', function() { 
        $('.nav-tabs-inner li').eq(0).addClass('this').find('a[data-toggle="tab"]').tab('show');
        $("html, body").animate({ scrollTop: $(' body').offset().top }, 0);
    });

    $('.gototab2').on('click', function() {      
        $('.nav-tabs-inner li').eq(1).addClass('this').find('a[data-toggle="tab"]').tab('show');
        $("html, body").animate({ scrollTop: $(' body').offset().top }, 0);
    });

    $('.gototab3').on('click', function() {      
        $('.nav-tabs-inner li').eq(2).addClass('this').find('a[data-toggle="tab"]').tab('show');
        $("html, body").animate({ scrollTop: $(' body').offset().top }, 0);    });

    $('.gototab4').on('click', function() {      
        $('.nav-tabs-inner li').eq(3).addClass('this').find('a[data-toggle="tab"]').tab('show');
        $("html, body").animate({ scrollTop: $(' body').offset().top }, 0);
    });


});


$(function(){
//IE9 密碼強制隱碼
 $('.change_to_password').attr('type','password'); 
// class="change_to_password"

//新增被保險人
$(".go-count2").on("click", function(){
        $(".result-box2").fadeIn();
})

//刪除被保險人
$(".delete_data").on("click", function(){
        $(this).parents(".delete_data_wrap").fadeOut(0);
})


});