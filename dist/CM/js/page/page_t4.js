/*
 * 開始手機驗證
 */
function goOtpSend() {

	$('#otpSend').show();
	$('#otpStard').hide();
	$('#payKind').hide();

	var params = "insureway=" + $('#insureway').val();
	// 以後或許要改回 ECV1_0301
	callAjax(ecDispatcher + 'EBPT_1400_T4/goOtpSend', params,
			goOtpSendCallBack, false);
	// 以後或許要改回 ECV1_0301
}


//function goOTP(key) {
//	// $("#UpdateProgress2").prop("style", "")
//	var url = "../EBP0_Z000/smSend?key=" + key;
//	$.get(url, function(data, status) {
//		// alert(data.msg)
//		var obj = jQuery.parseJSON(data.msg);
//
//		if (obj.retCode == "0000") {
//			// $("#UpdateProgress2").prop("style", "display:none")
//			$("#goOTPBtn").prop("style", "display:none")
//			$("#otpDiv").prop("style", "")
//			$("#sms_code").val("");
//			// var xx='{"msg":"測試:沒有送出簡訊!","otp": "53705","過期時間":
//			// "1468920599577","phone": "0933157657","pre": "729"}';
//
//			$("#otpPreShow").html(obj.pre + "-");
//			$("#hintPhone").html(obj.phone);
//			if (obj.otp) {
//				$("#sms_code").val(obj.otp);
//			}
//			// var obj2 = jQuery.parseJSON( data );
//			// alert( obj2.pre );
//		} else {
//			// alert("obj.retCode=="+obj.retCode)
//			alert(obj.msg)
//		}
//
//	});
//}


// 開始手機驗證返回處理
goOtpSendCallBack = function(data) {
	if (!isEmpty(data.error)) {
		alertMsg(data.error);

		return;
	}

	$('#otpPre').attr('value', data.verifyCodePre);
	$('#otpPreCheck').val(data.verifyCodePre);
	$('#otpCode').val('');
	$('#otpPreShow').html(data.verifyCodePre + '-');
	
	
	//
	//
	//
	$("#goOTPBtn").css("display", "none")
	$("#otpDiv").css("display", "")
	

	//$("#pre").html(obj.pre + "-");
	//$("#hintPhone").html(obj.phone);
	
	
}

/**
 * 檢核OTPCode
 */
function doCheckOTPCode() {
	clearWarnMsg('otpCode_msg');
	var otpCode = $('#otpCode').val();
	if (isEmpty(otpCode)) {
		setWarnMsg('otpCode_msg', '請輸入正確資訊!');
		$('#otpCode_msg').show();
		return false;
	}

	if (otpCode.length != 4) {
		setWarnMsg('otpCode_msg', '請輸入正確資訊!');
		$('#otpCode_msg').show();
		return false;
	}
	return true;
}

/**
 * 驗證OTP
 */
function doVerifyOTP() {
	doClearWarnMsg();

	if (!doCheckOTPCode()) {
		return false;
	}

	var params = 'otpCode=' + $('#otpCode').val() + '&otpPre='
			+ $('#otpPre').val();
	// 以後或許要改回 ECV1_0301
	callAjax(ecDispatcher + 'EBPT_1400_T4/doVerifyOTP', params,
			verifyOTPCallBack, false);
	// 以後或許要改回 ECV1_0301

}
//function checkOTP(key) {
//	if ($("#sms_code").val() == "") {
//		alert("請輸入驗證碼")
//	} else {
//		// alert($("#sms_code").val());
//		var url = "../EBP0_Z000/OTPCheck?key=" + key;
//		url += "&otp=" + $("#sms_code").val()
//		$.get(url, function(data, status) {
//			var msg = data.msg;
//			if (msg.indexOf("0000") >= 0) {
//				// savePolicy();
//				OtpSuccess()
//
//			} else if (msg.indexOf("8888") >= 0) {
//				alert("session 過期, 停滯時間過久!")
//				// location.replace("./");
//			} else if (msg.indexOf("7777") >= 0) {
//				alert("超過10分鐘!")
//				// location.replace("./");
//			} else {
//				alert("驗證碼錯誤!")
//				$("#sms_code").val("")
//			}
//
//		});
//	}
//
//}
// 驗證OTP的返回處理
verifyOTPCallBack = function(data) {
	if (!isEmpty(data.error)) {
		alertMsg(data.error);
		$('#otpCode').val('');
		enabledReSendOTP();
		return;
	}

//	$('#otpSend').hide();
//	$('#otpStard').hide();
//	$('#payKind').show();
//	$('#payBtn').show();
	OtpSuccess()
}

function OtpSuccess() {
	$('#otpCodeCheck').val($('#otpCode').val());
	// alert("驗證成功")
	$("#otpDiv").css("display", "none");
	$("#cardBtnDiv").css("display", "block");
	$("#webInfoSpanForWeb").css("display", "block");
}


/**
 * 重送OTP
 */
function doSendOTPAgain() {
	$('#reSendOTP').attr('disabled', true);
	var params = 'viewDevice=' + 'P';
	// 以後或許要改回 ECV1_0301
	callAjax(ecDispatcher + 'EBPT_1400_T4/doReSendOTP', params,
			reSendOTPCallBack, false);
	// 以後或許要改回 ECV1_0301
	setTimeout("enabledReSendOTP()", 60000);
	//<%-- 60秒後，再重新開放「再寄一次」按鈕--%>
}

// 重送OTP的返回處理
reSendOTPCallBack = function(data) {
	if (!isEmpty(data.error)) {
		alertMsg(data.error);
		return;
	}
	$('#otpPre').attr('value', data.verifyCodePre);
	$('#otpPreCheck').val(data.verifyCodePre);
	$('#otpCode').val('');
	$('#otpPreShow').html(data.verifyCodePre + '-');
}

// 恢復再寄一次按鈕
function enabledReSendOTP() {
	$('#reSendOTP').attr('disabled', false);
}

/**
 * 清除全部錯誤訊息
 */
function doClearWarnMsg() {
	clearWarnMsg('otpCode_msg');
}


function checkConvertInsureWayAndShow(){
	var insureWay = $('#insureWay').val();
	if("WEB" == insureWay.toUpperCase()){
		var params = "insureWay=" + insureWay + "&startDate=" +  $("#start_date").val();
		callAjax(ecDispatcher + 'EBPT_1400_T4/checkConvertInsureWay', params,
				function(data){
					if(data && data.result){
						if("true" == data.result){
							$("#webInfoSpanForWeb").css("display","");
							$("#webInfoSpanForFax").css("display","");
							$("#card_same_name").prop("disabled",false);
							$("#card_same_name_fax").prop("disabled",false);
							$("#card_same_name_fax").parent().toggleClass("checked");
							if ($("#card_same_name_fax").parent().hasClass('checked')== true) {
								$("#card_same_name_fax").attr('checked', true);
					        } else {
					        	$("#card_same_name_fax").attr('checked', false);
					        }
							$("#payCartWord").css("display","");
						}else{
							$("#webInfoSpanForWeb").css("display","");
							$("#webInfoSpanForFax").css("display","none");
						}					
					}
		}, false);
	}else{
		$("#webInfoSpanForWeb").css("display","none");
		$("#webInfoSpanForFax").css("display","");
	}
	
}



function switch_card_same_name(obj) {
	var insureWay = $("#insureWay").val();
	if("WEB" == insureWay){
		insureWay = "FAX";
	}else{
		insureWay = "WEB";
	}
	var settleNo = document.getElementsByName("settleNo")[0].value;
	var params = "settleNo=" + settleNo + "&insureWay=" + insureWay + "&startDate=" +  $("#start_date").val();
	callAjax(ecDispatcher + 'EBPT_1400_T4/changeInsureWay', params,
			function(data){
				if(data){
					if(data.errorMsg){
						alert(data.errorMsg);
						$("#card_same_name").parent().toggleClass("checked");
					}else{
						
						if("FAX" == insureWay){
							$("#insureWay").val("FAX");
							if("WEB" == obj){
								$("#card_same_name_fax").parent().toggleClass("checked");
								if ($("#card_same_name_fax").parent().hasClass('checked')== true) {
									$("#card_same_name_fax").attr('checked', true);
						        } else {
						        	$("#card_same_name_fax").attr('checked', false);
						        }
							}else if("FAX" == obj){
								$("#card_same_name").parent().toggleClass("checked");
								if ($("#card_same_name").parent().hasClass('checked')== true) {
									$("#card_same_name").attr('checked', true);
						        } else {
						        	$("#card_same_name").attr('checked', false);
						        }
							}
							alert("切換成傳真要保");
						}else{
							$("#insureWay").val("WEB");
							if("WEB" == obj){
								$("#card_same_name_fax").parent().toggleClass("checked");
								if ($("#card_same_name_fax").parent().hasClass('checked')== true) {
									$("#card_same_name_fax").attr('checked', true);
						        } else {
						        	$("#card_same_name_fax").attr('checked', false);
						        }
							}else if("FAX" == obj){
								$("#card_same_name").parent().toggleClass("checked");
								if ($("#card_same_name").parent().hasClass('checked')== true) {
									$("#card_same_name").attr('checked', true);
						        } else {
						        	$("#card_same_name").attr('checked', false);
						        }
							}
							alert("切換成網路投保");
						}
					}
				}
	}, false);
}
function checkTravelDate_fax(faxPreDays) {
	serverDateTime = $("#serverDateTime").val();

	var isNeedLock = false;
	var msg = null;
	var now = new Date(serverDateTime);
	var start_date = new Date($("#start_date").val())
	var mini_7day = 60 * 24 * 7;
	var diff_sec = Math.round((start_date - now) / 1000 / 60);

	var year = now.getFullYear();
	var month = now.getMonth();
	var day = now.getDate() + faxPreDays;
	var hour = now.getHours();
	var min = now.getMinutes();
	var sec = now.getSeconds();
	var max_dateTime = new Date(year, month, day, hour, min, sec)
	// alert(serverDateTime + " / " + max_dateTime)
	// alert(diff_sec +" < "+ mini_7day+" ? ")
	if (diff_sec < mini_7day) {
		msg = "請以會原本人之信用卡付款投保。謝謝";
		alert(msg);
		resetGoSubmitBtn()
		isNeedLock = true
	} else {
		isNeedLock = false;
		msg = "使用非會員本人之信用卡付款，請於投保兩天內回傳要保書及被保險人名冊。\n是否使用非會員本人信用卡付款?";

		var r = confirm(msg);
		if (r == true) {
			$("#insureWay").val("fax");
			goSubmit();
		} else {

			resetGoSubmitBtn()
		}
	}

	// lockSubmit(isNeedLock, msg)
}
function resetGoSubmitBtn() {
	// alert("resetGoSubmitBtn")
	$("#checkBoxDiv").addClass("checked")
	$("#card_same_name").prop("checked", true)
}
function save_frequently_travel(key) {
	var txt;
	var url = "../EBPT_1400_T4/save_frequently_travel?key=" + key;
	$.get(url, function(data, status) {
		if (data.isOk == true) {
			 alert("此投保內容已成功設為常用方案！後續您可至會員中心進行維護")
		} else {
			alert("儲存失敗")
		}
	});

}
function save_frequently_travel_insured(key, formName) {
	
	var form = $("form[name='" + formName + "']");
	var data = form.eq(1).serialize();
	var checked = form.eq(1).find("input[name='seqno']:checked");
	
	if(checked.length > 0){
		var url = "../EBPT_1400_T4/save_frequently_travel_insured?key=" + key;
		$.post(url, data, function(data, status) {
			if (data.isOk == true) {
				 alert("此被保人已成功設為常用人員！後續您可至會員中心進行維護")
				//console.log("ok is true")
			} else {
				alert("儲存失敗")
			}
		});
	}else{
		alert("未選取，自動關閉");
	}

	
}