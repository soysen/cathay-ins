var serverDateTime;
var last_travel_type;
var last_rate;
var travel_type = "All";
var isLoadCountries = true;
var countryArrayStr = $("#countryArrayStr").val();
function fix2(orgNum) {
	var newNum = "0" + orgNum;
	return newNum.substr(-2, 2)
}
function yyyy_mm_dd_hh_mm_ss(date) {
	var newFormat = "";
	newFormat += "" + date.getFullYear() + "-" + fix2(date.getMonth() + 1) + "-" + fix2(date.getDate())
	newFormat += " " + fix2(date.getHours()) + ":" + fix2(date.getMinutes()) + ":" + fix2(date.getSeconds())
	return newFormat;
}
function yyyy_mm_dd_hh_mm(date) {
	var newFormat = "";
	newFormat += "" + date.getFullYear() + "-" + fix2(date.getMonth() + 1) + "-" + fix2(date.getDate())
	newFormat += " " + fix2(date.getHours()) + "時" + fix2(date.getMinutes()) + "分"

	return newFormat;
}

function getPremiumApi(insureWay, PRODUCT_POLICY_CODE, travel_days, amount_21, amount_22, amount_23, amount_61) {

	var url = "../EBPT_1000_T0/getTravelPremium";
	url += "?insureWay=" + insureWay;
	url += "&PRODUCT_POLICY_CODE=" + PRODUCT_POLICY_CODE;
	url += "&travel_days=" + travel_days;
	url += "&amount_21=" + amount_21;
	url += "&amount_22=" + amount_22;
	url += "&amount_23=" + amount_23;
	url += "&amount_61=" + amount_61;
	$.get(url, function(data, status) {
		backPremium_from_getPremiumApi(data.result)
	});
}

function checkTravelDate(_serverDateTime) {
	var nDays = $("#nDays").val();
	if (nDays == undefined) {
		alert("nDays=" + nDays)
	}

	serverDateTime = _serverDateTime;

	var msg = null;
	var insureWay = $("#insureWay").val();//

	var ca = insureWay.toLowerCase() + "/" + $("#PRODUCT_POLICY_CODE").val();
	var now = new Date(serverDateTime);
	var date_str = $("#start_date").val().replace(/-/g, "/")

	var start_date = new Date(date_str)
	var mini_sec_web = 60 * $('#TRAVEL_NHOUR').val();
	var mini_24hr = 60 * 24;
	var mini_7day = 60 * 24 * nDays;
	var diff_sec = Math.round((start_date - now) / 1000 / 60);

	var year = now.getFullYear();
	var month = now.getMonth() + 2;
	var day = now.getDate();
	var hour = now.getHours();
	var min = now.getMinutes();
	var sec = now.getSeconds();
	var max_dateTime = new Date(year, month, day, hour, min, sec)

	var travel_days = $("#travel_days").val();

	if ($("#countryArrayStr").val() == "") {
		msg = "請選擇旅遊地點..";

		//console.log(" 76 =" + "  msg=" + msg)
	}
	// console.log(" 90 =" + " ca=" + ca)
	switch (ca) {
	case "web/PC01001":

		if (diff_sec < mini_sec_web) {
			msg = "出發時間距離現在時間須至少 1.5 小時，請重新選擇。 ";

			//console.log(" 86 =" + "  msg=" + msg)
		} else if (start_date > max_dateTime) {
			msg = "出發時間不得超過 2 個月 ";

			//console.log(" 90 =" + "  msg=" + msg)
		} else if (travel_days > 60) {
			msg = "國內旅遊不得大於 60 天! ";

			//console.log(" 94 =" + "  msg=" + msg)
		}

		break;
	case "web/PC01002":

		if (diff_sec < mini_sec_web) {
			msg = "出發時間距離現在時間須至少 1.5 小時，請重新選擇。 ";

			console.log(" 103 =" + "  msg=" + msg)
		} else if (diff_sec < mini_24hr) {
			if (travel_days < 2) {
				msg = "一天內出發之行程,國外旅遊期間不得少於於 2 天! ";
			} else if (travel_days > 180) {
				msg = "國內旅遊不得大於 180 天! ";
			}

		} else if (start_date > max_dateTime) {
			msg = "出發時間不得超過 2 個月 ";
		} else {
			if (travel_days < 1) {
				msg = "國內旅遊不得少於於 1 天! ";
			} else if (travel_days > 180) {
				msg = "國外旅遊不得大於 180 天! ";
			}
		}
		break;
	case "fax/PC01001":
		if (diff_sec < mini_7day) {
			msg = "當被保險人非會員本人、或前往地區為歐洲申根國家，則旅遊出發時間距離現在須至少" + nDays + " 天 ！ ";
		} else if (start_date > max_dateTime) {
			msg = "出發時間不得超過 2 個月 ";
		} else if (travel_days > 60) {
			msg = "國內旅遊不得大於 60 天! ";
		}
		break;
	case "fax/PC01002":
		if (diff_sec < mini_7day) {
			msg = "當被保險人非會員本人、或前往地區為歐洲申根國家，則旅遊出發時間距離現在須至少" + nDays + " 天 ！";
		} else if (start_date > max_dateTime) {
			msg = "出發時間不得超過 2 個月 ";
		} else if (travel_days > 180) {
			msg = "國外旅遊不得大於 180 天! ";
		}
		break;
	}
	return msg;
}

//FIXME
var countryCallBack = function ($this){
	var thisValue = $this.val();
	var disabled = false;
	
	var selectCountry = (null == thisValue || "" == thisValue) ? "" : thisValue.toString();
	var types = ( "" == selectCountry) ? "1" : (0<=selectCountry.indexOf("TW")) ? "2": "3";
	
	$("#travel_country").find("option").each(function(index, value) {
		var $thisOption = $(this);
		var optionValue = $thisOption.val();
		
		if ("2"  == types) {
			disabled = (optionValue == "TW_1_100") ? false : true;
		} else if ("3" == types) {
			disabled = (optionValue == "TW_1_100") ? true : false;
		} else if("1" == types){
			disabled = false;
		}
		
		$thisOption.prop("disabled", disabled);
		// 2016-11-15 2.拒保國家應能看到但不能選擇
		if ($(this).attr("isBlock") == "Y") {
			$(this).prop("disabled", true);
		}
	});
	$("#travel_country").select2({ maximumSelectionLength : 5 });
	$("#travel_type").val(travel_type);
};

function setCountryField(){
	var selectedCountry = $("#travel_country").val();
	if(null == selectedCountry || "" == selectedCountry){
		 $("#countryArrayStr").val("");
		 $("#fullCountryInfo").val("");
	}else{
		 $("#fullCountryInfo").val(selectedCountry.toString());
		 var temp = [];
		 $.each(selectedCountry.toString().split(","),function(index,value){
			 var array = value.split("_");
			 temp.push(array[0]);
		 });
		 $("#countryArrayStr").val(temp.toString());
	}
	
	var fullCountryInfo = $("#fullCountryInfo").val();
	$("#travel_type").val((fullCountryInfo == "TW_1_100") ? "I" : "A");
	
}

function checkInsureWay() {
	var isSchengen = $("#isSchengen").val();
	var numberOfPeople = $("#numberOfPeople").val();
	var startDate = $("#datepicker").val();
	var insured_u_id = $("#insured_u_id").val();
	var url = "../EBPT_1000_T0/getInsureWay?isSchengen=" + isSchengen + "&numberOfPeople=" + numberOfPeople + "&startDate=" + startDate + "&insured_u_id=" + insured_u_id;
	$.ajax({
		url : url, type : 'POST', async: false, success : function(data) {
			$("#insureWay").val(data['insureWay']);
		}
	});
}
function change_numberOfPeople() {
	checkInsureWay();
	firstTime = false;
	calPremium();
	var numberOfPeople = $("#numberOfPeople").val();
	if (numberOfPeople == 1 || numberOfPeople == 'Default') {
		$("#person_file").attr("style", "display:block");
	} else if (numberOfPeople > 1) {
		$("#person_file").attr("style", "display:none");
	}
}

function initSelectCountry() {
	if (countryArrayStr != "") {
		$("#travel_country").find("option").each(function(index, value) {
			var ta = $(this).val().split("_");
			var countryArray = countryArrayStr.split(",");
			for (i in countryArray) {
				if (ta[0] == countryArray[i]) {
					$(this).prop("selected", true)
				}
			}

		});
		$("#travel_country").select2({maximumSelectionLength : 5});
	}
	isLoadCountries = true;
	last_travel_type = travel_type;
	return;
}

function change_time(init) {

	var s_date_str = $("#datepicker").val() + " " + $("#tra_hour").val() + ":" + $("#tra_min").val() + ":00";
	var s_date = new Date(s_date_str)
	var e_date = new Date(s_date_str)
	var dates = s_date.getDate();
	var travel_days = $("#travel_days").val();
	var newDate = dates * 1 + travel_days * 1
	e_date.setDate(newDate);

	$("#start_date").val(yyyy_mm_dd_hh_mm_ss(s_date))
	$("#end_date").val(yyyy_mm_dd_hh_mm_ss(e_date))
	$("#end_time").html("至 " + yyyy_mm_dd_hh_mm(e_date) + " 止")

	if (travel_days > 60) {
		if ($("#countryArrayStr").val() == "0") {
		} else {
			$("#PRODUCT_POLICY_CODE").val("PC01002");// fax;
		}

	} else {

	}
	if (init == "init") {
		firstTime = true
	} else {
		firstTime = false;
	}
	checkInsureWay();
	calPremium();	

}

function moneyWanLabel(num) {
	var ans = num / 10000;
	return ans + "萬";
}
