switchProved(0)
var key = "";
initAllArea();
switch_natural_legal()
switch_company_register_nation();
switch_company_nation();
init_PolicyForm()
init_mailWay()
init_I_agree()
init_numberOfPeople()
init_all_insured_fill()
changeSendAsApplcnt()
selectPolicyForm();
function test() {
	alertMsg("test-myForm:" + $("#form1").serialize())
}

function changeInsuredAsApplcnt() {
	
	if ($("#isApplcntCB").prop("checked") == true) {

		$("#insured_name_0").prop("readonly", "readonly");
		$("#insured_u_id_0").prop("readonly", "readonly");
		$("#insured_name_0").val($("#member_name").val());
		$("#insured_u_id_0").val($("#member_u_id").val());
		$("#insured_name_0").attr("style", "cursor:not-allowed");
		$("#insured_u_id_0").attr("style", "cursor:not-allowed");

		$("#insured_0_yy").val($("#member_yy").val());
		$("#insured_0_mm").val($("#member_mm").val());
		$("#insured_0_dd").val($("#member_dd").val());
		$("#insured_0_yy").attr("style", "cursor:not-allowed");
		$("#insured_0_mm").attr("style", "cursor:not-allowed");
		$("#insured_0_dd").attr("style", "cursor:not-allowed");
		$("#insured_0_yy").prop("disabled", "disabled");
		$("#insured_0_mm").prop("disabled", "disabled");
		$("#insured_0_dd").prop("disabled", "disabled");
		
	} else {

		$("#insured_name_0").prop("readonly", "");
		$("#insured_u_id_0").prop("readonly", "");
		$("#insured_name_0").attr("style", "");
		$("#insured_u_id_0").attr("style", "");

		$("#insured_0_yy").attr("style", "");
		$("#insured_0_mm").attr("style", "");
		$("#insured_0_dd").attr("style", "");
		$("#insured_0_yy").prop("disabled", "");
		$("#insured_0_mm").prop("disabled", "");
		$("#insured_0_dd").prop("disabled", "");

	}
}

function changeSendAsApplcnt() {
	if ($("#isApplcntAddress").prop("checked") == true) {
		$("#mail_city").prop("disabled", "disabled");
		$("#mail_county").prop("disabled", "disabled");
		$("#mail_address").prop("readonly", "readonly");

		$("#mail_city").attr("style", "cursor:not-allowed");
		$("#mail_county").attr("style", "cursor:not-allowed");
		$("#mail_address").attr("style", "cursor:not-allowed");


		$("#mail_city").val($("#member_city").val());
		$("#mail_county").val($("#member_county").val());
		$("#mail_address").val($("#member_address").val());

	} else {

		$("#mail_city").prop("disabled", "");
		$("#mail_county").prop("disabled", "");
		$("#mail_address").prop("readonly", "");
		$("#mail_city").attr("style", "");
		$("#mail_county").attr("style", "");
		$("#mail_address").attr("style", "");
	}
}
function changeApplcntAsMember() {
	if ($("#isMemberCB").prop("checked") == true) {
		$("#applcnt_name").prop("readonly", "readonly");
		$("#applcnt_name").prop("style", "cursor: not-allowed")
		$("#applcnt_name").val($("#member_name").val());

		$("#applcnt_u_id").prop("readonly", "readonly");
		$("#applcnt_u_id").prop("style", "cursor: not-allowed")
		$("#applcnt_u_id").val($("#member_u_id").val());

		$("#applcnt_yy").prop("disabled", "disabled");
		$("#applcnt_yy").prop("style", "cursor: not-allowed")
		$("#applcnt_yy").val($("#member_yy").val());

		$("#applcnt_mm").prop("disabled", "disabled");
		$("#applcnt_mm").prop("style", "cursor: not-allowed")
		$("#applcnt_mm").val($("#member_mm").val());

		$("#applcnt_dd").prop("disabled", "disabled");
		$("#applcnt_dd").prop("style", "cursor: not-allowed")
		$("#applcnt_dd").val($("#member_dd").val());

		$("#applcnt_mobile").prop("disabled", "disabled");
		$("#applcnt_mobile").prop("style", "cursor: not-allowed")
		$("#applcnt_mobile").val($("#member_phone").val());

	} else {
		$("#applcnt_name").prop("readonly", "");
		$("#applcnt_name").prop("style", "cursor: text")
		$("#applcnt_u_id").prop("readonly", "");
		$("#applcnt_u_id").prop("style", "cursor: text")
		$("#applcnt_yy").prop("disabled", "");
		$("#applcnt_yy").prop("style", "cursor: pointer")
		$("#applcnt_mm").prop("disabled", "");
		$("#applcnt_mm").prop("style", "cursor: pointer")
		$("#applcnt_dd").prop("disabled", "");
		$("#applcnt_dd").prop("style", "cursor: pointer")
		$("#applcnt_mobile").prop("disabled", "");
		$("#applcnt_mobile").prop("style", "cursor: text")
	}
}
function switchProved(type, id, countryArrray) {
	if ($("#isSchengen").val() == "true") {
		for (var i = 0; i < 6; i++) {
			$("#insProvedCheckBox_" + i).parent().addClass("checked")
			$("#insProvedCheckBox_" + i).prop("checked", true);
			$("#insProvedDiv_" + i).css("display", "block");
		}
		if (type == 1) {
			if(! $("#insProvedCheckBoxDiv_" + i).hasClass("checked")){
				$("#insProvedCheckBoxDiv_" + i).addClass("checked");
			}
			alertMsg("旅遊地區為申根國家,一律申請英文投保證明")
		}
	}else{
		if("TW" == countryArrray){
			for (var i = 0; i < 6; i++) {
				$("#insProvedCheckBox_" + i).parent().removeClass("checked");
				$("#insProvedCheckBox_" + i).prop("checked", false);
				$("#insProvedDiv_" + i).css("display", "none");
			}
			alertMsg("國內旅遊不支援申請英文投保證明");
			return;
		}
		if ($("#insProvedCheckBox_" + id).prop("checked") == true) {
			for (var i = 0; i < 6; i++) {
				$("#insProvedCheckBox_" + i).parent().addClass("checked");
				$("#insProvedCheckBox_" + i).prop("checked", true);
				$("#insProvedDiv_" + i).css("display", "block");
			}
		} else {
			for (var i = 0; i < 6; i++) {
				$("#insProvedCheckBox_" + i).parent().removeClass("checked");
				$("#insProvedCheckBox_" + i).prop("checked", false);
				$("#insProvedDiv_" + i).css("display", "none");
			}
		}
	}
}

// 閏年

function check_month_day(who) {
	/*
	 * 逢4的倍數閏， 例如：西元1992、1996年等，為4的倍數，故為閏年。 逢100的倍數不閏，
	 * 例如：西元1700、1800、1900年，為100的倍數，當年不閏年。 逢400的倍數閏，
	 * 例如：西元1600、2000、2400年，為400的倍數，有閏年 逢4000的倍數不閏， 例如：西元4000、8000年，不閏年。
	 * 
	 */
	var year = $("#" + who + "yy").val() * 1 + 1911
	var month = $("#" + who + "mm").val()
	var mon_days = new Array()
	mon_days[1] = 31;
	mon_days[2] = 28;
	mon_days[3] = 31;
	mon_days[4] = 30;
	mon_days[5] = 31;
	mon_days[6] = 30;
	mon_days[7] = 31;
	mon_days[8] = 31;
	mon_days[9] = 30;
	mon_days[10] = 31;
	mon_days[11] = 30;
	mon_days[12] = 31;

	if (month == 2) {
		if (year % 4 == 0) {
			mon_days[2] = 29;
		}
		if (year % 100 == 0) {
			mon_days[2] = 28;
		}
		if (year % 400 == 0) {
			mon_days[2] = 29;
		}
		if (year % 4000 == 0) {
			mon_days[2] = 28;
		}
	}

	makeDayOption(mon_days[month], who)
}
function makeDayOption(days, who) {
	var html = "";
	html += '<option selected="selected" value="">請選擇日期</option>';
	for (var i = 1; i <= days; i++) {
		html += '<option value="' + i + '">' + i + '日</option>';
	}
	$("#" + who + "dd").html(html);

}

function changeRelation(id) {
	if ($("#relation_benefit_insured_" + id).val() == "L") {
		$("#benefit_name_" + id).prop("disabled", true);
		$("#benefit_mobile_" + id).prop("disabled", true);
		$("#benefit_" + id + "_city").prop("disabled", true);
		$("#benefit_" + id + "_county").prop("disabled", true);
		$("#benefit_addr_" + id).prop("disabled", true);

		$("#benefit_name_" + id).val("");
		$("#benefit_mobile_" + id).val("");
		$("#benefit_" + id + "_city").val("Default");
		$("#benefit_" + id + "_county").val("");
		$("#benefit_addr_" + id).val("");
		
		$("#benefit_name_" + id).prop("style", "cursor: not-allowed");
		$("#benefit_" + id + "_city").prop("style", "cursor: not-allowed");
		$("#benefit_" + id + "_county").prop("style", "cursor: not-allowed");
		$("#benefit_addr_" + id).prop("style", "cursor: not-allowed");

	} else {
		$("#benefit_name_" + id).prop("disabled", false);
		$("#benefit_mobile_" + id).prop("disabled", false);
		$("#benefit_" + id + "_city").prop("disabled", false);
		$("#benefit_" + id + "_county").prop("disabled", false);
		$("#benefit_addr_" + id).prop("disabled", false);
		
		$("#benefit_name_" + id).prop("style", "");
		$("#benefit_" + id + "_city").prop("style", "");
		$("#benefit_" + id + "_county").prop("style", "");
		$("#benefit_addr_" + id).prop("style", "cursor: ");

	}
}
function initAllArea() {
	if ($("#benefit_county_Html_save").val() != "") {
		$("#benefit_county").html($("#benefit_county_Html_save").val());
		$("#benefit_county").val($("#benefit_county_value_save").val());
	}
	if ($("#applcnt_county_Html_save").val() != "") {
		$("#applcnt_county").html($("#applcnt_county_Html_save").val());
		$("#applcnt_county").val($("#applcnt_county_value_save").val());
	}
}
function changeCity(who) {

	var _city = $("#" + who + "_city").val()

	$.post("../EBP0_Z000/getCountyByCityHtml", {
		city : _city
	}, function(data) {
		$("#" + who + "_county").html(data.msg);
	});
}

init_postnumber()
function init_postnumber() {
	changeCounty("benefit");
	changeCounty("applcnt")
	changeCounty("mail")
}
function changeCounty(who) {
	var county = $("#" + who + "_county").val();
	$("#" + who + "_county").find("option").each(function(i) {
		if ($(this).val() == county) {
			$("#" + who + "_postnumber").val($(this).attr("postnumber"));
		}
	});
	$("#benefit_county_Html_save").val($("#benefit_county").html());
	$("#benefit_county_value_save").val($("#benefit_county").val());
	$("#applcnt_county_Html_save").val($("#applcnt_county").html());
	$("#applcnt_county_value_save").val($("#applcnt_county").val());
	$("#mail_county_Html_save").val($("#mail_county").html());
	$("#mail_county_value_save").val($("#mail_county").val());

}
function switch_natural_legal() {
	var who = $('input[name=natural_legal_person]:checked').val();
	var a1 = $('input[name=natural_legal_person]');
	if (who == "natural_person") {
		// 自然人
		$("#legal_person_div").css("display", 'none');
		$("#natural_person_div").css("display", 'block');
		$(a1[0]).parent("div").addClass("checked")
		$(a1[1]).parent("div").removeClass("checked")
	} else {
		// 法人
		$("#legal_person_div").css("display", 'block');
		$("#natural_person_div").css("display", 'none');

		$(a1[0]).parent("div").removeClass("checked")
		$(a1[1]).parent("div").addClass("checked")
	}

}

function changeInsureMotive(obj,type){
	if("nature" == type){
		if(obj.value == "G"){
			$("#insure_motive_memo_nature").show();
		}else{
			$("#insure_motive_memo_nature").hide();
		}
	}else{
		if(obj.value == "G"){
			$("#insure_motive_memo_company").show();
		}else{
			$("#insure_motive_memo_company").hide();
		}
	}
}

function switch_company_nation() {
	var who = $('input[name=company_nation]:checked').val();

	var a1 = $('input[name=company_nation]');
	if (who == "other") {
		// 其他
		$("#company_other_nation").prop("disabled", false);

		$(a1[0]).parent("div").removeClass("checked")
		$(a1[1]).parent("div").addClass("checked")
	} else {
		// 台灣
		$("#company_other_nation").prop("disabled", true);
		$(a1[0]).parent("div").addClass("checked")
		$(a1[1]).parent("div").removeClass("checked")
	}

}

function switch_company_register_nation() {
	var who = $('input[name=company_register_nation]:checked').val();

	var a1 = $('input[name=company_register_nation]');
	if (who == "other") {
		// 其他
		$("#company_register_other_nation").prop("disabled", false);
		$(a1[1]).parent("div").addClass("checked")
		$(a1[0]).parent("div").removeClass("checked")
	} else {
		// 台灣
		$("#company_register_other_nation").prop("disabled", true);
		$(a1[0]).parent("div").addClass("checked")
		$(a1[1]).parent("div").removeClass("checked")

	}

}
function init_I_agree() {
	var who = $('input[name=I_agree]:checked').val();
	var a1 = $('input[name=I_agree]');
	if (who == "true") {
		$(a1[0]).parent("div").addClass("checked");

	} else {
		$(a1[0]).parent("div").removeClass("checked");

	}

}
function init_mailWay() {
	var who = $('input[name=mailWay]:checked').val();
	var a1 = $('input[name=mailWay]');
	$(a1[0]).parent("div").removeClass("checked");
	$(a1[1]).parent("div").removeClass("checked");

	if (who == "1") {
		$(a1[0]).parent("div").addClass("checked");

	} else if (who == "2") {

		$(a1[1]).parent("div").addClass("checked");

	}

}
function init_PolicyForm() {
	var who = $('input[name=policyForm]:checked').val();
	var a1 = $('input[name=policyForm]');

	switch (who) {
	case "P":
		$(a1[0]).parent("div").addClass("checked");
		$(a1[1]).parent("div").removeClass("checked");
		break;
	case "E":
		$(a1[0]).parent("div").removeClass("checked");
		$(a1[1]).parent("div").addClass("checked");

		break;
	}
}
function selectPolicyForm() {
	var who = $('input[name=policyForm]:checked').val();
	var a1 = $('input[name=policyForm]');

	switch (who) {
	case "P":
		$("#mailWay_ordinary").parent("div").removeClass("disabled");
		$("#mailWay_ordinary").prop("disabled", false);
		$("#mailWay_ordinary").parent("div").attr("style", "");

		$("#mailWay_registered").parent("div").removeClass("disabled");
		$("#mailWay_registered").prop("disabled", false);
		$("#mailWay_registered").parent("div").attr("style", "");

		$("#mailWay_ordinary").parent("div").addClass("checked");
		$("#mailWay_ordinary").prop("checked", true);

		$("#mailWay_registered").parent("div").removeClass("checked");
		$("#mailWay_registered").prop("checked", false);
		break;
	case "E":
		$("#mailWay_ordinary").parent("div").addClass("disabled");
		$("#mailWay_ordinary").prop("disabled", true);
		$("#mailWay_ordinary").parent("div").attr("style", "cursor:not-allowed");

		$("#mailWay_registered").parent("div").addClass("disabled");
		$("#mailWay_registered").prop("disabled", true);
		$("#mailWay_registered").parent("div").attr("style", "cursor:not-allowed");

		$("#mailWay_ordinary").parent("div").removeClass("checked");
		$("#mailWay_ordinary").prop("checked", false);

		$("#mailWay_registered").parent("div").removeClass("checked");
		$("#mailWay_registered").prop("checked", false);
		break;
	}
}

//取得每人的生日
function getPeopleBirthday(){
	var year = [];
	var month = [];
	var date = [];

	$("select[id^='insured_'][id$='_yy']").each(function() {
		var val =$(this).val();
		if(val && $.isNumeric(val)){
			year.push(parseInt(val) + 1911);	
		}
	});
	
	$("select[id^='insured_'][id$='_mm']").each(function() { 
		var val =$(this).val();
		if(val && $.isNumeric(val)){
			month.push(parseInt(val) < 10 ? '0' + val : val);	
		}
	});
	
	$("select[id^='insured_'][id$='_dd']").map(function() {
		var val =$(this).val();
		if(val && $.isNumeric(val)){
			date.push(parseInt(val) < 10 ? '0' + val : val);	
		}
	});
	return $.map(year, function(n, i) { return  year[i] + "-" + month[i] + "-" + date[i]; });
};

//取得每個「被保人身分證字號/居留證號碼」
function getPersonsId(){
	return $("input[id^='insured_u_id_']").map(function() { return $(this).val(); }).get().join().split(",").filter(function(v){return v!==''});
};

//取得每個「意外事故身故殘廢保險金」
function getAmount21S(size){
	var result = [];
	$("select[id^='group_amount_21_p_']").each(function(index) {
		if(index <= size){
			result.push($(this).val());
		}
	});
	return result;
};

//取得每個「被保人姓名」
function getInsuedName(){
	var result = [];
	$("input[id^='insured_name_']").each(function() {
		if("" != $(this).val()){
			result.push($(this).val());
		}
	});
	return result;
};

function goSubmit(_key) {
	var isOk = checkAll();
	if (isOk == true) {
		$.blockUI();
		//要保人 - 黑名單 -> 被保人 - 黑名單 -> 通算 -> 送出
		try {
			doBlockCheckType1(_key);
		}catch(e){
			$.unblockUI();
		}
	}
};

//要保人 - 黑名單
function doBlockCheckType1(_key){
	
	var type = ("natural_person" == $('input[name=natural_legal_person]:checked').val()) ? true : false;
	
	var rejectCode = "";
	
	var id = (type) ? $("#applcnt_u_id").val() : $("input[name='company_id']").val();
	var name = (type) ? $("#applcnt_name").val() : $("#companyName").val();
	
	var url = "../EBPT_1300_T3/blockCheck?id=" +id+ "&name=" +encodeURI(encodeURI(name))+ "&kind=true&type=" + type;
	
	$.ajax({
		url: url,
		type: 'GET',
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		success: function(data){
			rejectCode = data.reject;
		}
	}).done(function() {
		if("" != rejectCode){
			$.unblockUI();
			alertMsg("要保人『"+id+"』為黑名單["+rejectCode+"]");
			return;
		}else{
			doBlockCheckType2(type,_key);//被保人 - 黑名單
		}
	});
};

//被保人 - 黑名單
function doBlockCheckType2(type,_key){
	
	var url = "";
	
	var id = getPersonsId();
	var name = getInsuedName();
	
	var size = id.length -1;
	var blockIds = [];
	var rejectCode = [];
	var count = 0;
	
	for (var i = 0; i <= size; i++) {
		url = "../EBPT_1300_T3/blockCheck?id=" + id[i] + "&name=" + encodeURI(encodeURI(name[i])) + "&kind=false&type=" + type;
		$.ajax({
			url: url,
			type: 'GET',
			contentType: "application/x-www-form-urlencoded; charset=UTF-8",
			success: function(data){
				if("" != data.reject){
					blockIds.push(id[i]);
					rejectCode.push(data.reject);					
				}
				count++;
			}
		}).done(function(){
			if(count == size){
				if(0 < blockIds.length){
					$.unblockUI();
					alertMsg("被要保人『"+blockIds.toString()+"』為黑名單["+rejectCode.toString()+"]");
					return;
				}else{
					doCalLimit(_key);//通算
				}				
			}
		});	
	}
};

//通算
function doCalLimit(key){
	var id = getPersonsId();
	var birthday = getPeopleBirthday();
	var amount21s = getAmount21S(id.length -1);
	var OTP = insureWay == "web" ? "Y" : "N";
	var ISGROUP = "Y";
	var url = "../EBPT_1300_T3/calLimit?insureWay=" + insureWay + "&start_date=" + start_date + "&end_date=" + end_date + "&u_id=" + id + "&birthdays=" + birthday + "&amount21s=" + amount21s + "&key=" + key + "&OTP=" + OTP + "&ISGROUP=" + ISGROUP;
	var isOk = checkAll();
	if (isOk == true) {
		$.ajax({
			url: url,
		    type: 'GET',
		    success: function(data){
		    	if("" == data.errorMsg){
		    		$('#relation_applcnt_insured').attr('disabled', false);
		    		
		    		$('#refqueryid').val(data.referenceId);
		    		$("#form1").attr("action", "../EBPT_1400_T4/T4?key=" + key)
		    		$("#form1").submit();
		    	}else{
		    		$.unblockUI();
		    		alertMsg(data.errorMsg);
		    	}
		    }
		});
	}
};

function checkAll() {
	var isOk = true;
	if ($("#I_agree").prop("checked") == false) {
		alertMsg("請先同意上述須知及審閱相關聲明事項");
		return false;
	}
	
	// 要保人資料

	 var natural_legal_person="";
	  $("input[name=natural_legal_person]").each(function(){
		  if($(this).prop("checked")==true){
			  natural_legal_person=$(this).val();
		  }
	  });
	 
	if (natural_legal_person == "natural_person") {
		// 自然人(同會員本人)
		if ($("#applcnt_tel_zip").val() == "9999") {
			alertMsg("請輸入要保人日間市話區碼")
			$("#applcnt_tel_zip").focus();

			return false;
		}
		if ($("#applcnt_tel").val() == "") {
			alertMsg("請輸入要保人日間市話號碼")
			$("#applcnt_tel").focus();

			return false;
		}
		
		// alertMsg($("#isApplcntAddress").prop("checked"))
		if ($("#isApplcntAddress").prop("checked") == false) {
			// 寄單地址同通訊地址,不同時,須檢查
			if ($("#mail_city").val() == "Default") {
				alertMsg("請選擇要保人縣市")
				$("#mail_city").focus();

				return false;
			}
			if ($("#mail_county").val() == "") {
				alertMsg("請選擇要保人行政區")
				$("#mail_county").focus();

				return false;
			}
			if ($("#mail_address").val() == "") {
				alertMsg("請輸入要保人通訊地址")
				$("#mail_address").focus();

				return false;
			}
		}
		if ($("#applcnt_email").val() == "") {
			alertMsg("請輸入要保人電子信箱")
			$("#applcnt_email").focus();

			return false;
		}

		if ($('input[name=relation_applcnt_insured]:checked').val() == undefined) {
			alertMsg("請選擇要保人關係")
			$("#relation_applcnt_insured").focus();

			return false;
		}
		if(isShowPerson){
			if ($("#applcnt_occupation").val() == "") {
				alertMsg("請選擇要保人職業類別")
				$("#applcnt_occupation").focus();

				return false;
			}
			if ($("#applcnt_position").val() == "") {
				alertMsg("請選擇要保人職稱")
				$("#applcnt_position").focus();

				return false;
			}
		}
		if ($("#applcnt_yearIncome").val() == "") {
			alertMsg("請選擇要保人年收入與其他收入金額")
			$("#applcnt_yearIncome").focus();

			return false;
		}
		if ($("#applcnt_yearAssets").val() == "") {
			alertMsg("請選擇要保人財務與資產狀況金額")
			$("#applcnt_yearAssets").focus();

			return false;
		}

	} else {
		// 法人
		if ($("#companyName").val() == "") {
			alertMsg("請輸入公司名稱")
			$("#companyName").focus();

			return false;
		}
		if(isShowCompany){
			if ($("#company_occupation").val() == "") {
				alertMsg("請選擇公司行業別")
				$("#company_occupation").focus();

				return false;
			}
		}
		if ($("#company_id").val() == "") {
			alertMsg("請輸入公司統一編號")
			$("#company_id").focus();

			return false;
		}
		if ($("#calendar_date").val() == "") {
			alertMsg("請輸入公司西元成立年份")
			$("#calendar_date").focus();

			return false;
		}
		if (!validateNum($("#calendar_date").val())) {
			alertMsg("公司西元成立年份只允許輸入數字")
			$("#calendar_date").focus();

			return false;
		}
		if ($("#captl").val() == "") {
			alertMsg("請輸入公司資本額（萬元）")
			$("#captl").focus();

			return false;
		}
		if ($("#turnover").val() == "") {
			alertMsg("請輸入公司營業收入（萬元）")
			$("#turnover").focus();

			return false;
		}
		if ($("#representative").val() == "") {
			alertMsg("請輸入公司負責人姓名")
			$("#representative").focus();

			return false;
		}
		if ($("#representative_phone").val() == "") {
			alertMsg("請輸入公司手機號碼")
			$("#representative_phone").focus();

			return false;
		}
		if ($("#company_tel_zip").val() == "9999") {
			alertMsg("請輸入公司日間市話區碼")
			$("#company_tel_zip").focus();

			return false;
		}
		if ($("#company_tel").val() == "") {
			alertMsg("請輸入公司日間市話區碼")
			$("#company_tel").focus();

			return false;
		}
		
		if ($("#company_city").val() == "Default") {
			alertMsg("請選擇公司縣市")
			$("#company_city").focus();

			return false;
		}
		if ($("#company_county").val() == "Default") {
			alertMsg("請選擇公司行政區")
			$("#company_county").focus();

			return false;
		}
		if ($("#company_address").val() == "") {
			alertMsg("請輸入公司地址")
			$("#company_address").focus();

			return false;
		}
		if ($("#company_email").val() == "") {
			alertMsg("請輸入公司電子信箱")
			$("#company_email").focus();

			return false;
		}
		if ($("#company_yearIncome").val() == "") {
			alertMsg("請選擇公司金額")
			$("#company_yearIncome").focus();

			return false;
		}
		if ($("#company_yearAssets").val() == "") {
			alertMsg("請選擇公司金額")
			$("#company_yearAssets").focus();

			return false;
		}
		
	}

	if ($("#emergency_name").val() == "") {
		alertMsg("請輸入台灣緊急連絡人姓名")
		$("#emergency_name").focus();

		return false;
	}
	if ($("#emergency_mobile").val() == "") {
		alertMsg("請輸入台灣緊急連絡人手機號碼")
		$("#emergency_mobile").focus();

		return false;
	}
	if (!validateNum($("#emergency_mobile").val()) || $("#emergency_mobile").val().length != 10) {
		alertMsg("台灣緊急連絡人手機號碼只允許輸入數字10碼")
		$("#emergency_mobile").focus();

		return false;
	}
	if ($("#emergency_tel_zip").val() == "9999") {
		alertMsg("請輸入台灣緊急連絡人聯絡人日間市話區碼")
		$("#emergency_tel_zip").focus();

		return false;
	}
	if ($("#emergency_tel").val() == "") {
		alertMsg("請輸入台灣緊急連絡人日間市話號碼")
		$("#emergency_tel").focus();

		return false;
	}
	
	
	
	var count = 1 * $("#numberOfPeople").val();	
	var msg = "";
	
	$("input[name='insured_name']").each(function(i){
		i=i+1;
		if(i > count){
			return false;
		}		
		if ( $(this).val() == "" ) {
			msg = msg + "請輸入被保人"+ i +" 姓名" + "\n";
			isOk = false;
		}
	});
	var idArr = [];
	$("input[name='insured_u_id']").each(function(i){
		i=i+1;
		if(i > count){
			return false;
		}		
		if ( $(this).val() == "" ) {
			msg = msg + "請輸入被保人"+ i +" 身分證字號/居留證號碼" + "\n";
			isOk = false;
		}else{
			if(!validation.checkID($(this).val().toUpperCase())){
				msg = msg + "被保人"+ i +" 身分證字號/居留證號碼  錯誤" + "\n";
				isOk = false;
			}
			if(jQuery.inArray( $(this).val().toUpperCase() , idArr ) != -1 ){
				msg = msg + "被保人"+ i +" 身分證字號/居留證號碼 重複" + "\n";
				isOk = false;
			}
			idArr.push($(this).val().toUpperCase());
		}
	});
	$("select[name='insured_yy']").each(function(i){
		i=i+1;
		if(i > count){
			return false;
		}
		if ( $(this).val() == "Default" ) {
			msg = msg + "請選擇被保人"+ i +" 年份" + "\n";
			isOk = false;
		}
	});
	$("select[name='insured_mm']").each(function(i){
		i=i+1;
		if(i > count){
			return false;
		}
		if ( $(this).val() == "Default" ) {
			msg = msg + "請選擇被保人"+ i +" 月份"+"\n";
			isOk = false;
		}
	});
	$("select[name='insured_dd']").each(function(i){
		i=i+1;
		if(i > count){
			return false;
		}
		if ( $(this).val() == "Default" ) {
			msg = msg + "請選擇被保人"+ i +" 日期"+"\n";
			isOk = false;
		}
	});
	
	
	// 申請英文投保證明
	if ($("#insProvedCheckBox").prop("checked")) {
		$("input[name='pass_name']").each(function(i){
			i=i+1;
			if(i > count){
				return false;
			}
			if ( $(this).val() == "" ) {
				msg = msg + "請輸入被保人"+ i +" 護照英文姓名"+"\n";
				isOk = false;
			}else{
			    if(!(/^[^\u4E00-\u9FA5\uF900-\uFA2D]+$/.test( $(this).val() ))){   
				    msg = msg + "被保人"+ i +" 護照英文姓名不能輸入中文"+"\n";
					isOk = false;
			    }    
			}
		});
		$("input[name='pass_num']").each(function(i){
			i=i+1;
			if(i > count){
				return false;
			}
			if ( $(this).val() == "" ) {
				msg = msg + "請輸入被保人"+ i +"護照號碼"+"\n";
				isOk = false;
			}
		});
	}
	
	$("select[name='relation_benefit_insured']").each(function(i){
		i=i+1;
		if(i > count){
			return false;
		}
		if ( $(this).val() == "Def" ) {
			msg = msg + "請選擇被保人"+ i +" 受益人關係"+"\n";
			isOk = false;
		}else if( $(this).val() == "L"  ){
			//法定繼承人,不檢察
		}else{
			if($("input[name='relation_benefit_insured']").eq(i-1).val() == ""){
				msg = msg + "請輸入被保人"+ i +" 受益人姓名"+"\n";
				isOk = false;
			}
			if($("input[name='benefit_mobile']").eq(i-1).val() == ""){
				msg = msg + "請輸入被保人"+ i +" 受益手機"+"\n";
				isOk = false;
			}
			if (!validateNum($("input[name='benefit_mobile']").eq(i-1).val()) || $("input[name='benefit_mobile']").eq(i-1).val().length != 10) {
				msg = msg + "被保人"+ i +" 受益人手機號碼只允許輸入數字10碼"+"\n";
				isOk = false;
			}
			if($("select[name='benefit_city']").eq(i-1).val() == "Default"){
				msg = msg + "請選擇被保人"+ i +" 受益人縣市"+"\n";
				isOk = false;
			}
			if($("select[name='benefit_county']").eq(i-1).val() == "Default"){
				msg = msg + "請選擇被保人"+ i +" 受益人行政區"+"\n";
				isOk = false;
			}
			if($("input[name='benefit_addr']").eq(i-1).val() == ""){
				msg = msg + "請輸入被保人"+ i +" 受益人地址"+"\n";
				isOk = false;
			}
			
			
		}
	});
	
	
	if(!isOk){
		alertMsg(msg);
		return false;
	}
	return isOk;
}
function addInsuerd() {
	var numberOfPeople = $("#numberOfPeople").val() * 1;
	numberOfPeople++;
	$("#numberOfPeople").val(numberOfPeople)
	init_numberOfPeople()
	active_tab(numberOfPeople - 1);
	init_All_Totale_premium();
}
function deleteInsured(id) {
	var numberOfPeople = $("#numberOfPeople").val() * 1;
	if (numberOfPeople > 2) {
		numberOfPeople--;
		$("#numberOfPeople").val(numberOfPeople)
	} else {
		alertMsg("最少應有兩人!")
		return;
	}
	var new_id = 0;
	if (id == 0) {
		// 第一個
		new_id = 0;
	} else if (id == numberOfPeople) {
		// 最後一個
		new_id = id - 1;
	} else {
		// 中間的
		new_id = id - 1;
	}
	init_numberOfPeople();
	active_tab(new_id);
	init_All_Totale_premium();
}
function active_tab(id) {
	var thisLi = $("#p_" + id);
	$(thisLi).closest('.tabs').find('li').removeClass('active');
	$(thisLi).addClass('active');
	var liIndex = $(thisLi).index() + 1;
	$(thisLi).closest('.tabbed-content').find('.content>li').removeClass(
			'active');
	$(thisLi).closest('.tabbed-content').find(
			'.content>li:nth-of-type(' + liIndex + ')').addClass('active');
}
function init_numberOfPeople() {

	var numberOfPeople = $("#numberOfPeople").val() * 1;

	for (var i = 2; i < numberOfPeople; i++) {
		$("#p_" + i).attr("style", "display:block")
	}

	for (var i = numberOfPeople; i < 6; i++) {
		$("#p_" + i).attr("style", "display:none")
	}

	if($("#addInsuerdBtn")){
		if (numberOfPeople < 6) { 
			$("#addInsuerdBtn").prop("style", "display:block");
		}else{
			$("#addInsuerdBtn").prop("style", "display:none");
		}
	}

	for (var i = 0; i < numberOfPeople-1; i++){
		$("#btn_del_" + i).attr("style", "display:none");
	}

	$("#btn_del_" + (numberOfPeople-1)).attr("style", "display:block");
	if(numberOfPeople == 2){
		$("#btn_del_1").attr("style", "display:none");
	}
}

function change_amount_21(id) {
	var val = $("#group_amount_21_p_" + id).val()
	$("#group_amount_23_label_p_" + id).html(wanLabel(val))
	$("#group_amount_23_p_" + id).val(val)
	getPremiumById(id)
}

function getPremiumById(id) {

	var insureWay = $("#insureWay").val();
	var PRODUCT_POLICY_CODE = $("#PRODUCT_POLICY_CODE").val();
	var travel_days = $("#travel_days").val();
	var amount_21 = $("#group_amount_21_p_" + id).val();
	var amount_22 = $("#group_amount_22_p_" + id).val();
	var amount_23 = $("#group_amount_23_p_" + id).val();
	var amount_61 = $("#group_amount_61_p_" + id).val();

	var url = "../EBPT_1000_T0/getTravelPremium";
	url += "?insureWay=" + insureWay;
	url += "&PRODUCT_POLICY_CODE=" + PRODUCT_POLICY_CODE;
	url += "&travel_days=" + travel_days;
	url += "&amount_21=" + amount_21;
	url += "&amount_22=" + amount_22;
	url += "&amount_23=" + amount_23;
	url += "&amount_61=" + (amount_61||0);

	$.get(url, function(data, status) {
		figure_total_premium(id, data.result)
	});
}

function figure_total_premium(id, data) {
	var data_arr = data.split("__")
	var premium = data_arr[0];
	var group_combo = data_arr[1];
	var group_premium_combo = data_arr[4];

	$("#group_premium_label_p_" + id).html(premium)
	$("#group_premium_p_" + id).val(premium)

	$("#group_combo_p_" + id).val(group_combo)
	$("#group_premium_combo_p_" + id).val(group_premium_combo)
	init_All_Totale_premium()

}
function init_all_insured_fill() {
	// 畫面重整
	var numberOfPeople = $("#numberOfPeople").val();
	var group_premium_total = $("#group_premium_total_save").val();
	if (group_premium_total == 0) {
		return;
	}
	$("#group_premium_total").val(group_premium_total);
	for (var i = 0; i < numberOfPeople; i++) {
		var val = $("#group_amount_21_p_" + i).val();
		$("#group_amount_23_label_p_" + i).html(wanLabel(val));
		$("#group_amount_23_p_" + i).val(val);
		var group_premium = $("#group_premium_p_" + i).val();
		$("#group_premium_label_p_" + i).html(group_premium);

		$("#group_premium_label_total_" + i).html(group_premium_total);
	}

}
function init_All_Totale_premium() {
	var numberOfPeople = $("#numberOfPeople").val();
	var total = 0;
	for (var i = 0; i < numberOfPeople; i++) {
		total += $("#group_premium_p_" + i).val() * 1;
	}
	for (var i = 0; i < numberOfPeople; i++) {
		$("#group_premium_label_total_" + i).html(total);
	}
	$("#group_premium_total").val(total);
	$("#group_premium_total_save").val(total);
}
function wanLabel(num) {
	return num / 10000 + "萬";
}
var nowSelectObj = null;
function select_frequently_insured(insured_seqno_input) {
	$("#insured_seqno_input").val(insured_seqno_input);
}
function select_frequently_insured_sure(id) {

	if(!id||id==''){
		return;
	}

	var insured_seqno= $("#insured_seqno_input").val();
	if(insured_seqno != "" && insured_seqno != null){
		$("#isApplcntCB").prop("checked",false)
		$("#isApplcntCB").parent().removeClass("checked")
		changeInsuredAsApplcnt();
		$("input[name=insured_seqno]").each(function() {

			if ($(this).prop("checked") == true) {
				$(this).parent().removeClass("checked");
				$(this).prop("checked", false);
			}

		})
		
		var url = "../EBPT_1300_T3/get_frequently_travel_insured";
		var insured_seqno= $("#insured_seqno_input").val();
		var data="insured_seqno="+insured_seqno;
		$.post(url,data, function(data, status) {
			
			$("#insured_name_"+id).val(data.insured_name)
			$("#insured_u_id_"+id).val(data.insured_id)
			
			var insured_birthday_str=data.insured_birthday.substring(0,10);
			
			var insured_birthday_arr=insured_birthday_str.split("-")
			var year=insured_birthday_arr[0]-1911;
			
			$("#insured_"+id+"_yy").val(year)
			$("#insured_"+id+"_mm").val(parseInt(insured_birthday_arr[1]))
			check_month_day('insured_' + id + '_');
			$("#insured_"+id+"_dd").val(parseInt(insured_birthday_arr[2]))
		});
	}
}

function changeCompanyId(){
	var url = "../EBPT_1300_T3/getIsShowCompany?company_id="+$("#company_id").val();
	$.ajax({
	    url: url,
	    type: 'POST',
	    success: function(data){ 
	    	isShowCompany = !data.isShowCompany;
			var styleStr;
			if(isShowCompany){
				styleStr = "display:block";
			}else{
				styleStr = "display:none";
			}
			for(var i=0;i<=6;i++){
				$("#company_show_"+i).attr("style",styleStr);
			}
	    },
	    error: function(data) {
			console.log(data);
	    }
	});
}

function changeSelectByAge(idx){

	var yy = $("#insured_"+idx+"_yy").val();
	var mm = $("#insured_"+idx+"_mm").val();
	var dd = $("#insured_"+idx+"_dd").val();
	
	var today = new Date();
	if(today.getFullYear() - 1911 < parseInt(yy)
		|| (today.getFullYear() - 1911 == parseInt(yy) && today.getMonth() + 1 < parseInt(mm))
		|| (today.getFullYear() - 1911 == parseInt(yy) && today.getMonth() + 1 == parseInt(mm) && today.getDate() < parseInt(dd))){
		$("#insured_"+idx+"_dd").val("");
		alertMsg("被保人出生日期錯誤,請重新確認");
		return;
	}
	
	var birthday = (parseInt(yy)+1911)+"-"+mm+"-"+dd;
	var age = getAge(birthday);
	
	var insureWay = $("#insureWay").val();
	var selectValue = $("#group_amount_21_p_"+idx).val();

	if(!insureWay || !selectValue || !age){
		return;
	}
	
	var url = "../EBPT_1300_T3/getAgeMaxAmount";
	$.ajax({
	    url: url,
	    type: 'POST',
		data: {
			"age" : age,
			"insureWay" : insureWay,
			"selectValue" : selectValue
		},
	    success: function(data){ 
	    	if(!data.result){
				$("#group_amount_21_p_"+idx).val('');
				$("#group_amount_23_label_p_" + idx).html('&nbsp;');
				alertMsg("您的年齡無法投保此保額(意外事故身故殘廢保險金)，請重新選擇");
			}
	    },
	    error: function(data) {
			console.log(data);
	    }
	});
}

function getAge(birthday) {

	var EndDate = new Date(start_date.split(' ')[0]);

	var year = EndDate.getFullYear();
	var month = EndDate.getMonth() + 1;
	var day = EndDate.getDate();

	var BirthDate = new Date(birthday);
	var b_year = BirthDate.getFullYear();
	var b_month = BirthDate.getMonth() + 1;
	var b_day = BirthDate.getDate();

	var diff_year = EndDate.getFullYear() - b_year;
	if (month > b_month) {
		// 已滿
	} else if (month == b_month) {
		if (day >= b_day) {
			// 已滿
		} else {
			// 未滿
			diff_year--;
		}
	} else {
		// 未滿
		diff_year--;
	}
	return diff_year;

}

function changeRelativeToChief(id){
	// 若為其他則打開輸入框
	if($("#relative_to_chief_" + id).val() == "9"){
		$("#relative_to_chief_other_" + id).show();
	}else{
		$("#relative_to_chief_other_" + id).hide();
	}
}

/**
 * check the value is Number and between min and max
 */
function validateNum(v) {
	var filter  = /^(-?)((\d+(\.\d*)?)|((\d*\.)?\d+))$/;
	if (filter.test(v)) {
		return true;
	}else{
		return false;
	}
}