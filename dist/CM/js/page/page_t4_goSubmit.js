var orgSec15 = 15;
var sec15 = orgSec15;
var key = "key";
var myTimer = null;
function switchIndicate(s) {
	switch (s) {
	case true:
		$("#indicate").css("display", "block");
		$("#timer_info").css("display", "block");
		clearInterval(myTimer)
		myTimer = setInterval(function() {
			count15()
		}, 1000);
		break;
	case false:
		clearInterval(myTimer)
		$("#indicate").css("display", "none");
		$("#timer_info").css("display", "none");
		break;
	}
}
function count15() {
	sec15--;
	$("#timer_info").html("試算時間約" + sec15 + "秒，請耐心等候");

}

function goSubmit() {
	var key = $("#key").val();

	sec15 = orgSec15;
	count15();
	switchIndicate(true);

	ipCheck()

}
function ipCheck() {
	// 先取得各項旅遊保額限制
	var url = "../EBP0_Z001_UCBean/ipCheck"

	$.ajax({
		url : url,
		type : 'GET',
		success : function(data) {
			if (data.msg.indexOf("true") >= 0) {
				var numberOfPeople = $("#numberOfPeople").val();
				if (numberOfPeople == 1) {
					EffectStatus_check()
				} else {
					//switchIndicate(false);
					//alertMsg("團題, 往團體送")
					group_temp();
				}

			} else {
				switchIndicate(false);
				alertMsg("很抱歉！投保時您必須身在台、澎、金、馬地區，才可以投保本保險。")
			}
		},
		error : function(data) {
			// alertMsg('woops!'); //or whatever
			console.log("woops!")
			EffectStatus_check()
		}
	});

}
function group_temp(){
	checkOkAndSubmit();
}
function EffectStatus_check() {
	var u_id = $("#insured_u_id").val();
	var start_date = $("#start_date").val();
	var end_date = $("#end_date").val();
	var url = "../EBP0_Z000/getEffectStatus"
	url += "?CUSTOMER_ID=" + u_id;
	url += "&ORDER_TIME=" + start_date;
	url += "&e=e";
	// url = url.replace(/\//g, "-")
	$.get(url, function(data, status) {

		/*
		 * //res="回應:
		 * {"ORDER_ID":"A123456789","RETURN_MSG":"","ORDER_STATUS":"Y"}";
		 */
		var ORDER_STATUS = data.msg;

		$("#isCusEffect").val(ORDER_STATUS)
		var birthday = $("#insured_birthday").val();// +" 00:00:00";
		age = getAge(birthday, end_date);
		get_TravelAmountLimit(ORDER_STATUS, age);

	});

}
function getAge(birthday, EndDateStr) {
	var a=EndDateStr.split(" ");
	var d=a[0].split("-");
	var t=a[1].split(":");
	var EndDate = new Date(d[0],(d[1]-1),d[2],t[0],t[1],t[2]);
	
	var year = EndDate.getFullYear();
	var month = EndDate.getMonth() + 1;
	var day = EndDate.getDate();

	var aa=birthday.split(" ");
	var dd=aa[0].replace(/\//g,'-').split("-");
	var BirthDate = new Date(dd[0],(dd[1]-1),dd[2]);
	
	var b_year = BirthDate.getFullYear();
	var b_month = BirthDate.getMonth() + 1;
	var b_day = BirthDate.getDate();
	var diff_year = EndDate.getFullYear() - b_year;
	if (month > b_month) {
		// 已滿
	} else if (month == b_month) {
		if (day >= b_day) {
			// 已滿
		} else {
			// 未滿
			diff_year--;
		}
	} else {
		// 未滿
		diff_year--;
	}
	return diff_year;

}
function get_TravelAmountLimit(EffectStatus, age) {
	var OTP = $("#insureWay").val() == "WEB" ? "Y" : "N";
	var ISGROUP = $("#numberOfPeople").val() == "1" ? "N" : "Y";

	var url = "../EBP0_Z000/getTravelAmountLimit"
	url += "?OTP=" + OTP;
	url += "&ISGROUP=" + ISGROUP;
	url += "&EffectStatus=" + EffectStatus;
	url += "&age=" + age;

	$.get(url, function(data, status) {
		var obj = JSON.parse(data.msg);
		/*
		 * 回傳
		 * {"Bobe_MAX":"10000000","Business_MAX":"20000000","DEATH_MAX":"6000000","DEATH_MIN":"2000000","R6_MAX":"20000000","DEATH_STEP":"1000000","OTP_MAX":"6000000"}
		 */
		$("#Bobe_MAX").val(obj.Bobe_MAX)
		$("#Business_MAX").val(obj.Business_MAX)
		$("#DEATH_MAX").val(obj.DEATH_MAX)
		$("#DEATH_MIN").val(obj.DEATH_MIN)
		$("#R6_MAX").val(obj.R6_MAX)
		$("#DEATH_STEP").val(obj.DEATH_STEP)
		$("#OTP_MAX").val(obj.OTP_MAX)
		var u_id = $("#insured_u_id").val();
		var start_date = $("#start_date").val();
		var end_date = $("#end_date").val();
		ifHasBobePloicy_check(u_id, start_date, end_date)
	});
}

function ifHasBobePloicy_check(u_id, start_date, end_date) {
	// Bobe內部通算: 有保過就不行
	var url = "../EBP0_Z000/getIfHasBobePolicy"
	var para = "?CUSTOMER_ID=" + u_id;
	para += "&START_DATETIME=" + start_date;
	para += "&END_DATETIME=" + end_date;
	para += "&e=e";
	para = para.replace(/\//g, "-")
	url += para;
	$.get(url, function(data, status) {

		if (data.msg.indexOf("0000") >= 0) {

			var amount_21 = $("#amount_21").val() * 1;
			var Bobe_MAX = $("#Bobe_MAX").val() * 1;
			if (amount_21 <= Bobe_MAX) {
				InsuredAmount_check(u_id, start_date, end_date);
				$("#hasBobePolicy").val("其間內不曾在BOBE投保,保額在限制內")
			} else {
				switchIndicate(false);
				$("#hasBobePolicy").val("其間內不曾在BOBE投保, 保額超出!!")
				alertMsg("保額:" + amount_21 + " 超過 " + Bobe_MAX)
			}

		} else {
			$("#hasBobePolicy").val("其間內已經在BOBE投保")
			msg = "親愛的顧客您好：\n";
			msg += "被保人 " + u_id;
			msg += " 已在本網站投保旅遊險，訂單無法重複！\n";
			msg += "\n";
			msg += "本交易無法完成。如有任何問題，請洽保戶服務中心 0800-212-880，謝謝您的惠顧！\n";
			msg += "服務時間：每日上午 8:30 至晚上 9:00 \n";

			switchIndicate(false);
			alertMsg(msg)
		}

	});

}

function InsuredAmount_check(u_id, start_date, end_date) {
	// 內部通算
	var url = "../EBP0_Z000/getInsuredAmount"
	var para = "?CUSTOMER_ID=" + u_id;
	para += "&START_DATETIME=" + start_date;
	para += "&END_DATETIME=" + end_date;
	para += "&e=e";
	para = para.replace(/\//g, "-")
	url += para;
	// url="
	// http://10.176.18.213/INSABWeb/servlet/HttpDispatcher/ABZ7_0100/GetInsuredAmount?CUSTOMER_ID=A123456789&START_DATETIME=2010-05-01
	// 00:00:00&END_DATETIME=2010-05-31 00:00:00";
	$
			.get(
					url,
					function(data, status) {
						// {"RMBSMNT":"0.00","DEATH_DISABLER_INSURED":"0.00","SUDDENLY_CMPLNT":"0.00","TRAVEL_CONVNTE":"0"}
						var obj = JSON.parse(data.msg);
						var msg = "";
						$("#DEATH_DISABLER_INSURED").val(
								obj.DEATH_DISABLER_INSURED)
						if (obj.TRAVEL_CONVNTE > 0) {
							msg = "親愛的顧客您好：\n";
							msg += "您已在本公司投保旅遊不便險，無法重複投保！\n";
							msg += "\n";
							msg += "本交易無法完成。如有任何問題，請洽保戶服務中心 0800-212-880，謝謝您的惠顧！\n";
							msg += "服務時間：每日上午 8:30 至晚上 9:00 \n";
							alertMsg(msg)
							switchIndicate(false);
						} else {
							// alertMsg("insureWay " + $("#insureWay").val());
							// alertMsg("obj.TRAVEL_CONVNTE="+obj.TRAVEL_CONVNTE)
							/*
							 * alertMsg("已經投保金額:" +
							 * parseInt(obj.DEATH_DISABLER_INSURED) + "
							 * (DEATH_DISABLER_INSURED)")
							 */
							if ($("#insureWay").val() == "WEB") {

								var amount_21 = $("#amount_21").val() * 1;
								var DEATH_DISABLER_INSURED = $(
										"#DEATH_DISABLER_INSURED").val() * 1;
								// alertMsg(DEATH_DISABLER_INSURED)
								var R6_MAX = $("#R6_MAX").val() * 1;
								// alertMsg((amount_21+DEATH_DISABLER_INSURED)+" >
								// "+R6_MAX)
								if ((amount_21 + DEATH_DISABLER_INSURED) <= R6_MAX) {
									TravelUnion_check(u_id, start_date,
											end_date);
									$("#DEATH_DISABLER_INSURED").val(
											$("#DEATH_DISABLER_INSURED").val()
													+ " , 未超額 ")
								} else {
									switchIndicate(false);
									$("#DEATH_DISABLER_INSURED").val(
											$("#DEATH_DISABLER_INSURED").val()
													+ " , 超額")
									alertMsg("保額:" + amount_21 + " 超過R6_MAX: "
											+ R6_MAX)
								}

							} else {
								//alertMsg("fax")
								checkOkAndSubmit()
								return;
								//switchIndicate(false);
							}

							// EffectStatus_check();
							// switchIndicate(false);
						}

					});

}

function TravelUnion_check(u_id, start_date, end_date) {
	var url = "../EBP0_Z000/getDataFromTravelUnion";
	// alertMsg(start_date+" , "+ end_date)
	/*
	 * api_getDataFromTravelUnion.jsp ?customerIds=A121926863
	 * &customerIds=A123456789 &sourceFrom=02 &startDate=20160301
	 * &endDate=20160501 &queryType=Normal
	 */
	var para = "?customerIds=" + u_id;
	para += "&sourceFrom=01";
	para += "&startDate=" + start_date;
	para += "&endDate=" + end_date;
	para += "&queryType=Web";
	para = para.replace(/\//g, "");
	url += para;
	$
			.get(
					url,
					function(data, status) {
						// alertMsg(data.msg)
						xmlDoc = $.parseXML(data.msg);
						$xml = $(xmlDoc);

						// alertMsg($xml.find("processStatus").text())
						if ($xml.find("processStatus").text() != "SUCCESS") {
							switchIndicate(false)
							alertMsg("很抱歉，目前連線公會通算系統無回應，請稍後再重新投保。")
							return;
						}
						var referenceId = $xml.find("referenceId").text();
						// alertMsg("referenceId / "+referenceId)

						$xml
								.find("result")
								.each(
										function(i) { // 取得xml父節點
											if (i == 0) {
												return;
											}
											// <RestfulResult>
											// <rc>0000</rc>
											// <rm>����</rm>
											// <data>
											// <processStatus>SUCCESS</processStatus>
											// <referenceId>107049</referenceId>
											// <result>
											// <result>
											// <insuredId>S121926863</insuredId>
											// <premiumTotalAmount>0</premiumTotalAmount><insurerCount>0</insurerCount><webTotalAmount>0</webTotalAmount></result></result></data></RestfulResult
											// >

											var insuredId = $(this).children(
													"insuredId").text(); //
											var premiumTotalAmount = $(this)
													.children(
															"premiumTotalAmount")
													.text(); //
											//alertMsg("暫時將premiumTotalAmount 設為 0")
											//premiumTotalAmount=0;
											var insured_count = $(this)
													.children("insurerCount")
													.text(); //
											var webTotalAmount = $(this)
													.children("webTotalAmount")
													.text(); //
											$("#refqueryid").val(referenceId);
											$("#insuredId").val(insuredId);
											$("#insured_count").val(
													insured_count);
											$("#premiumTotalAmount").val(
													premiumTotalAmount);
											$("#webTotalAmount").val(
													webTotalAmount);
											// alertMsg(i+" insured_count:
											// "+insured_count)
											
											//TODO//這段要刪除 start
											if(insured_count >= 2){
//												alertMsg("已投保多家保險業者之旅遊險,為了測試,暫時放行");
												insured_count=1;
												$("#premiumTotalAmount").val(0)
												$("#webTotalAmount").val(0)
											}
//											alertMsg("略過已投保多家保險業者之旅遊險,為了測試,暫時放行")
												//這段要刪除 end
											if (insured_count < 2) {
												// 在這裡算出
												/*
												 * DEATH_MAX(單筆交易最高額度) 2.
												 * Bobe_MAX-站內累積額度 3. R6_MAX -
												 * DEATH_DISABLER_INSURED(死殘保額保額)
												 * 4. Business_MAX -
												 * premiumTotalAmount 5. OTP_MAX -
												 * webTotalAmount
												 */

												var DEATH_MAX = $("#DEATH_MAX")
														.val() * 1;
												var amount_21 = $("#amount_21")
														.val() * 1;

												var Business_MAX = $(
														"#Business_MAX").val() * 1;
												var premiumTotalAmount = $(
														"#premiumTotalAmount")
														.val() * 1;

												var OTP_MAX = $("#OTP_MAX")
														.val() * 1;
												var webTotalAmount = $(
														"#webTotalAmount")
														.val() * 1;

												if ((amount_21) > DEATH_MAX) {
													alertMsg("保額:" + amount_21
															+ " 超過 "
															+ DEATH_MAX)
													$("#MIN")
															.val(
																	amount_21
																			+ "  超額 ")
												} else if ((amount_21 + premiumTotalAmount) > Business_MAX) {
													alertMsg("保額:"
															+ (amount_21 + premiumTotalAmount)
															+ " 超過 "
															+ Business_MAX)
													$("#MIN")
															.val(
																	(amount_21 + premiumTotalAmount)
																			+ "  超額 ")
												} else if ((amount_21 + webTotalAmount) > OTP_MAX) {
													alertMsg("保額:"
															+ (amount_21 + webTotalAmount)
															+ " 超過 " + OTP_MAX)
													$("#MIN")
															.val(
																	(amount_21 + webTotalAmount)
																			+ "  超額 ")
												} else {
													switchIndicate(false);
													$("#MIN").val("審核通過")
													// alertMsg("保額:" +amount_21+"
													// 超過 "+Bobe_MAX)
													checkOkAndSubmit();
												}

											} else {
												var msg = "";
												msg += "親愛的顧客您好：\n";
												msg += "您已投保多家保險業者之旅遊險，無法繼續投保！\n";
												msg += "\n";
												msg += "本交易無法完成。如有任何問題，請洽保戶服務中心 0800-212-880，謝謝您的惠顧！\n";
												msg += "服務時間：每日上午 8:30 至晚上 9:00";
												msg += "";
												alertMsg(msg);
											}

										});
						//
						//
						//

						switchIndicate(false);
					});

	/*
	 * 回傳
	 * {RMBSMNT=0.00,DEATH_DISABLER_INSURED=0.00,SUDDENLY_CMPLNT=0.00,TRAVEL_CONVNTE=0}
	 * 
	 * <RestfulResult> <rc>0000</rc> <rm>����</rm><data>
	 * <processStatus>SUCCESS</processStatus> <referenceId>86128</referenceId>
	 * <result><result><insuredId>A123456789</insuredId>
	 * <premiumTotalAmount>28000000</premiumTotalAmount><insurerCount>3</insurerCount><webTotalAmount>0</webTotalAmount></result></result></data></RestfulResult>
	 * 
	 */

}
function checkOkAndSubmit() {

	switchIndicate(false);
	//2016/11/15
	var settleNo = document.getElementsByName("settleNo")[0];
	/*var url = "../EBPT_1400_T4/insertRefQueryId?key=" + settleNo.value+"&refqueryid="+$("#refqueryid").val();
	$.get(url, function(data, status) {
		if (data.isOk == true) {
			var insureway=$("#insureway").val();
			$("#insureway").val(insureway.toUpperCase());
			$("#form1").prop("action", "../EBPT_1400_T4/goSendData");
			$("#form1").submit();
		}
	});*/
	var insureway=$("#insureWay").val();
	$("#insureWay").val(insureway.toUpperCase());
	$("#form1").prop("action", "../EBPT_1400_T4/goSendData");
	$("#form1").submit();
}
function goCard() {
	
}