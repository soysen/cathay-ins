function checkCurrentKey(key) {
	var url = "../EBP0_Z000/checkCurrentKey?key=" + key + "&pollingTime=6";
	
	$.ajax({
		url: url,
	    type: 'GET',
	    cache: false,
	    success: function(data){
	    	if (data.msg.indexOf("diff") >= 0) {
				var msg = "親愛的顧客您好，\n";
				msg += "為確保您的資料安全，此頁面已閒置過久，我們須自動登出您的帳號。\n";
				msg += "如欲再使用，請您重新登入。";
				
				alert(msg)
				location.replace('../ECIE_1000/prompt')
			} else {
				checkCurrentKey(key);
			}
	    }
	});
}
