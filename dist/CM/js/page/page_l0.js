function login(requestURI) {

	// mem_id=&mem_pass=
	// alert($("#form1").serialize());
	var u_id = $("#mem_id").val().toUpperCase();

	// 內部通算
	//var url = "api_login_fast.jsp"
	//var url = "login_fast"
	var url = "/INSEBWeb/servlet/HttpDispatcher/EBPM_1000_M/login_fast";
	url += "?u_id=" + u_id;
	url += "&password=" + $("#mem_pass").val();
	 alert("requestURI="+requestURI)
	$.get(url, function(data, status) {
		// alert(data)
		var obj = JSON.parse(data.login);

		if (obj.returnCode == "0000") {
			//alert("oooo!---"+requestURI)
			location.replace(requestURI)
			hasLogin = true;
		} else {
			alert("密碼錯誤!")

		}

	});
}
// 身份字號檢驗

function check_ID(u_id) {
	var obj = new Object();
	obj.isId = true;
	obj.msg = "";
	var cx = new Array;
	cx[0] = 1;
	cx[1] = 9;
	cx[2] = 8;
	cx[3] = 7;
	cx[4] = 6;
	cx[5] = 5;
	cx[6] = 4;
	cx[7] = 3;
	cx[8] = 2;
	cx[9] = 1;

	var charArray = new Array;

	// A=10 B=11 C=12 D=13 E=14 F=15 G=16 H=17 J=18 K=19 L=20 M=21 N=22
	// P=23 Q=24 R=25 S=26 T=27 U=28 V=29 W=32 X=30 Y=31 Z=33 I=34 O=35

	charArray["A"] = 10
	charArray["B"] = 11
	charArray["C"] = 12
	charArray["D"] = 13
	charArray["E"] = 14
	charArray["F"] = 15
	charArray["G"] = 16
	charArray["H"] = 17

	charArray["J"] = 18
	charArray["K"] = 19
	charArray["L"] = 20
	charArray["M"] = 21
	charArray["N"] = 22

	charArray["P"] = 23
	charArray["Q"] = 24
	charArray["R"] = 25
	charArray["S"] = 26
	charArray["T"] = 27
	charArray["U"] = 28
	charArray["V"] = 29
	charArray["W"] = 32
	charArray["X"] = 30
	charArray["Y"] = 31
	charArray["Z"] = 33

	charArray["I"] = 34
	charArray["O"] = 35

	var total = 0;

	if (u_id.length != 10) {

		obj.isId = false;
		obj.msg = "身分證字號錯誤：要有 10 碼";
		return obj;
	}

	// if ((u_id.charCodeAt(0) >= 97 && u_id.charCodeAt(0) <= 122)) {
	// // 第一碼自動小寫囀大寫
	// u_id = String.fromCharCode(u_id.charCodeAt(0) - 32) + u_id.substring(1)
	// $("#u_id").val(u_id)
	// }

	if (!(u_id.charCodeAt(1) == 49 || u_id.charCodeAt(1) == 50)) {

		obj.isId = false;
		obj.msg = "身分證字號錯誤：性別錯誤";
		return obj;
	}
	if (!((u_id.charCodeAt(0) >= 65 && u_id.charCodeAt(0) <= 97))) {

		obj.isId = false;
		obj.msg = "身分證字號錯誤，第一碼必須是A-Z";
		return obj;
	}

	for (i = 1; i <= 10; i++) {
		if (u_id.charCodeAt(i) < 48 || u_id.charCodeAt(i) > 57) {
			obj.isId = false;
			obj.msg = "身分證字號錯誤，後九碼必須是 0-9 數字組合";
			return obj;
		}

	}
	new_uid = charArray[u_id.substr(0, 1)] + u_id.substring(1)
	var cnum = new_uid.split("");
	for (i = 0; i < 10; i++) {

		total += cnum[i] * cx[i];
	}

	// alert(total)
	var checkNum = total % 10;
	if (checkNum == 0) {

	} else {
		checkNum = 10 - checkNum;
	}

	if (checkNum == cnum[10]) {
		obj.isId = true;
		obj.msg = "身分證字號：" + u_id + " 正確!";
		return obj;
	} else {
		// alert("統一編號："+u_id+" 錯誤!");

		obj.isId = false;
		obj.msg = "身分證字號錯誤!";
		return obj;
	}
}