var countryArrayStr = $("#countryArrayStr").val();
var isLoadCountries = false;
var hasLogin = false;

var country_max = 5;
var selectObj = {
	maximumSelectionLength : country_max,
}
$("#travel_country").select2(selectObj).on("change", function(e) {
	// mostly used event, fired to the original element when the value
	// changes
	var data = $("#form1").serialize();
	console.log("travel_country change...." + data)
	select_travel_country(data);

})

initSelectCountry();

$(function() {
	if ($("#amount_21").val() > 0) {
		$("#amount_23_label").html(moneyWanLabel($("#amount_21").val()))
	}
});

// 選擇旅遊國家

function change_product_list() {
	console.log("...change_product_list...");
}
function changeAmount(amount_type) {
	// alert(travel_type+" / "+insureWay)
	var rate = $("#rate").val();
	var travel_type = $("#travel_type").val();
	var insureWay = $("#insureWay").val();
	if (travel_type == "A" && insureWay == "web") {
		// 國外, 網保
		amount_21 = $("#amount_21").val();
		amount_22 = $("#amount_22").val();
		amount_61 = $("#amount_61").val();

	} else if (travel_type == "A" && insureWay == "fax") {
		// 國外, 傳真要保
		amount_21 = $("#amount_21").val();
		amount_22 = $("#amount_22").val();
		amount_61 = $("#amount_61").val();
	} else if (travel_type == "I" && insureWay == "web") {
		// 國內, 網保
		amount_21 = $("#amount_21").val();
		amount_22 = $("#amount_22").val();
		amount_61 = $("#amount_61").val();
	} else if (travel_type == "I" && insureWay == "fax") {
		// 國內, 傳真要保
		amount_21 = $("#amount_21").val();
		amount_22 = $("#amount_22").val();
		amount_61 = $("#amount_61").val();
	}

	var PRODUCT_POLICY_CODE = $("#PRODUCT_POLICY_CODE").val();// travel_type
	// == "A" ?
	// "PC01002" :
	// "PC01001";
	var travel_days = $("#travel_days").val();// ;
	// alert(travel_days)

	/*
	 * console.log(amount_21 + " / " + amount_22 + " / " + amount_61 + " / ");
	 */
	// 這裡要考慮申根
	var isSchengen = $("#isSchengen").val();
	var limit
	limit = amount_21 * rate;
	// alert("rate=" + rate + " /limit= " + limit + " / 21=" + amount_21
	// + " / 22= " + amount_22 + " /61= " + amount_61)
	if (amount_type == 21) {
		$("#amount_23").val(amount_21)
		$("#amount_23_label").html(moneyWanLabel(amount_21))
		amount_23 = amount_21;
		if (isSchengen == "true") {
			var the_min = 99999999;
			// 自動填入最小值
			if ($("#amount_22").val() == 0) {
				the_min = 99999999;
				$("#amount_22").find("option").each(function(i) {
					if ($(this).val() != 0 && $(this).val() < the_min) {
						the_min = $(this).val()
					}
				})
				$("#amount_22").val(the_min);
			}

			if ($("#amount_61").val() == 0) {
				the_min = 99999999;
				$("#amount_61").find("option").each(function(i) {
					if ($(this).val() != 0 && $(this).val() < the_min) {
						the_min = $(this).val()
					}
				})
				$("#amount_61").val(the_min);
			}

		} else {
			if (amount_22 == 0 || amount_22 > limit) {
				$("#amount_22").val(limit)
				amount_22 = limit;
			}

			if (amount_61 == 0 || amount_61 > limit) {
				$("#amount_61").val(limit)
				amount_61 = limit;
			}
		}

	} else {

	}

	if (isSchengen == "false") {
		$("#amount_22").find("option").each(function(i) {

			if ($(this).val() > limit) {
				$(this).attr("disabled", "disabled");
				$(this).prop("style", "color: gray;");
				/* console.log("22:" + i + " : " + $(this).val()) */
			} else {
				$(this).prop("disabled", "");
				$(this).prop("style", "color: black;");
			}
		})
		$("#amount_61").find("option").each(function(i) {

			if ($(this).val() > limit) {
				$(this).prop("disabled", "disabled");
				$(this).prop("style", "color: gray;");
				/* console.log("61:" + i + " : " + $(this).val()) */
			} else {
				$(this).prop("disabled", "");
				$(this).prop("style", "color: black;");
			}
		})
	}

	// var url = "api_getTravelPremium.jsp";
	// url += "?from=web";
	// url += "&PRODUCT_POLICY_CODE=" + PRODUCT_POLICY_CODE;
	// url += "&travel_days=" + travel_days;
	// url += "&amount_21=" + amount_21;
	// url += "&amount_22=" + amount_22;
	// if (PRODUCT_POLICY_CODE == "PC01002") {
	// url += "&amount_61=" + amount_61;
	// }

	//
	// $.get(url, function(data, status) {
	// //alert(data)
	// var arr=data.split("__");
	//		
	// $("#p4_premiumTd").html(arr[0])
	// $("#totalPremium_4").val(arr[0])
	// $("#combo_4").val(arr[1])
	// $("#insname4").prop("checked", "checked")
	//
	// });
	getPremium();
}

function getPremium() {
	// 私有
	var insureWay = $("#insureWay").val();//
	var travel_type = $("#travel_type").val();//
	var PRODUCT_POLICY_CODE = $("#PRODUCT_POLICY_CODE").val();// "PC01001";
	// var PRODUCT_POLICY_CODE = travel_type == "A" ? "PC01002" : "PC01001";
	var travel_days = $("#travel_days").val();// 1;
	var amount_21 = $("#amount_21").val();// 2000000;
	var amount_22 = $("#amount_22").val();// 100000;
	var amount_23 = $("#amount_23").val();// 2000000;
	var amount_61 = $("#amount_61").val();// 100000;
	// alert(amount_21 + " , " + amount_22 + " , " + amount_23 + " , " +
	// amount_61)
	if (amount_21 > 0) {
		getPremiumApi(insureWay, PRODUCT_POLICY_CODE, travel_days, amount_21,
				amount_22, amount_23, amount_61)
	} else {
		console.log("參數有缺,未執行 ");
	}

}
function backPremium_from_getPremiumApi(data) {
	// 私有
	var arr = data.split("__");
	//	
	// var numberOfPeople = $('#numberOfPeople').val();
	var premium = arr[0];// * numberOfPeople

	$("#premium").val(premium)
	$("#premiumLabel").val(arr[3])
	// var showStr = arr[0] * $("#premium").val(premium)
	// $("#totalPremium_4").val(arr[0])
	$("#combo").val(arr[1])
	// $("#insname4").prop("checked", "checked")
	$("#premium_combo").val(arr[4])
	// console.log("--backPremium_from_getPremiumApi--");
	checkTravelDate(arr[2]);
	$("#mySubmit").prop("disabled", false)
	// fill_all_item_premium(arr[4]);
}
// function fill_all_item_premium(premium_combo){
// //alert(premium_combo)
// premium_combo=premium_combo.replace(/\)\(/g,",");
// premium_combo=premium_combo.replace(")","");
// premium_combo=premium_combo.replace("(","");
//	
// //alert(premium_combo)
//	
// var arr=premium_combo.split(",")
// //alert(arr)
// for(var i in arr){
// if(arr[i].indexOf("21:")>=0){
// //alert(arr[i].split(":")[1])
// $("#premium_21").val(arr[i].split(":")[1]);
// }
// if(arr[i].indexOf("22:")>=0){
// //alert(arr[i].split(":")[1])
// $("#premium_22").val(arr[i].split(":")[1]);
// }
// if(arr[i].indexOf("23:")>=0){
// //alert(arr[i].split(":")[1])
// $("#premium_23").val(arr[i].split(":")[1]);
// }
// if(arr[i].indexOf("61:")>=0){
// //alert(arr[i].split(":")[1])
// $("#premium_61").val(arr[i].split(":")[1]);
// }
//		
// }
//	
// }
var isNeedLock = false;
function lockSubmit(_isNeedLock, msg) {
	// alert("_isNeedLock="+_isNeedLock)
	// 私有
	isNeedLock = _isNeedLock
	$("#mySubmit").prop("disabled", isNeedLock)
	// if(isNeedLock==true){
	// $("#custom").prop("style", "cursor: not-allowed")
	// console.log("自選-上鎖");
	// }else{
	// $("#custom").prop("style", "cursor: pointer")
	// console.log("自選-解鎖");
	// }

	if (isNeedLock == true) {
		if (msg != null) {
			alert(msg)
		}

	}
}
function pickAmount_xxx(amount_type, rate) {
	/*
	 * alert("hi " + $("#p4_21").val() + " / " + $("#p4_22").val() + " / " +
	 * $("#p4_61").val()); ;
	 */

	var PRODUCT_POLICY_CODE = "<%=PRODUCT_POLICY_CODE%>";
	var travel_days = $("#travel_days").val();// ;
	// alert(travel_days)

	var amount_21 = 0;
	var amount_22 = 0;
	var amount_61 = 0
	amount_21 = $("#p4_21").val();// ;
	amount_22 = $("#p4_22").val();// ;
	amount_61 = $("#p4_61").val();

	/*
	 * console.log(amount_21 + " / " + amount_22 + " / " + amount_61 + " / ");
	 */
	// 這裡要考慮申根
	var limit
	limit = amount_21 * rate;

	if (amount_type == 21) {

		if (amount_22 > limit) {
			$("#p4_22").val(limit)
			amount_22 = limit;
		}

		if (amount_61 > limit) {
			$("#p4_61").val(limit)
			amount_61 = limit;
		}
	} else {

	}
	$("#p4_22").find("option").each(function(i) {

		if ($(this).val() > limit) {
			$(this).attr("disabled", "disabled");
			$(this).prop("style", "color: gray;");
			/* console.log("22:" + i + " : " + $(this).val()) */
		} else {
			$(this).prop("disabled", "");
			$(this).prop("style", "color: black;");
		}
	})
	$("#p4_61").find("option").each(function(i) {

		if ($(this).val() > limit) {
			$(this).prop("disabled", "disabled");
			$(this).prop("style", "color: gray;");
			/* console.log("61:" + i + " : " + $(this).val()) */
		} else {
			$(this).prop("disabled", "");
			$(this).prop("style", "color: black;");
		}
	})
	var url = "api_getTravelPremium.jsp";
	url += "?from=web";
	url += "&PRODUCT_POLICY_CODE=" + PRODUCT_POLICY_CODE;
	url += "&travel_days=" + travel_days;
	url += "&amount_21=" + amount_21;
	url += "&amount_22=" + amount_22;
	if (PRODUCT_POLICY_CODE == "PC01002") {
		url += "&amount_61=" + amount_61;
	}

	$.get(url, function(data, status) {
		// alert(data)
		var arr = data.split("__");

		$("#p4_premiumTd").html(arr[0])
		$("#totalPremium_4").val(arr[0])
		$("#combo_4").val(arr[1])
		$("#insname4").prop("checked", "checked")

	});
}

function checkIfSelect() {
	// alert($("#countryArrayStr").val())
	if ($("#countryArrayStr").val() == "") {
		return false
	} else {
		return true;
	}
}