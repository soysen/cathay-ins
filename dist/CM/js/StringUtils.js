String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, "");
}

String.prototype.isNumber = function() {
    return /^(-?)((\d+(\.\d*)?)|((\d*\.)?\d+))$/.test(this);
}

String.prototype.isBlank = function() {
    return (this == null) || (this.length == 0) || /^\s+$/.test(this);
}

String.prototype.byteLen = function() {
    return this.replace(/[^\x00-\xff]/g, "rr").length;
}

function substring(str, start, length) {
    return str.length > length ? str.substring(start, start + length) : str;
}