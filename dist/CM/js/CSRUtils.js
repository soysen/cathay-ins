/*
	Usage :
			1. 1 + 2 +3 + 4 + 5
			new Decimal(1).add(2,3,4,5).getValue(); // 15

			到小數點第二位 四捨五入
			2.  521.6 + 13.45 + 16.853
			new Decimal(521.6).add(13.45,16.853).setScale(2).getValue() ;// 551.9

			到小數點第二位 四捨五入
			3. 15.893
			new Decimal(15.893).setScale(2).getValue() // 15.89

			到小數點第二位 無條件捨去
			4.  521.6 + 13.45 + 16.853
			new Decimal(521.6).add(13.45,16.853).setScale(2,'down').getValue() ;// 551.9

			到小數點第二位 無條件進入
			5. new Decimal(19.363).setScale(2,'up').getValue(); // 19.37
*/
var Decimal = function(val) {

	this.total = parseFloat(val);
	this.dp = new Array();

	initDp = function() {
		dp = 0;	
	}
	
	setMaxDp = function(_dp) {
		dp = (parseInt(_dp) > dp ? _dp : dp);
	}
	
	getMaxDp = function() {
		return dp;	
	}
	
	argsToArray = function() {
		var arr = [];
		for(var i = 0; i < arguments.length; i++) {
			arr.push(arguments[i]);
		}
		return arr;
	}
	
	newNumArr = function(_arr) {
		var arr =[];
		initDp();
		
		// setMax dp
		for (var i = 0; i < _arr.length; i++) {
			var str = _arr[i].toString();
			if(str.indexOf('.') != -1) {
				setMaxDp(str.split('.')[1].length);
			}
		}
		
		for (var i = 0; i < _arr.length; i++) {
			var str = _arr[i].toString();
			if(str.indexOf('.') != -1 ){
				var len = getMaxDp() - str.split('.')[1].length;
				var temp = '';
				for(var j = 0; j < len; j++) {
					temp += '0';
				}
				str = parseFloat(str.replace('.', '')).toString();
				str = str.concat(temp);
			}else{
				var temp = '';
				for(var j = 0; j < getMaxDp(); j++) {
					temp +='0';
				}
				str = str.concat(temp);
			}
			// alert('str [' + str + ']');
			arr.push(parseFloat(str));
		}
		return arr;
	}
	
	sum = function(arr) {
		var temp = 0;
		for(var i = 0; i<arr.length; i++) {
			temp += arr[i];
		}
		return temp / Math.pow(10, getMaxDp());
	}
	
	innerMultiply = function(arr) {
		var temp = 1;
		for(var i = 0; i < arr.length; i++) {
			temp *= arr[i];
		}
		var base = Math.pow(10,getMaxDp());
		return temp / Math.pow(base, arr.length);
	}
	
	this.add = function() {
		var arr = argsToArray.apply(this, arguments);
		arr.push(this.total.toString());
		arr = newNumArr(arr);
		return new Decimal(sum(arr));
	}
	
	this.subtract = function() {
		var arr = argsToArray.apply(this, arguments);
		arr = newNumArr(arr);
		
		// sum of sutract number
		var _arr = newNumArr([this.total.toString(), sum(arr)]);
		return new Decimal((_arr[0] - _arr[1]) / Math.pow(10, getMaxDp()));
	}
	
	this.multiply = function(){
		var arr = argsToArray.apply(this, arguments);
		arr.push(this.total.toString());
		return new Decimal(innerMultiply(newNumArr(arr)));
	}
	
	this.divide = function() {
		var arr = argsToArray.apply(this, arguments);
		arr = newNumArr([this.total.toString(), innerMultiply(newNumArr(arr))]);
		return new Decimal(arr[0] / arr[1]);
	}
	
	this.decimalPoint = 0;
	this.roundingMode = 'half_up';

	this.setScale = function(delimalPoint, roundingMode) {
		this.decimalPoint = delimalPoint || this.decimalPoint;
		this.roundingMode = roundingMode || this.roundingMode;
		return this;
	}
	
	this.getValue = function(dp, rm) {
		this.setScale(dp, rm);
		var str = this.total.toString();
		var decimalLength = this.decimalPoint + 1;
		if (str.indexOf('.') != -1) {
			if (str.split('.')[1].length >= decimalLength) {
				str = str.split('.')[0] + '.' + str.split('.')[1].substring(0, decimalLength);
				// 123.23456 --> 123.234 
			}else{
				var times = decimalLength - str.split('.')[1].length;
				for (var i = 0; i < times; i++) {
					str = str.concat('0');
				}
				// 123.2    --> 123.200
			}
		}

		if ('half_up' == this.roundingMode) {
			if (str.indexOf('.') != -1) {
				var temp;
				if (this.total >= 0) {
					temp = ((parseFloat(str.replace('.','')) + 5) / Math.pow(10, decimalLength)).toString();
				} else {
					temp = ((parseFloat(str.replace('.','')) - 5) / Math.pow(10, decimalLength)).toString();
				}
				if (temp.indexOf('.') != -1) {
					if (temp.split('.')[1].length > this.decimalPoint) {
						str = temp.substring(0, temp.length - 1);	
					} else {
						str = temp.substring(0, temp.length);
					}	
				} else {
					str = temp;
				}
			}
			str = parseFloat(str);	
		} else if ('down' == this.roundingMode) {
			if(str.indexOf('.') != -1) {
				str = str.substring(0, str.length-1);
			}
			str = parseFloat(str);	
		} else if ('up' == this.roundingMode) {
			if (str.indexOf('.') != -1) {
				var temp;
				if (this.total >= 0) {
					temp = ((parseFloat(str.replace('.','')) + 10) / Math.pow(10, decimalLength)).toString();
				} else {
					temp = ((parseFloat(str.replace('.','')) - 10) / Math.pow(10, decimalLength)).toString();
				}
				if (temp.indexOf('.') != -1) {
					if (temp.split('.')[1].length > this.decimalPoint) {
						str = temp.substring(0, temp.length-1);	 
					} else {
						str = temp.substring(0, temp.length);		
					}
				} else {
					str = temp;	
				}
			}
			str = parseFloat(str);	
		}
		return str;
	}
}

var DateUtils = {};

DateUtils._delimiter = '';

/**
 * 將傳入的日期(dateStr)轉為民國日期 YYY/MM/DD, YY/MM/DD
 */
DateUtils.toROC = function(dateStr) {
	return DateUtils.toInputROC(dateStr, '/');
}

/**
 * 將傳入的日期(dateStr)轉為民國日期, 可指定年月日中間區隔符號(delimiter)
 */
DateUtils.toInputROC = function(dateStr, delimiter) {
	if (!dateStr) {
		return '';
	}
	if (!delimiter) {
		delimiter = '';
	}
	var dateObj = DateUtils.parseToDate(dateStr);
	var newYear = dateObj.getFullYear();
	var newMonth = dateObj.getMonth();
	var newDay = dateObj.getDate();
	return '' + (newYear - 1911) + delimiter + ((newMonth + 1) > 9 ? (newMonth + 1) : '0' + (newMonth + 1)) + delimiter + (newDay > 9 ? newDay : '0' + newDay);
}

/**
 * 取得西元今天日期, days 為加減天數, 可指定年月日中間區隔符號(delimiter), isROC 輸出是否轉為民國年
 */
DateUtils.today = function(days, delimiter, isROC) {
	var today = new Date();
	var year = today.getFullYear();
	var month = today.getMonth();
	var day = today.getDate();
	try {
		if (days && parseInt(days)) {
			day += days;
		}
		today = new Date(year, month, day);
		year = today.getFullYear();
		month = today.getMonth();
		day = today.getDate();
	} catch (e) {
	}
	var result = '';
	if (!delimiter) {
		delimiter = '';
	}
	if (isROC) {
		result = '' + (year - 1911) + delimiter + ((month + 1) > 9 ? (month + 1) : '0' + (month + 1)) + delimiter + (day > 9 ? day : '0' + day);
	} else {
		result = '' + year + delimiter + ((month + 1) > 9 ? (month + 1) : '0' + (month + 1)) + delimiter + (day > 9 ? day : '0' + day);
	}
	return result;
}

/**
 * 取得民國今天日期, days 為加減天數, 可指定年月日中間區隔符號(delimiter)
 */
DateUtils.todayROC = function(days, delimiter) {
	return DateUtils.today(days, delimiter, true);
}

/**
 * 將傳入的民國日期(dateStr)轉為西元日期 YYYY/MM/DD
 */
DateUtils.toY2K = function (dateStr) {
	return DateUtils.toInputY2K(dateStr, '-');
}

/**
 * 將傳入的民國日期(dateStr)轉為西元日期, 可指定年月日中間區隔符號(delimiter)
 */
DateUtils.toInputY2K = function(dateStr, delimiter) {
	if (!dateStr) {
		return '';
	}
	if (!delimiter) {
		delimiter = '';
	}
	var dateObj = DateUtils.parseToDate(dateStr);
	var newYear = dateObj.getFullYear();
	var newMonth = dateObj.getMonth();
	var newDay = dateObj.getDate();
	return '' + newYear + delimiter + ((newMonth + 1) > 9 ? (newMonth + 1) : '0' + (newMonth + 1)) + delimiter + (newDay > 9 ? newDay : '0' + newDay);
}

/**
 * 將傳入日期(dateStr), 進行加減運算, 西元或是民國年皆可
 */
DateUtils.add = function (dateStr, year, month, day, delimiter) {
	if (!dateStr) {
		return '';
	}
	try {
		var dateObj = DateUtils.parseToDate1(dateStr, year, month, day);
		if (!delimiter) {
			delimiter = DateUtils._delimiter;
		}
		var newYear = dateObj.getFullYear();
		var newMonth = dateObj.getMonth();
		var newDay = dateObj.getDate();
		return '' + newYear + delimiter + ((newMonth + 1) > 9 ? (newMonth + 1) : '0' + (newMonth + 1)) + delimiter + (newDay > 9 ? newDay : '0' + newDay);
	} catch (e) {
		var dateObj = DateUtils.parseToDate2(dateStr, year, month, day);
		if (!delimiter) {
			delimiter = DateUtils._delimiter;
		}
		var newYear = dateObj.getFullYear();
		var newMonth = dateObj.getMonth();
		var newDay = dateObj.getDate();
		return '' + (newYear - 1911) + delimiter + ((newMonth + 1) > 9 ? (newMonth + 1) : '0' + (newMonth + 1)) + delimiter + (newDay > 9 ? newDay : '0' + newDay);
	}
}

/**
 * 計算日期(dateStr2 - dateStr1)的差異天數, 如果 dateStr1 或是 dateStr2 未傳入, 預設為今天日期, dateStr1, dateStr2 西元日期或是民國日期皆可
 */
DateUtils.subtract = function(dateStr1, dateStr2) {
	if (!dateStr1) {
		dateStr1 = DateUtils.today(0, '-');
	}
	if (!dateStr2) {
		dateStr2 = DateUtils.today(0, '-');
	}
	var dateObj1 = DateUtils.parseToDate(dateStr1);
	var dateObj2 = DateUtils.parseToDate(dateStr2);
	return (dateObj2 - dateObj1) / 86400000;
}

/**
 * 比對兩日期是否相等[-1 : dateStr1 < dateStr2, 0 : dateStr1 = dateStr2, 1 : dateStr1 > dateStr2], 如果 dateStr1 或是 dateStr2 未傳入, 預設為今天日期, dateStr1, dateStr2 西元日期或是民國日期皆可
 */
DateUtils.compare = function(dateStr1, dateStr2) {
	if (!dateStr1) {
		dateStr1 = DateUtils.today(0, '-');
	}
	if (!dateStr2) {
		dateStr2 = DateUtils.today(0, '-');
	}
	var dateObj1 = DateUtils.parseToDate(dateStr1);
	var dateObj2 = DateUtils.parseToDate(dateStr2);
	var result = -1;
	if (dateObj1.getTime() > dateObj2.getTime()) {
		result = 1;
	} else if (dateObj1.getTime() == dateObj2.getTime()) {
		result = 0;
	}
	return result;
}

/*
 * 將傳入日期字串，轉換為 Javascript Date 物件，西元日期或是民國日期皆可
 */
DateUtils.parseToDate = function(dateStr) {
	try {
		return DateUtils.parseToDate1(dateStr);
	} catch(e) {
		return DateUtils.parseToDate2(dateStr);
	}
}

// Y2K Date formate pattern
DateUtils.Y2KDatePattern = [
	/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2}).(\d{1, 3})/, //YYYY-MM-DD hh:mm:ss.s
	/^(\d{4})\/(\d{2})\/(\d{2}) (\d{2}):(\d{2}):(\d{2}).(\d{1, 3})/, //YYYY/MM/DD hh:mm:ss.s
	/^(\d{4})(\d{2})(\d{2}) (\d{2}):(\d{2}):(\d{2}).(\d{1, 3})/, //YYYYMMDD hh:mm:ss.s

	/^(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/, //YYYY-MM-DD hh:mm:ss
	/^(\d{4})\/(\d{2})\/(\d{2}) (\d{2}):(\d{2}):(\d{2})/, //YYYY/MM/DD hh:mm:ss
	/^(\d{4})(\d{2})(\d{2}) (\d{2}):(\d{2}):(\d{2})/, //YYYYMMDD hh:mm:ss

	/^(\d{4})-(\d{2})-(\d{2})/, //YYYY-MM-DD
	/^(\d{4})\/(\d{2})\/(\d{2})/, //YYYY/MM/DD
	/^(\d{4})(\d{2})(\d{2})/ //YYYYMMDD
];

DateUtils.parseToDate1 = function(dateStr, year, month, day) {
	var result = '';
	for (var i = 0; i < DateUtils.Y2KDatePattern.length; i++) {
		var pattern = dateStr.match(DateUtils.Y2KDatePattern[i]);
		if (pattern) {
			DateUtils._delimiter = i % 3 == 0 ? '-' : i % 3 == 1 ? '/' : '';
			try {
				year = year ? new Number(year) : 0;
			} catch (e) {
				year = 0
			}
			try {
				month = month ? new Number(month) : 0;
			} catch (e) {
				month = 0
			}
			try {
				day = day ? new Number(day) : 0;
			} catch (e) {
				day = 0
			}
			result = new Date(new Number(pattern[1]) + year, new Number(pattern[2]) + month - 1, new Number(pattern[3]) + day, pattern[4] ? new Number(pattern[4]) : 0, pattern[5] ? new Number(pattern[5]) : 0, pattern[6] ? new Number(pattern[6]) : 0);
			isValid = true;
			break;
		}
	}
	if (result == '') {
		throw new Error('[' + dateStr + '] is not validate format');
	} else {
		return result;
	}
}

// ROC Date formate for YYY-MM-DD, YYYMMDD, YYY/MM/DD, YY-MM-DD, YYMMDD, YY/MM/DD
DateUtils.ROCDatePattern = [
	/^(\d{3})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2}).(\d{1, 3})/, //YYY-MM-DD hh:mm:ss.s
	/^(\d{3})\/(\d{2})\/(\d{2}) (\d{2}):(\d{2}):(\d{2}).(\d{1, 3})/, //YYY-MM-DD hh:mm:ss.s
	/^(\d{3})(\d{2})(\d{2}) (\d{2}):(\d{2}):(\d{2}).(\d{1, 3})/, //YYY-MM-DD hh:mm:ss.s

	/^(\d{2})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2}).(\d{1, 3})/, //YYY-MM-DD hh:mm:ss.s
	/^(\d{2})\/(\d{2})\/(\d{2}) (\d{2}):(\d{2}):(\d{2}).(\d{1, 3})/, //YYY-MM-DD hh:mm:ss.s
	/^(\d{2})(\d{2})(\d{2}) (\d{2}):(\d{2}):(\d{2}).(\d{1, 3})/, //YYY-MM-DD hh:mm:ss.s

	/^(\d{3})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/, //YYY-MM-DD hh:mm:ss
	/^(\d{3})\/(\d{2})\/(\d{2}) (\d{2}):(\d{2}):(\d{2})/, //YYY-MM-DD hh:mm:ss
	/^(\d{3})(\d{2})(\d{2}) (\d{2}):(\d{2}):(\d{2})/, //YYY-MM-DD hh:mm:ss

	/^(\d{2})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/, //YYY-MM-DD hh:mm:ss
	/^(\d{2})\/(\d{2})\/(\d{2}) (\d{2}):(\d{2}):(\d{2})/, //YYY-MM-DD hh:mm:ss
	/^(\d{2})(\d{2})(\d{2}) (\d{2}):(\d{2}):(\d{2})/, //YYY-MM-DD hh:mm:ss

	/^(\d{3})-(\d{2})-(\d{2})/, //YYY-MM-DD
	/^(\d{3})\/(\d{2})\/(\d{2})/, //YYY/MM/DD
	/^(\d{3})(\d{2})(\d{2})/, //YYYMMDD

	/^(\d{2})-(\d{2})-(\d{2})/, //YY-MM-DD
	/^(\d{2})\/(\d{2})\/(\d{2})/, //YY/MM/DD
	/^(\d{2})(\d{2})(\d{2})/, //YYMMDD

	/^(\d{1})-(\d{2})-(\d{2})/, //Y-MM-DD
	/^(\d{1})\/(\d{2})\/(\d{2})/, //Y/MM/DD
	/^(\d{1})(\d{2})(\d{2})/ //YMMDD
];

DateUtils.parseToDate2 = function(dateStr, year, month, day) {
	var result = '';
	for (var i = 0; i < DateUtils.ROCDatePattern.length; i++) {
		var pattern = dateStr.match(DateUtils.ROCDatePattern[i]);
		if (pattern) {
			DateUtils._delimiter = i % 3 == 0 ? '-' : i % 3 == 1 ? '/' : '';
			try {
				year = year ? new Number(year) : 0;
			} catch (e) {
				year = 0
			}
			try {
				month = month ? new Number(month) : 0;
			} catch (e) {
				month = 0
			}
			try {
				day = day ? new Number(day) : 0;
			} catch (e) {
				day = 0
			}
			result = new Date(new Number(pattern[1]) + year + 1911, new Number(pattern[2]) + month - 1, new Number(pattern[3]) + day, pattern[4] ? new Number(pattern[4]) : 0, pattern[5] ? new Number(pattern[5]) : 0, pattern[6] ? new Number(pattern[6]) : 0);
			break;
		}
	}
	if (result == '') {
		throw new Error('[' + dateStr + '] is not validate format');
	} else {
		return result;
	}
}

DateUtils.toY2KString = function(d) {
	if(!d) return '';
	var withoutTime = d.length <= 10;
	var date = DateUtils.parseToDate(d);
	var r = date.toLocaleString();
	return withoutTime ? r.split(' ')[0] : r
}

DateUtils.toROCString = function(d) {
	if(!d) return '';
	var r = this.toY2KString(d);
	var yyyy = r.substring(0, 4);
	return (yyyy-1911) + r.substring(4);
}

/*
 *	new PageController(Array, 每頁顯示筆數, 資料顯示的 tbody or table Element, 資料顯示的 function)
 */
var PageController = Class.create({

	/**
	 * data : 欲顯示資料 需為 Array Object
	 * pageSize : 每頁顯示筆數
	 * tbody : 資料顯示的 TBODY 區塊，為 TBODY ELEMENT
	 * renderFunction : 每頁顯示資料的實作
	 * rowSpan : 指定幾行 TR 為一筆資料，控制資料筆數分色顯示
	 * beforeFunction : 換頁動作執行前，可指定 Function 執行
	 * afterFunction : 換頁動作執行後，可指定 Function 執行
	 * disableSearch : 停用資料搜尋功能 true or boolean, 預設為開啟
	 * searchKey : 搜尋功能啟動時，指定搜尋參考資料範圍, 須存在於 data keyset 當中
	 * trTitle : 資料title列 TR element(or tr element ID) @2014-05-09修改，支援多個TR
	 * ascSort : 遞增排序Function
	 * descSort : 遞減排序Function
	 * searchDiv : (div element)可指定『搜尋』出現位置，預設為tbody下方。
	 * 
	 * ※ 資料排序用法說明：
	 * 標題列每個TD 
	 * 1.class『non-sort』or TD 沒有 key屬性：表示不排序 
	 * 2.class『number』：表示為數字排序
	 * 3.請加上屬性名稱『key』，屬性值『欄位key名稱』
	 *
	 * 目前針對陣列(data 參數一)裡可能存放的3種元素資料，實作排序功能：
	 * 1.Object [{},{},{}] 
	 * 2.Hash   [Hash, Hash, Hash]
	 * 3.[['String','String'], ['String','String']]
	 * 其它非上述結構資料，則必須自己實作『遞增排序』和『遞減排序』Function，並傳入 
	 * 範例：CMZ30100.jsp		   
	 **/
	 
	initialize : function(data, pageSize, tbody, renderFunction, rowSpan, beforeFunction, afterFunction, disableSearch, searchKeySet, trTitle, ascSort, descSort, searchDiv) {
		this.data = data;
		this.pageSize = parseFloat(pageSize);
		this.tbody = tbody;
		this.renderFunction = renderFunction;
		this.rowSpan = rowSpan || 1;
		this.beforeFunction = beforeFunction;
		this.afterFunction = afterFunction;
		this.disableSearch = Object.isUndefined(disableSearch) ? true : disableSearch === false ? false : true;
		this.searchKeySet = searchKeySet;
		this.searchDiv = $(searchDiv);
		this.currentPageNo = 0;
		this.resetData(data);
		//this.trTitle = $(trTitle);
		this.trTitleList = []; //maybe多個TR
		var that = this;
		if(Object.isArray(trTitle)){
			trTitle.each(function(el){that.trTitleList.push($(el))});		
		} else {
			this.trTitleList.push($(trTitle));
		}

		if(trTitle){
			this.initSort();
		}
		this.DESC = false;
		this.columnIndex = 0;
		this.ascSort = ascSort;
		this.descSort = descSort;
		this.columnKey = '';
		this.isNumberSort = false;
    },

	footTemplate : new Template(
		'<tfoot id="TFOOT_#{tfootId}">' +
			'<tr>' +
			'<td colspan="#{colspan}" class="tbBlue2" nowrap="nowrap">' +
				'<table width="100%" border="0" align="center" cellpadding="0" cellspacing="1" border="0">' +
				'<tr class="tbBlue2">' +
				'<td width="60%">&nbsp;</td>' +
				'<td nowrap="nowrap" align="right" class="text10">' +
					'<input type="text" class="textBox2" id="KEY_WORD#{tfootId}" maxLength="20" size="24">' +
					'<input type="checkbox" id="LIKE_SEARCH#{tfootId}" value="1">' +
					'完整比對' +
					'<input type="button" class="button" id="btnSearch#{tfootId}" value="搜尋">' +
				'</td>' + 
				'<td class="text10" align="right" nowrap="nowrap">' +
					'<img src="../../../images/CM/previous2.gif" width="20" height="15" id="firstPage#{tfootId}">' +
					'<img src="../../../images/CM/previous1.gif" width="20" height="15" id="previousPage#{tfootId}">' +
					'<img src="../../../images/CM/next1.gif" width="20" height="15" id="nextPage#{tfootId}">' +
					'<img src="../../../images/CM/next2.gif" width="20" height="15" id="lastPage#{tfootId}">' +
				'</td>' +
				'<td nowrap="nowrap" align="right" class="text10" id="TD_#{tfootId}">&nbsp;</td>' +
				'</tr>' + 
				'</table>' + 
			'</td>' + 
			'</tr>' + 
		'</tfoot>'
	),

	render : function(startIndex, endIndex) {
		if (!(Object.isArray(this.data) && this.data.size() > 0)) {
			return;
		}
		var that = this;
		$R(startIndex, endIndex).each(function(index) {
			if (that.data[index]) {
				that.renderFunction(that.data[index], index);
			}
		});
		var colspan = 1;
		this.tbody.childElements().each(function(tr) {
			tr.childElements().each(function(td) {
				colspan += td.colSpan ? td.colSpan : 1;
			});
			throw $break;
		});
		this.resetClass();
		if (!$('TFOOT_' + that.tbody.id)) {
			if(that.searchDiv){
				var table = new Element('table', {width : '100%', border : 0 , cellpadding : 0, cellspacing : 1, className : 'tbBox2'});
				that.searchDiv.insert(table);
				table.insert(that.footTemplate.evaluate({'tfootId' : that.tbody.id, 'pageInfo' : that.tbody.id, 'colspan' : colspan}));
			} else {
				this.tbody.up('table').insert(that.footTemplate.evaluate({'tfootId' : that.tbody.id, 'pageInfo' : that.tbody.id, 'colspan' : colspan}));
			}
			
			$('firstPage' + that.tbody.id).onclick = function() {
				that.gotoPage(0);
			};
			$('nextPage' + that.tbody.id).onclick = function() {
				that.gotoPage(that.currentPageNo + 1);
			};
			$('previousPage' + that.tbody.id).onclick = function() {
				that.gotoPage(that.currentPageNo - 1);
			};
			$('lastPage' + that.tbody.id).onclick = function() {
				that.gotoPage(that.totalPages - 1);
			};
			$('btnSearch' + that.tbody.id).onclick = function() {
				that.doSearch($('KEY_WORD' + that.tbody.id));
			};
			if (this.disableSearch === false) {
				$('btnSearch' + that.tbody.id).up('td').hide();
			}
		}
		$('TD_' + that.tbody.id).innerText = '第 ' + (that.currentPageNo + 1) + '/' + that.totalPages + '頁,  共' + that.totalRecord + '件';
	},

	resetClass : function() {
		var rowSpan = this.rowSpan;
		this.tbody.childElements().each(function(tr, index) {
			tr.removeClassName('tbYellow2');
			tr.removeClassName('tbBlue3');
			tr.addClassName((parseInt(index / rowSpan) % 2 == 0) ? 'tbYellow2' : 'tbBlue3');
		});
	},

	getCurrentPage : function() {
		return this.currentPageNo;
	},

	firstPage : function() {
		this.gotoPage(0);
	},

	nextPage : function() {
		this.gotoPage(this.currentPageNo + 1);
	},

	previousPage : function() {
		this.gotoPage(this.currentPageNo - 1);
	},

	lastPage : function() {
		this.gotoPage(this.totalPages - 1);
	},

	gotoPage : function(pageNo) {
		try {
			this.tbody.childElements().each(function(tr) {tr.remove()});
		} catch (e) {
		}
		if (Object.isUndefined(this.data) || Object.isUndefined(this.pageSize) || Object.isUndefined(this.tbody) || !Object.isFunction(this.renderFunction)) {
			return;
		}
		pageNo = parseFloat(pageNo);
		if (pageNo < 0) {
			this.currentPageNo = 0;
		} else if (pageNo >= this.totalPages) {
			this.currentPageNo = this.totalPages - 1;
		} else {
			this.currentPageNo = pageNo;
		}
		var startIndex = this.currentPageNo * this.pageSize;
		var endIndex = (this.currentPageNo * this.pageSize) + (this.pageSize - 1);
		if (Object.isFunction(this.beforeFunction)) {
			this.beforeFunction();
		}
		this.render(startIndex, endIndex);
		if (Object.isFunction(this.afterFunction)) {
			this.afterFunction();
		}
	},

	resetData : function(data) {
		this.data = data;
		this.totalRecord = Object.isArray(this.data) ? this.data.size() : 0;
		this.totalPages = Math.floor((this.totalRecord / this.pageSize)) + (this.totalRecord % this.pageSize == 0 ? 0 : 1);
		if($('TFOOT_' + this.tbody.id)) {
			$('TFOOT_' + this.tbody.id).remove();
		}
		this.gotoPage(this.currentPageNo);
	},
	
	_searchIndex : {pageCurrent : -1, trIndexCurrent : -1, tdIndexCurrent : -1, page1stFound : -1, trIndex1stFound : -1, tdIndex1stFound : -1},
	
	doSearch : function(obj) {
		if (obj.value.blank()) {
			return;
		}
		
		var keyword = obj.value;
		
		var isFound = false;
		var isFound1st = false;
	
		var page = -1;
		var trIndex = -1;
		var tdIndex = -1;
	
		if (this.getCurrentPage() != this._searchIndex.pageCurrent) {
			this._searchIndex.trIndexCurrent = -1;
			this._searchIndex.tdIndexCurrent = -1;
		
			this._searchIndex.page1stFound = -1;
			this._searchIndex.trIndex1stFound = -1;
			this._searchIndex.tdIndex1stFound = -1;
		}
	
		this._searchIndex.pageCurrent = this.getCurrentPage();
		var that = this;
		this.data.each(function(vo, index) {
			if(vo.IGNORE_SEARCH) {
				return;
			}
			if (isFound) {
				throw $break;
			}
			var keySet = $A();
			if (Object.isUndefined(that.searchKeySet) || !Object.isArray(that.searchKeySet)) {
				try {
					keySet = $H(vo.toObject()).keys();
				} catch (e) {
					keySet = $H(vo).keys();
				}
			} else {
				keySet = that.searchKeySet;
			}
			keySet.each(function(key, i) {
				var value = vo[key];
				if ($('LIKE_SEARCH' + that.tbody.id).checked) {
					if (value == keyword) {
						page = Math.floor(index / that.pageSize);
						trIndex = index % that.pageSize;
						tdIndex = i;
						if (!isFound1st) {
							that._searchIndex.page1stFound = page;
							that._searchIndex.trIndex1stFound = trIndex;
							that._searchIndex.tdIndex1stFound = tdIndex;
							isFound1st = true;
						}
						if ((page > that._searchIndex.pageCurrent) ||
							(page == that._searchIndex.pageCurrent && trIndex > that._searchIndex.trIndexCurrent)// || 
							//(page == that._searchIndex.pageCurrent && trIndex == that._searchIndex.trIndexCurrent && tdIndex > that._searchIndex.tdIndexCurrent)
						) {
							that._searchIndex.pageCurrent = page;
							that._searchIndex.trIndexCurrent = trIndex;
							that._searchIndex.tdIndexCurrent = tdIndex;
							isFound = true;
							throw $break;
						}
					}
				} else {
					value = '' + value;
					if (value.toUpperCase().indexOf(keyword.toUpperCase()) >= 0) {
						page = Math.floor(index / that.pageSize);
						trIndex = index % that.pageSize;
						tdIndex = i;
						if (!isFound1st) {
							that._searchIndex.page1stFound = page;
							that._searchIndex.trIndex1stFound = trIndex;
							that._searchIndex.tdIndex1stFound = tdIndex;
							isFound1st = true;
						}
						if ((page > that._searchIndex.pageCurrent) ||
							(page == that._searchIndex.pageCurrent && trIndex > that._searchIndex.trIndexCurrent)// || 
							//(page == that._searchIndex.pageCurrent && trIndex == that._searchIndex.trIndexCurrent && tdIndex > that._searchIndex.tdIndexCurrent)
						) {
							//alert(that._searchIndex.pageCurrent + '/' + that._searchIndex.trIndexCurrent + '/' + that._searchIndex.tdIndexCurrent + '\n' + page + '/' + trIndex + '/' + tdIndex);
							that._searchIndex.pageCurrent = page;
							that._searchIndex.trIndexCurrent = trIndex;
							that._searchIndex.tdIndexCurrent = tdIndex;
							isFound = true;
							throw $break;
						}
					}
				}
			});
		});
		if (isFound) {
			this.gotoPage(this._searchIndex.pageCurrent);
			var tr = this.tbody.childElements()[this.rowSpan * this._searchIndex.trIndexCurrent];
			tr.addClassName('textBlue');
			try {
				//(tr.childElements()[this._searchIndex.tdIndexCurrent]).addClassName('textRed');
			} catch (e) {
			}
		} else if (isFound1st) {
			this._searchIndex.pageCurrent = this._searchIndex.page1stFound;
			this._searchIndex.trIndexCurrent = this._searchIndex.trIndex1stFound;
			this._searchIndex.tdIndexCurrent = this._searchIndex.tdIndex1stFound;
			this.gotoPage(this._searchIndex.page1stFound);
			var tr = this.tbody.childElements()[this.rowSpan * this._searchIndex.trIndex1stFound];
			tr.addClassName('textBlue');
			try {
				//(tr.childElements()[this._searchIndex.tdIndex1stFound]).addClassName('textRed');
			} catch (e) {
			}
		} else {
			this._searchIndex.pageCurrent = -1;
			alert('查無符合資料');
		}
	},
	
	initSort : function(){
				var that = this;
				if(!that.trTitleList){
					return;
				}
				$$('span[name="@ICON_SORT"]').each(Element.remove);
				
				that.trTitleList.each(function(trTitle, outIndex){
					trTitle.childElements().each(
					function(td, index){
						var key = td.key;
						var nonSort = td.hasClassName('non-sort');
						if(nonSort || !key){
							return;
						}
						var span = new Element('span', {innerText : td.innerText});
						var span2 = new Element('span', {innerText : '◆', name : '@ICON_SORT', id : 'ICON_SORT_' + outIndex + '$'+ index});
							td.update('');
							td.stopObserving('click');
							td.insert(span).insert(span2);
							td.observe('click', function(){
									that.doSorting(this, outIndex, index);
							});	
					})
				});
				
				
	},
	
	getSortKey : function(){
		return this.columnKey;
	},
	
	getSortIndex : function(){
		return this.columnIndex;
	},
	
	getDatas : function(){
		return this.data;
	},
	
	isNumberSort : function(){
		return this.isNumberSort;
	},

	doSorting : function(obj, outIndex, index){
		this.claerSortIcon();
		var key = obj.key;
		this.columnKey = key;
		this.columnIndex = index;
		this.isNumberSort = obj.hasClassName('number');

		if(Object.isFunction(this.descSort) && Object.isFunction(this.ascSort)){
			if(this.DESC){
				this.descSort();
			}else{
				this.ascSort();
			}
		}else{
			var dataList = this.data.clone();
			this._sorting(dataList, key, index, obj.hasClassName('number'));
			this.resetData(dataList);
		}

		
		if(this.DESC){
			$('ICON_SORT_' + outIndex + '$' + index).update('▼');
			this.DESC = false;
		}else {
			$('ICON_SORT_' + outIndex + '$' + index).update('▲');
			this.DESC = true;
		}
		
	},
	
	_sorting : function(dataList, key, index, isNumber){
	
		var toEmpty = function(s){ return Object.isUndefined(s) ? '' : s; }
		var toZero =  function(s){ return isNaN(s) ? '' : s; }
		
		var elementIsArray = Object.isArray(dataList[0]);
		var elementIsHash = Object.isHash(dataList[0]);
		for(var j=0; j < dataList.length; j++){
			for(var i=0; i<dataList.length - 1; i++){
				var obj1 = dataList[i];
				var obj2 = dataList[i+1];
				var value1;
				var value2;
				if(elementIsArray){
					value1 = isNumber ? parseFloat(toZero(obj1[index])) : toEmpty(obj1[index]);
					value2 = isNumber ? parseFloat(toZero(obj2[index])) : toEmpty(obj2[index]);
				}else if(elementIsHash){
					value1 = isNumber ? parseFloat(toZero(obj1.get(key))) : toEmpty(obj1.get(key));
					value2 = isNumber ? parseFloat(toZero(obj2.get(key))) : toEmpty(obj2.get(key));
				}else {
					value1 = isNumber ? parseFloat(toZero(obj1[key])) : toEmpty(obj1[key]);
					value2 = isNumber ? parseFloat(toZero(obj2[key])) : toEmpty(obj2[key]);
				}
				var isSwap = false;
				if( value1 === ''){
					isSwap = true;
				} else if(value2 === '') {
					//
				} else if(this.DESC ? (value1 < value2) : (value1 > value2) ){
					isSwap = true;
				}
				if(isSwap) {
					var temp = obj1;
					dataList[i] = obj2;
					dataList[i+1] = temp;
				}
			}
		}
	},
	
	claerSortIcon : function(){
		this.trTitleList.each(
			function(trTitle, outIndex){
				trTitle.childElements().each(
					function(td,index){
						if($('ICON_SORT_' + outIndex + '$' + index)){
							$('ICON_SORT_' + outIndex + '$' +index).update('');
						}
					})
				});
		
	}
	
});


//**********************************************************************************
/** 提供資料遮蔽
 	1.getMask(str, start, end) 
 		ex : MaskUtils.getMask('0123456789', 5)    --> 01234***** (從第5碼開始遮蔽)
 			 MaskUtils.getMask('0123456789', 5, 7) --> 01234**789 (從第5碼到第6碼遮蔽)
 	
 	2.getMaskLast(str, len) :
 		ex : MaskUtils.getMaskLast('0123456789', 5) --> 01234***** (遮蔽最後5碼)
 			 MaskUtils.getMaskLast('0123456789', 2) --> 01234567** (遮蔽最後2碼)		 
 			 
 	3.getMaskID(id) : 
 		ex : MaskUtils.getMaskLast('A123456789', 5) --> A1234***** (以星號遮蔽最後5碼)
 		
 	4.getMaskPolicyNo(policyNo, len) :
 	 	ex : MaskUtils.getMaskPolicyNo('0123456789')    --> 012****789(以星號遮蔽中間4碼)
 	 	ex : MaskUtils.getMaskPolicyNo('0123456789', 5) --> 012*****89(以星號遮蔽中間5碼)
 	
 	5.getMaskBankAccount(str) :
 		ex : MaskUtils.getMaskBankAccount('1234789034564567') --> *456* (取最後5碼並以星號遮蔽第1碼跟最後1碼)
 		
 	6.getMaskCreditCard(str) : (同getMaskPolicyNo)
 		ex : MaskUtils.getMaskBankAccount('1234789034564567') --> *456* (取最後5碼並以星號遮蔽第1碼跟最後1碼)	
 		
 	7.getMaskName(name) : 	
 		ex : MaskUtils.getMaskName('我')     --> 我* (若傳入一個字，以*新增至最後一個字)
 		ex : MaskUtils.getMaskName('我是')   --> 我* (若傳入二個字，以*取代最後一個字之間的文字)
 		ex : MaskUtils.getMaskName('我是誰') --> 我*誰 (若傳入三個字，保留第一個字與最後一個字，其餘以*取代)
 		ex : MaskUtils.getMaskName('碧斯蔚.梓佑') --> 碧斯***佑 (若傳入四個字（含）以上，僅保留前二個字與最後一個字，其餘以*取代)
   
   8.getMaskDate(date) : 
   		ex : MaskUtils.getMaskDate('70/01/01') --> 70/01/** (以*取代最後2碼)	
   
   9.getMaskROCDate(rocDate) :
   		ex : MaskUtils.getMaskROCDate('民國99年1月1日') --> 民國99年01月**日 (以*取代日) 		
   
   10.getMaskPhone(phone) : 
   		ex : MaskUtils.getMaskPhone('02-87936999') --> 02-87****99 (以星號遮蔽倒數第3碼~倒數6碼) 
   		
   11.getMaskCell(cell) : (同getMaskPhone)
   		ex : MaskUtils.getMaskCell('0910111222') --> 0910****22 (以星號遮蔽倒數第3碼~倒數6碼) 	
   		
   12.getMaskAddress(address) : 
   		ex : MaskUtils.getMaskAddress('台北市內湖區瑞光路510號') --> 台北市********** (取得地址前三碼，若不足三碼直接取得該地址，並於所取得之地址後方加上"*"，共計回傳 13 碼)					 
	
   13.getMaskZipCodeAdd(address) : 
   		ex : MaskUtils.getMaskZipCodeAdd('114台北市內湖區瑞光路510號') --> 114台北市********** (保留郵遞區號和中文前3碼，其餘*遮蔽)					 
	
*/ 
//**********************************************************************************

var MaskUtils = {};
		
		MaskUtils.getStar =  function(n) {
			var s = '';
			while(n > 0) {
				s = s + '*';
				n--;
			}
			return s;		
		}
		
		MaskUtils.getMask = function(str, start, end) {
			if(!str){
				return '';
			}
			if(Object.isNumber(str)){
				str = str.toString();
			}
			if(Object.isString(str)){
				if(Object.isNumber(start) && start >=0 ){
					if(!Object.isNumber(end) || end < start || end > str.length){
						end = str.length;
					}
					var s = end - start;
					return str.substring(0, start) + this.getStar(s) + str.substring(end);	
				}
			}
			return str;
		}		
		
		MaskUtils.getMaskLast = function(str, len){
			if(!str){
				return '';
			}
			if(Object.isNumber(str)){
				str = str.toString();
			}
			if(Object.isString(str)){
				if(Object.isNumber(len) && len > 0){
					return 	this.getMask(str, str.length - len)
				}
			}	
			return str;
		}
		
		MaskUtils.getMaskID = function(id) {
			return this.getMaskLast(id, 5);
		}
		
		MaskUtils.getMaskPolicyNo = function(policyNo, len){
			if(!policyNo){
				return '';
			}
			
			if(Object.isNumber(policyNo)) {
				policyNo = policyNo.toString();
			}
			if(Object.isString(policyNo)){
				if(!Object.isNumber(len) || len <= 0){
					len = 4;
				}
				var m = (policyNo.length/2).floor() - (len/2).floor() ;
				if(m > 0){
					return this.getMask(policyNo, m, m+len);
				}
			}	
			return policyNo;
		}
		
		MaskUtils.getMaskBankAccount = function(str){
			if(!str){
				return '';
			}
			if(Object.isNumber(str)) {
				str = str.toString();
			}
			if(Object.isString(str)){
				if(str.length > 5){
					return '*' + str.substring(str.length - 4, str.length - 1) + '*';
				}
			}
			return str;
		}
		
		MaskUtils.getMaskCreditCard = function(str){
			return this.getMaskBankAccount(str);
		}
		
		MaskUtils.getMaskName = function(name){
			if(!name){
				return '';
			}
			if(Object.isString(name)){
				var _size = name.length;
				switch(_size){
					case 1 : return name + '*'; break;
					case 2 : return name.substring(0,1) + '*'; break;
					case 3 : return name.substring(0,1) + '*' + name.substring(2,3); break;
					default : return name.substring(0,2) + this.getStar(_size - 3) + name.substring(_size - 1, _size); break;
				}
			}
			return name;
		}
		
		MaskUtils.getMaskDate = function(date){
			return this.getMaskLast(date,2);
		}
		
		MaskUtils.getMaskROCDate = function(rocDate){
			if(!rocDate){
				return '';
			}
			if(Object.isString(rocDate)){
				var indexM = rocDate.indexOf('月');
				var indexD = rocDate.indexOf('日');
				if(indexM != -1 && indexD != -1 && indexD > indexM ){
					return rocDate.substring(0, indexM + 1) + '**' + rocDate.substring(indexD);
				}	
			}
			return rocDate;
		}
		
		MaskUtils.getMaskPhone = function(phone){
			if(!phone){
				return '';
			}
			if(Object.isNumber(phone)){
				phone = phone.toString();
			}
			if(Object.isString(phone)){
				return this.getMask(phone, phone.length - 6, phone.length - 2);
			}
			return phone;
		}
		
		MaskUtils.getMaskCell = function(cell){
			return getMaskPhone(cell);
		}
		
		MaskUtils.getMaskAddress = function(address) {
			if(!address){
				return '';
			}
			if(Object.isNumber(address)){
				address = address.toString();
			}
			if(Object.isString(address)){
				return address.substring(0,3) + this.getStar(10);
			}
			return address;
		}
		
		MaskUtils.getMaskZipCodeAdd = function(address) {
			if(!address){
				return '';
			}
			if(Object.isNumber(address)){
				address = address.toString();
			}
			if(Object.isString(address)){
				for(var i=0; i < address.length; i++){
					if(!/\d/.test(address.charAt(i))){
						return this.getMask(address, i+3);
					}
				}
			}
			return address;
		}
		
/**
 * @真分頁查詢
 * @Note : 只適用 Ajax Query
 * @使用方法請參考 ：CMZ00391
 */

var PageQuery = Class.create(
	{
		initialize : function(url, param){
			this.url = url;
			this.param = param;
		}
	}	
);

var PageUtil = Class.create({
	
	initialize: function(param) {
    	this.param = param  || {};
		if(Object.isHash(this.param)){
			this.param = this.param.toObject();
		}

		this.pageSize = this.param.PAGE_SIZE || 10;
		this.tbodyId = this.param.TBODY_ID;
		this.fieldTotalCount = this.param.FIELD_TOTAL_COUNT;
		this.ajaxQuery = this.param.AJAX_QUERY;	
		this.totalCount = 0;
		this.pageNoClick = 1;
		this.pageNum = 10;
		this.funcOnSuccess = this.param.AJAX_QUERY.param.onSuccess;
 	},

	_setPageInfo : function(that){
		if($('TD_PAGE_INFO')) return;
		var tbid = this.tbodyId;
		var td = new Element('td', {colspan : that._getTDSpan(tbid), id : 'TD_PAGE_INFO', align : 'right'});
		var tr = new Element('tr', {className : 'tbYellow2'}).insert(td);
		$(this.tbodyId).up('table').insert(new Element('tfoot').insert(tr));
	},
	
	_getTDSpan : function(tbid){
			var colspan = 1;
			var _tr = $(tbid).up('table').down('tr', 0);
			_tr.childElements().each(function(td){
				colspan += td.colSpan ? td.colSpan : 1;
			});
			return colspan;
	},
	
	_setMouseover : function (ele){
		ele.observe('mouseover', function(){
						this.style.cursor='hand';
						this.removeClassName('textPageOff');
						this.addClassName('textPageOn')
					  });
	},
		
	_setMouseout : function (ele){
		ele.observe('mouseout', function(){
						this.addClassName('textPageOff');
						this.removeClassName('textPageOn');
					  });
	},
	
	_setPageNo : function(pageNo){
			$('TD_PAGE_INFO').update('');
			var pages = (this.totalCount / this.pageSize).ceil();
			var _block = (pageNo / this.pageNum).ceil();
			var _max = _block * this.pageNum;
			var _min = _max - (this.pageNum - 1);
			_max = _max > pages ? pages : _max;
			var isDivisible = this.totalCount % this.pageNum == 0; 
			var that = this;
			for(var i = _min; i <= _max; i++){
			
				if(i % this.pageNum == 1 && i != 1 ){
					var _previous = new Element('span', {className : 'textPageOff', innerHTML : '&nbsp;' + '<<' + '&nbsp;', id : 'SPAN_PREVIOUS_NO_' + i, PAGE_NO : i});
					this._setMouseover(_previous);
					this._setMouseout(_previous);
					$('TD_PAGE_INFO').insert(_previous);
					_previous.observe('click', function(){that._setPageNo(_min - 1 );});  
				}
				
				var t = new Element('span', {className : 'textPageOff', innerHTML : '&nbsp;' + i + '&nbsp;', id : 'SPAN_PAGE_NO_' + i, PAGE_NO : i});
				if(this.pageNoClick == i) t.addClassName('textPageThis');
					this._setMouseover(t);
					this._setMouseout(t);
					t.observe('click', function(){that._clickPageNo(this, that);});  
				$('TD_PAGE_INFO').insert(t);
				
				if(i % this.pageNum == 0 && !(isDivisible && i == pages)){
					var _next = new Element('span', {className : 'textPageOff', innerHTML : '&nbsp;' + '>>' + '&nbsp;', id : 'SPAN_NEXT_NO_' + i, PAGE_NO : i});
					this._setMouseover(_next);
					this._setMouseout(_next);
					$('TD_PAGE_INFO').insert(_next);
					_next.observe('click', function(){that._setPageNo(_max + 1 );});  
				}
			}
			
			var _total = new Element('span', {className : 'textPageOff', innerHTML : '&nbsp;' + '共' + that.totalCount + '筆' + '&nbsp;', id : 'SP_TOTAL_COUNT'});
			$('TD_PAGE_INFO').insert(_total);
	},
	
	_clickPageNo : function(obj, that){
		if(this.pageNoClick == obj.PAGE_NO) return;
		var a = $('SPAN_PAGE_NO_' + this.pageNoClick);
		if(a) a.removeClassName('textPageThis');
		this.pageNoClick = obj.PAGE_NO;
		obj.addClassName('textPageThis');
		that.doQuery(obj.PAGE_NO, true);
	},

	doQuery : function(pageNo, unReset) {
		if(!unReset) this.pageNoClick = 1;
		pageNo = pageNo ||1;
		var e = pageNo * this.pageSize;
		var s = e - (this.pageSize - 1);
			
		this.ajaxQuery.param.parameters.START_INDEX = s; 
		this.ajaxQuery.param.parameters.END_INDEX = e; 
		var that = this;		
		this.ajaxQuery.param.onSuccess = this.funcOnSuccess.wrap(
			function(_onSuc, XHT, resp){
				_onSuc(XHT, resp);
				that.totalCount = resp[that.fieldTotalCount] || 0;
				that._setPageInfo(that);
				that._setPageNo(pageNo);	
			}
		);	
		$(this.tbodyId).update('');
		new Ajax.Request(this.ajaxQuery.url, this.ajaxQuery.param);	
		
	}
});



//fix element has name without id
if(!document._originalGetElementById){ // dup load js will occur stack overflow
	document._originalGetElementById = document.getElementById;
	document.getElementById = function(id) {
		var ele = document._originalGetElementById(id);
		if(ele){
			return ele;
		}
		var elems = document.getElementsByName(id); 
		if (elems && elems.length > 0) {
			ele = elems[0];
		}
		return ele;
	}
}

var _TABLE = Class.create(
	{
		initialize : function(){
			this.collectTR = [];
			this.isColorful = true;
			this.sheetName = '';
			this.width = 0;
		},
		
		addTR : function(td) {
			this.collectTR.push(td);
		},
		
		setColorful : function(isColorful) {
			this.isColorful = isColorful;
		},
		
		setSheetName : function(sheetName){
			this.sheetName = sheetName;
		},
		
		setWidth : function(w){
			this.width = w;
		}
	}	
);

var _TR = Class.create(
	{
		initialize : function(){
			this.collectTD = [];
		},
		
		addTD : function(td) {
			this.collectTD.push(td);
		}
	}	
);

var _TD = Class.create(
	{
		initialize : function(){
			this.colspan = 1;
			this.rowspan = 1;
			this.align = 'left';
			this.text = '';
			this.color = '';
			this.width = 0;
		},
		setColspan : function(c) {
			this.colspan = c;
		},
		setRowspan : function(r) {
			this.rowspan = r;
		},
		setAlign : function(a) {
			this.align = a;
		},
		setText : function(t) {
			this.text = t;
		},
		setColor : function(c) {
			this.color = c;
		},
		setWidth : function(w) {
			this.width = w;
		}
	}	
);

var Sheet = Class.create(
(function(){
	
	function initialize (tbid) {
		this.tableId = tbid;
		this._table = new _TABLE();
	}
	
	function setSheetName(sheetName) {
		this._table.setSheetName(sheetName);
	}
	
	function setColorful(isColorful) {
		this._table.setColorful(isColorful);
	}
	
	function isVisible(td) {
		return td.visible() && td.up('tr').visible();
	}
	
	function getTDColor(td) {
		var r = td.getStyle('background-color');
		if(r == 'transparent'){
			r = td.up('tr').getStyle('background-color');
			if(r == 'transparent'){
				r = td.up('table').getStyle('background-color');
			}
		}
		return r == 'transparent' ? '#FFFFFF' : r;
	}
	
	function getTable(){
	var __table = this._table;
		$(this.tableId).descendants().each(
			function(tr, index){
				if(tr.tagName == 'TR'){
					var _tr = new _TR();
					var trAlign = tr.align;
					tr.descendants().each(
						function(td){
							if(td.tagName == 'TD' && isVisible(td)){
								var _td = new _TD();
								if(trAlign) _td.setAlign(trAlign);
								if(td.align) _td.setAlign(td.align);
								if(td.colSpan) _td.setColspan(td.colSpan);
								if(td.rowSpan) _td.setRowspan(td.rowSpan);
									_td.setText(td.innerText);
									//_td.setWidth(td.offsetWidth);
									_td.setColor(getTDColor(td));
								_tr.addTD(_td);
							}
						}
					);
					__table.addTR(_tr);
				}
			}
		)
		//_table.setWidth($(tableId).offsetWidth);
		return __table;
	}

	return {
		initialize : initialize,
		setColorful : setColorful,
		setSheetName : setSheetName,
		getTable : getTable

	}
})()
);

var ExcelConvert = Class.create(
(function(){

	var formId = 'EXCEL_FORM';
	var excelField = 'EXCEL_FIELD';
	var _webcontext = '/INSWeb/servlet/HttpDispatcher/CMZ3_0901/downloadExcel'
	
	function initialize () {
		//create a new form
		if(!$(formId)){
			var newForm  = new Element('form', {id : formId, name : formId, method : 'post'});
			document.body.appendChild(newForm);
		}
		this.collectTable = [];
		this._fileName = 'report';
		this._form = $(formId);
	}

	function setFileName(fileName) {
		this._fileName = fileName;
	}
	
	function addSheet(sheet) {
		this.collectTable.push(sheet.getTable());
	}

	function download() {
		
		if(this.collectTable.size() == 0){
			alert('no data for convert...');
			return;
		}
	
		CSRUtil.ProcessPageCover();
		var n = '$iframe$';
		if(!$(n)){
			var _div = new Element('div');
  			var _iframe = new Element('iframe', {style : 'display:none', src : 'about:blank', id : n, name : n});
  	  		_div.insert(_iframe);
  	 		//$(_form).insert(_div);
  	 		document.body.appendChild(_div);
		}
		if(!$(excelField)){
			var _input = new Element('input', {type : 'hidden', name : excelField, id : excelField});
			this._form.insert(_input); 
		}
  		$(excelField).value = Object.toJSON({FILE_NAME : this._fileName,collectTable : this.collectTable});
 		this._form.target = n;
 		this._form.action = _webcontext
 		this._form.submit();
 		CSRUtil.hidePageCover.delay(1.5);
	} 
	
	if(!window.clearProcessPageCover){
		window.clearProcessPageCover = function(){
			CSRUtil.hidePageCover();
		} 
	}
	
	return {
		initialize : initialize,
		download : download,
		setFileName : setFileName,
		addSheet : addSheet
	}
})()
);

/*
* @產生浮動小視窗，顯示訊息
*/
var MsgBox = (function(){
	var template  = '<div id="#{BOX_ID}" style="position:absolute;background-color:#C7E49C;cursor:hand;left:0px;top:0px;display:none;z-index:100;">';
		template += '<table style="background-color:#{BGCOLOR}; border-style:double;border-width:9px" width="100%" height="100%" border="0" cellpadding="0" cellspacing="1">';
		template += '<tr valign="top"><td><span class="#{FONT}">#{MSG}</span></td></tr>';
		template += '</table>';
		template += '</div>';
	
	var boxid = '$msgBox';	
	var width = 200; 
	var height = 50;
	var bgColor = '#EEF1A7';
	var fontClass = 'textBold';
	var classOver = 'tbYellow3';
	var changeClass = true;
	
	//自訂style : {width : '視窗寬度', height : '視窗高度', bgColor : '背景顏色', fontClass : '字形樣式'} 
	function register(el, msg, style) { 
		if(Object.isString(el)) el = $(el);
		if(!el) return;
		if(style){
			if(style.width) width = style.width;
			if(style.height) height = style.height;
			if(style.bgColor) bgColor = style.bgColor;
			if(style.fontClass) fontClass = style.fontClass;
			if(style.leftPx) leftPx = style.leftPx;
			if(style.topPx) topPx = style.topPx;
			if(style.classOver) classOver = style.classOver;
			if(!Object.isUndefined(style.changeClass)) changeClass = style.changeClass;
		}
		if(Object.isUndefined(msg) || msg === null) msg = '';
		if(!Object.isString(msg)) msg = msg.toString();
		el.observe('mouseover', function(){createMsgBox(this, msg)});
		el.observe('mouseout',  function(){removeMsgBox(this)});
	}
	
	function unregister(el) {
		if(Object.isString(el)) el = $(el);
		el.stopObserving('mouseover');
		el.stopObserving('mouseout');
	}

	function createMsgBox(obj, msg){
		var msgBox = new Template(template).evaluate({BOX_ID : boxid, MSG : msg.gsub('\n', '<br>'), BGCOLOR : bgColor, FONT : fontClass});
		document.body.insertAdjacentHTML('afterBegin', msgBox);
		
		var scrollTop = document.body.scrollTop;
		var rect = obj.getBoundingClientRect();
		var area = getArea(msg);
		var w = document.body.clientWidth;
		var h = document.body.clientHeight;
		var div = $(boxid);
		
		var _left = rect.left;
		var _top = rect.top + 25 + scrollTop;
		
		if(_left + area.width > w){
			_left = _left - area.width;
		}
		
		if(_top + area.height > h){
			_top = _top - area.height - 50;
		}
		
		div.style.display = '';
    	div.style.width = area.width + "px";
    	div.style.height = area.height + "px";
    	div.style.left = _left + "px";
    	div.style.top = _top + "px";
    	if(changeClass) obj.addClassName(classOver); 
	}
	
	function getArea(msg) {
		var arr = msg.split('\n');
		var max = 0;
		arr.each(function(s){
			var len = s.replace(/[^\x00-\xff]/g, "rr").length;
			max = max > len ? max : len;
		});	
		var w = max * 9, h = arr.size() * 25;
		if(w < 200) w = 200;
		if(h < 50) h = 50;
		return {width : w, height : h};
	}
	
	function removeMsgBox(obj){
		if($(boxid)) $(boxid).remove();
		if(changeClass) obj.removeClassName(classOver); 
	}

	return {
		register : register,
		unregister : unregister
	}
})();

/*
 * @usage :
 * Tools.clearTbody(tbodyId) : 清除tbody內容
 * Tools.u2Empty(value) : 若value is undefined，則回傳空白 else 回傳value。
 * Tools.u2Zero(value) : 若value is undefined，則回傳0 else 回傳value。
 * Tools.u2Default(value, default) : 若value is undefined，則回傳default else 回傳value。
 * Tools.setOptions('下拉選單ID', 資料集合, value實作, innerText實作) : 新增下拉選項。
 * Tools.clearOptions('下拉選單ID', '是否全部清除(false會保留第一個option)') : 清除下拉選項。
 * Tools.getValidator() : 取得 validator物件，重新包裝過，新增reset和ok方法供使用。
 */
var Tools = (function(){
	
	function clearTbody(tbodyId) {
		var tb = $(tbodyId);  
		var i = tb.rows.length;
		while(i > 0) {
			tb.deleteRow(i-1);
			i--;
		}
		//use PageController
		var tFoot = tb.next('tfoot');
		if(tFoot && tFoot.id.startsWith('TFOOT_')) tFoot.remove();
	}
	
	function u2Default(v, d) {
		return Object.isUndefined(v) ? d : v;
	}
	
	function u2Empty(v) {
		return this.u2Default(v,'');
	} 
	
	function u2Zero(v) {
		return this.u2Default(v, '0');
	} 
	
	function setOptions(selectId, voList, vc, ic) {
		voList.each(
			function(vo){
				var opt = new Element('option');
				opt.value = vc(vo);
				opt.innerText = ic(vo);
				$(selectId).insert(opt);
			}
		)
	}
	
	function clearOptions(selectId, removeAll) {
		$(selectId).childElements().each(
			function(opt, index){
				if(!removeAll && index ==0) return;
				opt.remove();
			}
		)
	}

	function getValidator() {
		var v =  new Validator();
   			v.errHandler = new AlertHandler();
   			v.reset = function () {
   				this.errHandler.clear();
    			this.clear();
    			this.errHandler = new AlertHandler();
   			}
   			
   			v.ok = function(){
   				var r = this.validate();
   				if(!r) this.errHandler.display(); 
   				return r;
   			}
   		return v;	
	}

	return {
		clearTbody : clearTbody, 
		u2Default : u2Default,
		u2Empty : u2Empty,
		u2Zero : u2Zero,
		setOptions : setOptions,
		clearOptions : clearOptions,
		getValidator : getValidator
	}
})();

var FileUtils = Class.create(
(function(){
	var _webcontext = '/INSWeb/servlet/HttpDispatcher/CMZ3_0901/fileDownload';
	var winSet = '';
	function initialize () {
		this._fileName = '';
		this._location = '';
	}

	function setFileName(fileName) {
		this._fileName = fileName;
	}
	
	function setLocation(location) {
		this._location = location;
	}
	
	function download(){
		if(!this._location){
			alert('請設定檔案路徑!');
			return;
		}
		var param = new Hash();
		var that = this;
			param.set('FILE_NAME', that._fileName);
			param.set('LOCATION', that._location);
		window.open(_webcontext + '?' + param.toQueryString(), '_blank', winSet);
		window.focus();
	}

	return {
		initialize : initialize,
		setFileName : setFileName,
		setLocation : setLocation,
		download : download
	}
})()
);

/*
@ 在IE8以上，未開啟相容性檢視，使用prototype new Element會有問題 
@ 故以該function取代
*/
function getElement(tagName, attributes) {
	var el, elName, clazz;
	if(attributes) {
		elName = attributes.name;
		clazz = attributes.className;
	}
	try{
		el = new Element(tagName, attributes);
	}catch(e) {
		if(e && e.message && (e.message == 'InvalidCharacterError' || e.message.indexOf('INVALID_CHARACTER_ERR') != -1)) {
			//ignore
		} else {
			throw e;
		}
		el = document.createElement(tagName);
	}
	for(var p in attributes) {
		try{
			el[p] = attributes[p];
		}catch(e2){/*ignore*/}
		el.setAttribute(p, attributes[p]);
	}
	if(elName) el.name = elName;
	if(clazz) el.setAttribute('class', clazz);
	return el;
}

/****以scroll元件顯示資料***
參數說明:
TABLE_HEADER : table header element(or id)
TABLE_DATA: table data element(or id)
HEIGHT : 資料區高度default 200 px;
RENDER_FUNCTION : 每頁顯示資料的實作(function)
DATA : 資料陣列
TR_SORT : 排序列TR(可以為多個)
ASC_SORT : 遞增排序(function(data) 回傳排序過的資料陣列)
DESC_SORT : 遞減排序(function(data) 回傳排序過的資料陣列)
SEARCH_ENABLE : 是否開啟搜尋功能(true or false，預設false)
*/
var PageScroll = Class.create(
(function(){
	function initialize(param) {
		this.param = param||{};
		this.height = param.HEIGHT||200;
		this.renderFunction = param.RENDER_FUNCTION;
		this.data = param.DATA||[];
		var trHeader = param.TR_SORT||[];
		
		this.tableHeader = param.TABLE_HEADER;
		if(Object.isString(param.TABLE_HEADER)) this.tableHeader = $(param.TABLE_HEADER);
		
		this.tableData = param.TABLE_DATA;
		if(Object.isString(param.TABLE_DATA)) this.tableData = $(param.TABLE_DATA);

		this.tbodyData = this.tableData.descendants().detect(function(el){return el.match('tbody')});

		this.divHeader = this.tableHeader.up('div');
		this.divData = this.tableData.up('div');
		
		//set table css fix width
		this.tableData.setStyle({'word-wrap' : 'break-word', 'tableLayout' : 'fixed'});
		this.divData.setStyle({'word-wrap' : 'break-word', 'table-layout' : 'fixed'});
		
		//set div scrollable
		this.divHeader.setStyle({'overflow-x' : 'hidden', 'overflow-y' : 'scroll'});
		this.divData.setStyle({'overflow': 'auto', height : this.height});
		
		this.trHeaders = []; //maybe多個TR
		var self = this;
		if(Object.isArray(trHeader)){
			trHeader.each(function(el){self.trHeaders.push($(el))});		
		} else if(trHeader){
			this.trHeaders.push($(trHeader));
		}
		if(this.trHeaders.size() > 0 ){
			initSort(this);
		}
		this.desc = false;
		this.ascSort = param.ASC_SORT;
		this.descSort = param.DESC_SORT;
		this.searchEnable = param.SEARCH_ENABLE;
		this.serNo = (Math.random()*100000).toFixed(0);
	}
	
	function initSearch(self) {
		var w = self.tableData.getWidth(); 
		var _html = new Template('<table id="@SEARCH_TABLE_#{SER_NO}" width="#{WIDTH}"><tr class="tbBlue2" align="right"><td><span class="text10"></span>&nbsp;&nbsp;&nbsp;<input type="text" class="textBox2" maxLength="20" size="24"><input type="button" class="button" value="搜尋"></td></tr></table>');
		self.divData.insert({after : _html.evaluate({SER_NO : self.serNo, WIDTH : w})});
		var table = $('@SEARCH_TABLE_' + self.serNo);
		var input = table.down('input.textBox2');
		var span = table.down('span',0);
		var btn = table.down('input.button');
			btn.observe('click', function(){
				span.update('');
				var keyWord = input.value.toLowerCase();
				if(keyWord.blank()) return;
				var isFirst = true, isFind = false;
				self.tbodyData.childElements().each(
					function(tr, trIndex) {
						tr.childElements().each(
							function(td, tdIndex){
								td.removeClassName('tdRedDashed'); 	
								if(td.innerText.toLowerCase().include(keyWord)){
									isFind = true;
									if(isFirst) {
										var y=1, maxTry = 10000;
										while(td.getBoundingClientRect().top > self.topMax + 3){
											self.divData.scrollTop += y;
											y++;
											if(y > maxTry) break;
										}
										y=1;
										while(td.getBoundingClientRect().top < self.topMin - 3){
											self.divData.scrollTop -= y;
											y++;
											if(y > maxTry) break;
										}
									}
									isFirst = false;
									td.addClassName('tdRedDashed');
								}
							}
						)
					}
				);
				if(!isFind) span.update('搜尋失敗');
			});
	}
	
	function initSort(self) {
		$$('span[name="@ICON_SORT"]').each(Element.remove);
			self.trHeaders.each(function(trTitle, outIndex){
			trTitle.childElements().each(
			function(td, index){
				var key = td.key;
				var nonSort = td.hasClassName('non-sort');
				if(nonSort || !key){
					return;
				}
				var span = new Element('span', {innerText : td.innerText});
				var span2 = new Element('span', {innerText : '◆', name : '@ICON_SORT', id : 'ICON_SORT_' + outIndex + '$'+ index});
					td.update('');
					td.stopObserving('click');
					td.insert(span).insert(span2);
					td.observe('click', function(){
							doSorting(self, this, outIndex, index);
					});	
			})
		});
	}
	
	function sorting (self, dataList, key, index, isNumber){
	
		var toEmpty = function(s){ return Object.isUndefined(s) ? '' : s; }
		var toZero =  function(s){ return isNaN(s) ? '' : s; }
		
		var elementIsArray = Object.isArray(dataList[0]);
		var elementIsHash = Object.isHash(dataList[0]);
		for(var j=0; j < dataList.length; j++){
			for(var i=0; i<dataList.length - 1; i++){
				var obj1 = dataList[i];
				var obj2 = dataList[i+1];
				var value1;
				var value2;
				if(elementIsArray){
					value1 = isNumber ? parseFloat(toZero(obj1[index])) : toEmpty(obj1[index]);
					value2 = isNumber ? parseFloat(toZero(obj2[index])) : toEmpty(obj2[index]);
				}else if(elementIsHash){
					value1 = isNumber ? parseFloat(toZero(obj1.get(key))) : toEmpty(obj1.get(key));
					value2 = isNumber ? parseFloat(toZero(obj2.get(key))) : toEmpty(obj2.get(key));
				}else {
					value1 = isNumber ? parseFloat(toZero(obj1[key])) : toEmpty(obj1[key]);
					value2 = isNumber ? parseFloat(toZero(obj2[key])) : toEmpty(obj2[key]);
				}
				var isSwap = false;
				if( value1 === ''){
					isSwap = true;
				} else if(value2 === '') {
					//
				} else if(self.desc ? (value1 < value2) : (value1 > value2) ){
					isSwap = true;
				}
				if(isSwap) {
					var temp = obj1;
					dataList[i] = obj2;
					dataList[i+1] = temp;
				}
			}
		}
	}
	
	function doSorting(self, obj, outIndex, index) {
		claerSortIcon(); 
		var key = obj.key, dataList;
		if(Object.isFunction(self.descSort) && Object.isFunction(self.ascSort)){
			if(self.desc){
				dataList = self.descSort(self.data, key);
			}else{
				dataList = self.descSort(self.data, key);
			}
		}  else {
			dataList = self.data.clone();
			sorting(self, dataList, key, index, obj.hasClassName('number'));
		}		
		Tools.clearTbody(self.tbodyData);
		dataList.each(self.renderFunction);
		
		if(self.desc){
			$('ICON_SORT_' + outIndex + '$' + index).update('▼');
			self.desc = false;
		}else {
			$('ICON_SORT_' + outIndex + '$' + index).update('▲');
			self.desc = true;
		}
	}
	
	function claerSortIcon() {
		$$('span[name="@ICON_SORT"]').each(function(el){el.update('')});
	}
	
	function display() {
		this.data.each(this.renderFunction);
		var len = this.tableData.rows.length;
		if(len == 0) return;
		adjustWidth(this);
		adjustHeight(this);
		if(this.searchEnable) {
			initSearch(this);
		}
		
		var firstTR = this.tableData.rows[0];
		var lastTR = this.tableData.rows[len - 1];
		this.divData.scrollTop = 99999;
		var topMax = lastTR.getBoundingClientRect().top;
		this.divData.scrollTop = 0;
		var topMin = firstTR.getBoundingClientRect().top;
		this.topMax = topMax;
		this.topMin = topMin;
	} 
	
	function adjustWidth(self) {
	
		self.tableData.getWidth();
		self.tableHeader.getWidth();
		var dataWidths = [], sumCross = 0;
		
		//data
		var firstTR = self.tableData.descendants().detect(function(el){return el.match('tr')});
		if(!firstTR) return;
		firstTR.childElements().each(
			function(td){
				dataWidths.push(td.getWidth());
			}
		);
		
		//force make scroll bar appear
		self.divHeader.setStyle({height : self.divHeader.getHeight() - 1});
		
		//header
		var lastTR = self.tableHeader.rows[self.tableHeader.rows.length - 1];
		$(lastTR).childElements().each(
			function(td,index){
				var cross = td.cross||1, sumWidth = 0, cnt=0;
				while(cnt < cross){
					sumWidth += dataWidths[index + sumCross + cnt];
					cnt++;
				}
				sumCross += (cross - 1);
				td.width = sumWidth;
			}
		);
	}
	
	function adjustHeight(self) {
		var dataHeight = self.tableData.getHeight();
		if(dataHeight < self.height)  
		self.divData.setStyle({height : dataHeight - 1});
	}
	
	function clear() {
		Tools.clearTbody(this.tbodyData);
		this.data = [];
		var search = $('@SEARCH_TABLE_' + this.serNo);
		if(search) search.remove();
		this.divData.setStyle({height : 0});
	}
	
	
	return {
		initialize : initialize,
		display : display,
		clear : clear
	};
})()
);

/*自訂user互動元件
* 結合alert, confirm, prompt元件功能
*/
var PopWin = Class.create(
(function(){
	function initialize () {
		this.buttons = [];
		this.callBack = function(){};
		this.afterClose = function(){};
		this.inputText = false;
		this.title = '';
		this.message = '';
	}
	
	function setButton() {
		this.buttons = [];
		for (var i = 0, length = arguments.length; i < length; i++) {
			this.buttons.push(arguments[i]);
		}
	}
	
	function setAfterClose(func) {
		this.afterClose = func;
	}
	
	function setTitle(title) {
		this.title = title;
	}
	
	function setMessage(msg) {
		this.message = msg;
	}
	
	function setCallBack(func) {
		this.callBack = func;
	}
	
	function setInputText(isVisible) {
		this.inputText = !!isVisible;
	}

	function createMask(self) {
		var table = new Element('table', {className : 'tbBox2'});
			var tbody = new Element('tbody');
			var title = new Element('tr', {align : 'center', className : 'tbBlue'});
			var td1 = new Element('td', {align : 'center'});
			var _div = new Element('div', {align : 'right'});
			var closeBtn = new Element('input', {type : 'button', value : 'X'});
				closeBtn.setStyle({fontFamily: 'sans-serif'});
				closeBtn.observe('click', function(){
					closeWin(self);
					self.afterClose();
				});
				td1.insert(_div.insert(closeBtn));
				td1.insert(new Element('span', {align : 'center', innerText : self.title}));
				
				title.insert(td1);
				tbody.insert(title);
				table.insert(tbody);
				
			var tr2 = new Element('tr', {align : 'center', className : ''});
			var td2 = new Element('td');
			var textArea = new Element('textarea', {innerText : self.message, readOnly : true}); 
				textArea.setStyle(
					{ overflow : 'hidden',
					  height : 0,
					  width : '101%',
					  border : 0
					}
				);
				tr2.insert(td2.insert(textArea));
				tbody.insert(tr2);
			
			//set input text
			if(self.inputText) {
				var tr4 = new Element('tr', {align : 'center', className : ''});	
				var td4 = new Element('td');
				var _inputText = new Element('input', {type : 'text', className : 'textBox2', id : '_popWinInputText', size : 30});
				td4.insert(_inputText);
				tr4.insert(td4);
				tbody.insert(tr4);
			}

			//set button
			var tr3 = new Element('tr', {align : 'center', className : ''});	
			var td3 = new Element('td');
			self.buttons.each(
				function(b,index){
					var btn = new Element('input', {type : 'button', className : 'button', value : b});
						btn.observe('click', function(){
							var text = '';
							if($('_popWinInputText')) text = $F('_popWinInputText');
							self.callBack(index, text);
							closeWin(self);
						});
					td3.insert(btn);
				}
			)
			tr3.insert(td3);
			tbody.insert(tr3);	

			table.setStyle({
				backgroundColor : '#FFFFFF',
				border : 1,	
				position : 'absolute',
				cellpadding : 0,
				cellspacing : 0,
				border : 1
			});
	
		var div = new Element('div');
			div.setStyle({
				backgroundColor : '#B1B1B1',
				position : 'absolute',
				filter : 'alpha(opacity=70)',
				opacity : 0.7,
				posLeft : 0,
				posTop : 0,
				width : Math.max(document.body.clientWidth, document.body.scrollWidth),
				height : Math.max(document.body.clientHeight, document.body.scrollHeight)
			});
		 document.body.appendChild(div);
		 document.body.appendChild(table);	
		 self.div = div;
		 self.table = table;
		 
		 //adjust rectangle
		 var i=0;	
		 while(tr2.getHeight() < 80){
		 	textArea.innerText = textArea.innerText + getSeparator(i++);
		 }
		 
		 if(table.getWidth() < 200) table.setStyle({width : 200});
		 
		 var theInput = $('_popWinInputText');
		 if(theInput) {
		 	var _trWidth = tr2.getWidth();
		 	while(theInput.getWidth() < _trWidth) {
		 		theInput.size = theInput.size + 1;
		 	}
		 } 			
		 var temp = $(document.body).cumulativeScrollOffset().toArray();
		 var _w = table.getWidth(), _h = table.getHeight();	
		 var w = document.body.clientWidth, h = document.body.clientHeight;
		 //var w = Math.max(document.body.clientWidth, document.body.scrollWidth);
		 //var h = Math.max(document.body.clientHeight, document.body.scrollHeight);
		 table.setStyle({posLeft : (w - _w)/2 + temp[0]});
		 table.setStyle({posTop : (h - _h)/2 + temp[1]}); 
		 if(document.activeElement) document.activeElement.blur();
	}
	
	function closeWin(self) {
		self.div.remove();
		self.table.remove();
	}
	
	function getSeparator(i) {
		var s = '';
		while(i--){
			s += '\n';
		}
		return s;
	}

	function show() {
		createMask(this);
	} 
	
	return {
		initialize : initialize,
		show : show,
		setButton : setButton,
		setCallBack : setCallBack,
		setAfterClose : setAfterClose,
		setInputText : setInputText,
		setTitle : setTitle,
		setMessage : setMessage
	}
})()
);
