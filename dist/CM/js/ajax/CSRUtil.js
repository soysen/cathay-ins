// CSRUtil.js
var CSRUtil = {};

// help to get the ReturnMessage object from response
CSRUtil.getReturnMessage = function (resp){
	return resp['ErrMsg'];
}

// help to know the return code in return message is zero or not
CSRUtil.isSuccess = function(resp){
	var msg = CSRUtil.getReturnMessage(resp);
	if(msg){
		return (msg.returnCode == 0);
	}
	return false;
}

CSRUtil.isSelect = function(e){
	return e.tagName.toLowerCase() == 'select' && e.type.toLowerCase() == 'select-one';
}

CSRUtil.isButton = function(e){
	return e.tagName.toLowerCase() == 'input' && e.type.toLowerCase() == 'button';
}

CSRUtil.focusControl = function(){
	var thisElement = document.activeElement;
	if(thisElement){
		if(CSRUtil.isSelect(thisElement) || CSRUtil.isButton(thisElement)){
			if(!CSRUtil.focusElement){
				CSRUtil.focusElement = thisElement;
				CSRUtil.focusElement.blur();
			}
		}
	}
}

CSRUtil.collectHidden = [];

CSRUtil.ProcessPageCover = function () {
	
	try{
		CSRUtil.focusControl();
	}catch(e){}

    var docBody = document.body;
    var cover = $('_frontCover');
    if (!cover) {
        cover = document.createElement('div');
        cover.id = '_frontCover';
        cover.style.background = 'white';
        cover.style.filter = 'alpha(opacity=50)';
        cover.style.opacity = '0.5';
        cover.style.position = 'absolute';
        cover.style.cursor = 'progress';
        cover.style.posLeft = 0;
        cover.style.posTop = 0;
        cover.style.zIndex = 150;
        cover.style.display = 'none';
        cover.align = 'center';
        cover.innerHTML = '<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0"><tr valign="middle"><td align="center"><img name="ajax-loading" src="../../../images/CM/ajax-loading.gif" width="50" height="50" border="0" alt=""><br><font style="font-size:20pt;color:red;font-family: "Arial", "Helvetica", "sans-serif";">資料處理中.......請稍候</font></td></tr></table>';
        docBody.appendChild(cover);
    }
    cover.style.width = (docBody.clientWidth > docBody.scrollWidth) ? docBody.clientWidth : docBody.scrollWidth;
    cover.style.height = (docBody.clientHeight > docBody.scrollHeight) ? docBody.clientHeight : docBody.scrollHeight;
    //當cover是hide時，則show conver及hide已show的combo box
    if ($('_frontCover').style.display == 'none') {
        Element.show('_frontCover');
        
        if(CSRUtil.getMsgBoard()){
        	CSRUtil.getMsgBoard().innerText = '';
        }

        $$('select').each(function (cb) {
            if (cb.style.visibility != 'hidden' && cb.style.display != 'none') {
                CSRUtil.collectHidden.push(cb);
                cb.style.filter = 'alpha(opacity=0)';
            }
        });
        
        $$('input[type="button"]').each(function (cb, index) {
            if (cb.style.visibility != 'hidden' && cb.style.display != 'none') {
               	 CSRUtil.collectHidden.push(cb);
           		 cb.style.filter = 'alpha(opacity=0)';
            }
        });
        
    }
}

CSRUtil.hidePageCover = function () {
	
    Element.hide('_frontCover'); 
    while(true){
    	var el = CSRUtil.collectHidden.shift();
    	if(!el) break;
    	el.style.filter = 'alpha(opacity=100)';
    }

    try{
    	if(CSRUtil.focusElement){
    		try{
    			CSRUtil.focusElement.focus(); //maybe can't focus
    		}catch(e1){}
    		CSRUtil.focusElement = null;
    	}
    }catch(e){}
   
}

var isDebug=false;
var createCnt=0;
var totalRsCnt=0;
var checkRs1Cnt=0;
var checkRs2Cnt=0;
var completeCnt=0;
var exceptionCnt=0;

// The cover function only work on IE
CSRUtil.defaultAjaxHandler = {
	onCreate: function (ajaxRequest) {
		CSRUtil.ProcessPageCover();
		try{
			ajaxRequest.options.requestHeaders = {'Cache-Control' : 'no-cache'};
		}catch(e){
		}
		if(isDebug){
			createCnt++;
			var msgBoard1 = CSRUtil.getMsgBoard();
			msgBoard1 ? msgBoard1.innerHTML = ('<font color="red"> Ajax.activeRequestCount:' + Ajax.activeRequestCount + ' , createCnt:' + createCnt + ' , totalRsCnt:' + totalRsCnt + ' , checkRs1Cnt:' + checkRs1Cnt + ' , checkRs2Cnt:' + checkRs2Cnt + ' , completeCnt:' + completeCnt + ' , exceptionCnt:' + exceptionCnt + '</font>') : '';
		}
	},
	onComplete: function (XHR, transp, resp) {
        //每次有錯都會產生訊息
        if(isDebug){
	        totalRsCnt++;
        }
        var rtMsg = null;
        if(resp){
        	if(isDebug){
	        	checkRs1Cnt++;
        	}
        	rtMsg = resp['ErrMsg'] ? resp['ErrMsg'] : null;
        }
        if (rtMsg) {
        	if(isDebug){
	            checkRs2Cnt++;
        	}
            var msgBoard = CSRUtil.getMsgBoard();
            var msgArray = $A(rtMsg.displayMsgDescs.split('\\n'));
            var str = '';
            msgArray.each(function(s) {
                str += s + '\n';
            });

            // 若returnCode == 0 且訊息不為空白字串，則顯示訊息
            if (rtMsg.returnCode == 0 && str.trim() != '') {
                msgBoard ? msgBoard.innerHTML = str : alert(str);
            }
            if (rtMsg.returnCode != 0) {
                alert("回傳碼:" + rtMsg.returnCode
                        + "\n訊息說明:\n" + str);
            }
        }
        if(isDebug){
			completeCnt++;
			var msgBoard1 = CSRUtil.getMsgBoard();
	        msgBoard1 ? msgBoard1.innerHTML = ('<font color="red"> Ajax.activeRequestCount:' + Ajax.activeRequestCount + ' , createCnt:' + createCnt + ' , totalRsCnt:' + totalRsCnt + ' , checkRs1Cnt:' + checkRs1Cnt + ' , checkRs2Cnt:' + checkRs2Cnt + ' , completeCnt:' + completeCnt + ' , exceptionCnt:' + exceptionCnt + '</font>') : '';
		}
        //當所有的request都處理完，才hide cover及show被hide的combo box
        if (Ajax.activeRequestCount == 0) {
            CSRUtil.hidePageCover();
        }
    },
	//script 執行有誤時的錯誤處理
	onException:function(XHR, ex){
		var errMsg = 'Error :\n';
		errMsg += 'url : ' + XHR['url'];
		errMsg += '\nparams: ' + XHR['options']['parameters'];
		errMsg += '\nException: \n';
		for(var p in ex){
			errMsg += p + '=' + ex[p] + '\n';
		}
		if(isDebug){
			exceptionCnt++;
		}
		alert(errMsg);
		if (Ajax.activeRequestCount == 0) {
            CSRUtil.hidePageCover();
        }
	}
}

// register default request/response handler for submit once ui control
Ajax.Responders.register(CSRUtil.defaultAjaxHandler);

// find the CSR message board
CSRUtil.getMsgBoard = function(){
 try{
	if(top.leftFrame && top.leftFrame.bottomFrame && top.leftFrame.bottomFrame.cathay_common_msgBoard){
		return top.leftFrame.bottomFrame.cathay_common_msgBoard;
	}else if(parent.bottomFrame && parent.bottomFrame.cathay_common_msgBoard){
		return parent.bottomFrame.cathay_common_msgBoard;
	}else if(top.bottomFrame && top.bottomFrame.cathay_common_msgBoard){
		return top.bottomFrame.cathay_common_msgBoard;
	}
 }catch(e){
	if(top.bottomFrame && top.bottomFrame.cathay_common_msgBoard){
		return top.bottomFrame.cathay_common_msgBoard;
	}else if(parent.bottomFrame && parent.bottomFrame.cathay_common_msgBoard){
		return parent.bottomFrame.cathay_common_msgBoard;
	}
 }	
}
////////////////////////////////////////////////////////////////////////////////////////////////////////
// format with usually used pattern #,###.00  #,### ...
CSRUtil.$fmt = function(v, pattern){
	if (!pattern) {
		pattern = '#,###';
	}
	var sign = '';
	var str = '' + v;
	if (str.startsWith('-')) {
		sign = '-';
		str = str.substring(1);
	}
	var i = pattern.indexOf('.');

	var k = str.indexOf('.');
	if(k<0){
		str = str + '.0';
		k = str.indexOf('.');
	}

	var ks = str.substr(k+1,str.length);
	var dotLen = 0;

	var ps = pattern.substr(i+1,pattern.length);

	var pfc = 0;
	for(var psf=0;psf<ps.length;psf++){
			if(ps.substr(psf,1)==0){
				pfc=psf+1;
			}
	}

	if(i!=-1 && pfc>0){
		dotLen = pfc;

	}else if(ks.length >0){

		var z = '';
		for(var f=0;f<ks.length;f++){
			z = z+'0';
		}
		var ksa = ks.substr(ks.indexOf('0'),ks.length);
		var ksb = ks.substr(0,ks.indexOf('0'));

		if(z!=ks){

			if(ksa.length==0 || ksa.length==ks.length){
				dotLen = pfc+ks.length;

			}else{
				dotLen = pfc+ks.length-ksa.length;

			}
		}
	}

	var n;
	if(k!=-1){
		n = new Number(str).toPrecision(k + dotLen);
	}else{
		n = new Number(str).toPrecision(str.length + dotLen);
	}

	var sep = pattern.indexOf(',');

	if(sep!=-1){
		sep = (i!=-1?i-sep-1:pattern.length-sep-1);
		k = n.indexOf('.');
		var tmpSrc;
		if(k!=-1){
			tmpSrc = n.substring(0, k);
		}else{
			tmpSrc = n;
		}

		var m = tmpSrc.length;
		var tmp = '';

		if(m > sep){
			while(m>0){
				if(m-sep < 0 ){
					tmp = tmpSrc.substr(0, m) + ',' + tmp;
					break;
				}
				if(m==tmpSrc.length){
					tmp = tmpSrc.substr(m-sep, sep);
				}else{
					tmp = tmpSrc.substr(m-sep, sep) + ',' + tmp;
				}
				m-=sep;
			}
			tmpSrc= tmp;
		}

		if(k!=-1){
			tmpSrc += n.substring(k);
		}
		n = tmpSrc;
	}
	return sign + n;
}

function html(args){
	this.toUppercase = toUppercase;
	function toUppercase(o){
		o.value = o.value.toUpperCase();
	}
}

var LIBs = new Hash();
LIBs.set('pageControllerForMultiRows.js', '');
LIBs.set('AlertHandler.js', '');
LIBs.set('calender.js', '');
LIBs.set('callModule.js', '');
LIBs.set('HotKey.js', '');
LIBs.set('StringUtils.js', '');
LIBs.set('tableHeadFix.js', '');
LIBs.set('utility.js', '');
LIBs.set('Validator.js', '');
LIBs.set('zipcode.js', '');
LIBs.set('sorttable.js', 'sorttable/');
LIBs.set('scriptaculous.js', 'aculous/');
LIBs.set('CSRUtils.js', '');

var DefaultLib = {
	Version: '1.0',
  	require: function(libraryName) {
    	// inserting via DOM fails in Safari 2.0, so brute force approach
    	document.write('<script type="text/javascript" src="'+libraryName+'"><\/script>');
	},
  	REQUIRED_PROTOTYPE: '1.6.0',
  	load: function() {
    	function convertVersionString(versionString) {
      		var r = versionString.split('.');
      		return parseInt(r[0])*100000 + parseInt(r[1])*1000 + parseInt(r[2]);
    	}
    	if ((typeof Prototype == 'undefined') || (typeof Element == 'undefined') || (typeof Element.Methods=='undefined') || (convertVersionString(Prototype.Version) < convertVersionString(DefaultLib.REQUIRED_PROTOTYPE))) {
   			throw("requires the Prototype JavaScript framework >= " + DefaultLib.REQUIRED_PROTOTYPE);
    	}
    	$A (document.getElementsByTagName("script")).findAll( function(s) {
      		return (s.src && s.src.match(/CSRUtil\.js(\?.*)?$/))
    	}).each(
    		function(s) {
	    		var path = s.src.replace(/CSRUtil\.js(\?.*)?$/,'') + '../';
			    //var includes = s.src.match(/\?.*load=([a-z,]*)/);
      			LIBs.each(
    	   			function(lib) {
    	   				DefaultLib.require(path + lib.value + lib.key);
    	   			}
    	   		);
    		}
    	);
  	}
}

DefaultLib.load();