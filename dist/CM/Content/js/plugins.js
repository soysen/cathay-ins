// Avoid `console` errors in browsers that lack a console.
(function () {
	var method;
	var noop = function () { };
	var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
	];
	var length = methods.length;
	var console = (window.console = window.console || {});

	while (length--) {
		method = methods[length];

		// Only stub undefined methods.
		if (!console[method]) {
			console[method] = noop;
		}
	}
}());


// Place any jQuery/helper plugins in here.
; (function ($) {
	jQuery.fn.tagName = function () {
		return this.prop("tagName").toLowerCase();
	};
})(jQuery);

//tabs.js
; (function ($) {
	$.fn.tabs = function (options) {
		var defaults = {
		}
		if (this.length == 0) return this;
		// support mutltiple elements
		if (this.length > 1) {
			this.each(function () { $(this).tabs(options) });
			return this;
		}

		var el = this;

		var tabs = {};

		var init = function () {
			tabs.settings = $.extend({}, defaults, options);

			if ($(el).find('[data-href]').hasClass('active')) {
				var active = $(el).find('.active').data('href');
				$(active).fadeIn(300).siblings().hide();
			} else {
				$(el).find('[data-href]').eq(0).addClass('active');
				var active = $(el).find('[data-href]').eq(0).data('href');
				$(active).fadeIn(300).siblings().hide();
			}
		};

		var start = function () {
			init();
			$(el).find('[data-href]').on('click', function (e) {
				e.preventDefault();
				$(this).addClass('active').siblings().removeClass('active');
				var active = $(el).find('.active').data('href');
				$(active).fadeIn(300).siblings().hide();
			});
		};

		start();
		return this
	};

})(jQuery);

//collapse.js
; (function ($) {
	$.fn.collapse = function (options) {
		var defaults = {
			itemClass: '.collapse__item',
			handlerClass: '.collapse__handler',
			contentClass: '.collapse__content',
			oneByOne: true,
			slideDownDuration: 250,
			slideUpDuration: 150,
		}
		if (this.length == 0) return this;
		// support mutltiple elements
		if (this.length > 1) {
			this.each(function () { $(this).collapse(options) });
			return this;
		}

		var el = this;

		var collapse = {};

		var init = function () {
			collapse.settings = $.extend({}, defaults, options);

			var handler = collapse.settings.handlerClass,
				content = collapse.settings.contentClass,
				item = collapse.settings.itemClass,
				oneByOne = collapse.settings.oneByOne,
				slideUpDuration = collapse.settings.slideUpDuration,
				slideDownDuration = collapse.settings.slideDownDuration;

			$(el).find(item + ':not(.active)').each(function () {
				$(this).find(content).hide();
			});

			if ($(el).find('active').length > 0) {
				var $active = $(el).find('.active');
				$active.find(content).slideDown(slideDownDuration);
				if (oneByOne) {
					$active.siblings().find(content).slideUp(slideUpDuration);
				}
			}

			$(el).find(handler).on('click', function () {
				if ($(this).parent(item).hasClass('active')) {
					$(this).next(content).slideUp(slideUpDuration);
				} else {
					$(this).next(content).slideDown(slideDownDuration);
					if (oneByOne) {
						$(this).parent(item).siblings().removeClass('active');
						$(this).parent(item).siblings().find(content).slideUp(slideUpDuration);
					}
				}
				$(this).parent(item).toggleClass('active');
			});
		};

		init();
		return this
	};

})(jQuery);

	//Bobeprogress
	; (function($){
		$.fn.bobeprogress = function(options){
			return this.each(function(el, i){
				var ul = $('<ul class="progressStep"></ul>').appendTo(this);
				for(i = 0; i < options.steps.length; i++ ){
					ul.append('<li class="progressStep__item">\
									<div class="progressStep__number">' + (i + 1 )+ '</div>\
									<div class="progressStep__title">' + options.steps[i].text + '</div>\
							</li>');
				}
				ul.find('li').each(function(i){
					if( i < options.active ){
						$(this).addClass('prev');
					}else if( i == options.active ){
						$(this).addClass('active');
					}
				});
			});
		};
	})(jQuery);

//fullscreen
// ; (function ($) {
// 	$.fn.fullScreen = function (options) {
// 		if (this.length == 0) return this;
// 		var $window = this;
// 		$window.on('resize.fullScreen', function () {
// 			var wh = $(this).height(),
// 				headerH = $('header').outerHeight(),
// 				footerH = $('footer').outerHeight();

// 			$('.main').css('min-height', wh - headerH - footerH + 1);
// 		}).trigger('resize');
// 		return this
// 	};

// })(jQuery);

//emailAutocomplete
// ; (function () {

// 	$.fn.emailAutocomplete = function (option) {
// 		if (this.length == 0) return this;
// 		var el = $(this);
// 		var min = 3;
// 		el.on('keyup.emailAutocomplete', function (e) {
// 			var keycode = e.which || e.keyCode;
// 			var $this = $(this);
// 			var val = $this.val();
// 			var dropdown = $this.next('.emailAutocomplete__dropdown');
// 			var domains = ['@gmail.com', '@hotmail.com', '@yahoo.com.tw'];

// 			if (val.length > min && keycode == 13) {
// 				$('body').trigger('click');
// 				return false;
// 			}

// 			if (val.length > min && keycode == 40) {
// 				var nowIndex = dropdown.find('.focusing').index();
// 				keydown(nowIndex);
// 				return false;
// 			}

// 			if (val.length > min && keycode == 38) {
// 				var nowIndex = dropdown.find('.focusing').index();
// 				keyup(nowIndex);
// 				return false;
// 			}

// 			if (val.length > min) {
// 				$this.addClass('active');
// 				dropdown.empty().append(makeOptions(val));
// 				dropdown.off('mouseenter.emailAutocomplete');
// 				dropdown.on('mouseenter.emailAutocomplete', 'a', function () {
// 					$(this).addClass('focusing').siblings('a').removeClass('focusing');
// 					var val = $(this).text();
// 					$this.val(val);
// 				});
// 			} else {
// 				$this.removeClass('active');
// 			}

// 			function keydown(nowIndex) {
// 				nowIndex++;
// 				if (nowIndex > dropdown.children().length - 1) {
// 					return false;
// 				} else {
// 					dropdown.children()
// 						.eq(nowIndex)
// 						.trigger('mouseenter')
// 						.addClass('focusing')
// 						.siblings().removeClass('.focusing');
// 				}
// 			}
// 			function keyup(nowIndex) {
// 				nowIndex--;
// 				if (nowIndex < 0) {
// 					return false;
// 				} else {
// 					dropdown.children()
// 						.eq(nowIndex)
// 						.trigger('mouseenter')
// 						.addClass('focusing')
// 						.siblings().removeClass('.focusing');
// 				}
// 			}
// 			function makeOptions(val) {
// 				var dom = domains.reduce(function (acc, domain) {
// 					var text = val.indexOf('@') != -1 ? val.slice(0, val.indexOf('@')) : val;
// 					return '<a href="javascript:void(0)">' + text + domain + '</a>' + acc;
// 				}, '');
// 				return dom;
// 			};
// 		});
// 		$('body').on('click.closeAutocomplete', function () {
// 			el.removeClass('active');
// 		});

// 		return this
// 	};
// })(jQuery);



//�Ŀ��,�U����~�i��g
//element��JĲ�o��selecter
//��disable�����e,�[�W disabled �P data-group="disable"
function inputEnable(element) {
	$(element).on('change', function () {
		var checked = $(element).is(':checked');
		if (checked) {
			$('[data-group="disable"]').prop('disabled', false);
			$('.transformSelect').removeClass('disabled');
		} else {
			$('[data-group="disable"]').prop('disabled', true);
			$('.transformSelect').addClass('disabled');
		}
	})
};


//�Ŀ��,�U��ĤG���i�}
function radioToggle() {
	$('[data-handler]').change(function () {
		$('[data-handler]').each(function () {

			var val = $(this).data('handler');
			var target = $('[data-content=' + val + ']');

			if ($(this).prop('checked')) {
				target.addClass('toggleOpen');
			} else {
				target.removeClass('toggleOpen');
			}
		})
	});
};

//�Ŀ��,�U��show hide
function radioShow() {
	$('[data-handler]').change(function () {
		$('[data-handler]').each(function () {

			var val = $(this).data('handler');
			var target = $('[data-content=' + val + ']');

			if ($(this).prop('checked')) {
				target.fadeIn();
			} else {
				target.hide();
			}
		})
	});
};

//�Ŀ��,�~�i���U��պ���s
function ableCountmoney() {
	$('[data-required]').on('change', function () {
		var checkItem = $(this).attr('name');
		var checked = $('[name="' + checkItem + '"]').is(':checked');
		var checkbtn = $('[data-disabled="' + checkItem + '"]')

		if (checked == true) {
			$(checkbtn).removeClass('btn--disabled');
		} else {
			$(checkbtn).addClass('btn--disabled');
		}
	});
};

//Checkbox ��������
function checkboxAllCheck() {
	$('[data-allcheck]').change(function () {
		var thisname = $(this).attr('name');
		var allcheckbox = $('input[name="' + thisname + '"]');
		if ($(this).is(':checked')) {
			allcheckbox.prop('checked', 'checked');
		} else {
			allcheckbox.removeAttr('checked');
		};

	})
};

//Checkbox �p���������
function moCheckboxAllCheck() {
	$('[data-allcheck-mobile]').on('click', function () {
		var thisname = $(this).data('name');
		var allcheckbox = $('input[name="' + thisname + '"][type="checkbox"]');
		allcheckbox.prop('checked',false);

	});
};

//contentToggle
function contentToggle(element) {
	$(element).change(function () {
		$(element).each(function () {
			var target = $(this).parents('[data-target="thisgroup"]');

			if ($(this).prop('checked')) {
				target.addClass('ToggleOpen');
			} else {
				target.removeClass('ToggleOpen');
			}
		})
	});
};
