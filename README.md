# AJA F2E Starter Kit #

為方便未來處理前端需求與打包，製作快速入門 F2E starter kit

## 安裝

1. 下載到開發環境
2. 打開 terminal
3. ```cd project_folder ```
4. 執行 ```npm install```

## 指令

### gulp

產出 HTML / JS / CSS 到 dist folder

### gulp watch

產出 HTML / JS / CSS 到 dist folder，並呼叫 browserSync，可在開發時做即時監控

### gulp minify

把 JS / CSS / IMG 做壓縮

### gulp archive

打包 dist 到  archive/project-name_20190101.zip


### package.json
```
"f2e-configs": {
    "useJade": true,  // 使用 jade，false 為 HTML
    "scripts": [  // 整合外部 JS 為 lib.js
        "./node_modules/jquery/dist/jquery.js",
        "./node_modules/bootstrap/dist/js/bootstrap.js"
    ],
    "styles": [   // 整合外部 CSS 為 lib.css
    "./node_modules/bootstrap/dist/css/bootstrap.css"
    ],
    "fonts": []  // 載入外部字型到 fonts
},
```

